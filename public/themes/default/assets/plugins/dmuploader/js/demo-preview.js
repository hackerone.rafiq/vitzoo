(function( $, window, undefined ) {
  $.danidemo = $.extend( {}, {
    
    addLog: function(id, status, str){
      var d = new Date();
      var li = $('<li />', {'class': 'img-' + status});
       
      var message = '[' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + '] ';
      
      message += str;
     
      li.html(message);
      
      $(id).prepend(li);
    },
    
    addFile: function(id, i, file){
		var template = '<div id="img-file' + i + '">' +
		                   '<img src="" class="demo-image-preview" />' +
		                   '<div class="progress progress-striped active completeUpload">'+
		                       '<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: 0%;">'+
		                           '<span class="sr-only">0% Complete</span>'+
		                       '</div>'+
		                   '</div>'+
		               '</div>';
		               
		var j = $(id).attr('file-counter');
		if (!j){
			$(id).empty();
			
			j = 0;
		}
		
		j++;
		
		$(id).attr('file-counter', j);
		
		$(id).append(template);
	},
	
	
	updateFileProgress: function(i, percent){
		$('#img-file' + i).find('div.progress-bar').width(percent);
		
		$('#img-file' + i).find('span.sr-only').html(percent + ' Complete');
	},
	
	humanizeSize: function(size) {
      var i = Math.floor( Math.log(size) / Math.log(1024) );
      return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    }

  }, $.danidemo);
})(jQuery, this);

