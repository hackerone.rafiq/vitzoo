var overlay = '<div class="bg-overlay">' +
    '<div class="ajax-loading">' +
    '<img src="' + siteUrl + '/loading.gif" width="50" height="50" alt="Ajax Loading..." />' +
    '</div></div>';

/**
 * display bootstrap modal with content load by ajax
 * @param id
 */
function loadModalContent(id) {
    var url = id.attr('data-url');
    $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {
            $("body").append(overlay);
        },
        error: function () {
            $('.bg-overlay').remove();
        },
        success: function (data) {
            $('.modal-body').html(data);
            $('.modal').modal('show');
            $('.bg-overlay').remove();
            return false;
        },
        cache: false,
        async: false
    });
}

/**
 * display messi modal
 * @param m_title
 * @param m_class
 * @param message
 */
function modalDisplay(m_title, m_class, message) {
    new Messi(
        message,
        {
            modal: true,
            modalOpacity: 0.5,
            title: m_title,
            titleClass: m_class
        }
    );
}

/**
 * display messi error message modal
 * @param message
 * @returns {*}
 */
function modalError(message) {
    return modalDisplay('Error', 'error', message);
}

/**
 * display messi warning message modal
 * @param message
 * @returns {*}
 */
function modalWarning(message) {
    return modalDisplay('Warning', 'warning', message);
}

/**
 * display messi success message modal
 * @param message
 * @returns {*}
 */
function modalSuccess(message) {
    return modalDisplay('Success', 'success', message);
}

/**
 * display messi success message modal with redirect url
 * @param message
 * @param link
 */
function successPopup(message, link) {
    new Messi(
        message,
        {
            modal: true,
            modalOpacity: 0.5,
            title: 'Success',
            titleClass: 'success',
            buttons: [
                {id: 0, label: 'Yes', val: 'Y'}
            ],
            callback: function (val) {
                window.location.replace(link);
            }
        }
    );
}

/**
 * ajax change status
 * @param ajaxUrl
 * @param obj
 */
function changeStatus(ajaxUrl, obj) {
    var status_id = obj.attr('id');
    $.ajax({
        type: "POST",
        url: ajaxUrl,
        data: {id: status_id},
        dataType: "JSON",
        success: function (result) {
            if (result.success) {
                if (result.data == 'Enabled' || result.data == 'Active') {
                    obj.removeClass('label-danger').addClass('label-success');
                } else {
                    obj.removeClass('label-success').addClass('label-danger');
                }
                obj.text(result.data);
            } else {
                modalError(result.message);
            }
            return false;
        }
    });
}

/**
 * ajax remove multi records has checked
 * @param ids_value
 * @param message
 * @param ajaxUrl
 * @param redirectUrl
 */
function modalDelete(ids_value, message, ajaxUrl, redirectUrl) {
    new Messi(
        message,
        {
            modal: true,
            modalOpacity: 0.5,
            title: 'Confirmation',
            titleClass: 'warning',
            buttons: [
                {id: 0, label: 'Yes', val: 'Y'},
                {id: 1, label: 'No', val: 'N'}
            ],
            callback: function (val) {
                if (val == 'Y') {
                    // sent ajax to remove
                    ajaxSubmit(ajaxUrl, {ids: ids_value}, redirectUrl);
                }
            }
        }
    );
}

function ajaxSubmit(ajaxUrl, ajaxData, redirectUrl) {
    $.ajax({
        type: "POST",
        url: ajaxUrl,
        data: ajaxData,
        dataType: "JSON",
        cache: false,
        async: false,
        beforeSend: function () {
            $("body").append(overlay);
        },
        error: function () {
            $('.bg-overlay').remove();
        },
        success: function () {
            $('.bg-overlay').remove();
            if (redirectUrl) {
                if (!redirectUrl) {
                    window.location.reload();
                } else {
                    window.location.replace(redirectUrl);
                }
            }

            return false;
        }
    });
}

$(document).ready(function() {
    /**
     * add csrf token for ajax request
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * re-style checkbox & radio inputs by iCheck
     */
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });

    /**
     * click to check all
     */
    $("#bulkDelete").on('ifClicked', function () {
        var status = this.checked;
        if (!status) {
            $(".deleteRow").iCheck('check');
        } else {
            $(".deleteRow").iCheck('uncheck');
        }
    });
});

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function sendFormByAjax(url, redirect, data) {
    $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            beforeSend: function () {
                $('#loading').show();
            }
        })
        .done(function (str) {
            $('#loading').hide();
            $('.form-group').removeClass('has-error');
            if (str.success == false) {
                for (var key in str.message) {
                    if (str.message.hasOwnProperty(key)) {
                        var obj = str.message[key];
                        for (var prop in obj) {
                            if (obj.hasOwnProperty(prop)) {
                                key = capitalizeFirstLetter(key.replace('_', " ")).replace('-', ' ');
                                obj[prop] = capitalizeFirstLetter(obj[prop]).replace('.', ' ');
                                $('.reg-box-msg').addClass('text-red').append('<li>' + key + ": " + obj[prop] + '</li>');
                            }
                        }
                    }
                }

                $('#' + str.flag).parent().addClass('has-error');
            } else {
                $('.form-group').removeClass('has-error');
                $('#pop-up').modal('hide');
                window.location.href = redirect;
            }

        });
}

function sendAjaxForm(url, redirect, data) {
    $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            beforeSend: function () {
                $('#loading').show();
            }
        })
        .done(function (str) {
            $('#loading').hide();
            $('.form-group').removeClass('has-error');
            if (str.success == false) {

                $('label').each(function () {
                    text = $(this).attr('for');
                    if (typeof text != 'undefined') {
                        text = text == 'fullname' ? 'full_name' : (text == 'three_size' ? 'three_sizes' : text);
                        text = capitalizeFirstLetter(text).replace('_', " ").replace('-', ' ');
                        $(this).text(text).removeClass('text-red');
                    }
                });
                for (var key in str.message) {
                    if (str.message.hasOwnProperty(key)) {
                        var obj = str.message[key];
                        for (var prop in obj) {
                            if (obj.hasOwnProperty(prop)) {
                                $('label[for=' + key + ']').addClass('text-red').text(obj[prop]);
                            }
                        }
                    }
                }
            } else {
                $('#pop-up').modal('hide');
                window.location.reload(true);
            }

        });
}

$(document).keyup(function (e) {
    /*//27 = esc
     //13 = enter
     //89 = y;*/
    if (e.keyCode == 27) {
        $('.messi-closebtn').trigger('click');
        /*or .click()*/
        $('#pop-up').modal('hide');
        $('#image-pop-up').modal('hide');
        $('.messi-btnbox .btn[value="N"]').click();
        $('#loading').hide();
    }
    if (e.keyCode == 89) {
        $('.messi-btnbox:visible .btn[value="Y"]').click();
    }
    if (e.keyCode == 13) {
        $('#pop-up:visible button[type="submit"]').click();
    }
});

function loadCountryData(data, division, url) {
    var current = data.current;
    if (current) current = parseInt(current);
    delete data.current;
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'JSON',
        data: {data: data, division: division},
        beforeSend: function () {
            $('#loading').show();
        }
    })
    .done(function (e) {
        if (division === 'states') {
            var html = '<option hidden selected disabled>Select state</option>';

            for (var i = 0; i < e.length; i++) {
                if (current === e[i].subdivision_id)
                    html += '<option selected value=' + e[i].subdivision_id + '>' + e[i].subdivision_1_name + '</option>';
                else
                    html += '<option value=' + e[i].subdivision_id + '>' + e[i].subdivision_1_name + '</option>';
            }
        }
        else {
            var html = '<option selected hidden disabled>Select city</option>';
            for (var i = 0; i < e.length; i++) {
                if (current === e[i].id)
                    html += '<option selected value=' + e[i].id + '>' + e[i].City + '</option>';
                else
                    html += '<option value=' + e[i].id + '>' + e[i].City + '</option>';
            }
        }
        $('#' + division).html(html);
    });
}