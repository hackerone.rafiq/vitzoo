@extends($theme_layout)
@section('css_wrapper')
    <style>
        body {
            height: 100vh;
        }
    </style>
@stop
@section('content')
    <div class="mainpage">
        <div class="tab-content container side-body">
            <!-- home-page -->
            <div id="home">
                <div class="row">
                    <div class="col-md-6 col-md-offset-1 Text-content">
                        <h2>{{trans('fr_home.text_translate_title_home')}}</h2>
                        <?php echo trans('fr_home.text_translate_home')?>
                    </div>
                    @if(!isset($sentinel) || empty($sentinel))
                        <div class="col-md-4 col-md-offset-1 home-form">

                            <div class="rlForm">
                                <div class="slForm regform">
                                    <div class="form-top bg-title top-radius text-center">
                                        <h2>{{trans('fr_home.create_account')}}</h2>
                                    </div>
                                    <div class="form-body">
                                        <div id="reauth-email" style="text-align: center; color: red;"></div>
                                        {!! Form::open(['url' => Helper::url('doRegister'), 'id' => 'registerForm','class'=>'registerForm', 'onsubmit'=>'event.preventDefault()']) !!}
                                        <div class="form-group">
                                            <input class="form-control" name="first_name"
                                                   placeholder="{{trans('fr_home.first_name')}}"/>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" name="last_name"
                                                   placeholder="{{trans('fr_home.last_name')}}"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email" name="email"
                                                   placeholder="Email"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password-signup"
                                                   name="password" placeholder="{{trans('fr_home.password')}}"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="retype-password"
                                                   name="retype-password"
                                                   placeholder="{{trans('fr_home.confirm_password')}}"/>
                                        </div>

                                        <div class="checkbox">
                                            <input type="checkbox" name="re_checkbox"> <span class="i-agree">{{trans('fr_home.accept')}}
                                                <a
                                                        data-toggle="modal"
                                                        data-target="#myModal">{{trans('fr_home.accept_the_site_tems')}}</a></span>
                                            @if(isset($token_invite) && $token_invite)
                                                <input class="form-control" name="token_invite" type="hidden" value="{{$token_invite}}"/>
                                            @endif
                                        </div>

                                        <button type="submit"
                                                class="btn btn-danger btn-register-login btn-sign-up"
                                                disabled>{{trans('fr_home.sign_up')}}</button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="form-bottom bottom-radius text-center">
                                        {{trans('fr_home.sign_in_with_your_vitzoo_account')}}? <a href="#" id="goLogin"><strong>{{trans('fr_home.sign_in')}}</strong></a>
                                    </div>
                                </div>

                                <div class="slForm logform hidden">
                                    <div class="form-top top-radius bg-title text-center">
                                        <h2>{{trans('fr_home.sign_in')}}</h2>
                                    </div>
                                    <div class="form-body">
                                        {!! Form::open(['url' => Helper::url('doLogin'), 'id' => 'loginForm','class'=>'loginForm', 'onsubmit'=>'event.preventDefault()']) !!}
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="login-email"
                                                   placeholder="Email"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password"
                                                   id="loginpassword" placeholder="{{trans('fr_home.password')}}"/>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-7 col-sm-7 col-xs-7 checkbox-remember">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"
                                                               name="remember"/> {{trans('fr_home.keep_me_signed_in')}}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-5 checkbox-forget">
                                                <a href="#" id="forgotPass">{{trans('fr_home.forgotten_account')}}?</a>
                                            </div>
                                        </div>

                                        <button type="submit"
                                                class="btn btn-danger btn-register-login">{{trans('fr_home.sign_in')}}</button>
                                        {!! Form::close() !!}

                                        {{--<div class="form-group socials-login clearfix">
                                            <span class="pull-left">{{trans('fr_home.or_sign_in_with')}}</span>
                                            <ul class="footer-socials pull-right">
                                                <li>
                                                    <a class="icon-border" href="#"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a class="icon-border" href="#"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a class="icon-border" href="#"><i
                                                                class="fa fa-google-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div>--}}
                                    </div>
                                    <div class="form-bottom bottom-radius text-center">
                                        {{trans('fr_home.not_have_an_account')}}? <a href="#"
                                                                                     id="goRegister"><strong>{{trans('fr_home.sign_up_now')}}</strong></a>
                                    </div>
                                </div>

                                <div class="slForm forgotform hidden">
                                    <div class="form-top top-radius bg-title text-center">
                                        <h2>{{trans('fr_home.forgotten_account')}}</h2>
                                    </div>
                                    <div class="form-body">
                                        {!! Form::open(['url' => Helper::url('doRegister'), 'id' => 'fogotpass','class'=>'fogotpass', 'onsubmit'=>'event.preventDefault()']) !!}
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" id="forgot-email"
                                                       placeholder="Email"/>
                                            </div>

                                            <button type="submit"
                                                    class="btn btn-danger">{{trans('fr_home.take_the_password')}}</button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="form-bottom bottom-radius text-center">
                                        <a href="#"
                                           id="returnRegister"><strong>{{trans('fr_home.sign_up')}}</strong></a> {{trans('fr_home.if_you_have_not_account')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
@if(!isset($sentinel) || empty($sentinel))
@section('js_wrapper')
    @include('default::modules.frontend.register.register_js')
@stop
@endif
