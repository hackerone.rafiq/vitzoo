@extends($theme_layout)
@section('content')
    <div class="mainpage">
        <div class="tab-content container side-body">
            <!-- service-page -->
            <div id="service">
                <div class="col-md-6 col-md-offset-3 title text-center page-title">
                    <h2>{{trans('fr_home.service_packs')}}</h2>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-10 col-md-offset-1 pg-service">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3 sv-member sv-m1">
                                    <div class="form-top top-radius-left bg-title text-center">
                                        <h4>{{trans('fr_home.free_member')}}</h4>
                                    </div>
                                    <div class="form-body">
                                        <ul class="sv-ul">
                                            <li class="sv-li">Lorem ipsum dolor sit</li>
                                            <li>adipisicing</li>
                                            <li>Sed do eiusmod tempor</li>
                                            <li>Incididunt ut labore</li>
                                            <li>Dolore magna aliqua</li>
                                        </ul>
                                    </div>
                                    <div class="form-bottom bottom-radius-left text-center">
                                        <span class="checkout">{{trans('fr_home.regist')}}</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 sv-member sv-m2">
                                    <div class="form-top bg-title text-center">
                                        <h4>{{trans('fr_home.copper_member')}}</h4>
                                    </div>
                                    <div class="form-body">
                                        <ul class="sv-ul">
                                            <li class="sv-li">Lorem ipsum dolor sit</li>
                                            <li>adipisicing</li>
                                            <li>Sed do eiusmod tempor</li>
                                            <li>Incididunt ut labore</li>
                                            <li>Dolore magna aliqua</li>
                                        </ul>
                                    </div>
                                    <div class="form-bottom text-center">
                                        <span class="checkout">{{trans('fr_home.regist')}}</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 member-silver sv-m3">
                                    <div class="form-top bg-title text-center top-radius-left top-radius-right">
                                        <h4>{{trans('fr_home.sliver_member')}}</h4>
                                    </div>
                                    <div class="form-body">
                                        <ul class="sv-ul">
                                            <li class="sv-li">Lorem ipsum dolor sit</li>
                                            <li>adipisicing</li>
                                            <li>Sed do eiusmod tempor</li>
                                            <li>Incididunt ut labore</li>
                                            <li>Dolore magna aliqua</li>
                                        </ul>
                                    </div>
                                    <div class="form-bottom bottom-radius text-center">
                                        <span class="checkout">{{trans('fr_home.regist')}}</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 sv-member sv-m4">
                                    <div class="form-top top-radius-right bg-title text-center">
                                        <h4>{{trans('fr_home.golden_member')}}</h4>
                                    </div>
                                    <div class="form-body">
                                        <ul class="sv-ul">
                                            <li class="sv-li">Lorem ipsum dolor sit</li>
                                            <li>adipisicing</li>
                                            <li>Sed do eiusmod tempor</li>
                                            <li>Incididunt ut labore</li>
                                            <li>Dolore magna aliqua</li>
                                        </ul>
                                    </div>
                                    <div class="form-bottom bottom-radius-right text-center">
                                        <span class="checkout">{{trans('fr_home.regist')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js_wrapper')
    <script type="text/javascript">
        $('body').attr('id', 'bg-service');
    </script>
@stop