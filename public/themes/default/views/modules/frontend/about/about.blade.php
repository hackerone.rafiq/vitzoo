@extends($theme_layout)
@section('content')
    <div class="mainpage">
        <div class="tab-content container side-body">
            <!-- home-page -->
            <!-- about-page -->
            <div id="about">
                <div class="col-md-6 col-md-offset-3 title text-center page-title">
                    <h2>Giới thiệu</h2>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-1 col-sm-4">
                        <img class="img-about" src="{{url('themes/default/asset_frontend/img/about.jpg')}}" alt="Viet Information Technology Project" />
                    </div>
                    <div class="col-md-6 col-sm-8 ab-introduce">
                        <p>
                            VITPR là một công ty trẻ được thành lập ở Việt Nam, VITPR là một nhóm gồm những
                            nhà phát triển trẻ tuổi và năng động, hợp tác cùng làm việc một thời gian
                            dài. Trong hơn 6 năm, chúng tôi đã thiết kế và phát triển rất nhiều sản phẩm
                            websites và ứng dụng điện thoại. Năng động, nhanh chóng, chuyên nghiệp, công bằng
                            và trung thực chính là phương châm của chúng tôi.
                        </p>
                        <p>
                            Công ty VITPR
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js_wrapper')
    <script type="text/javascript">
        $('body').attr('id', 'bg-about');
    </script>
@stop