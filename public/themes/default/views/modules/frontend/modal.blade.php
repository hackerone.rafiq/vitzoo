<!-- popup dieu khoan su dung -->
<div id="myModal" class="modal fade modals" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
                <h4 class="modal-title">{{trans('fr_home.terms_of_use')}}</h4>
            </div>
            <div class="modal-body">
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
            </div>
        </div>
    </div>
</div>
<!-- success Modal global-->
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content panel-success">

            <div class="modal-body panel-heading text-center">
                <p></p>
            </div>

        </div>

    </div>
</div>
<!-- Modal Error-->
<div id="errorModal" class="modal fade" role="dialog">
    <div class="modal-dialog centerDiv">

        <!-- Modal content-->
        <div class="modal-content panel-danger">

            <div class="modal-body panel-heading text-center">
                <p></p>
            </div>
        </div>

    </div>
</div>

<!-- Ajax loading-->
<div id="css-loading" style="display: none;">
    <div class="cssload-container">
        <div class="cssload-whirlpool"></div>
    </div>
</div>
<!--modal confirm -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>{{trans('fr_home.warning')}}</h4>
            </div>
            <div class="modal-body" style="text-align: center">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('fr_home.cancel')}}</button>
                <a class="btn btn-danger btn-ok">{{trans('fr_home.delete')}}</a>
            </div>
        </div>
    </div>
</div>

{{--
<form action="https://vitzoo.on:4040/" method="post">
    <input name="abc" value="abcf" type="text"/>
    <input type="submit" value="submit">
</form>--}}
<div id="cModalAcceptCall" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog centerDiv">
        <!-- Modal content-->
        <div class="modal-content">
            {{--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="gridSystemModalLabel">Vitzoo</h5>
            </div>--}}
            <div class="modal-body panel-heading text-center">
                <div class="button" id="acceptCallBox">
                    <img style="display:inline-block;border-radius: 50%;width: 60px;"
                         class="video-avatar video-avatar-caller" src="/themes/default/asset_frontend/img/pf.jpg">
                    <span class="rtc_user_name" data-lang="wants to have a call with you"></span>
                    <span class="button-accept-decline" data-easyrtc="0" data-caller="0" data-roomid="0">
                    <button type="button" class="btn btn-default accept" id="callAcceptButton" data-dismiss="modal">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-default hangup" id="callRejectButton" data-dismiss="modal">
                        <i class="fa fa fa-phone" style="transform: rotate(135deg);"></i>
                    </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="cDetectWifi" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog centerDiv">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body panel-heading text-center">
               <span><i class="fa fa-wifi" aria-hidden="true"></i> {{trans('fr_home.check_network_connection')}}</span>
            </div>
        </div>
    </div>
</div>

<!-- detect IE -->
<div id="detectIE" class="modal fade modals" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
                <h4 class="modal-title">{{trans('fr_home.switch_browsers_to_make_calls')}}</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p>{{trans('fr_home.no_support_ie')}}</p>
                <div class="detect-browser">
                    <img src="/themes/default/asset_frontend/img/chrome.png" alt="" style="width: 10%;">
                    <img src="/themes/default/asset_frontend/img/firefox.png" alt="" style="width: 10%;">
                </div>
            </div>
        </div>
    </div>
</div>