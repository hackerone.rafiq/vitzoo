{{--profile_chat_user--}}
<?php $attribute_array = [1, 'gender', 'mobile', 'address', 'birthday', 'nation', 'provinces', 'city', 'language'];?>

<div class="header-chat-profile">
    <h5>{{trans('fr_home.account')}}</h5>
    <button class="btn btn-menu closebtn" type="button" onclick="closeNav('addfriend')">
        <a href="javascript:void(0)"><i
                    class="fa fa-remove"></i></a>
    </button>
</div>

<div id="click_slideup" class="receive-tab clearfix">
    <div class="main-tab">
        <h5>{{trans('fr_home.profile_photo')}}</h5>
        <div class="avata-chat-user">
            <a href="{{Helper::url('profile/'.$user->id)}}">
            @if(isset($user) && $user->url)
                <img src="{{url('themes/default/asset_frontend/img_avatar/'.$user->url)}}"/>
            @else
                <img src="{{url('themes/default/asset_frontend/img/avatar.png')}}" style="border-radius: 50%"/>
            @endif
            </a>
        </div>
        <div class="content-chat-user row">
            <div class="clearfix">
                @if(isset($user))
                <h5 class="col-md-4">{{trans('fr_home.full_name')}}</h5>
                <a href="{{Helper::url('profile/'.$user->id)}}"><span class="col-md-8">{{$user->last_name.' '.$user->first_name}}</span></a>@endif
            </div>
            <div class="clearfix status-chat-user col-md-8 col-md-offset-4 user-status">
                <ul style="padding-left: 12px">

                </ul>
            </div>
            @if(isset($user) && $user && $user->email)
                <div class="clearfix">
                    <h5 class="col-md-4">Email</h5>
                    <div class="col-md-8 inp-form-chat">
                        <span>{{$user->email}}</span>
                    </div>
                </div>
            @endif
            @if(isset($user_attribute) && count($user_attribute)>0)
                @foreach($user_attribute as $item)
                    @if($item->values)
                    @if($item->attribute_id == 5)
                        <div class="clearfix">
                            <h5 class="col-md-4">{{trans('fr_home.'.$attribute_array[$item->attribute_id])}}</h5>
                            <div class="col-md-8 inp-form-chat">
                                <span>@if($eloquent_country = $item->joinCountry()->first()){{$eloquent_country->Country}}@endif</span>
                            </div>
                        </div>
                    @elseif($item->attribute_id == 6)
                        <div class="clearfix">
                            <h5 class="col-md-4">{{trans('fr_home.'.$attribute_array[$item->attribute_id])}}</h5>
                            <div class="col-md-8 inp-form-chat">
                                <span>@if($eloquent_country = $item->joinCity()->first()){{$eloquent_country->subdivision_1_name}}@endif</span>
                            </div>
                        </div>
                    @elseif($item->attribute_id == 7)
                        <div class="clearfix">
                            <h5 class="col-md-4">{{trans('fr_home.'.$attribute_array[$item->attribute_id])}}</h5>
                            <div class="col-md-8 inp-form-chat">
                                <span>@if($eloquent_country = $item->joinCity()->first()){{$eloquent_country->City}}@endif</span>
                            </div>
                        </div>
                    @elseif($item->attribute_id == 8)
                        <div class="clearfix">
                            <h5 class="col-md-4">{{trans('fr_home.'.$attribute_array[$item->attribute_id])}}</h5>
                            <div class="col-md-8 inp-form-chat">
                                <span>@if($eloquent_country = $item->joinLanguage()->first()){{$eloquent_country->title}}@endif</span>
                            </div>
                        </div>
                    @elseif($item->id && ($eloquent_attribute = $item->joinAttribute()->first()))
                        <div class="clearfix">
                            <h5 class="col-md-4">{{trans('fr_home.'.$eloquent_attribute->attribute_name)}}</h5>
                            <div class="col-md-8 inp-form-chat">
                                <span>{{$eloquent_attribute->attribute_name == 'gender'?trans('fr_home.'.$item->values):$item->values}}</span>
                            </div>
                        </div>
                    @endif
                    @endif
                @endforeach
            @endif
        </div>
        @if(isset($status_relative) && $status_relative)
            <div class="btn-chat-user user-relative-chat" data-id="{{$user->id}}"
                 data-url="{{Helper::url('chat/addRemoveFriend')}}"
                 data-option="{{$status_relative['option']}}">
                @if($status_relative['status'] == 'unfriended_blocked' || $status_relative['status'] =='unfriended' || $status_relative['status'] =='stranger')
                    <button type="button" style="" class="btn btn-danger btn-un-frd ajax-relative"
                            data-action="addfriend">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>{{trans('fr_home.addfriend')}}
                    </button>
                    <button type="button" class="btn btn-danger btn-bllock ajax-relative"
                            data-action="block">
                        <i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}
                    </button>
                @elseif($status_relative['status'] == 'waiting')
                    <button type="button" class="btn btn-danger btn-add-frd ajax-relative"
                            data-action="cancel-request">
                        <i class="fa fa-user-times" aria-hidden="true"></i>{{trans('fr_home.cancel_request')}}
                    </button>
                    <button type="button" class="btn btn-danger btn-bllock ajax-relative"
                            data-action="block">
                        <i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}
                    </button>
                @elseif($status_relative['status'] == 'confirm')
                    <button type="button" class="btn btn-danger btn-add-frd ajax-relative"
                            data-action="confirm">
                        <i class="fa fa-user-times" aria-hidden="true"></i>{{trans('fr_home.confirm')}}
                    </button>
                    <button type="button" style="" class="btn btn-danger btn-un-frd ajax-relative"
                            data-action="delete-request">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>{{trans('fr_home.delete_request')}}
                    </button>
                    <button type="button" class="btn btn-danger btn-bllock ajax-relative"
                            data-action="block">
                        <i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}
                    </button>
                @elseif($status_relative['status'] == 'friend')
                    <button type="button" class="btn btn-danger btn-add-frd ajax-relative"
                            data-action="unfriend">
                        <i class="fa fa-user-times" aria-hidden="true"></i>{{trans('fr_home.unfriend')}}
                    </button>
                    <button type="button" class="btn btn-danger btn-bllock ajax-relative"
                            data-action="block">
                        <i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}
                    </button>
                @elseif($status_relative['status'] == 'blocked')
                    <button type="button" style="" class="btn btn-danger btn-unlock ajax-relative"
                            data-action="unblock">
                        <i class="fa fa-unlock-alt" aria-hidden="true"></i>{{trans('fr_home.unblock')}}
                    </button>
                @endif
            </div>
        @endif
    </div>
</div>

