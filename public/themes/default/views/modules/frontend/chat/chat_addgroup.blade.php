<div class="right-content tab-pane tabgroup" id="tabaddgroup">
    <div class="rows">
        <div class="col-conservation tab-content">
            <div class="right-content col-widthd clearfix ctn-addfriend main-chat-content content-addgroup"
                 id="main_addgroup">
                <header class="right-title clearfix">
                    <div class="user-chat col-md-7">
                        <div class="user-chat-addfriend">
                            <div class="avatar">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h5>{{trans('fr_home.name_group')}}</h5>
                        </div>
                    </div>
                    <div class="dropdown dot-menu col-md-5">
                        {{--<button class="btn btn-menu" type="button" onclick="openNav()">
                            <i class="fa fa-info"></i>
                        </button>--}}
                        <form role="search" class="stylish-input-group" onsubmit="event.preventDefault()">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="{{trans('fr_home.search')}}"
                                       id="search-friend-tabgroup">
                                <span class="input-group-addon">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                            </div>
                        </form>
                    </div>
                </header>
                <div class="nanos scrollview ctn-search-addfriend chat-tab-container">
                    <div class="content-chat clearfix nano-content">
                        <article>
                            <div class="titile-allfriend">
                                <p>{{trans('fr_home.all_friend')}}</p>
                            </div>
                            <div class="cont-addfi append-all-friend">
                                <div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>

            <div class="col-attach sidenav user_info voltaic-group" id="voltaic_holder_addgroup">
                <div class="header-chat-profile">
                    <h5>{{trans('fr_home.account')}}</h5>
                    <button class="btn btn-menu closebtn" type="button" onclick="closeNav()">
                        <a href="javascript:void(0)"><i
                                    class="fa fa-remove"></i></a>
                    </button>
                </div>

                <div class="receive-tab clearfix">
                    <div class="main-tab">
                        {{--<h5>Profile Photo</h5>--}}
                        <div class="avata-chat-user" style="border-radius: 50%;">
                            <div class="hovereffect"
                                 @if(isset($user) && !empty($user->url))style="background-image: url('{{Helper::getLinkAvatar().$user->url}}');">
                                @else style="background-image: none;">
                                <span><i class="fa fa-users"></i></span>
                                @endif
                                {{-- version 2<div class="overlay">
                                    <div class="pf-content-hover">

                                        <a class="info" href="#" id="file-upload"
                                        ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            <p style="margin: 0;padding: 0;">
                                                {{trans('fr_home.change_avatar')}}
                                            </p>
                                        </a>
                                        <a class="info" href="#"></a>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                        <div class="content-chat-user row">
                            <div class="clearfix">
                                <div class="col-md-12">
                                    <div class="right-inner-addon">
                                        <i class="fa fa-pencil"></i>
                                        <span style="line-height: 35px;"
                                              class="group-edit-name span-addnamegroup">{{trans('fr_home.name_group')}}</span>
                                        <input type="text"
                                               class="form-control input-addnamegroup"
                                               placeholder="{{trans('fr_home.name_group')}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <h5 class="col-md-12"
                                    style="border-bottom: 1px solid rgba(255, 255, 255, 0.15)">{{trans('fr_home.the_group_members')}}</h5>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="main-tab scroll-tab-group nano">
                    <div class="cont-addfi clearfix member-addtogroup-sidebar nano-content">
                        <ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

