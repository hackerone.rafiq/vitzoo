@if(isset($message) && isset($timezone)&& count($message)>0)
    {{--@foreach($timeDistinct as $i=>$value)--}}
    <?php $count = count($message);
    if (empty($current)) $current = 0;
    ?>
    @for($x = $count - 1; $x >= 0; $x--)
        <?php $jvalue = $message[$x]; $sec = strtotime($jvalue->date);$test = $current !== $sec ?>
        @if($test)
            <article data-time="{{($sec+(-60)*$timezone)}}" class="clearfix article">
                <div class="date-time" data-item="a1"
                     data-time="{{date('Y/m/d H:i:s',$sec+(-60)*$timezone)}}">
                    {{--<p>Thứ hai, ngày 04 tháng 04 năm 2016</p>--}}
                    <p>{{Helper::getDatetimeChat($sec)}}</p>
                </div>
                <div class="row row-append">
                    {{--@foreach($message as $j=>$jvalue)--}}
                </div>
            </article>
        @endif
        <?php $current = strtotime($jvalue->date) ?>
        @if($user_join = $jvalue->joinUser()->first())
            <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                <div class="col-md-1 col-xs-1 col-st">
                    <div class="avatar">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                <div class="col-md-11 col-xs-11 ct-chat">
                    <div class="chat-box">
                        <div class="messages msg_sent">
                            <time datetime="2009-11-13T20:00">
                                <span>{{$user_join->last_name.' '.$user_join->first_name}}</span>
                                @if(isset($timezone) && $timezone){{Helper::getHourMinutes(strtotime($jvalue->created_at),$timezone)}} @endif
                            </time>
                            <div class="msg">
                                @if($jvalue->is_file ==1)
                                    <a href="{{url('themes/default/asset_frontend/img_file/'.Helper::decrypt($jvalue->content))}}"
                                       download> {{Helper::getNameFile(Helper::decrypt($jvalue->content),"/")[1]}}</a>
                                @else
                                    {{Helper::decrypt($jvalue->content)}}

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endfor
@endif