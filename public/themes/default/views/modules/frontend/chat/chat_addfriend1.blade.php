<div class="right-content col-widthd clearfix ctn-addfriend" id="main">
    <header class="right-title clearfix" id="righttitle">
        <div class="user-chat col-md-8">
            <div class="user-chat-addfriend">
                <i class="fa fa-user-plus"></i>
                <h5>{{trans('fr_home.addfriend')}}</h5>
            </div>
        </div>
        <div class="dropdown dot-menu col-md-4">
            {{--<button class="btn btn-menu" type="button" onclick="openNav()">
                <i class="fa fa-info"></i>
            </button>--}}
            <form role="search" class="stylish-input-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Tìm kiếm..." id="search-addfriend">
                        <span class="input-group-addon">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                </div>
            </form>
        </div>
    </header>
    <div class="nanos scrollview ctn-search-addfriend">
        <div class="content-chat clearfix nano-content">
            <article>
                <div class="titile-allfriend">
                    <p>{{trans('fr_home.all_friend')}}</p>
                </div>
                <div class="cont-addfi">
                    <div>
                        <div class="col-md-4 col-sm-6" onclick="openNav()">
                            <div class="user-chat">
                                <div class="avatar"><i class="fa fa-user"></i></div>
                                <div class="active-user-info">
                                    <div class="chat-us-tt clearfix">
                                        <span class="color-text-ddd">ThinhGlory</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="status">Hue, VietNam</span>
                                    </div>
                                </div>
                                <div class="icon-addfriend" style="display: none;"><i class="fa fa-user-plus"
                                                                         aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" onclick="openNav()">
                            <div class="user-chat">
                                <div class="avatar"><i class="fa fa-user"></i></div>
                                <div class="active-user-info">
                                    <div class="chat-us-tt clearfix">
                                        <span class="color-text-ddd">ThinhGlory</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="status">Hue, VietNam</span>
                                    </div>
                                </div>
                                <div class="icon-addfriend" style="display: none;"><i class="fa fa-user-plus"
                                                                                      aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" onclick="openNav()">
                            <div class="user-chat">
                                <div class="avatar"><i class="fa fa-user"></i></div>
                                <div class="active-user-info">
                                    <div class="chat-us-tt clearfix">
                                        <span class="color-text-ddd">ThinhGlory</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="status">Hue, VietNam</span>
                                    </div>
                                </div>
                                <div class="icon-addfriend" style="display: none;"><i class="fa fa-user-plus"
                                                                                      aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" onclick="openNav()">
                            <div class="user-chat">
                                <div class="avatar"><i class="fa fa-user"></i></div>
                                <div class="active-user-info">
                                    <div class="chat-us-tt clearfix">
                                        <span class="color-text-ddd">ThinhGlory</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="status">Hue, VietNam</span>
                                    </div>
                                </div>
                                <div class="icon-addfriend" style="display: none;"><i class="fa fa-user-plus"
                                                                                      aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" onclick="openNav()">
                            <div class="user-chat">
                                <div class="avatar"><i class="fa fa-user"></i></div>
                                <div class="active-user-info">
                                    <div class="chat-us-tt clearfix">
                                        <span class="color-text-ddd">ThinhGlory</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="status">Hue, VietNam</span>
                                    </div>
                                </div>
                                <div class="icon-addfriend" style="display: none;"><i class="fa fa-user-plus"
                                                                                      aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" onclick="openNav()">
                            <div class="user-chat">
                                <div class="avatar"><i class="fa fa-user"></i></div>
                                <div class="active-user-info">
                                    <div class="chat-us-tt clearfix">
                                        <span class="color-text-ddd">ThinhGlory</span>
                                    </div>
                                    <div class="clearfix">
                                        <span class="status">Hue, VietNam</span>
                                    </div>
                                </div>
                                <div class="icon-addfriend" style="display: none;"><i class="fa fa-user-plus"
                                                                                      aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <div class="text-chat" id="textchat">
        <div class="rows clearfix">
            <form>
                <div class="chatbox">
                    <div class="input-group">
                        <input type="text" class="form-control emojis-wysiwyg" aria-label=""
                               placeholder="Nhập tin nhắn" data-emojiable="true"/>
                        <div class="input-group-addon">
                            <input type="file" id="upload" name="upload" style="display: none"
                                   multiple/>
                            <a onclick="document.getElementById('upload').click(); return false"
                               title="#">
                                <i class="fa fa-paperclip"></i>
                            </a>
                            <a href="#" title="#">
                                <i class="fa fa-video-camera"></i>
                            </a>
                            <a class="emoji-button" id="btn-smile" href="#" title="Emojis"
                               onclick="document.getElementById('emotion').click(); return false">
                                <i class="fa fa-smile-o"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="chatbutton">
                    <button type="button" class="btn btn-dangers">Gửi</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-attach sidenav" id="voltaic_holder">
    <div class="header-chat-profile">
        <h5>{{trans('fr_home.infomation_member')}}</h5>
        <button class="btn btn-menu closebtn" type="button" onclick="closeNav()">
            <a href="javascript:void(0)"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </button>
    </div>

    <div id="click_slideup" class="receive-tab clearfix">
        <div class="main-tab">
            <h5>{{trans('fr_home.profile_photo')}}</h5>
            <div class="avata-chat-user">
                <img src="{{url('themes/default/asset_frontend/img/pf.jpg')}}"/>
            </div>
            <div class="content-chat-user row">
                <div class="clearfix">
                    <h5 class="col-md-4">Họ tên</h5>
                    <span class="col-md-8">Pham Nhu Hiep</span>
                </div>
                <div class="clearfix status-chat-user col-md-8 col-md-offset-4">
                    <li class="init status"><i class="online"></i>online</li>
                </div>
                <div class="clearfix">
                    <h5 class="col-md-4">Mood</h5>
                    <span class="col-md-8">“Tôi có thể ăn thủy tinh mà không hại gì.”</span>
                </div>
                <div class="clearfix">
                    <h5 class="col-md-4">Email</h5>
                    <span class="col-md-8">nhuhiep@vitpr.com</span>
                </div>
                <div class="clearfix">
                    <h5 class="col-md-4">Phone</h5>
                    <span class="col-md-8">0121090909</span>
                </div>
                <div class="clearfix">
                    <h5 class="col-md-4">Location</h5>
                    <span class="col-md-8">Hue, Viet Nam</span>
                </div>
            </div>
            <div class="btn-chat-user">
                <button type="button" class="btn btn-danger btn-add-frd">
                    <i class="fa fa-user-times" aria-hidden="true"></i>Unfriend
                </button>
                <button type="button" style="display: none" class="btn btn-danger btn-un-frd">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>Add friend
                </button>
                <button type="button" class="btn btn-danger btn-bllock">
                    <i class="fa fa-lock" aria-hidden="true"></i>Block
                </button>
                <button type="button" style="display: none" class="btn btn-danger btn-unlock">
                    <i class="fa fa-unlock-alt" aria-hidden="true"></i>Unblock
                </button>
            </div>
        </div>
    </div>
</div>

