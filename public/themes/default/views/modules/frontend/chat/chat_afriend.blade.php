@if(isset($friend) && isset($user) && isset($type))
    <?php
        $name = $user->last_name . ' ' . $user->first_name;
    ?>
    <div class="@if($type != 'addgroup') none-dashboard @endif col-md-6  col-sm-6 col-xs-12 user-afriend" onclick="openNav()" data-name="{{Helper::stripVowelAccent($name)}}">
        <div class="user-chat" data-id="{{$user->id}}" data-url="{{Helper::url('chat/addRemoveFriend')}}">
            <div class="avatar">@if(!$user->url)<i class="fa fa-user"></i>@else<img
                        src="{{Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/'.$user->url))}}"
                        alt="avatar" class="img-responsive center-block">@endif</div>
            <div class="active-user-info" data-id="{{$user->id}}">
                <div class="chat-us-tt clearfix">
                    <span class="color-text-ddd">{{$name}}</span>
                </div>
                <div class="clearfix chat-us-tt">
                    <span class="status truncate" style="font-style: italic;">@if($mood = $friend->mood){{$mood}}@else {{trans('fr_home.no_mood')}} @endif</span>
                </div>
            </div>
            @if($type == 'addgroup')
                <div class="icon-addfriend icon-addgroup" style="display: none;"><i class="fa fa-user-plus" aria-hidden="true"></i>
                </div>
                @elseif($type =='addfriend')
                <div class="icon-addfriend icon-removefriend"><i class="fa fa-check" aria-hidden="true"></i>
                </div>
                @elseif($type == 'nonefriend')
                <div class="icon-addfriend icon-addOnefriend"><i class="fa fa-user-plus"
                                                                                        aria-hidden="true"></i>
                </div>
                @elseif($type == 'waiting')
                <div class="icon-addfriend"><i class="fa fa-spinner"
                                                                                        aria-hidden="true" title="{{trans('fr_home.waiting_to_accept')}}"></i>
                </div>
            @endif
        </div>
    </div>
@endif