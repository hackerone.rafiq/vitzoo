<!doctype html>
<html lang="en">
<head>
    <style>
        body {
            height: 100vh;
            overflow: hidden;
            background-image: url('{{Helper::getDataURI(url('themes/default/asset_frontend/img/background.jpg'))}}');
        }

        #bg-about {
            background-image: url('{{Helper::getDataURI(url('themes/default/asset_frontend/img/bg-about.jpg'))}}');
        }

        #bg-service {
            background-image: url('{{Helper::getDataURI(url('themes/default/asset_frontend/img/bg-dichvu.jpg'))}}');
        }

        #bg-faq {
            background-image: url('{{Helper::getDataURI(url('themes/default/asset_frontend/img/bg-faq.jpg'))}}');
        }

        #bg-contact {
            background-image: url('{{Helper::getDataURI(url('themes/default/asset_frontend/img/bg-contact.jpg'))}}');
        }

        #bg-profile {
            background-image: url('{{Helper::getDataURI(url('themes/default/asset_frontend/img/bg-profile.jpg'))}}');
        }
    </style>
    @include('default::modules.frontend.head')
    {!! SEO::generate() !!}
		<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {!! Assets::css() !!}
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src   = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-32694432-7', 'auto');
        ga('send', 'pageview');
    </script>
    <?php
    $user_session = Sentinel::check();
    ?>
    @if($user_session)
        <script>
            var helperUrl = '{{Helper::geturl('4040')}}';
            var helperRtc = '{{Helper::geturl('5555')}}';

            var token         = '{{session('jwt_token')}}';
            var sessionGlobal = '{{Sentinel::check()->id}}';
            var nameSession   = '{{Sentinel::check()->last_name.' '.Sentinel::check()->first_name}}';
            var lang          = '{{\LaravelLocalization::getCurrentLocale()}}';
            var urlGlobal     = '{{url('')}}';
        </script>
    @endif
</head>
<body>
@yield('css')
<div class="chat-content clearfix">
    <div class="main-box-chat clearfix">
        <div class="menu-mobile">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><i class="fa fa-reorder"></i></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">
                                <li class="gn-search-item clearfix">
                                    <input placeholder="{{trans('fr_home.search')}}" type="search" class="gn-search">
                                    <a class="gn-icon gn-icon-search"><i class="fa fa-search"></i>
                                        <span>{{trans('fr_home.search')}}</span></a>
                                </li>
                                <li class="clearfix">
                                    <a class="gn-icon"><i class="fa fa-home"></i>{{trans('fr_home.home')}}</a>
                                </li>
                                <li class="clearfix"><a class="gn-icon"><i
                                                class="fa fa-user-plus"></i>{{trans('fr_home.new')}}</a></li>
                                <li class="clearfix">
                                    <a class="gn-icon" id="showfriends"><i
                                                class="fa fa-user"></i>{{trans('fr_home.friend')}}</a>
                                    <div class="nano" id="showfriend" style="display: none;">
                                        <ul class="list-user nano-content">
                                        </ul>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <a class="gn-icon" id="showgroups"><i
                                                class="fa fa-users"></i>{{trans('fr_home.group')}}</a>
                                    <div class="nano" id="showgroup" style="display: none;">
                                        <ul class="list-user nano-content">
                                        </ul>
                                    </div>
                                </li>
                                <li class="clearfix"><a class="gn-icon"><i class="fa fa-heart"></i>Gần đây</a></li>
                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href=""><img src="{{url('themes/default/asset_frontend/img/logo.png')}}" alt="ViTZOOM"
                                    class="img-responsive center-block"/></a></li>
            </ul>
        </div>
        <div class="sideContainer clearfix">
            <div class="left-content">
                <div id="hd" class="clearfix">
                    <header>
                        <div class="dropdown">
                            <span class="btn btn-menu dropdown-toggle" type="button" data-toggle="dropdown">
                                <i class="fa fa-navicon chat-drop"></i>
                                <i class="fa fa-times chat-exit" style="display: none;"></i>
                            </span>

                        </div>
                        <div class="tk dropdown add-menu class-count-notice">
                            <a class="btn btn-danger btn-chat btn-notifi btn-bell-notifi"
                               data-href="{{Helper::url('chat/loadNotification')}}" href="javascrip:void(0)"
                               style="padding: 0 10px;"><i class="fa fa-bell"
                                                           aria-hidden="true"></i>
                                {{--<span class="count-notifi">1</span>--}}
                            </a>
                            <div class="dropdown-menu animated notifications notifi-box" style="left: 0;">
                                <div class="topnav-dropdown-header">
                                    <span>{{trans('fr_home.notifications')}}</span>
                                    <a class="setting-button" href="#"
                                       title="Settings">
                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="scroll-pane has-scrollbar notice-nano">
                                    <ul class="media-list scroll-content nano-content" tabindex="0"
                                        style="right: -17px;" data-url="{{Helper::url('chat/updateIsClick')}}">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <a href="{{Helper::url()}}" title="Logo">
                            <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img/logo.png'))}}"
                                 alt="ViTZOOM"
                                 class="img-responsive center-block img-res-logo"/>
                        </a>
                    </header>
                    <ul class="chat-dropdown-menu" style="display: none;">
                        <li data-id="1" class="add-group-chat"><i class="fa fa-users"></i><a href="#"
                                                                                             data-href="{{Helper::url('chat/getFriend')}}">{{trans('fr_home.create_group')}}</a>
                        </li>
                        <li data-id="2" class="invite-friend-chat"><i class="fa fa-users"></i><a href="#"
                                                                                             data-href="">{{trans('fr_home.invite_friend')}}</a>
                        </li>
                        <li data-id="2" class="add-friend-chat"><i class="fa fa-user"></i><a href="#"
                                                                                             data-href="{{Helper::url('chat/getFriend')}}">{{trans('fr_home.addfriend')}}</a>
                        </li>
                        {{--<li><i class="fa fa-wrench"></i><a href="">Cài đặt</a></li>--}}
                        <li class="logout-chat"><a href="{{Helper::url('logout')}}"><i
                                        class="fa fa-sign-out"></i>{{trans('fr_home.logout')}}</a></li>
                    </ul>
                    {{--<div class="active-user clearfix" onclick="openNav()">--}}
                    <div class="active-user clearfix">
                        @if($user_session && $user_session->url)
                            <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/'.$user_session->url))}}"
                                 class="img-responsive center-block" alt="avatar"/>
                        @else
                            <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img/avatar.png'))}}"
                                 class="img-responsive center-block" alt="avatar"/>
                        @endif
                        <div class="active-user-info">
                            <a href="{{Helper::url('profile/'.$user_session->id)}}"><span
                                        class="usname-truncate">{{$user_session->last_name.' '. $user_session->first_name}}</span></a>
                            <p class="username-hv"
                               style="display: none;">{{$user_session->last_name.' '. $user_session->first_name}}</p>

                            <div class="user-status">
                                <div class="offline-spinner"><i
                                            class="fa fa-refresh spinner" aria-hidden="true"></i>Offline
                                </div>
                                <span class="status color-text-ddd">@if(isset($status) && $status)<i
                                            class="{{$status->status}}"></i>{{trans('fr_home.'.$status->status)}} @endif
                                    </span>
                                <i class="fa fa-angle-down status-down" aria-hidden="true"></i>
                                <ul style="padding-left: 12px">
                                    {{--<li class="init status" style="float: left; width: 40%;"><i
                                                class="{{\Modules\User\Models\StatusModel::getStatusById($user_session->id)}}"></i>
                                        {{trans('fr_home.'.\Modules\User\Models\StatusModel::getStatusById($user_session->id))}}
                                    </li>--}}

                                    <div class="sel-opt-status">
                                        <li data-value="value-st" data-attribute="1"><i class="online"></i>Online</li>
                                        <li data-value="value-st" data-attribute="2"><i
                                                    class="invisible"></i>{{trans('fr_home.invisible')}}
                                        </li>
                                        <li data-value="value-st" data-attribute="3"><i
                                                    class="busy"></i>{{trans('fr_home.busy')}}</li>
                                    </div>
                                </ul>

                            </div>

                            <div class="user-mood">
                                <?php $mood = trim($status->mood); ?>
                                <i class="truncate"
                                   data-mood="{{!!$mood}}">{{$mood ? $mood :trans('fr_home.how_do_you_feel')}}</i>
                            </div>
                        </div>

                    </div>
                    <div class="main-nav">
                        <div id="search">
                            <div class="panel-body">
                                <form role="search" class="stylish-input-group" onsubmit="event.preventDefault();">
                                    <div class="input-group">
                                        <input type="text" class="form-control"
                                               placeholder="{{trans('fr_home.search')}}..." id="search-list-user">
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-tab">
                    <style id="search_style"></style>
                    <ul class="nav nav-tabs" id="nav-tabs">
                        <li data-target=".nano" class="@if($acronym === 'f-') active @endif"><a href="#friend"
                                                                                                data-toggle="tab">{{trans('fr_home.friend')}}</a>
                        </li>
                        <li data-target=".nano1" class="@if($acronym === 'g-') active @endif"><a href="#group"
                                                                                                 data-toggle="tab">{{trans('fr_home.group')}}</a>
                        </li>
                        <li data-target=".nano2"
                            class="recent-notification @if($acronym !== 'g-' && $acronym !== 'f-') active @endif"><a
                                    href="#recent"
                                    data-toggle="tab">{{trans('fr_home.recent')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="left-content-bar"> <!--nano-->
                        <div id="friend" class="tab-pane nano nas @if($acronym === 'f-') active @endif">
                            <div class="nano-content">
                                <ul class="list-user friend-listing nav-tabs-tab" role="tablist">
                                    <?php $user_arr = [];
                                    $group_arr = []; ?>
                                    @if(count($listuser))
                                        @foreach($listuser as $friend)
                                            @if($friend->friend_id !== $session->id)
                                                <?php $room_id = Helper::uniqueRoom($friend->friend_id, $session->id);
                                                if (isset($user_arr[ $friend->friend_id ])) {
                                                    $list = $user_arr[ $friend->friend_id ];
                                                } else {
                                                    $list = $friend->joinUser()->select(['first_name', 'last_name', 'url', 'id'])->first();
                                                    $user_arr[ $friend->friend_id ] = $list;
                                                }
                                                ?>

                                                @if($list)
                                                    <li class="clearfix new-friend"
                                                        data-name="{{Helper::stripVowelAccent($list->last_name.' '.$list->first_name)}}"
                                                        data-id="{{$friend->friend_id}}"
                                                        data-roomid="{{$room_id}}" id="f-{{$room_id}}">
                                                        <a class="clearfix" data-id="{{$list->id}}"
                                                           data-url="{{Helper::url('chat/getMessage')}}"
                                                           {{--href="#tab{{$room_id}}"--}}
                                                           {{--data-toggle="tab"--}} data-action="one-to-one"
                                                           data-roomid="{{$room_id}}">
                                                            <div class="avatar">
                                                                @if($friend->status === 'friend')
                                                                    @if($list->url)
                                                                        <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/'.$list->url))}}"
                                                                             alt="avatar"
                                                                             class="img-responsive center-block"/>
                                                                    @else
                                                                        <i class="fa fa-user"></i>
                                                                    @endif
                                                                @else
                                                                    <i class="fa fa-question"></i>
                                                                @endif
                                                            </div>
                                                            <div class="active-user-info">
                                                                <span class="user_name">{{$list->last_name.' '.$list->first_name}}</span>
                                                                <span class="status">
                                                            <i class="offline"></i>{{trans('fr_home.offline')}}</span>
                                                            </div>
                                                        </a>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div id="group" class="tab-pane nano1 nas @if($acronym === 'g-') active @endif">
                            <div class="nano-content">
                                <ul id="group-listing" class="list-user">
                                    @if(count($roomUser))
                                        @foreach($roomUser as $list)
                                            <?php if (isset($group_arr[ $list->room_id ])) {
                                                $room = $group_arr[ $list->room_id ];
                                            } else {
                                                $room = $list->joinRoom()->select(['_id', 'title', 'room_admin', 'type', 'url'])->where('type', 'room')->first();
                                                $group_arr[ $list->room_id ] = $room;
                                            }
                                            ?>
                                            @if($room)
                                                <?php $is_admin = $room->room_admin == $session->id ? 1 : 0;?>
                                                <li class="clearfix new-friend" data-roomid="{{$room->_id}}"
                                                    data-name="{{Helper::stripVowelAccent(trim($room->title))}}"
                                                    id="g-{{$room->_id}}"
                                                    data-isadmin="{{$is_admin}}">
                                                    <a class="clearfix"
                                                       data-url="{{Helper::url('chat/getMessageRoom')}}"
                                                       {{--href="#tab{{$room->_id}}"--}}
                                                       {{--data-toggle="tab"--}} data-action="one-to-many">
                                                        <div class="avatar">@if($room->url)
                                                                <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/'.$room->url))}}"
                                                                     alt="avatar" class="img-responsive center-block"/>
                                                            @else
                                                                <i class="fa fa-users"></i>@endif</div>
                                                        <div class="active-user-info active-user-group">
                                                            <span class="user_name">{{trim($room->title)}}</span>
                                                        </div>
                                                    </a>
                                                </li>

                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div id="recent"
                             class="tab-pane nano2 nas @if($acronym !== 'g-' && $acronym !== 'f-') active @endif">
                            <div class="nano-content">
                                <ul class="list-user" id="recent-active-room">
                                    @include('default::modules.frontend.chat.chat_recent_rooms')
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield('content')
    </div>
</div>
@include('default::modules.frontend.layouts.translate')
@include('default::modules.frontend.modal')
{!! Assets::js() !!}
@yield('js')
@if($user_session)
    <script>
        new gnMenu(document.getElementById('gn-menu'));
        //todo
        //easyrtc.setUsername('{{$user_session->last_name.' '.$user_session->first_name}}');
    </script>
@endif
@include('default::modules.frontend.chat.chat_js')
</body>
</html>
