@if(isset($room_id) && isset($roomInfo) && isset($session) && isset($listFriend))
    <div class="rows">
        <div class="col-conservation tab-content">
            <div class="right-content col-widthd clearfix ctn-addfriend main-chat-content content-addgroup"
                 id="main_{{$room_id}}">
                <header class="right-title clearfix">
                    <div class="user-chat col-md-7">
                        <div class="user-chat-addfriend">
                            <div class="avatar">
                                @if($roomInfo->url) <img
                                        src="{{url('themes/default/asset_frontend/img_avatar/'.$roomInfo->url)}}"
                                        alt="avatar" class="img-responsive center-block">@else<i
                                        class="fa fa-users"></i>@endif </div>

                            <h5 style="text-transform: capitalize;">{{$roomInfo->title}}</h5>
                        </div>
                    </div>
                    <div class="dropdown dot-menu">
                        <button class="btn btn-menu btn-listmember-group" type="button"
                                onclick="openNav('{{$room_id}}')">
                            <i class="fa fa-info"></i>
                        </button>
                    </div>
                    <div class="dropdown add-menu">
                        <button class="btn btn-menu add-menu-button" data-url="{{Helper::url('chat/getFriendNotice')}}"
                                type="button">
                            <i class="fa fa-user-plus"></i>
                        </button>
                        <div class="dropdown-menu animated notifications add-menu-notifications" style="right: 0;">
                            <div class="topnav-dropdown-header">
                                <form role="search" class="stylish-input-group" onsubmit="event.preventDefault();">
                                    <div class="input-group" style="
    border-radius: 5px;
    border: 1px solid #ddd;
    line-height: 30px;
">
                                        <input type="text" class="form-control search-box-addtogroup"
                                               placeholder="{{trans('fr_home.search')}}" style="
    height: 30px;
    color: #333;

" id="search-box-{{$room_id}}">
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="scroll-pane has-scrollbar">
                                <ul class="media-list scroll-content" tabindex="0" style="right: -17px;">
                                </ul>
                            </div>
                            {{--<div class="topnav-dropdown-footer">
                                <a href="#">See all notifications</a>
                            </div>--}}
                        </div>
                    </div>


                </header>
                <div id="box{{$room_id}}" data-room="{{$room_id}}"
                     class="chat-tab-container chat-tab-insertion nanos scrollview"
                     style="height: 0px;">
                    <div class="content-chat clearfix nano-content">
                        {{--{!! $messages !!}--}}
                        @include('default::modules.frontend.chat.chat_messages')
                        <div style="display: none;" class="typing ontyping">
                            <p><span class="user_is_typing"><span
                                            class="group-typing"> {{trans('site.and_others')}}</span></span>
                                &nbsp;
                            <p class="is-typing">{{trans('site.is_typing')}}</p></p>
                            <div class="cssload-jumping">
                                <span></span><span></span><span></span><span></span><span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-chat">
                    <div class="rows clearfix">
                        {!! Form::open(['url'=>Helper::url('chat/uploadFile'),'class'=>'upload_form', 'onsubmit' => 'event.preventDefault()']) !!}
                        <div class="chatbox" data-roomid="{{$room_id}}">
                            <div class="input-group">
                                    <textarea
                                            class="textarea-scrollbar scrollbar-outer form-control emojis-wysiwyg input-message"
                                            aria-label=""
                                            placeholder="{{trans('fr_home.enter_message')}}" data-emojiable="true"
                                            data-roomid="{{$room_id}}"></textarea>
                                <div class="input-group-addon">
                                    <a class="pseudo-file-upload"
                                       title="#">
                                        <i class="fa fa-paperclip"></i>
                                    </a>
                                    <a data-roomid="{{$room_id}}" class="emoji-button" href="javascript:void(0)"
                                       title="Emojis">
                                        <i class="fa fa-smile-o"></i>
                                    </a>
                                    <a class="setting-button" href="javascript:void(0)" data-roomid="{{$room_id}}"
                                       title="Settings">
                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="chatbutton">
                            <button type="submit"
                                    class="btn btn-dangers buttonSaveFile">{{trans('fr_home.send')}}</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-attach sidenav user_info voltaic-group" id="voltaic_holder_{{$room_id}}">
                <div class="header-chat-profile header-chat-group">
                    {{-- <a class="setting-group" href="javascript:void(0)" title="Settings">
                         <i class="fa fa-cog" aria-hidden="true"></i>
                     </a>--}}
                    <h5>@if($isAdmin)<a class="setting-group" href="javascript:void(0)" title="Settings"
                                        style="margin-right: 10px;"><i class="fa fa-cog" aria-hidden="true"></i>
                        </a>@endif{{trans('fr_home.account')}}</h5>

                    <button class="btn btn-menu closebtn" type="button" onclick="closeNav('{{$room_id}}')">
                        <a href="javascript:void(0)"><i
                                    class="fa fa-remove"></i></a>
                    </button>
                    @if($isAdmin)
                        <ul class="ul-setting-group" style="display: none;">
                            <li value="delete" data-roomid="{{$room_id}}"><a href="javascript:void(0)"><i
                                            class="fa fa-trash-o"
                                            aria-hidden="true"
                                            style="margin-right: 5px;"></i>{{trans('fr_home.delete_group')}}</a>
                            </li>
                        </ul>
                    @endif
                </div>

                <div class="receive-tab clearfix">
                    <div class="">
                        {{--<h5>Profile Photo</h5>--}}
                        <div class="avata-chat-user clearfix" style="border-radius: 50%;">
                            <div class="hovereffect"
                                 @if($roomInfo->url)style="background-image: url('{{$room_picture}}');">
                                @else
                                    style="background-image: none;" ><span><i class="fa fa-users"></i></span>
                                @endif
                                @if($isAdmin)
                                    <div class="overlay change-group-picture" data-roomid="{{$room_id}}">
                                        <div class="pf-content-hover">
                                            <a class="info group-picture" href="javascript:void(0)"><i
                                                        class="fa fa-picture-o" aria-hidden="true"></i>
                                                <p style="margin: 0;padding: 0;">
                                                    {{trans('fr_home.change_avatar')}}
                                                </p>
                                            </a>
                                            <a class="info" href="javascript:void(0)"></a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="content-chat-user row">
                            <div class="clearfix">
                                <div class="col-md-12">
                                    @if($isAdmin)
                                        <div class="right-inner-addon">
                                            <i class="fa fa-pencil"></i>

                                            <span style="line-height: 35px;"
                                                  class="group-edit-name">{{$roomInfo->title}}</span>
                                            <input type="text"
                                                   class="form-control input-addnamegroup"
                                                   placeholder="{{trans('fr_home.name_group')}}"
                                                   data-roomid="{{$room_id}}"/>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix">
                                <h5 class="col-md-12"
                                    style="border-bottom: 1px solid rgba(255, 255, 255, 0.15)">{{trans('fr_home.the_group_members')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-tab scroll-tab-group nano" data-roomid="{{$room_id}}">
                    <div class="cont-addfi clearfix nano-content nano-content">
                        <ul>
                            @if(isset($roomMember))
                                @foreach($roomMember as $list)
                                    @if(($userInfo = $list->joinUser()->select(['id','first_name','last_name'])->first()) && $list->user_id != $session->id)
                                        <li class="clearfix">
                                            <div class="user-chat col-md-12">
                                                <div class="active-user-info" data-id="{{$userInfo->id}}">
                                                    <span class="color-text-ddd group-addfriend message-name"
                                                          style="cursor: pointer;"><?php $status = Helper::checkInArray($listFriend, $userInfo->id, 'friend_id');?>@if( $status == 'friend')
                                                            <i
                                                                    class="offline"></i>@elseif($status == 'waiting')<i
                                                                    class="fa fa-spinner" aria-hidden="true"
                                                                    title="{{trans('fr_home.waiting_to_accept')}}"></i>@else
                                                            <i
                                                                    class="fa fa-user-plus"
                                                                    title="{{trans('fr_home.addfriend')}}"></i>@endif{{$userInfo->last_name.' '.$userInfo->first_name}}</span>
                                                </div>
                                                @if($isAdmin)
                                                    <div class="icon-addfriend icon-removegroup"
                                                         style="display: none;"><i
                                                                class="fa fa-times"
                                                                aria-hidden="true"></i>
                                                    </div>
                                                @endif
                                            </div>

                                        </li>
                                    @endif
                                @endforeach
                            @endif

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif