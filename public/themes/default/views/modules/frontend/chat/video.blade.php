<head>
    <title>Vitzoo</title>
    {!! Assets::css() !!}
</head>
<body>
@if($receiver && $room_id)
    <?php
    $avatar_link = 'img/pf.jpg';
    if ($receiver->url)
        $avatar_link = 'img_avatar/' . $receiver->url;
    ?>
    <div class="video-call-wrapper clearfix" data-room-id="room_id"
         id="call_room_id">
        <div class="video-content-caller">
            <div class="video-calling">
                <img style="display:inline-block" class="video-avatar video-avatar-caller"
                     src="/themes/default/asset_frontend/{{$avatar_link}}">
                <p class="video-name-connecting">{{$receiver->last_name.' '.$receiver->first_name}}</p>
                <p class="video-status" data-connect="' + reserve + '">{{trans('site.connecting')}}...</p>
            </div>
            <video autoplay class="remote-video" style="width: 100%"></video>
            <div class="video-content-myself resizable">
                <div style="position: relative">
                    <video autoplay muted class="local-video easyrtcMirror"></video>
                    <i class="fa fa-compress resize_video"
                       style="position: absolute; bottom:0; right:0; transform:rotate(90deg);    font-size: 1.5rem; cursor:nwse-resize;display: none;"></i>
                </div>
            </div>
            <div class="video-button-control">
                <button role="button" title="Disable your video" class="js_5 _video-control"><i
                            class="fa fa-video-camera">
                    </i></button>
                <button role="button" title="Mute your microphone" class="_microphone-control js_5"><i
                            class="fa fa-microphone"></i></button>
                <button role="button" class="js_5 _hangup-control" title="hang up">
                    <i class="fa fa fa-phone" style="transform: rotate(135deg);"></i></button>
                <button role="button" title="full screen" class="js_5 _fullscreen-control">
                    <i class="fa fa-arrows-alt">
                    </i></button>
            </div>
        </div>
    </div>
    @if(Sentinel::check())
        <script>
            var helperUrl = '{{Helper::geturl('4040')}}';
            var helperRtc = '{{Helper::geturl('5555')}}';
            var token = '{{session('jwt_token')}}';
            var sessionGlobal = '{{Sentinel::check()->id}}';
            var nameSession = '{{Sentinel::check()->last_name.' '.Sentinel::check()->first_name}}';
            var lang = '{{\LaravelLocalization::getCurrentLocale()}}';
            var urlGlobal = '{{url('')}}';
            var room_idGlobal = '{{$room_id}}';
            var receiver_idGlobal = '{{$receiver->id}}';
        </script>
    @endif
@endif
@include('default::modules.frontend.layouts.translate')
{!! Assets::js() !!}
<script src="/themes/default/asset_frontend/chat/callrtc.js?version={{Helper::returnVersion()}}" type="text/javascript"></script>
</body>
