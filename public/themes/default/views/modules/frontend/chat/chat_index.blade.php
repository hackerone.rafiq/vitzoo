@extends('default::modules.frontend.chat.chat_layout')
@section('content')
    <div class="tab-content chatContainer clearfix message-append" id="chat-container">
        @include('default::modules.frontend.chat.chat_dashboard')
        @include('default::modules.frontend.chat.chat_addgroup')
        @include('default::modules.frontend.chat.chat_group')
        @include('default::modules.frontend.chat.chat_addfriend')
        @include('default::modules.frontend.chat.invite_friend')
    </div>
    <div id="cModalChangeAvatar" class="modal fade" role="dialog">
        <div class="modal-dialog centerDiv">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{{trans('fr_home.create_profile_picture')}}</h4>
                </div>
                <div class="modal-body">
                    <button style="margin: 10px 0; background-color: rgba(255, 255, 255, 0.1);" id="_pf-choose-image"><i
                                class="fa fa-picture-o"
                                aria-hidden="true"></i>{{trans('fr_home.choose_image')}}
                    </button>
                    <div class="image-editor">
                        <input type="file" class="cropit-image-input" style="display: none;" id="upload_avatar">
                        <div class="_pfcenter-parent" style="overflow: hidden;">
                            <div class="_pfcenter">
                                <div class="cropit-preview">
                                </div>
                            </div>
                        </div>
                        <div class="controls-wrapper">
                            <div class="rotation-btns">
                                <button class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                <button class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                            </div>
                            <div class="slider-wrapper">
                                <span class="icon icon-image small-image"><i class="fa fa-picture-o"
                                                                             aria-hidden="true"></i></span>
                                <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
                                <span class="icon icon-image large-image"><i class="fa fa-picture-o"
                                                                             aria-hidden="true"></i></span>
                            </div>
                        </div>

                    </div>
                    <div class="button-editor-img"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-popup-avatar" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default export">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(function () {
            $(document).on('click', '.change-group-picture', function (e) {
                $('#cModalChangeAvatar').modal({
                    backdrop: 'static',
                    keyboard: false
                }).data('roomid', this.dataset.roomid);
                $('.cropit-image-input').val('');
                $('.cropit-preview img').removeAttr('src');
            });

            $('#cModalChangeAvatar').on('mousewheel DOMMouseScroll', '.modal-body', function (e) {
                e = e.originalEvent;
                var input_range = $('.cropit-image-zoom-input.custom')[0];
                var step = +input_range.step;
                var delta = e.wheelDelta > 0 || e.detail < 0 ? step : -step;
                input_range.value = +input_range.value + delta;
            });

            var input = document.getElementById('upload_avatar');
            var fileName;
            $('#_pf-choose-image').on('click', function (e) {
                e.preventDefault();
                input.click();
            });

            $(".pf-btn-button-cancel").click(function () {
                $('.hovereffect').css('background-image', 'url(' + linkAvata + ')');
            });
            var editor = $('.image-editor');
            editor.cropit({
                imageBackground: true,
                smallImage: true,
                allowDragNDrop: true,
                width: '{{env('width_avatar')}}',
                height: '{{env('height_avatar')}}',
            });

            $('.cropit-image-input').change(function () {
                var file;
                if (file = this.files[0]) {
                    fileName = file.name;
                }
            });

            $('.rotate-cw').click(function () {
                $('.image-editor').cropit('rotateCW');
            });
            $('.rotate-ccw').click(function () {
                $('.image-editor').cropit('rotateCCW');
            });

            $('.export').click(function () {
                var self = this;
                var imageData = $('.image-editor').cropit('export');
                if (imageData) {
                    var file = dataURItoBlob(imageData);
                    var fd = new FormData();
//                fd.append('fname', fileName);
                    var popup = $(this).closest('#cModalChangeAvatar');
                    var room_id = popup.data('roomid');
                    fd.append('data', file, fileName);
                    fd.append('room_id', room_id);
                    var hovereffect = $('.hovereffect');
                    hovereffect.find('i.fa-users').remove();
                    $.ajax({
                        url: '{{Helper::url('chat/group_picture')}}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: fd,
                        processData: false,
                        contentType: false,
                        enctype: "multipart/form-data",
                        success: function (str) {

                        }
                    }).fail(function (err) {

                    }).always(function () {
                        $('#cModalChangeAvatar').modal('hide');
                    });
                } else {

                }
            });
        });
    </script>
@stop