<?php $arr = [];?>
@if(isset($message) && isset($timezone)&& count($message)>0)
    {{--@foreach($timeDistinct as $i=>$value)--}}
    <?php $count = count($message);
    if (empty($current)) $current = 0;
    ?>
    @for($x = $count - 1; $x >= 0; $x--)
        <?php $jvalue = $message[$x];?>
        @if($jvalue)
            <?php $timeString = strtotime($jvalue->created_at); $time = $timeString + (-60) * $timezone; $temp = date('dmY', $time); $test = $current !== $temp;
            $current = $temp;
            ?>
            @if($test)
                <article data-time="{{$current}}" class="clearfix article">
                    <div class="date-time" data-item="a1"
                         data-time="{{date('Y/m/d', $time)}}">
                        {{--<p>Thứ hai, ngày 04 tháng 04 năm 2016</p>--}}
                        <p>{{Helper::getDatetimeChat($time)}}</p>
                    </div>
                    <div class="row row-append">
                        {{--@foreach($message as $j=>$jvalue)--}}
                    </div>
                </article>
            @endif
            <?php
            if ($is_group) {
                if (isset($arr[$jvalue->user_id])) {
                    $user_join = $arr[$jvalue->user_id];
                } else {
                    $user_join = $jvalue->joinUser()->first();
                    $arr[$jvalue->user_id] = $user_join;
                }
            } else {
                if ($jvalue->user_id === $user->id) {
                    $user_join = $user;
                } else {
                    $user_join = $session;
                }
            }
            if ($user_join && $user_join->url) {
                $img = url('themes/default/asset_frontend/img_avatar/' . $user_join->url);
            }
            ?>
            @if($user_join )
                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" data-userid="{{$jvalue->user_id}}" data-isgroup="{{$is_group}}">
                    <div class="col-md-1 col-xs-1 col-st">
                        @if($user_join->id !== $session->id)
                            <div class="avatar">
                                @if($user_join->url)
                                    <img width="100%" class="user_avatar"
                                         src="{{$img}}"/>
                                @else
                                    <i class="fa fa-user"></i>
                                @endif
                            </div>
                        @endif
                    </div>
                    <?php $isCurrentUser = $user_join->id === $session->id; $fullname = $user_join->last_name . ' ' . $user_join->first_name; if(isset($user)) $fullnameReceiver = $user->last_name . ' ' . $user->first_name?>
                    <div class="col-md-11 col-xs-11 ct-chat @if(!$isCurrentUser) received-message @endif">
                        <div @if($isCurrentUser) style="float:right" @endif class="chat-box">
                            <div class="messages msg_sent">
                                <time datetime="2009-11-13T20:00">
                                    <span @if(!$isCurrentUser) class="message-name" style="cursor: pointer;" @endif >{{$fullname}}</span>
                                    {{Helper::getHourMinutes($timeString,$timezone)}}
                                </time>
                                <div class="msg">
                                    <?php $decrypted = Helper::decrypt($jvalue->content);
                                    ?>
                                    @if($jvalue->is_file === 1)
                                        <a href="{{url('themes/default/asset_frontend/img_file/'.Helper::stripVowelAccent($user_join->id.$user_join->first_name).'/'.$decrypted)}}"
                                           download> {{$decrypted}} </a>
                                    @elseif($jvalue->is_file === 3)
                                        <p><i class="fa fa fa-phone"
                                              style="color: blue; margin-right: 10px;"></i>{{trans('fr_home.call_end')}}</p>
                                    @elseif($jvalue->is_file === 2)
                                        <p><i class="fa fa fa-phone"
                                                                  style="
                                                                  color: red; margin-right: 10px;"></i>@if(!$isCurrentUser){{ucfirst(trans('fr_home.you')) .' '.trans('fr_home.miss_call').' '.$fullnameReceiver}} @else {{$fullnameReceiver.' '.trans('fr_home.miss_call').' '.trans('fr_home.you')}} @endif</p>
                                    @else
                                        {!! $decrypted !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    @endfor
@endif
