<div class="rows">
    <div class="col-conservation tab-content">
        <div class="col-widthd clearfix main-chat-content" id="main_{{$room_id}}">
            <header class="right-title clearfix">
                <div class="user-chat">
                    <div class="avatar">@if($user->url) <img
                                src="{{url('themes/default/asset_frontend/img_avatar/'.$user->url)}}"
                                alt="avatar" class="img-responsive center-block">@else<i class="fa fa-user"></i>@endif
                    </div>
                    <div class="active-user-info">
                        <div class="chat-us-tt clearfix">
                            <span class="color-text-ddd">{{$user->last_name.' '.$user->first_name}}</span>
                            {{--<i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true" style="display: none;"></i>--}}
                        </div>
                        <div class="clearfix">
                            <div class="user-mood-tt">
                                <i class="truncate" style="
    max-width: 500px;
    display: block;
" title="{{$status->mood}}">@if($status->mood){{$status->mood}}@else{{trans('fr_home.no_mood')}}@endif</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dropdown dot-menu">
                    <button class="btn btn-menu" type="button" onclick="openNav('{{$room_id}}')">
                        <i class="fa fa-info"></i>
                    </button>
                    {{--<ul class="dropdown-menu">
                        <li><a href="#" onclick="openNav()">Tập tin của bạn</a></li>
                        <!--onclick="return showattach();"-->
                        <li><a href="#">Tất cả</a></li>
                        <li><a href="#">Tải xuống</a></li>
                        <li><a href="#">Trợ giúp</a></li>
                    </ul>--}}
                </div>
            </header>
            <div id="box{{$room_id}}" data-room="{{$room_id}}"
                 class="chat-tab-container chat-tab-insertion nanos scrollview"
                 style="height: 0px;">
                <div class="content-chat clearfix nano-content">
                    {{--{!! $messages !!}--}}
                    @include('default::modules.frontend.chat.chat_messages')
                    <div style="display: none;" class="typing ontyping">
                        <p><span class="user_is_typing"><span class="group-typing"> {{trans('site.and_others')}}</span></span>
                            &nbsp;
                        <p class="is-typing">{{trans('site.is_typing')}}</p></p>
                        <div class="cssload-jumping">
                            <span></span><span></span><span></span><span></span><span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-chat">
                <div class="rows clearfix">
                    {!! Form::open(['url'=>Helper::url('chat/uploadFile'),'enctype'=>"multipart/form-data",'class'=>'upload_form', 'onsubmit' => 'event.preventDefault()']) !!}
                    <div class="chatbox" @if(isset($user) && isset($room_id)) data-receiver="{{$user->id}}"
                         data-roomid="{{$room_id}}" @endif>
                        <div class="input-group">
                                    <textarea
                                            @if(!empty($status_relative) && $status_relative['status'] === 'blocked') disabled
                                            @endif class="form-control emojis-wysiwyg textarea-scrollbar scrollbar-outer input-message"
                                            aria-label=""
                                            placeholder="{{trans('fr_home.enter_message')}}" data-emojiable="true"
                                            @if(isset($user) && isset($room_id)) data-receiver="{{$user->id}}"
                                            data-roomid="{{$room_id}}" @endif></textarea>
                            <div class="input-group-addon">

                                <a class="pseudo-file-upload"
                                   title="#">
                                    <i class="fa fa-paperclip"></i>
                                </a>
                                <a href="javascript:void(0)" title="#" class="chat-video"
                                   data-roomid="{{$room_id}}" data-receiver="{{$user->id}}"
                                   data-type="video"
                                   data-url="{{Helper::url('chat/video')}}">
                                    <i class="fa fa-video-camera"></i>
                                </a>
                                <a href="javascript:void(0)" title="#" class="chat-audio"
                                   data-roomid="{{$room_id}}" data-receiver="{{$user->id}}"
                                   data-type="audio"
                                   data-url="{{Helper::url('chat/video')}}">
                                    <i class="fa fa-phone"></i>
                                </a>

                                <a class="emoji-button" href="#" data-roomid="{{$room_id}}" title="Emojis">
                                    <i class="fa fa-smile-o"></i>
                                </a>
                                <a class="setting-button" href="#" data-roomid="{{$room_id}}" title="Settings">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="chatbutton">
                        <button type="submit" class="btn btn-dangers buttonSaveFile">{{trans('fr_home.send')}}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        @include('default::modules.frontend.chat.chat_information_user')
    </div>
</div>