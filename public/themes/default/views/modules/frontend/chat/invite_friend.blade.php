<div class="right-content tab-pane tabgroup" id="tabinvite_friend">
    <div class="rows">
        <div class="col-conservation tab-content">
            <div class="right-content col-widthd clearfix ctn-addfriend main-chat-content content-addgroup"
                 id="main_addgroup">
                <header class="right-title clearfix">
                    <div class="user-chat col-md-7">
                        <div class="user-chat-addfriend">
                            <div class="avatar">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h5>{{trans('fr_home.invite_friend')}}</h5>
                        </div>
                    </div>
                </header>
                <div class="chat-tab-container chat-invite-container">
                    <article>
                        <div class="area-copylink" style="margin-top: 10%;">
                            <div style="display: table;margin: auto;text-align: center;width: 100%;max-width: 70%;padding: 10px 20px;border-radius: 5px;">
                                <form role="search" class="stylish-input-group" onsubmit="event.preventDefault()">
                                    <div class="input-group">
                                        <input type="text" class="form-control content-invitefriend"
                                               @if(isset($session) && $session) value="{{Helper::url('invite/'.base64_encode($session->id))}}" @endif readonly style="background-color: rgba(255, 255, 255, .1);height: 50px;">
                                        <span class="input-group-addon">
                            <button type="button" class="button-copylink">
                                <a href="">{{trans('fr_home.copy_link')}}</a>
                            </button>
                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </article>
                </div>


            </div>
        </div>
    </div>
</div>

