<script type="text/javascript">
    var tnotice = 'Please enter no more than 20 characters!';
    tnotice = lang == 'vi' ? 'Vui lòng nhập nhỏ hơn 20 kí tự' : tnotice;
    function classAddUserSidebar() {
        return $('.member-addtogroup-sidebar');
    }

    function returnTab(room_id) {
        return $('#tab' + room_id);
    }

    var opened = false;
    function openNav(id) {
        if (id != 'addfriend') {
            /*openChatTab(id, true);*/
        }
        opened = true;
        var voltaic, main, tab;

        if (!id) {
            voltaic = document.getElementById("voltaic_holder_addgroup");
            main = document.getElementById("main_addgroup");
            tab = document.getElementById('tabaddgroup');
        } else {
            voltaic = document.getElementById("voltaic_holder_" + id);
            main = document.getElementById("main_" + id);
            tab = document.getElementById('tab' + id);
            var $main = $(main);
            $main.addClass("edit");
        }
        if (voltaic) {
            var width = voltaic.clientWidth;
            voltaic.style.transform = 'translateX(0)';
        }

        if (tab) {
            tab.style.width = 'calc(100% - ' + (width - 1) + 'px)';
        }
    }

    var calculation = (function () {
        var friend_tab_height, chat_tab_height, height1;
        var
                hd = $("#hd"),
                navTabs = $("#nav-tabs"),
                $height3 = hd.outerHeight(true),
                $height4 = navTabs.outerHeight(true);
        var friend_tab = $("#left-content-bar");
        var left_content = friend_tab.find('.nano-content');
        var mobile_menu = $('.menu-mobile');
        var self = {};
        friend_tab_height = 'calc(100vh - ' + ($height3 + $height4 + 3) + 'px)';
        friend_tab.css('height', friend_tab_height);

        window.friends = self.friends = function () {
            var content = $('.right-content.active');
            var rightTitle = content.find(".right-title"),
                    textChat = content.find(".text-chat"),
                    main_content = $(".main-chat-content"),
                    chat_tab = main_content.find(".chat-tab-container");
            var $height1 = rightTitle.outerHeight(true),
                    $height2 = textChat.outerHeight(true),
                    $height3 = hd.outerHeight(true),
                    $height4 = navTabs.outerHeight(true),
                    $viewportHeight = window.innerHeight;
            var mobile_m_height = 0;
            if (window.innerWidth <= 894) mobile_m_height = mobile_menu.outerHeight(true);
            friend_tab_height = 'calc(100vh - ' + ($height3 + $height4 + 30) + 'px)';
            chat_tab_height = 'calc(100vh - ' + ($height1 + $height2 + mobile_m_height + 2) + 'px)';
            height1 = $viewportHeight - $height4;
            main_content.css('min-height', chat_tab_height);
            chat_tab.css('height', chat_tab_height);
            left_content.css('height', friend_tab_height);
            $(".nanoe").css('height', height1);
        };
        openNav();
        return self;
    })();
    window.addEventListener('resize', calculation.friends);
    window.addEventListener('load', calculation.friends);
    function showattach() {
        var right_content = $('#show-attachfile .right-content');
        right_content.removeClass('col-width');
        right_content.addClass('col-widths');
        $("#voltaic_holder").css("display", "block");
    }

    function showfriend() {
        $("#showfriend").css("display", "block");
    }
    $(document).ready(function () {
        var searchFriend = $('#search-friend-tabgroup');
        var searchDasbord = $('#search-friend-tabdashboard');
        searchFriend.length && searchFriend.val('');
        searchDasbord.length && searchDasbord.val('');
        /*js dropdown menu chat*/
        $(".chat-drop").click(function (e) {
            e.preventDefault();
            $(".chat-dropdown-menu").slideDown(350, 'linear', friends);
            $(".chat-exit").css("display", "block", "important");
            $(".chat-drop").css("display", "none");
            $('.notifi-box').slideUp('slow');
        });

        $(".chat-exit").click(function (e) {
            e.preventDefault();
            $(".chat-dropdown-menu").slideUp(350, friends);
            $(".chat-exit").css("display", "none");
            $(".chat-drop").css("display", "block");
        });

        $(".chat-dropdown-menu li").click(function (e) {
            e.preventDefault();
            $(".chat-dropdown-menu").slideUp("350", friends);
            $(".chat-exit").css("display", "none");
            $(".chat-drop").css("display", "block");
        });
        //js when click outside slideUp
        $(document).click(function (e) {
            showImage('', false);
            window_is_active = true;
            var dropdown = $('.chat-dropdown-menu');
            var target = $(e.target);
            if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0) {
                dropdown.slideUp("slow", friends);
                $(".chat-exit").css("display", "none");
                $(".chat-drop").css("display", "block");
            }
            var menu = $('.add-menu');
            if (!menu.is(e.target) && menu.has(e.target).length === 0) {
                $(".add-menu-notifications").slideUp("slow", friends);
                $('.notifi-box').hide("slow", friends);
            }
            var emoji = $('.emoji-menu');
            var emoji_button = $('.emoji-button');
            if (!emoji.is(e.target) && !emoji.has(e.target).length && !emoji_button.is(e.target) && !emoji_button.has(e.target).length) {
                emoji.hide('slow');
            }
            var mood = $('.user-mood');
            var inputGroup = $('.right-inner-addon');
            if (!mood.is(e.target) && !mood.has(e.target).length && !$(e.target).hasClass('mood-change') && !$(e.target).is('i')) {
                var input = mood.find('input.mood-change');
                if (input.length) {
                    changeMood(mood, input);
                }
            }
            if (!inputGroup.is(e.target) && !inputGroup.has(e.target).length && inputGroup.hasClass('editing')) {
                inputGroup.removeClass('editing');
                blockNoneInputNameGroup('edited');
                var $input = inputGroup.find('input');
                var text = $input.val();
                var $roomid = $input.data('roomid');
                if (text.length > 20) {
                    var modal = $('#errorModal');
                    modal.find('p').text(tnotice);
                    modal.modal('show');
                }
                if (text && text.length <= 20) {
                    var action = 'changeTitle';
                    if ($roomid == undefined || $roomid == "") {
                        action = 'friendAndGroup';
                        $roomid = 0;
                    }
                    ajax_addGroup({
                        action: action,
                        room_id: $roomid,
                        friend_id: 0,
                        title: text
                    }, '{{Helper::url('chat/addgroup')}}');
                }
            }
            if (!(target.is('.basic-settings') || target.closest('.basic-settings').length) && !(target.is('.setting-button') || target.closest('.setting-button').length)) {
                $(('.basic-settings.fullScale')).css({
                    transform: 'scale(0)'
                }).removeClass('fullScale')
            }
        });

        $("#showfriends").click(function () {
            $("#showfriend").toggle('fast');
        });
        $("#showgroups").click(function () {
            $("#showgroup").toggle('fast');
        });

        var copylink = document.querySelector('.button-copylink');
        if (copylink)
            copylink.addEventListener('click', function (e) {
                e.preventDefault();
                document.execCommand('copy', false, document.querySelector('.content-invitefriend').select());
            });

        $('.add-group-chat').click(function () {
            //todo:getfriend after append into addgroup
            /*var url = $(this).find('a').data('href');*/
            var tab = $("#tabaddgroup");
            tab.find('.input-addnamegroup').data('roomid', '');
            tab.find(classAddUserSidebar()).find('ul').empty();
            tab.find('.input-addnamegroup').val('');
            tab.find('.span-addnamegroup').html('{{trans('fr_home.name_group')}}');
            history.pushState(null, null, '/' + lang + '/chat/');
            var url = fullUrl;
            url = url + '/chat/searchUser';
            ajax_loadFriend(url, 'addgroup', 'addgroup');

            active_tab(tab);
            openNav();
            sortByName.searchClient('search-friend-tabgroup', '.user-afriend');
        });

        $('.logout-chat').click(function () {
            window.location.href = $(this).find('a').attr('href');
        });

        $(document).on('click', '.message-name', function () {

            var pa = $(this).closest('.msg_container');
            var user_id = pa.data('userid');
            if(!pa.length){
                pa = $(this).parent();
                user_id = pa.data('id');
            }
            if(user_id){
                var friend = $('li.new-friend[data-id="'+user_id+'"]');
                if(friend.length)
                    friend.click();
                else{
                    create_recent(user_id);

                }
            }


        });

        $(document).on('click', '.add-menu-button', function () {
            var url = $(this).data('url');
            var room_id = $(this).closest('.tabgroup').data('roomid');
            if (url && room_id)
                if (sessionStorage['addtogroup' + room_id] != 1)
                    ajaxFriendNotification(url, 'friend', room_id);
                else {
                    $('.add-menu-notifications').toggle('slow');
                }
            sortByName.searchClient('search-box-' + room_id + '', '#main_' + room_id + ' .addli-togroup');
        });

        $(document).on('click', '.btn-notifi', function (e) {
            e.preventDefault();
            $('.notifi-box').toggle('slow');

        });

        $(document).on('click', '.emoji-button', function (e) {
            e.preventDefault();
            var offset = this.getBoundingClientRect(),
                    top = offset.top,
                    left = offset.left;
            var width = window.innerWidth,
                    height = window.innerHeight;
            $('.emoji-menu').css({
                bottom: (height - top + 20 ) + 'px',
                right: (width - left - 10 ) + 'px',
            }).toggle(function () {
                setTimeout(function () {
                    emo_items_wrap.nanoScroller();
                }, 100);
            }).data('room_id', $(this).data('roomid'));
        });

        $(document).on('click', '.setting-button', function (e) {
            e.preventDefault();
            var offset = this.getBoundingClientRect(),
                    top = offset.top,
                    left = offset.left;
            var width = window.innerWidth,
                    height = window.innerHeight;
            var room_id = $(this).data('roomid');
            var soundLevel;
            if (!room_id) {
                var body = $(document.body);
                soundLevel = +localStorage.baseVolume;
            }
            else {
                var chat_tab = $('#main_' + room_id);
                if (localStorage['sound' + room_id] === undefined) {
                    localStorage['sound' + room_id] = 100;
                }
                soundLevel = +localStorage['sound' + room_id];
            }
            var settings;
            if ($(this).data('settings') !== 1) {
                var iClass = 'fa-volume-off';
                if (soundLevel > 50) {
                    iClass = 'fa-volume-up';
                } else if (soundLevel > 0) {
                    iClass = 'fa-volume-down';
                }
                var data = room_id ? 'data-roomid="' + room_id + '" ' : 'id="basic-settings"';
                var settingElement = '<div ' + data + ' class="basic-settings">' +
                        '<ul class="clearfix">' +
                        '<li><label for="volume"></label>' +
                        '<input type="range" name="volume" class="volume-level" value="' + soundLevel + '">' +
                        '<i class="mute fa ' + iClass + '" aria-hidden="true">' +
                        '</i></li>' +
                        '</ul>' +
                        '</div>';
                if (chat_tab) {
                    chat_tab.append(settingElement);
                    $(this).data('settings', 1);
                    settings = chat_tab.find('.basic-settings').css({
                        transform: 'scale(0)'
                    });
                } else {
                    body.append(settingElement);
                    settings = body.find('#basic-settings').css({
                        transform: 'scale(0)'
                    });
                }
            } else {
                if (chat_tab) {
                    settings = chat_tab.find('.basic-settings');
                } else {
                    settings = $('#basic-settings');
                }
            }
            setTimeout(function () {
                if (settings.hasClass('fullScale')) {
                    settings.css({
                        transform: 'scale(0)'
                    }).removeClass('fullScale')
                } else {
                    settings.css({
                        bottom: (height - top + 20 ) + 'px',
                        right: (width - left - 10 ) + 'px',
                        transform: 'scale(1)'
                    }).addClass('fullScale');
                }
            }, 10);
        });

        $(document).on('input', '.volume-level', function (e) {
            var target = $(this);
            var i = target.next();
            var value = target.val();
            var wrapper = target.closest('.basic-settings');
            var room_id = wrapper.data('roomid');
            soundIcon(value, i);
            if (room_id) {
                localStorage['sound' + room_id] = value;
            } else {
                localStorage.baseVolume = value;
            }
        });

        $(document).on('click', '.mute', function () {
            var target = $(this);
            var input = target.prev();
            var volume = input.val();
            var wrapper = target.closest('.basic-settings');
            var room_id = wrapper.data('roomid');
            if (volume > 0) {
                target.data('volume', volume);
                input.val(0);
            } else {
                input.val(target.data('volume'));
                target.data('volume', 0);
            }
            volume = input.val();
            soundIcon(volume, target);
            if (room_id) {
                localStorage['sound' + room_id] = input.val();
            } else {
                localStorage.baseVolume = input.val();
            }
        });

        $(document).on('click', '.right-inner-addon:not(.editing)', function (e) {
            $this = $(this);
            $this.addClass('editing');
            blockNoneInputNameGroup('editing');
        });

        $(document).on('click', '.emoji-menu-tab:not(.selected)', function (e) {
            e.preventDefault();
            var tab = this;
            var target = +this.dataset.target;
            var tabs = $('.emoji-menu-tab');
            $(tabs).removeClass('selected');
            $(this).addClass('selected');
            $('.emoji-category').hide();
            var category = $('.emoji-category[data-category="' + target + '"]');
            if (!target) {
                category.html(createRecentIcons());
            }
            category.show(function () {
                setTimeout(function () {
                    emo_items_wrap.nanoScroller();
                }, 100);
            });
        });

        $(document).on('click', '.select-emoji', function (e) {
            e.preventDefault();
            var emote = this.title;
            var textarea = $('#main_' + $(this).closest('.emoji-menu').data('room_id')).find('textarea');
            if (textarea.prop('disabled')) return;
            textarea.val(textarea.val() + ' ' + emote);
            textarea.trigger('focus');
            if (!e.shiftKey) {
                $('.emoji-menu').hide('slow');
                $('.emoji-category[data-category="0"]').html(createRecentIcons(emote));
            } else {
                createRecentIcons(emote, true);
            }
        });
        $('.user-mood').click(function (e) {
            var mood_wrapper = $(this);
            var input = $(this).find('input.mood-change');
            var text, mood;
            if (!input.length) {
                mood = mood_wrapper.find('i');
                if (mood.data('mood') === 1) {
                    text = mood.html();
                } else {
                    text = '';
                }
                mood.remove();
                $('<input>', {
                    'type': 'text',
                    'value': text,
                    'class': 'mood-change form-control tx-general tx-hide tx-show'
                }).appendTo(mood_wrapper);
            }
        });
        $(document).on('keyup', '.mood-change', function (e) {
            if (e.key === 'Enter') {
                var mood_wrapper = $('.user-mood');
                var input = $(this);
                changeMood(mood_wrapper, input);
            }
        });
    });

    function active_tab(element) {
        $('#chat-container').find('.tab-pane').removeClass('active');
        element.addClass('active');
        element.find('textarea').trigger('focus');
    }

    $('#introModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var titleId = button.data('title'); // Extract info from data-pro_id attributes
        var modal = $(this);
        modal.find('#titlemodal').html(titleId);
    });

    /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
    function closeNav(id) {
        opened = false;
        var voltaic, main, tab;
        if (!id) {
            voltaic = document.getElementById("voltaic_holder_addgroup");
            main = document.getElementById("main_addgroup");
            tab = document.getElementById('tabaddgroup');
        } else {
            voltaic = document.getElementById("voltaic_holder_" + id);
            main = document.getElementById("main_" + id);
            tab = document.getElementById('tab' + id);
        }
        if (voltaic)
            voltaic.style.transform = '';
        if (tab) {
            tab.style.width = '';
        }
        $(main).removeClass("edit");
    }
    $(document).ready(function () {

        /*js ajax*/
        $('.i-contact-profile').on('click', function () {
            var id = $(this).data('id');
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{Helper::url('chat/getUserById')}}',
                type: 'POST',
                dataType: 'JSON',
                data: {id: id, _token: token},
                beforeSend: function () {
                    $('#add-friend').find('.add-friend-address').html('');
                },
                success: function (str) {
                    $add = $('#add-friend');
                    $add.find('h4').html(str.last_name + ' ' + str.first_name);
                    if (str.address)
                        $add.find('.add-friend-address').html(str.address);
                    $add.find('.add-friend-chat').data('id', str.id);
                    $add.modal('show');

                }
            });
        });

        $(document).on('click', '.group-addfriend .fa-user-plus', function (e) {
            var url = fullUrl + '/chat/addRemoveFriend';
            var id = $(this).closest('.active-user-info').data('id');
            var self = $(this);
            var action = 'addfriend';
            var data = {id: id, action: action};
            addFriend(data, url, self);
            var classRemove = $('.cont-addfi').find('.active-user-info[data-id="' + id + '"] .fa-user-plus');
            classRemove.parent().prepend('<i class="fa fa-spinner" aria-hidden="true" title="{{trans('fr_home.waiting_to_accept')}}"></i>');
            classRemove.remove();
            /**/
        });

        $(document).on('click', '.ajax-relative', function (e) {
            var parent = $(this.parentElement);
            var id = parent.data('id');
            var url = parent.data('url');
            var action = $(this).data('action');
            var type = parent.data('type');
            var self = $(this);
            var data = {id: id, action: action};
            addFriend(data, url, self);

            var notice_button = $('.notice-btn-control[data-id="' + id + '"]');
            notice_button.length && notice_button.find('button').removeClass('ajax-relative');
            if (action == 'confirm') {
                var btn_confirm = notice_button.find('.btn-confirm');
                btn_confirm.length && btn_confirm.click();
            } else if (action == 'delete-request') {
                var btn_delete = notice_button.find('.btn-delete-request');
                btn_delete.length && btn_delete.click();
            }
        });

        $(document).on('click', '.btn-listmember-group', function () {
            var room_id = $(this).closest('.tabgroup').data('roomid');
            setTimeout(function () {
                searchByName.calculatorNanoGroup(room_id);
                searchByName.nanoScrollerFunction($('.scroll-tab-group'));
            }, 1000);
        });

        $(document).on('click', '.icon-removegroup', function () {
            var $info = $(this).siblings('.active-user-info');
            var friend_id = $info.data('id');
            var main_tab = $(this).closest('.main-tab');
            var $roomid = main_tab.data('roomid');
            if (friend_id && $roomid) {
                var action = 'deleteUser';
                ajax_addGroup({
                    action: action,
                    friend_id: friend_id,
                    room_id: $roomid,
                    title: 'no title'
                }, '{{Helper::url('chat/addgroup')}}');
                var tab = returnTab($roomid);
                var ul = tab.find('.add-menu-notifications ul');
                if (ul.length) {
                    var name = $info.find('span').text();
                    searchByName.nanoScrollerFunction($('.scroll-tab-group'));
                }
                ul.append(appendUserNotifiBox({name: name, id: friend_id}));
            }
        });

        $(document).on('click', '.icon-addgroup', function () {
            var $info = $(this).siblings('.active-user-info');
            var friend_id = $info.data('id');
            var $roomid = $('.input-addnamegroup').data('roomid');
            var name = $info.find('.color-text-ddd').text();
            if (friend_id) {
                var action = 'friend';
                if ($roomid == undefined || $roomid == "") {
                    action = 'friendAndGroup';
                    $roomid = 0;
                }
                ajax_addGroup({
                    action: action,
                    friend_id: friend_id,
                    room_id: $roomid,
                    title: 'no title',
                    name: name
                }, '{{Helper::url('chat/addgroup')}}');
                searchByName.nanoScrollerFunction($('.scroll-tab-group'));
            }
        });

        $(document).on('click', '.icon-addgroup-box', function () {

            var $info = $(this).siblings('.active-user-info');
            var friend_id = $info.data('id');
            var $roomid = $(this).closest('.tabgroup').data('roomid');
            var name = $info.find('span').text();
            var self = $(this);
            if (friend_id) {
                var action = 'friend';
                var data = {
                    action: action,
                    friend_id: friend_id,
                    room_id: $roomid,
                    title: 'no title',
                    name: name
                };
                if (!self.hasClass('disable'))
                    $.ajax({
                        url: '{{Helper::url('chat/addgroup')}}',
                        type: 'POST',
                        dataType: 'JSON',
                        data: data,
                        beforeSend: function () {
                            self.addClass('disable');
                        },
                    }).done(function (str) {
                        self.removeClass('disable');
                        if (str.success) {
                            var room_id = str.data.room_id;
                            var tab = returnTab(room_id);
                            var friend_id = str.data.friend_id;
                            var title = str.data.title;

                            var child = tab.find('.active-user-info[data-id="' + friend_id + '"]').closest('.addli-togroup');
                            child && child.remove();
                            var main = tab.find('.main-tab .cont-addfi');
                            var string = appendUserRightSidebar({friend_id: friend_id, name: str.data.name});
                            main.find('ul').append(string);
                            searchByName.nanoScrollerFunction($('.scroll-tab-group'));
                            socket.emit('createGroup', {
                                room_id: room_id,
                                action: action,
                                friend_id: friend_id,
                                title: title
                            });
                        }
                    }).fail(function (err) {

                    });
            }
        });

        $(document).on('keypress', '.input-addnamegroup', function (event) {

            if (event.key == "Enter") {
                var text = $(this).val();
                var $roomid = $(this).data('roomid');
                if (text.length > 20) {
                    var modal = $('#errorModal');
                    modal.find('p').text(tnotice);
                    modal.modal('show');
                    $(this).val('');
                }
                if (text && text.length <= 20) {
                    var action = 'changeTitle';
                    if ($roomid == undefined || $roomid == "") {
                        action = 'friendAndGroup';
                        $roomid = 0;
                    }
                    if (!$(this).hasClass('disable'))
                        ajax_addGroup.call(this, {
                            action: action,
                            room_id: $roomid,
                            friend_id: 0,
                            title: text
                        }, '{{Helper::url('chat/addgroup')}}');
                }
            }
        });
        sessionStorage.clear();

        $(document).on('click', 'ul.list-user li.new-friend', function (e) {
            e.preventDefault();
            var a = $(this.firstElementChild);
            $('ul.list-user li').removeClass('active');
            window.currentUserName = $(this).find('.user_name').html();
            history.pushState({id: this.id}, null, '/' + lang + '/chat/room/' + this.id);
//            var popStateEvent = new PopStateEvent('popstate', {state : {id: this.id}});
//            dispatchEvent(popStateEvent);
            var url = a.data('url');
            var id = a.data('id');
            var action = a.data('action');
            var room_id = $(this).data('roomid');
            var $tab_room = $("#tab" + room_id);
            var chat_content = $tab_room.find('.content-chat');
            $tab_room.find('.nanos').first().nanoScroller({scroll: 'bottom'});
            if (chat_content.length) chat_content[0].scrollTop = chat_content[0].scrollHeight;
            id = !id ? 0 : id;
            $(this).addClass('active');
            var data = {
                url: url,
                id: id,
                action: action,
                room_id: room_id,
                $tab_room: $tab_room,
                status: 'socket_join'
            };
            loadAjaxTabMessage(data);
        });

        $(document).on('click', 'li.read-more:not(.disabled)', function (e) {
            e.preventDefault();
            var page = +this.dataset.page;
            var max = +this.dataset.max;
            var that = this;
            var self = $(this);
            var parent = self.parent();
            $.ajax({
                method: 'POST',
                url: self.data('href'),
                data: {page: page, max: max},
                beforeSend: function () {
                    self.addClass('disabled read-more-loading').addClass('read-more-loading');
                }
            }).always(function () {
                self.removeClass('disabled read-more-loading');
            }).done(function (e) {
                if (e.success) {
                    that.dataset.page = page++;
                    self.remove();
                    parent.append(e.message);
                }
            })
        });

        $(document).on('mouseenter', '.user_avatar', function (e) {
            var self = $(this);
            var that = this;
            self.data('hovered', true);
            var i = setTimeout(function () {
                self.data('timeout', 0);
                showImage(that);
            }, 3500);
            self.data('timeout', i);
        }).on('mouseleave', '.user_avatar', function (e) {
            var self = $(this);
            var that = this;
            self.data('hovered', false);
            var timeout = self.data('timeout');
            showImage(that, false);
            if (timeout) {
                clearTimeout(timeout);
                self.data('timeout', 0);
            }
        });

        $(document).on('click', '.right-content.active', function () {
            var room_id = $(this).data('roomid') || $(this).find('.chat-tab-container').data('room');
            var liactive = $('#recent-active-room').find('li[data-roomid="' + room_id + '"]').find('.count-message');
            if (liactive.length)
                countNotifiMessage.socketIsRead(room_id, 'message');
        });

        $(document).on('focus', 'textarea.input-message', function () {
            var room_id = $(this).data('roomid');
            countNotifiMessage.socketIsRead(room_id, 'message');
        });

        window.onpopstate = function (e) {
            var data = e.state;
            if (!data) return;
            var id = data.id;
            var chat_tab = $('#' + id);
            if (chat_tab.length) {
                chat_tab.trigger('click');
            }
        };

        $(window).on('load', function () {
            var ul = document.getElementById('nav-tabs');
            if (ul) {
                var activeTab = function (nano, timeOut) {
                    setTimeout(function () {
                        $(nano).nanoScroller();
                    }, timeOut);
                };
                $(ul).on('click', 'li', function () {
                    activeTab(this.dataset.target, 0);
                });
                var url_arr = location.href.split('/');
                var hash = url_arr[url_arr.length - 1];
                var hashTag = hash.indexOf('#');
                if (hashTag > 10) {
                    hash = hash.substring(0, hashTag);
                }
                var query = '';
                    if (hash.indexOf('f-') === 0) {
                    activeTab('.nano', 600);
                    query = '[href="#friend"]';
                } else if (hash.indexOf('g-') === 0) {
                    activeTab('.nano1', 600);
                    query = '[href="#group"]';
                } else if (hash.indexOf('rg-') === 0 || hash.indexOf('rf-') === 0) {
                    activeTab('.nano2', 600);
                    query = '[href="#recent"]';
                } else {
                    activeTab('.nano2', 600);
                }
                query && ul.querySelector('a' + query).click();
                var chat_tab = $('#' + hash);
                if (chat_tab.length) {
                    chat_tab.trigger('click');
                }
            }
            /*appendCountTitle();*/
        });
    });

    function chatTabInserted(e) {
        if (e.animationName == "nodeInserted") {
            var str = this;
            var target = e.target;
            var tab = $('#' + e.target.id);
            tab.next().find('textarea').trigger('focus');
            setTimeout(function () {
                calculation.friends();
                tab.nanoScroller({scroll: 'bottom'});
                tab.bind("scrolltop", scrollUpLoadMessages);
                tab.bind("scrollend", function () {
                    this.dataset.bottom = 'true';
                });
            }, 0);
            target.dataset.load = str.flag;
            target.dataset.page = str.data.page;
            target.dataset.id = str.data.id;
            tab.removeClass('chat-tab-insertion');
            document.removeEventListener("animationend", str.clickListenerBind, false);
            document.removeEventListener("webkitAnimationEnd", str.clickListenerBind, false);
        }
    }

    function loadAjaxTabMessage(data) {
        var url = data.url;
        var id = data.id;
        var first = 50;
        var action = data.action;
        var room_id = data.room_id;
        var $tab_room = data.$tab_room;
        var date = new Date();
        var timezone = date.getTimezoneOffset();
        var status = data.status;
        var tabpan = $('#chat-container');

        if (!$tab_room.length) {
            if (action === 'one-to-many') {
                tabpan.find('.tab-pane').removeClass('active');
                tabpan.append('<div class="right-content active tab-pane tabgroup" id="tab' + room_id + '" data-roomid="' + room_id + '"></div>');
            }
            else if (status !== 'no_socket_join') {
                tabpan.find('.tab-pane').removeClass('active');
                tabpan.append('<div class="right-content active tab-pane" id="tab' + room_id + '"></div>');
            }
            else {
                tabpan.append('<div class="right-content tab-pane" id="tab' + room_id + '"></div>');
            }
        } else if (status != 'no_socket_join') {
            active_tab($tab_room);
        }

        if (sessionStorage['load' + room_id] !== '1') {
            sessionStorage['load' + room_id] = '1';
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'JSON',
                data: {
                    id: id,
                    first: first,
                    action: action,
                    timezone: timezone,
                    status: status,
                    room_id: room_id
                },
                beforeSend: function () {
                    if (room_id != roomActive) {
                        var spinner = '<div class="spin-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
                        $tab_room.html(spinner);
                    }
                }
            }).done(function (str) {
                if (str.success) {
                    var tabt = $("#tab" + str.data.room_id);
                    var tab = tabt.html(str.data.html_message).css({
                        display: '',
                        height: '100%'
                    });
                    lastest_messages.set(str.data.room_id, 0);
                    var parent = tab.parent();
                    parent.find('.textarea-scrollbar').scrollbar();
                    str.clickListenerBind = chatTabInserted.bind(str);
                    document.addEventListener("animationend", str.clickListenerBind, false);
                    document.addEventListener("webkitAnimationEnd", str.clickListenerBind, false);
                    setTimeout(function () {
                        var height = document.querySelector(".content-chat");
                        height.scrollTop = height.scrollHeight;
                    }, 100);
                    sessionStorage['load' + room_id] = '1';
                    if (messageInActive) {
                        var tab_active = tabt.find('textarea.input-message');
                        tab_active.val(messageInActive);
                        messageInActive = '';
                    }

                } else {
                    sessionStorage['load' + room_id] = '0';
                    var error_message = '<div class="error load-message-error"><span>There was an error when we try to load the page, please try again.</span></div>';
                    $tab_room.html(error_message).css({
//                        display: 'flex',
                        'align-items': 'center',
                        'justify-content': 'center',
                        height: '100%'
                    });
                }

            }).fail(function (e) {
                sessionStorage['load' + room_id] = '0';
                var error_message = '<div class="error load-message-error"><span>There was an error when we try to load the page, please try again.</span></div>';
                $("#tab" + room_id).html(error_message).css({
//                    display: 'flex',
                    'align-items': 'center',
                    'justify-content': 'center',
                    height: '100%'
                });
            })
        }
    }

    function scrollUpLoadMessages(e) {
        if (this.dataset.load === 'false') return;
        this.dataset.bottom = 'false';
        var self = this;
        this.dataset.load = 'false';
        var page = +this.dataset.page;
        var content = self.firstElementChild;
        var firstChild = content.firstElementChild;
        var current = firstChild.dataset.time;
        var date = new Date();
        var timezone = date.getTimezoneOffset();
        var data = {
            timezone: timezone,
            id: this.dataset.id,
            page: page,
            current: current,
            room_id: this.dataset.room
        };
        $.ajax({
            url: '/{{LaravelLocalization::getCurrentLocale()}}/chat/olderMessages',
            type: 'POST',
            dataType: 'JSON',
            data: data
        }).done(function (str) {
            if (str.success) {
                self.dataset.load = str.flag;
                self.dataset.page = ++page;
                var len = str.data.length;
                var i = 0;
                while (i !== len) {
                    var message = str.data[i++];
                    var time = new Date(message.created_at).getTime();
                    time = convertToLocalTime(time);
                    var test = toDMY(time);
                    var article = content.querySelector('[data-time="' + test);
                    if (!article) {
                        article = insertArticle(time, message.format);
                        content.insertBefore(article, content.firstElementChild);
                    }
                    article.insertAdjacentHTML('afterend', message.content);
                }
                $('#' + self.id).nanoScroller({scrollTo: $(firstChild)});
            }
            else {
                self.dataset.load = 'true';
            }
        }).fail(function (x, e) {
            self.dataset.load = 'true';
        });
    }

    function takeContentButtonRelative(status) {
        var st = '';
        if (status == 'unfriended_blocked' || status == 'unfriended' || status == 'stranger') {
            st = '<button type="button" style="" class="btn btn-danger btn-un-frd ajax-relative" data-action="addfriend">';
            st += '<i class="fa fa-user-plus" aria-hidden="true"></i>{{trans('fr_home.addfriend')}}';
            st += '</button>';
            st += '<button type="button" class="btn btn-danger btn-bllock ajax-relative" data-action="block">';
            st += '<i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}';
            st += '</button>';
        }
        else if (status == 'waiting') {
            st = '<button type="button" class="btn btn-danger btn-add-frd ajax-relative" data-action="cancel-request">';
            st += '<i class="fa fa-user-times" aria-hidden="true"></i>{{trans('fr_home.cancel_request')}}';
            st += '</button>';
            st += '<button type="button" class="btn btn-danger btn-bllock ajax-relative" data-action="block">';
            st += '<i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}';
            st += '</button>';
        }
        else if (status == 'confirm') {
            st = '<button type="button" class="btn btn-danger btn-add-frd ajax-relative" data-action="confirm">';
            st += '<i class="fa fa-user-times" aria-hidden="true"></i>{{trans('fr_home.confirm')}}';
            st += '</button>';
            st += '<button type="button" style="" class="btn btn-danger btn-un-frd ajax-relative" data-action = "delete-request" >';
            st += '<i class="fa fa-user-plus" aria-hidden="true"></i>{{trans('fr_home.delete_request')}}';
            st += '</button>';
            st += '<button type="button" class="btn btn-danger btn-bllock ajax-relative" data-action="block">';
            st += '<i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}';
            st += '</button>';
        }
        else if (status == 'friend') {
            st = '<button type="button" class="btn btn-danger btn-add-frd ajax-relative" data-action="unfriend">';
            st += '<i class="fa fa-user-times" aria-hidden="true"></i>{{trans('fr_home.unfriend')}}';
            st += '</button>';
            st += '<button type="button" class="btn btn-danger btn-bllock ajax-relative" data-action="block">';
            st += '<i class="fa fa-lock" aria-hidden="true"></i>{{trans('fr_home.block')}}';
            st += '</button>';
        }
        else if (status == 'blocked') {
            st = '<button type="button" style="" class="btn btn-danger btn-unlock ajax-relative" data-action = "unblock" >';
            st += '<i class="fa fa-unlock-alt" aria-hidden="true"></i>{{trans('fr_home.unblock')}}';
            st += '</button>';
        }
        return st;
    }

    function convertToLocalTime(date, type) {
        date = new Date(date);
        var local_date = new Date();
        var offset = date.getTimezoneOffset() * 60000;
        if (type == 'notice')
            offset = 0;
        var localOffset = local_date.getTimezoneOffset() * 60000;
        var localTime = date.getTime() + offset - localOffset;
        date = new Date(localTime);
        return date;
    }

    function ajax_loadFriend(url, type, room_id) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {type: type, name: ''},
            beforeSend: function () {
                var spinner = '<div class="spin-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
                $("#tab" + room_id).find('.append-all-friend div').html(spinner);
            },
        }).done(function (str) {
            if (str.data)
                $('.append-all-friend').find('div').html(str.data.htmlUser);
        }).fail(function (err) {

        })
    }
    /*apply, call, bind
     * func.apply('x', [a,c])
     * func.call('x', a,c)
     * */

    function ajax_addGroup(data, url) {
        var self = false;
        if ($(this).is('input.input-addnamegroup')) {
            self = $(this);
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            beforeSend: function () {
                self && self.addClass('disable');
            },
        }).done(function (str) {
            self && self.removeClass('disable');
            if (str.success) {
                var room_id = str.data.room_id;
                var title = str.data.title;
                var friend_id = str.data.friend_id;
                var action = str.data.action;

                $('.input-addnamegroup').data('roomid', room_id);
                $('.scroll-tab-group').data('roomid', room_id);
                if (action == 'friendAndGroup' || action == 'changeTitle') {
                    $('.right-inner-addon').removeClass('editing');
                    blockNoneInputNameGroup('edited');
                    var tab = returnTabRoom({room_id: room_id});
                    $('.span-addnamegroup').text(title);
                    tab.find('.right-inner-addon span').text(title);
                    var tabUser = returnTabUser(room_id);
                    if (tabUser.length) {
                        tabUser.find('.user_name').text(title);
                    }
                    else {
                        $('#group').find('ul').append(appendGroupForTabUser({room_id: room_id, title: title}));
                        sortByName.sort_group_name();
                    }
                    var main = returnMainChat(room_id);
                    if (main.length)
                        main.find('h5').text(title);

                }

                if (friend_id != '0') {
                    var st = appendUserRightSidebar({friend_id: friend_id, name: str.data.name});
                    var classAdd = classAddUserSidebar();
                    classAdd.find('ul').append(st);
                    var child = $('.append-all-friend .active-user-info[data-id="' + friend_id + '"]').siblings('.icon-addfriend');
                    var childSideBar = $('.member-addtogroup-sidebar .active-user-info[data-id="' + friend_id + '"]').siblings('.icon-addfriend');

                    if (action != 'deleteUser') {
                        child.removeClass('icon-addgroup');
                        child.addBack('icon-addedgroup');
                        child.html('<i class="fa fa-check" aria-hidden="true"></i>');
                        socket.emit('createGroup', {
                            room_id: room_id,
                            action: action,
                            friend_id: friend_id,
                            title: title
                        });
                    }
                    else {
                        var temp;
                        if ((temp = $('#voltaic_holder_' + room_id).find('ul .active-user-info[data-id="' + friend_id + '"]').closest('li')).length) {
                            temp.remove();
                        } else {
                            child.addClass('icon-addgroup');
                            child.removeClass('icon-addedgroup');
                            child.html('<i class="fa fa-user-plus" aria-hidden="true"></i>');
                            childSideBar.closest('li').remove();
                        }
                        socket.emit('deleteGroup', {room_id: room_id, type: 'deleteUserGroup', friend_id: friend_id});
                    }
                }
            }
        }).fail(function (err) {

        })
    }

    function appendUserRightSidebar(data) {
        var check = $('.list-user li.active').data('isadmin');
        var st = '<li class="clearfix">' +
                '<div class="user-chat col-md-12">' +
                '<div class="active-user-info" data-id=' + data['friend_id'] + '>' +
                '<span class="color-text-ddd message-name" style="cursor: pointer;"><i class="offline"></i>' + data['name'] + '</span>' +
                '</div>';
        if (check == "1")
            st += '<div class="icon-addfriend icon-removegroup" style="display: none;"><i class="fa fa-times" aria-hidden="true"></i></div>';

        st += '</div>' +
                '</li>';
        return st;
    }

    function appendUserNotifiBox(data) {
        return '<li class="media notification-success addli-togroup" data-name="' + stripVowelAccent(data['name']) + '"><a href="javascript:void(0)"><div class="active-user-info" data-id="' + data['id'] + '"><span>' + data['name'] + '</span></div><div class="icon-addfriend icon-addgroup-box" style="display:none;margin-right: 20px;"><i class="fa fa-user-plus" aria-hidden="true"></i></div></a></li>'
    }

    function appendGroupForTabUser(data) {
        var image_url = '<i class="fa fa-users"></i>';
        if (data['url'])
            image_url = '<img src="' + data['url'] + '" alt = "avatar" class="img-responsive center-block"/>';
        $('.list-user li').removeClass('active');
        return '<li id="g-' + data.room_id + '" class="clearfix new-friend active" data-name="' + stripVowelAccent(data['title']) + '" data-roomid="' + data['room_id'] + '" data-isadmin=1>' +
                '<a data-url="{{Helper::url('chat/getMessageRoom')}}" href="#tab' + data['room_id'] + '" data - toggle = "tab" data-action = "one-to-many" >' +
                '<div class="avatar">' +
                image_url +
                '</div>' +
                '<div class="active-user-info active-user-group">' +
                '<span class="user_name">' + data['title'] + '</span>' +
                '</div>' +
                '</a>' +
                '</li>';
    }

    function ajaxFriendNotification(url, type, room_id) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {type: type, room_id: room_id},
            beforeSend: function () {
            },
        }).done(function (str) {
            if (str.success) {
                var noticeBox = $('.add-menu-notifications');
                noticeBox.find('ul').html(str.data.htmlMessage);
                noticeBox.toggle('slow');
                sessionStorage['addtogroup' + room_id] = 1;
            }

        }).fail(function (err) {

        })
    }

    function blockNoneInputNameGroup(type) {
        var $span = $('.right-inner-addon span');
        var $input = $('.right-inner-addon input');
        if (type == 'editing') {
            $span.css('display', 'none');
            $input.css('display', 'block');
        } else {
            $span.css('display', 'block');
            $input.css('display', 'none');
        }

    }

    function runPausedAnimation(element, type) {
        element = document.querySelector(element);
        if (element) {
            if (type == 'running')
                element.setAttribute("style", "-webkit-animation-play-state: running;-moz-animation-play-state: running;animationPlayState: running;");
            else
                element.setAttribute("style", "-webkit-animation-play-state: paused;-moz-animation-play-state: paused;animationPlayState: paused;");
        }
    }

    /**
     * This method allows you add, remove, block, unblock, confirm, delete request, unblock friend.
     * @param data include 2 param(id:friend_id, action: addfriend,cancel-request,block,confirm, delete-request)
     * @param url
     */
    function addFriend(data, url, $this) {
        var self = $this;
        var action = data.action;
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            beforeSend: function () {
                $('#add-friend').modal('hide');
            },
            success: function (str) {
                if (str.success) {
                    var textarea = self.closest('.tab-pane').find('textarea.input-message');
                    var room_id = textarea.data('roomid');
                    if (action === 'block') {
                        textarea.prop('disabled', 'disabled');
                    } else if (action === 'unblock') {
                        textarea.attr('disabled', false);
                    } else if (action === 'addfriend' || action === 'confirm') {
                        addFriendData(str);
                        sortByName.sort_all_users(online_users);
                    } else if (action === 'cancel-request') {
                        $('#f-' + room_id).remove();
                    }
                    if (action === 'block' || action === 'unfriend') {
                        var id = textarea.data('receiver');
                        socket.emit('remove user', {user_id: sessionGlobal, target_id: id, room_id: room_id});
                        delete online_users[id];
                        //todo
                        /*if (room_id) {
                         easyrtc.leaveRoom(room_id, null, function (room_id) {
                         rtc.roomJoined.delete(room_id)
                         });
                         }*/
                        sortByName.sort_all_users(online_users);
                        $('#f-' + room_id).remove();
                        $('#rf-' + room_id).find('.avatar').html('<i class="fa fa-question" aria-hidden="true"></i>');
//                        var recent = $('#rf-' + data.room_id);
//                        recent.find('.status').html('<i class="offline"></i>Offline</span>');
                    }
                    var relative = self.closest('.user-relative-chat');
                    if (!relative.length) {
                        var side_tab = $('#voltaic_holder_' + str.room_id);
                        relative = side_tab.find('.user-relative-chat');
                    }
                    relative.html(takeContentButtonRelative(str.data.status_me));
                    relative.data('option', str.data.option);
                }
                else {
                    $('#errorModal p').html(str.message);
                    $('#errorModal').modal('show');
                }
            }
        });
    }
    /**
     * create recent to create event click for list friend
     * @param user_id
     */
    function create_recent(user_id) {
        var url = fullUrl+'/chat/recent';
        var data = {user_id:user_id};
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
            data: data,
            beforeSend: function () {
            },
            success: function (str) {
                str.data = {};
                if(str){
                    str.data.friend_id = str.friend_id;
                    str.recent_flag = 1;
                    str.position = 'last';
                    addFriendData(str);
                    $('li.new-friend[data-id="' + user_id + '"]').click();
                }

            }
        });
    }
</script>
