<div class="right-content tab-pane tab-video-audio-content class-opentab-user" id="tabdashboard">
    <div class="rows">
        <div class="col-conservation tab-content">
            <div class="col-widthd clearfix main-chat-content" id="main_videoAudio">
                <header class="right-title clearfix" id="righttitle">
                    <div class="user-chat col-md-8">
                        <div class="user-chat-addfriend">
                            <div class="avatar">
                                <i class="fa fa-user"></i>
                            </div>
                            <h5 style="text-transform: uppercase;">{{trans('fr_home.all_friends')}}</h5>
                        </div>
                    </div>
                    <div class="dropdown dot-menu col-md-4">
                        {{--<button class="btn btn-menu" type="button" onclick="openNav()">
                            <i class="fa fa-info"></i>
                        </button>--}}
                        <form role="search" class="stylish-input-group" onsubmit="event.preventDefault()">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="{{trans('fr_home.search')}}..."
                                       id="search-friend-tabdashboard">
                                <span class="input-group-addon">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                            </div>
                        </form>
                    </div>
                </header>
                <div class="ctn-search-addfriend chat-tab-container nanos scrollview">
                    <div class="content-chat clearfix nano-content">
                        <article>
                            {{--<div class="titile-allfriend">
                                <p>Tất cả bạn bè</p>
                            </div>--}}
                            <div class="cont-addfi append-all-friend">
                                <div>
                                    {{--<div class="col-md-4 col-sm-6" onclick="openNav()">
                                        <div class="user-chat">
                                            <div class="avatar"><i class="fa fa-user"></i></div>
                                            <div class="active-user-info">
                                                <div class="chat-us-tt clearfix">
                                                    <span class="color-text-ddd">ThinhGlory</span>
                                                </div>
                                                <div class="clearfix">
                                                    <span class="status">Hue, VietNam</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




