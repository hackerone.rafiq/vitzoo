<?php if (empty($room)) $room = []; if (empty($user_arr)) $user_arr = [];?>
@if(isset($recent) && count($recent))
    @foreach($recent as $list)
        <?php
        $room = $list->joinRoom()->select(['_id', 'title', 'room_admin', 'type', 'url'])->first();
        if ($room && $room->type !== 'room') {
            $roomUser = $room->joinRoomUser()->where('user_id', '!=', $user_session->id)->first();
            if ($roomUser) {
                $user = $roomUser->joinUser()->select('last_name', 'id', 'url', 'first_name')->first();
            } else {
                $user = false;
            }
        }
        ?>
        @if($room)
            @if($room->type === 'room')
                <?php $is_admin = $room->room_admin == $user_session->id ? 1 : 0;?>
                <li class="clearfix new-friend" data-roomid="{{$room->_id}}"
                    id="rg-{{$room->_id}}"
                    data-isadmin="{{$is_admin}}"
                    data-name="{{Helper::stripVowelAccent(trim($room->title))}}">
                    <a class="clearfix"
                       data-url="{{Helper::url('chat/getMessageRoom')}}"
                       {{--href="#tab{{$room->_id}}"--}}
                       {{--data-toggle="tab" --}}
                       data-action="one-to-many">
                        <div class="avatar">
                            @if($room->url)
                                <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/'.$room->url))}}"
                                     alt="avatar"
                                     class="img-responsive center-block"/>
                            @else
                                <i class="fa fa-users"></i>
                            @endif
                        </div>
                        <div class="active-user-info active-user-group">
                            <span class="user_name">{{trim($room->title)}}</span>
                        </div>
                        @if($messageStatus = $list->countMessageIsRead($room->_id,$user_session->id))
                            <span class="count-message">{{$messageStatus->is_read}}</span>
                        @endif
                    </a>
                </li>
            @elseif($user)
                <?php $room_id = $list->room_id ?>
                <li class="clearfix new-friend"
                    data-name="{{Helper::stripVowelAccent($user->last_name.' '.$user->first_name)}}"
                    data-roomid="{{$room_id}}" id="rf-{{$room_id}}" data-id="{{$user->id}}">
                    <a class="clearfix" data-id="{{$user->id}}"
                       data-url="{{Helper::url('chat/getMessage')}}"
                       {{--href="#tab{{$room_id}}"--}}
                       {{--data-toggle="tab" --}}
                       data-action="one-to-one"
                       data-roomid="{{$room_id}}">
                        <div class="avatar">
                            @if($user->isFriend($user_session->id))
                                @if($user->url)
                                    <img src="{{Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/'.$user->url))}}"
                                         alt="avatar"
                                         class="img-responsive center-block"/>
                                @else
                                    <i class="fa fa-user"></i>
                                @endif
                            @else
                                <i class="fa fa-question"></i>
                            @endif
                        </div>
                        <div class="active-user-info">
                            <span class="user_name">{{$user->last_name.' '.$user->first_name}}</span>
                            <span class="status">
                                                            <i class="offline"></i>{{trans('fr_home.offline')}}</span>
                        </div>
                        @if($messageStatus = $list->countMessageIsRead($room_id,$user_session->id))
                            <span class="count-message">{{$messageStatus->is_read}}</span>
                        @endif
                    </a>
                </li>
            @endif
        @endif
    @endforeach
    @if($show_max)
        <li class="read-more" data-page="{{$page}}" data-max="{{$max_page}}" data-href="{{url('chat/recent_rooms')}}">
            <a href="#" title="More messages">{{trans('fr_home.earlier_messages')}}</a>
            <i class="fa fa-spinner fa-pulse room-loading"></i>
        </li>
    @endif
@endif