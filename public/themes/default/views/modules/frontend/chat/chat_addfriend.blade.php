<div class="right-content tab-pane tabaddfriend active" id="tabaddfriend">
    <div class="rows">
        <div class="col-conservation tab-content">
            <div class="right-content col-widthd clearfix ctn-addfriend main-chat-content content-addgroup"
                 id="main_addfriend">
                <header class="right-title clearfix">
                    <div class="user-chat col-md-7">
                        <div class="user-chat-addfriend">
                            <div class="avatar">
                                <i class="fa fa-user-plus"></i>
                            </div>
                            <h5>{{trans('fr_home.addfriend')}}</h5>
                        </div>
                    </div>
                    <div class="dropdown col-md-5">
                        {{--<button class="btn btn-menu" type="button" onclick="openNav()">
                            <i class="fa fa-info"></i>
                        </button>--}}
                        {!! Form::open(['url' => Helper::url('chat/searchUser'), 'id' => 'search-form-addfriend','class'=>'stylish-input-group', 'onsubmit'=>'event.preventDefault()']) !!}
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="{{trans('fr_home.search')}}"
                                   id="search-addfriend">
                            <span class="input-group-addon">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                        </form>
                    </div>
                </header>
                <div class="nanos scrollview ctn-search-addfriend chat-tab-container">
                    <div class="content-chat clearfix nano-content">
                        <article>
                            <div class="titile-allfriend">
                                <p>{{trans('fr_home.all_friend')}}</p>
                            </div>
                            <div class="cont-addfi append-all-friend">
                                <div>
                                </div>
                                <div class="col-md-12"></div>
                            </div>
                        </article>

                        <div class="search_more" data-page="" data-max=""
                             data-href="{{url('chat/searchMoreFriend')}}" style="display: none;">
                            <a href="#" title="More messages">{{trans('fr_home.search_more')}}</a>
                            <i class="fa fa-spinner fa-pulse room-loading"></i>
                        </div>

                    </div>

                </div>

            </div>
            <div class="col-attach sidenav user_info" id="voltaic_holder_addfriend">
            </div>
        </div>
    </div>
</div>

