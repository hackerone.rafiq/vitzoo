@extends($theme_layout)
@section('content')
    <div class="mainpage">
        <div class="tab-content container side-body">
            <!-- faq-page -->
            <div id="faq">
                <div class="col-md-6 col-md-offset-3 title text-center page-title">
                    <h2>{{trans('fr_home.instructions_for_use')}}</h2>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="faq-accordian">
                            <h3 class="bg-active top-radius">{{trans('fr_home.guides_join_in_chat')}}<i class="fa fa-minus"></i></h3>
                            <ul class="nav-tabs active">
                                <li>
                                    <a href="#reglog" class="active"><i class="fa fa-angle-right"></i>{{trans('fr_home.registration_and_activation')}}</a>
                                </li>
                                <li>
                                    <a href="#findfriend"><i class="fa fa-angle-right"></i>{{trans('fr_home.search_for_friend_and_addfriend')}}</a>
                                </li>
                                <li>
                                    <a href="#create_room"><i class="fa fa-angle-right"></i>{{trans('fr_home.create_chat_group')}}</a>
                                </li>
                                <li>
                                    <a href="#join_room"><i class="fa fa-angle-right"></i>{{trans('fr_home.join_group')}}</a>
                                </li>
                                <li>
                                    <a href="#group_chat"><i class="fa fa-angle-right"></i>{{trans('fr_home.personal_chat')}}</a>
                                </li>
                            </ul>

                            <h3 class="bg-active top-radius">{{trans('fr_home.guides_join_in_chat')}}<i
                                        class="fa fa-minus"></i></h3>
                            <ul class="nav-tabs">
                                <li>
                                    <a href="#reglog" class="active"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.registration_and_activation')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="#findfriend"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.search_for_friend_and_addfriend')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="#create_room"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.create_chat_group')}}</a>
                                </li>
                                <li>
                                    <a href="#join_room"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.join_group')}}</a>
                                </li>
                                <li>
                                    <a href="#group_chat"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.personal_chat')}}</a>
                                </li>
                            </ul>

                            <h3 class="bg-active top-radius">{{trans('fr_home.guides_join_in_chat')}}<i
                                        class="fa fa-minus"></i></h3>
                            <ul class="nav-tabs">
                                <li>
                                    <a href="#reglog" class="active"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.registration_and_activation')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="#findfriend"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.search_for_friend_and_addfriend')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="#create_room"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.create_chat_group')}}</a>
                                </li>
                                <li>
                                    <a href="#join_room"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.join_group')}}</a>
                                </li>
                                <li>
                                    <a href="#group_chat"><i
                                                class="fa fa-angle-right"></i>{{trans('fr_home.personal_chat')}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 faq-tab-ct">
                        <div class="full-radius faq-content tab-content">
                            <div id="reglog" class="tab-pane fade in active">
                                <h4 class="text-center">{{trans('fr_home.registration_and_activation')}}</h4>
                                <div class="faq-answer">
                                    <p>
                                        Việc đăng ký và tham gia Vitzoo rất đơn giản.
                                    </p>
                                    <p>
                                        1. Đăng ký tài khoản tại trang chủ : <a href="https://vitzoo.com">https://vitzoo.com</a>
                                    </p>
                                    <p>
                                        2. Đăng nhập ngay khi bạn tạo tài khoản thành công.
                                    </p>
                                </div>
                            </div>

                            <div id="findfriend" class="tab-pane fade">
                                <h4 class="text-center">{{trans('fr_home.search_for_friend_and_addfriend')}}</h4>
                                <div class="faq-answer">
                                    <p>
                                        Bạn nên gửi lời mời kết bạn đến bạn bè, người thân và những người
                                        khác mà mình biết và tin tưởng, bạn có thể kết bạn bằng cách tìm
                                        kiếm và gửi cho họ lời mời kết bạn tại công cụ tìm kiếm trong mục
                                        chat ngay khi tham gia, sau đó có thể trò chuyện hay gọi video với bất
                                        kỳ ai trong danh sách bạn bè của mình.
                                    </p>
                                </div>
                            </div>

                            <div id="create_room" class="tab-pane fade">
                                <h4 class="text-center">{{trans('fr_home.create_chat_group')}}</h4>
                                <div class="faq-answer">
                                    <p>
                                        Với Vitzoo bạn có thể tạo Group (nhóm / phòng) để trò chuyện với
                                        những người bạn thân hoặc với đối tác làm ăn, nhân viên công ty hay bất
                                        cứ ai một cách thuận tiện mà không cần phải nhắn tin đến từng người
                                        làm tốn nhiều thời gian.
                                    </p>
                                    <p>
                                        Ngoài ra bạn có thể đổi tên nhóm, thêm hoặc bớt bất kỳ thành viên
                                        nào trong nhóm hay gửi ảnh, gọi audio hoặc video, tập tin với tất cả
                                        các thành viên trong nhóm đó.
                                    </p>
                                </div>
                            </div>

                            <div id="join_room" class="tab-pane fade">
                                <h4 class="text-center">{{trans('fr_home.join_group')}}</h4>
                                <div class="faq-answer">
                                    <p>
                                        Bạn có thể tham gia một nhóm bất kỳ trong danh sách của bạn hoặc
                                        được người chủ nhóm cho phép tham gia.
                                    </p>
                                </div>
                            </div>

                            <div id="group_chat" class="tab-pane fade">
                                <h4 class="text-center">{{trans('fr_home.personal_chat')}}</h4>
                                <div class="faq-answer">
                                    <p>
                                        Nhắn tin cá nhân trong Vitzoo cho phép bạn có thể liên hệ ngay với
                                        những người trong danh sách bạn bè của mình , bạn có thể gửi tin
                                        nhắn, tập tin, hình ảnh, thực hiện cuộc gọi audio hoặc video ngay trên
                                        trình duyệt của bạn một cách nhanh chóng.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js_wrapper')
    <script type="text/javascript">
        $('body').attr('id', 'bg-faq');
    </script>
@stop