<footer class="footer side-body">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 col-md-push-8">
                {{--<ul class="footer-socials pull-right">
                    <li>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </li>
                </ul>--}}
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 col-md-pull-4">
                <span class="copyright">Copyright &copy; 2016 <a href="http://www.vietprojectgroup.com" target="_blank">vitzoo. Site Published by ViTPR</a></span>
            </div>
        </div>
    </div>
</footer>