@extends($theme_layout)
@section('content')
    <div class="mainpage">
        <div class="tab-content container side-body">
            <!-- contact-page -->
            <div id="contact">
                <div class="col-md-6 col-md-offset-3 title text-center page-title">
                    <h2>{{trans('fr_home.contact')}}</h2>
                    <hr />
                </div>
                <div class="embed-maps">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 col-sm-6 ct-map">
                                <iframe class="maps-google" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.4093352644923!2d107.58613631456959!3d16.454798133360825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a1465d3d3b7d%3A0x5b9cda8123c1155f!2zMjMvMzMgTmd1eeG7hW4gVHLGsOG7nW5nIFThu5kgdOG7lSA0LCBQaMaw4bubYyBWxKluaCwgdHAuIEh14bq_LCBUaOG7q2EgVGhpw6puIEh14bq_LCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1444360952919" width="100%" height="465" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-4 col-md-offset-7 contact-form col-sm-6">
                                <form id="contactForm">
                                    <div class="form-group">
                                        <input class="form-control" name="contact-fullname" placeholder="{{trans('fr_home.fullname')}}" />
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="contact-email" placeholder="Email" />
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="5" class="form-control" name="contact-message" placeholder="{{trans('fr_home.enter_message')}}"></textarea>
                                    </div>

                                    {{--<div class="form-group">
                                        <select class="form-control" name="contact-staff">
                                            <option value="sales">------</option>
                                            <option value="sales">- AAAA</option>
                                            <option value="sales">- BBBB</option>
                                            <option value="sales">- CCCC</option>
                                            <option value="sales">- DDDD</option>
                                        </select>
                                    </div>--}}

                                    <button type="submit" class="btn btn-danger">{{trans('fr_home.send')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js_wrapper')
    <script type="text/javascript">
        $('body').attr('id', 'bg-contact');
    </script>
@stop