<meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width,initial-scale=1">
<meta http-equiv="Expires" content="1"/>
<link class="favicon" rel="icon" href="{{url('themes/default/asset_frontend/img/favicon.ico')}}" type="image/x-icon">
<link class="favicon" rel="shortcut icon" type="image/png" href="{{url('themes/default/asset_frontend/img/favicon.png')}}"/>