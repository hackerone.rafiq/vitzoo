<script type="text/javascript">

    function EnableSubmit(val) {
        //var sbmt = document.getElementsByClassName("btn-register-login");
        /*if(val.checked == true){
         sbmt.disabled = false;
         }
         else
         {
         sbmt.disabled = true;
         }*/
    }


    /*$('#loginForm').validate({
     ignore: [],
     rules: {
     email: {
     required: true,
     email: true
     },
     password: "required",
     },
     messages: {}
     , submitHandler: function (form, event) {
     event.preventDefault();
     url = $(form).attr('action')
     data = $(form).serialize();
     $.ajax({
     url: url,
     type: 'POST',
     dataType: 'JSON',
     data: data,
     beforeSend: function () {
     $('#css-loading').show();
     },
     success: function (str) {
     $('#css-loading').hide();
     $('.form-group').removeClass('has-error');
     if (!str.success) {
     $('#reauth-login').html(str.message);
     } else {
     $('#pop-up').modal('hide');
     if (!str.data)
     window.location.reload(true);
     else {
     var segments = window.location.href.split('/');
     segments[3] = str.data;
     /!*socket.emit('user online');*!/
     redirect = segments.join('/');
     window.location.href = redirect;
     }
     }
     }
     });
     }
     });*/
    $('#registerForm').validate({
        ignore: [],
        rules: {
            email: {
                required: true,
                email: true
            },
            first_name: {
                required: true,
                maxlength: 20
            },
            last_name: {
                required: true,
                maxlength: 40
            },
            'password': {
                required: true,
                minlength: 5
            }
            /* "g-recaptcha-response": "required"*/
        },
        messages: {}
        , submitHandler: function (form, event) {
            event.preventDefault();
            var url = $(form).attr('action'),
                    data = $(form).serialize();
            if (validatePassword()) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    beforeSend: showLoader
                }).done(function (str) {
                    if (str.success == false) {
                        $('#reauth-email').html(str.message);
                    } else {
                        location.reload(true);
                    }
                }).fail(function (e) {
                    var response = JSON.parse(e.responseText);
                    var message = response.message;
                    $('#reauth-email').html(message);
                }).always(hideLoader)
            }
        }
    });

    $('#loginForm').validate({
        ignore: [],
        rules: {
            email: {
                required: true,
                email: true
            },
            password: "required",
        },
        messages: {}
        , submitHandler: function (form, event) {
            // event.preventDefault();
            var url = $(form).attr('action'),
                    data = $(form).serialize();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: function () {
                    $('#css-loading').show();
                },
                success: function (str) {
                    $('#css-loading').hide();
                    if (str.success)
                        location.reload(true);
                    else {
                        $('#errorModal p').html(str.message);
                        $('#errorModal').modal('show');
                    }

                }
            });
        }
    });

    $('#fogotpass').validate({
        ignore: [],
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        messages: {}
        , submitHandler: function (form, event) {
            // event.preventDefault();
            var url = $(form).attr('action'),
                    data = $(form).serialize();
        }
    });

    window.onload = function () {
        document.getElementById("password-signup").onchange = validatePassword;
        document.getElementById("retype-password").onkeyup = validatePassword;
    };

    function validatePassword() {
        var validate = 'Xác nhận và mật khẩu phải giống nhau';
        if(lang == 'en')
            validate = 'Confirm and password must match';
        $('#retype-password-error').remove();
        var pass2 = document.getElementById("password-signup");
        var pass1 = document.getElementById("retype-password");
        if (pass1.value != pass2.value) {
            $('#retype-password').after('<label id="retype-password-error" class="error"">'+validate+'.</label>');
            return false;
        }
        else {
            $('#retype-password-error').remove();
            return true;
        }

    }
    $(document).ready(function () {
       $('.registerForm .iCheck-helper').click(function () {
           var check = $('.registerForm .icheckbox_minimal-red');
           if(check.hasClass('checked'))
               $('.btn-sign-up').prop('disabled', false);
           else
               $('.btn-sign-up').prop('disabled', true);
       }) ;
    });
</script>
