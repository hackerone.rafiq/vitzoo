@extends($theme_layout)

@section('content')
    <p id="power">0</p>
    <?php
    var_dump($theme_layout);
    ?>
@stop

@section('js_wrapper')
    <script>
        var socket = io('http://localhost:3000');
        /* var socket = io('http://192.168.10.10:3000'); */
        socket.on("test-channel:Modules\\Frontend\\Events\\TestEvent", function(message){
            // increase the power everytime we load test route
            console.log(message);
            $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
        });
    </script>
@stop