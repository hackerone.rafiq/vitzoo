@if(session('message'))
    @if(isset(session('message')['success']))
        <script type="text/javascript">
            $('#successModal p').html('{{session('message')['success']}}');
            $('#successModal').modal('show');
        </script>
    @endif
    @if(isset(session('message')['error']))
        <script type="text/javascript">
            $('#errorModal p').html('{{session('message')['error']}}');
            $('#errorModal').modal('show');
        </script>
    @endif
@endif
