<!doctype html>
<html lang="en">
<head>
    @include('default::modules.frontend.head')
    {!! SEO::generate() !!}
		<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @if(Sentinel::check())
        <script>
            var helperUrl = '{{Helper::geturl('4040')}}';
            var helperRtc = '{{Helper::geturl('5555')}}';

            var token = '{{session('jwt_token')}}';
            var sessionGlobal = {{Sentinel::check()->id}};
            var nameSession = '{{Sentinel::check()->last_name.' '.Sentinel::check()->first_name}}';
            var urlGlobal = '{{url('')}}';
        </script>
    @endif
    {!! Assets::css() !!}
    <script>
        @if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false)
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-32694432-7', 'auto');
        ga('send', 'pageview');
                @endif
        var lang = '{{\LaravelLocalization::getCurrentLocale()}}';
    </script>
</head>
<body class="clearfix">
@if(Sentinel::check())
    <?php $sentinel = Sentinel::check();$user_session = Sentinel::check();?>
@endif
@yield('css_wrapper')
@include('default::modules.frontend.taskbar.taskbar_nonelogin')
@yield('content')
@include('default::modules.frontend.footer')
@include('default::modules.frontend.modal')
@include('default::modules.frontend.layouts.translate')
{!! Assets::js() !!}
@yield('js_wrapper')
@include('default::modules.frontend.layouts.master_js')
@if(isset($sentinel) && !empty($sentinel))
    @include('default::modules.frontend.chat.chat_js')
@endif
@if(!Sentinel::check())
    <script type="text/javascript">
        document.getElementById("registerForm").reset();
    </script>
@endif
</body>
</html>