<script>
    var trans = (function () {
        return {
            how_are_you: '{{trans('site.how_are_you')}}',
            confirm: '{{trans('site.confirm')}}',
            delete_request: '{{trans('site.delete_request')}}',
            confirmed: '{{trans('site.confirmed')}}',
            deleted_request: '{{trans('site.deleted_request')}}',
            offline: '{{trans('site.offline')}}',
            online: '{{trans('site.online')}}',
            busy: '{{trans('site.busy')}}',
            not_online: '{{trans('site.not_online')}}',
            stop_current_call: '{{trans('site.stop_current_call')}}',
            connected: '{{trans('site.connected')}}',
            connecting: '{{trans('site.connecting')}}',
            file_too_large: '{{trans('site.file_too_large')}}',
            error_during_upload: '{{trans('site.error_during_upload')}}',
            upload_canceled: '{{trans('site.upload_canceled')}}',
            cannot_upload_file: '{{trans('site.cannot_upload_file')}}',
            cancel : '{{trans('site.cancel')}}',
            is_typing: '{{trans('site.is_typing')}}',
            are_typing: '{{trans('site.are_typing')}}',
            waiting_to_accept : '{{trans('fr_home.waiting_to_accept')}}',
            are_you_sure_delete_group:'{{trans('fr_home.are_you_sure_delete_group')}}',
            group_has_been_deleted: '{{trans('fr_home.group_has_been_deleted')}}',
            you: '{{trans('fr_home.you')}}',
            miss_call: '{{trans('fr_home.miss_call')}}',
            call_end: '{{trans('fr_home.call_end')}}',
            close_window_before_call: '{{trans('site.close_window_before_call')}}',
            user_busy_call_later: '{{trans('site.user_busy_call_later')}}',
        }
    })();
</script>