<script>
    /* js year*/
    var file;
    var isChange = false;
    var linkAvata;
    jQuery.validator.addMethod("notEqualTo", function (value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "{{trans('site.new_password_unchanged')}}");

    /*js month*/

    /*js get city*/
    $(document).on('click', '.btt-cancel-tdmk', function () {
        $('.prf-ipu-pass label.error').remove();
        $('.tdmk .form-return').empty();
    });

    $(document).on('click', '.country li', function () {
        var idCountry = $(this).data('value');
        var token = '{{csrf_token()}}';
        ajaxProvinces(idCountry);
        $('#provinces').empty();
        $('#city').empty();
    });

    $(document).on('click', '.select-country li', function () {
        var idProvinces = $(this).attr('value');
        var idCountry = $('.select-country').data('idCountry');
        ajaxCity(idProvinces, idCountry);
        $('#city').empty();
    });

    function ajaxCity(idProvinces, idCountry) {
        var token = '{{csrf_token()}}';
        $.ajax({
            url: '{{Helper::url('getCity')}}',
            type: 'POST',
            dataType: 'JSON',
            data: {idProvinces: idProvinces, _token: token, idCountry: idCountry},
            success: function (str) {
                $('#display-city').removeClass('divNone');
                /*if ($('#provinces').data('clicked') == 'true') {
                    $('#city').empty();
                }*/
                $('.select-city').html(str);
            }
        });
    }

    function ajaxProvinces(idCountry) {
        var token = '{{csrf_token()}}';
        $.ajax({
            url: '{{Helper::url('getCountry')}}',
            type: 'POST',
            dataType: 'JSON',
            data: {idCountry: idCountry, _token: token},

            success: function (str) {
                $('.display-provinces').css('display', 'block');
                $('.select-country').data('idCountry', idCountry);
                /*if ($('#nation').data('clicked') == 'true') {
                    $('#provinces').empty();
                    $('#city').empty();
                }*/
                $('.select-country').html(str);
            }
        });
    }

    $(document).ready(function () {
        $('#personal-editpassword').validate({
            ignore: [],
            rules: {
                current_password: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 5,
                    notEqualTo: "#current_password"
                },
                confirm_password: {
                    equalTo: "#password"
                }
            },
            messages: {
                confirm_password: {
                    equalTo: "{{trans('site.same_as_password')}}"
                }
            }
            , submitHandler: function (form, event) {
                event.preventDefault();
                var url = $(form).attr('action');
                var data = $(form).serialize();
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    beforeSend: showLoader,
                    success: function (str) {
                        var message = $(form).find('.form-return').html(str.message);
                        if (str.success) {
                            message.addClass('aqua');
                            form.reset();
                        } else {
                            message.addClass('red');
                        }
                    }
                }).always(hideLoader);
            }
        });

        var input = document.getElementById('upload_avatar');
        var fileName;
        $('#file-upload').on('click', function (e) {
            e.preventDefault();
            /*input.click();*/
            $('#cModalChangeAvatar').modal({backdrop: 'static', keyboard: false});
        });

        $('#_pf-choose-image').on('click', function (e) {
            e.preventDefault();
            input.click();
        });
        /*input.onchange = function () {
         readURL(this);
         };*/

        $(".pf-btn-button-save").click(function () {
//           var file = $("#upload_avatar")[0].files;
            var form = document.createElement('form');
            form.appendChild(input);
            var data = new FormData(form);
            $.ajax({
                url: '{{Helper::url('profile/uploadAvatar')}}',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                processData: false,
                contentType: false,
                enctype: "multipart/form-data",
                success: function (str) {
                    form = null;
                    isChange = false;
                }
            });
        });
        $(".pf-btn-button-cancel").click(function () {
            $('.hovereffect').css('background-image', 'url(' + linkAvata + ')');
        });

        $('.image-editor').cropit({
            imageBackground: true,
            smallImage: true,
            allowDragNDrop: true,
            width: '{{env('width_avatar')}}',
            height: '{{env('height_avatar')}}',
        });

        {{--$('.image-editor').cropit('previewSize', {--}}
            {{--width: '{{env('width_avatar')}}',--}}
            {{--height: '{{env('height_avatar')}}',--}}
{{--//            imageBackground: true--}}
        {{--});--}}

        $('.cropit-image-input').change(function () {
            var file;
            if (file = this.files[0]) {
                fileName = file.name;
            }
        });

        $('.rotate-cw').click(function () {
            $('.image-editor').cropit('rotateCW');
        });
        $('.rotate-ccw').click(function () {
            $('.image-editor').cropit('rotateCCW');
        });

        $('.export').click(function () {
            var imageData = $('.image-editor').cropit('export');
            if (imageData) {
                var file = dataURItoBlob(imageData);
                var fd = new FormData();
                fd.append('fname', fileName);
                fd.append('data', file);
                $.ajax({
                    url: '{{Helper::url('profile/uploadAvatar')}}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: fd,
                    processData: false,
                    contentType: false,
                    enctype: "multipart/form-data",
                    success: function (str) {
                        form = null;
                        isChange = false;
                        if (str.success) {
                            var url = '{{Helper::getLinkAvatar()}}' + str.data['url'];
                            var hovereffect = $('.hovereffect');
                            hovereffect.css('background-image', 'url(' +
                                    url + ')');
                            hovereffect.find('i.fa-users').remove();
                            $('#cModalChangeAvatar').modal('hide');
                        }
                    }
                });
            } else {
                $('#cModalChangeAvatar').modal('hide');
            }
        });
    });

    /*$('.close-popup-avatar').click(function () {
     $('.cropit-preview').empty();
     });*/

    /* Show preview for image*/
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (isChange == false) {
                    isChange = true;
                    var bg = $('.hovereffect').css('background-image');
                    linkAvata = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
                }
                $('.hovereffect').css('background-image', 'url(' + e.target.result + ')');
                $(".pf_button").fadeIn("slow");
            };
            return reader.readAsDataURL(input.files[0]);
        }
        return false
    }

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = decodeURI(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    }


</script>