@extends($theme_layout)
@section('content')
    <?php
    $user_session = Sentinel::check();
    $attribute_name = [1, 'gender', 'mobile', 'address', 'birthday', 'nation', 'provinces', 'city', 'language'];
    ?>
    <div class="mainpage">
        <div class="container side-body page-profile">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 page-title">
                    <h2>{{trans('fr_home.profile')}}</h2>
                </div>

            </div>
            <div class="row profile-content">
                <div class="col-md-3">
                    <div class="profile-left-1">
                        <div class="col-md-12 col-sm-6">
                            <div class="hovereffect"
                                 @if(isset($user) && !empty($user->url))style="background-image: url('{{Helper::getLinkAvatar().$user->url}}');" @endif>
                                {{--<input type="file" id="upload_avatar"
                                               class="upload-file" name="upload"
                                               style="display: none">--}}
                                @if($user_session->id == $user->id)
                                    <div class="overlay">
                                        <div class="pf-content-hover">

                                            <a class="info" href="#" id="file-upload"
                                            ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                <p style="margin: 0;padding: 0;">
                                                    {{trans('fr_home.change_avatar')}}
                                                </p>
                                            </a>
                                            <a class="info" href="#"></a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-6 prf-Tla">
                            <div class="f-profile">
                                <i class="fa fa-user"></i>
                                <div class="profile-name profile-fullname">
                                    <span class="pf-user truncate">{{$user->last_name}} {{$user->first_name}}</span>
                                </div>
                            </div>
                            <div class="f-profile">
                                <i class="fa fa-envelope"></i>
                                <div class="profile-name">
                                    <span class="pf-email">{{$user->email}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-left-2">
                        <ul class="nav nav-tabs nav-stacked">
                            <li class="active f-profile"><a href="#thong-tin-ca-nhan"><i
                                            class="fa fa-male"></i>{{trans('fr_home.personal_information')}}</a></li>
                            @if($user_session->id == $user->id)
                                <li class="f-profile"><a href="#change-password"><i
                                                class="fa fa-lock"></i>{{trans('fr_home.change_password')}}</a>
                                </li>

                                {{--do not permit delete<li class="f-profile"><a href="#buddy"><i
                                                class="fa fa-users"></i>{{trans('fr_home.friend')}}(23)</a></li>--}}
                                {{--do not permit delete<li class="f-profile"><a href="#buddy"><i class="fa fa-users"></i>BOT</a></li>--}}
                            <!-- profile-left-3 -->
                                {{--do not permit delete-- important<li class="profile-left-3"></li>
                                <li class="f-profile"><a href="#install"><i
                                                class="fa fa-cog"></i>{{trans('fr_home.setting')}}</a></li>
                                <li class="f-profile"><a href="#notice"><i
                                                class="fa fa-bell"></i>{{trans('fr_home.notification')}}</a></li>--}}
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-right">
                        <div class="tab-content">
                            <!-- profile-content-2 -->
                            <div id="thong-tin-ca-nhan" class="tab-pane fade in f-profile active">
                                <div class="thongtincanhan">
                                    <div class="pf-content clearfix nano-content">
                                        <!-- Thông tin cá nhân -->
                                        <article class="ttcn">
                                            <div class="pf-title-page" data-item="a1">
                                                <h5>
                                                    <i class="fa fa-male icon-tt-prf"></i>{{trans('fr_home.personal_information')}}
                                                    @if($user_session->id == $user->id)
                                                        <i
                                                                class="fa fa-pencil edit-ttcn"></i>
                                                    @endif
                                                </h5>
                                            </div>

                                            <div id="personal-information" class="row">
                                                {!! Form::open(['url' => Helper::url('profile/edit'),'class'=>'editProfile','id'=>'editProfile', 'onsubmit'=>'event.preventDefault()']) !!}
                                                <div class="col-md-10 col-md-offset-1 text-change clearfix">

                                                    <!-- Địa chỉ email -->
                                                    <div class="row ttcc-margin">
                                                        <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.email_address')}}
                                                        </div>
                                                        <div class="col-md-9 col-sm-9 col-xs-9 prf-res-9">
                                                            <div class="prf-email">{{$user->email}}</div>
                                                        </div>
                                                    </div>
                                                    <!-- Tên  -->
                                                    <div class="row ttcc-margin">
                                                        <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.user_name')}}
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                            <input type="text"
                                                                   class="form-control tx-general tx-hide"
                                                                   readonly placeholder="" id="first-name"
                                                                   value="{{$user->first_name}}" name="first_name"/>
                                                        </div>

                                                    </div>
                                                    <!-- Họ  -->
                                                    <div class="row ttcc-margin">
                                                        <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.last_name')}}</div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                            <input type="text"
                                                                   class="form-control tx-general tx-hide"
                                                                   placeholder="" id="last-name" readonly=""
                                                                   value="{{$user->last_name}}" name="last_name"/>
                                                        </div>
                                                    </div>
                                                @if($user_session->id == $user->id)
                                                    @if(isset($attribute) && $attribute)
                                                        @foreach($attribute as $item)
                                                            @if($item->id)
                                                                <?php $eloquent_userAttribute = $item->joinUserAttribute($user->id)->first(); ?>
                                                                @if($item->id == 4)
                                                                    @if($eloquent_userAttribute && ($timestamp = strtotime(str_replace('/', '-', $eloquent_userAttribute->values))) !== false) @endif
                                                                    <!-- Date -->
                                                                        <div class="row attribute-date ttcc-margin"
                                                                             data-attribute="{{$item->id}}">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.date_of_birth')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <fieldset class="date  date-error">
                                                                                    <dl
                                                                                            class="year-sample disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#">
                                                                                            <span class="y-ch"
                                                                                                  id="birth-year">@if(isset($timestamp) && $timestamp){{date('Y',$timestamp)}} @endif</span>
                                                                                                <i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul class="change-y">
                                                                                                <?php
                                                                                                for ($i = date("Y"); $i > (date("Y") - 100); $i--) {
                                                                                                    $j = $i < 10 ? '0' . $i : $i;
                                                                                                    echo '<li class="y-change" value="' . $j . '"><a>' . $j . '</a></li>';
                                                                                                }
                                                                                                ?>
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                    <dl
                                                                                            class="month-sample disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#">
                                                                                            <span class="m-ch"
                                                                                                  id="birth-month">@if(isset($timestamp) && $timestamp){{date('m',$timestamp)}} @endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul class="change-m">
                                                                                                <?php
                                                                                                for ($i = 1; $i < 12 + 1; $i++) {
                                                                                                    $j = $i < 10 ? '0' . $i : $i;
                                                                                                    echo '<li class="m-change" value="' . $j . '"><a>' . $j . '</a></li>';
                                                                                                }
                                                                                                ?>
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                    <dl
                                                                                            class="date-sample disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#">
                                                                                                <span id="birth-day">@if(isset($timestamp) && $timestamp){{date('d',$timestamp)}} @endif</span>
                                                                                                <i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul class="select-day">
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#"><span
                                                                                                        class="status status-birth-day">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif </span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif
                                                                @if($item->id == 1)
                                                                    <!-- Giới tính  -->
                                                                        <div class="row ttcc-margin">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.sex')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#">
                                                                                            <span id="gender"
                                                                                                  data-attribute="{{$item->id}}">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->values)}}@endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)">{{trans('fr_home.male')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)">{{trans('fr_home.female')}}</a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#"><span
                                                                                                        class="status status-gender">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif
                                                                @if($item->id == 2)
                                                                    <!-- Số điện thoại -->
                                                                        <div class="row ttcc-margin">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.phone_number')}}
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <input type="text"
                                                                                       class="form-control tx-general tx-hide"
                                                                                       placeholder="" id="phone-number"
                                                                                       value="@if($eloquent_userAttribute){{$eloquent_userAttribute->values}}@endif"
                                                                                       readonly
                                                                                       data-attribute="{{$item->id}}"
                                                                                       name="phone_number"/>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#"><span
                                                                                                        class="status status-phone-number">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif
                                                                @if($item->id == 5)
                                                                    <!-- Nation -->
                                                                        <div class="row ttcc-margin">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.nation')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="country-sample disabled dropdown dropdown-abc
                                                                     form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#"><span
                                                                                                        id="nation"
                                                                                                        data-attribute="{{$item->id}}">@if($eloquent_userAttribute && $eloquent_userAttribute->values && $eloquent_country = $eloquent_userAttribute->joinCountry()->first()){{$eloquent_country->Country}}
                                                                                                    <span data-value="{{$eloquent_country->country_iso_code}}"></span>@endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul class="country">
                                                                                                <?php
                                                                                                foreach ($country as $value) {
                                                                                                    echo '<li value="' . $value->Country . '" data-value="' . $value->country_iso_code . '"><a href="javascript:void(0)">' . $value->Country . '</a></li>';
                                                                                                }
                                                                                                ?>
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#"><span
                                                                                                        class="status status-nation">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif </span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif

                                                                @if($item->id == 7)
                                                                    <!-- Thành phố -->
                                                                        <div class="row ttcc-margin" id="display-city">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.city')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="javascript:void(0)"><span
                                                                                                        id="city"
                                                                                                        data-attribute="{{$item->id}}">@if($eloquent_userAttribute && $eloquent_userAttribute->values && ($eloquent_city = $eloquent_userAttribute->joinCity()->first())){{$eloquent_city->City}}
                                                                                                    <span data-value="{{$eloquent_city->id}}"></span>@endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul class="select-city">

                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="javascript:void(0)"><span
                                                                                                        class="status status-city">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif
                                                                @if($item->id == 3)
                                                                    <!-- Địa chỉ -->
                                                                        <div class="row ttcc-margin">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.address')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <input type="text"
                                                                                       class="form-control tx-general tx-hide"
                                                                                       placeholder=""
                                                                                       value="@if($eloquent_userAttribute){{$eloquent_userAttribute->values}} @endif"
                                                                                       id="address" readonly
                                                                                       data-attribute="{{$item->id}}"
                                                                                       name="address"/>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="javascript:void(0)"><span
                                                                                                        class="status status-address">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif
                                                                @if($item->id == 6)
                                                                    <!-- Tỉnh -->
                                                                        <div class="row display-provinces ttcc-margin">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.provinces')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="country-sample disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="#">
                                                                                            <span
                                                                                                    id="provinces"
                                                                                                    data-attribute="{{$item->id}}">
@if($eloquent_userAttribute && $eloquent_userAttribute->values && $eloquent_country = $eloquent_userAttribute->joinCity()->first()){{$eloquent_country->subdivision_1_name}}
                                                                                                <span data-value="{{$eloquent_country->id}}"
                                                                                                      data-iso-code="{{$eloquent_country->subdivision_1_iso_code}}"></span>@endif                                                                                         </span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i>
                                                                                            </a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul class="select-country">

                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="javascript:void(0)"><span
                                                                                                        class="status status-provinces">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                @endif
                                                                @if($item->id == 8)
                                                                    <!-- Ngôn ngữ -->
                                                                        <div class="row ttcc-margin">
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.language')}}</div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="javascript:void(0)"><span
                                                                                                        id="language"
                                                                                                        data-attribute="{{$item->id}}">@if($eloquent_userAttribute && $eloquent_userAttribute->values && ($eloquent_language = $eloquent_userAttribute->joinLanguage()->first())){{$eloquent_language->title}}
                                                                                                    <span data-value="{{$eloquent_language->id}}"></span>@endif</span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                @if(isset($language) && $language)
                                                                                                    @foreach($language as $item)
                                                                                                        <li value="{{$item->code}}"
                                                                                                            data-value="{{$item->id}}">
                                                                                                            <a href="javascript:void(0)">{{$item->title}}</a>
                                                                                                        </li>

                                                                                                    @endforeach
                                                                                                @endif
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3s">
                                                                                <fieldset class="date">
                                                                                    <dl
                                                                                            class="disabled dropdown dropdown-abc form-control tx-general tx-hide sample">
                                                                                        <dt>
                                                                                            <a href="javascript:void(0)"><span
                                                                                                        class="status status-language">@if($eloquent_userAttribute){{trans('fr_home.'.$eloquent_userAttribute->status)}}@else {{trans('fr_home.public')}} @endif </span><i
                                                                                                        class="fa fa-caret-down fa-hide-show"
                                                                                                        aria-hidden="true"></i></a>
                                                                                        </dt>
                                                                                        <dd>
                                                                                            <ul>
                                                                                                <li value="1"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="1">{{trans('fr_home.public')}}</a>
                                                                                                </li>
                                                                                                <li value="2"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="2">{{trans('fr_home.private')}}</a>
                                                                                                </li>
                                                                                                {{--<li value="3"><a
                                                                                                            href="javascript:void(0)"
                                                                                                            data-status="3">{{trans('fr_home.friends')}}</a>
                                                                                                </li>--}}
                                                                                            </ul>
                                                                                        </dd>
                                                                                    </dl>
                                                                                </fieldset>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row btt btt-ttcn">
                                                                            <button type="submit"
                                                                                    class="btn btn-default btt-save-ttcn"
                                                                                    data-url="{{Helper::url('profile/edit')}}"
                                                                                    data-token="{{csrf_token()}}">{{trans('fr_home.save')}}
                                                                            </button>
                                                                            <button type="cancel"
                                                                                    class="btn btn-default btt-cancel-ttcn">{{trans('fr_home.cancel')}}
                                                                            </button>
                                                                        </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @elseif(isset($user_attribute_other) && count($user_attribute_other))
                                                    @foreach($user_attribute_other as $item)
                                                        @if($item->values)
                                                            <!-- Nation -->
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3 prf-res-3 old-value">{{trans('fr_home.'.$attribute_name[$item->attribute_id])}}</div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-6 prf-res-6">
                                                                        <fieldset class="date">
                                                                            <dl
                                                                                    class="country-sample disabled dropdown dropdown-abc
                                                                     form-control tx-general tx-hide sample">

                                                                                <dt>
                                                                                    <a href="#">
                                                                                        @if($item->attribute_id == 5)
                                                                                            <span>@if($eloquent_country = $item->joinCountry()->first()){{$eloquent_country->Country}}@endif</span>
                                                                                        @elseif($item->attribute_id == 6)
                                                                                            <span>@if($eloquent_country = $item->joinCity()->first()){{$eloquent_country->subdivision_1_name}}@endif</span>
                                                                                        @elseif($item->attribute_id == 7)
                                                                                            <span>@if($eloquent_country = $item->joinCity()->first()){{$eloquent_country->City}}@endif</span>
                                                                                        @elseif($item->attribute_id == 8)
                                                                                            <span>@if($eloquent_country = $item->joinLanguage()->first()){{$eloquent_country->title}}@endif</span>
                                                                                        @else
                                                                                            <span>{{$item->values}}</span>

                                                                                        @endif

                                                                                        <i class="fa fa-caret-down fa-hide-show"
                                                                                           aria-hidden="true"></i></a>
                                                                                </dt>

                                                                            </dl>
                                                                        </fieldset>
                                                                    </div>
                                                                </div>


                                                            @endif
                                                        @endforeach

                                                    @endif
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <div id="change-password" class="tab-pane fade f-profile">
                                <div class="thaydoimatkhau">
                                    <div class="pf-content clearfix nano-content">
                                        <!-- Thay đổi mật khẩu -->
                                        <article class="tdmk">
                                            <div class="pf-title-page" data-item="a1">
                                                <h5><i class="fa fa-lock icon-tt-prf"></i>
                                                {{trans('fr_home.change_password')}}
                                                <!--<i class="fa fa-pencil edit-tdmk"></i>-->
                                                </h5>
                                            </div>
                                            {!! Form::open(['url' => \Helper::url('profile/editPassword'), 'id' => 'personal-editpassword']) !!}
                                            <div class="row">
                                                <div class="col-sm-12 form-message"><span class="form-return"></span>
                                                </div>
                                                <div class="col-md-8 col-md-offset2 clearfix">
                                                    <!-- Mật khẩu hiện tại -->
                                                    <div class="row prf-ipu-pass">
                                                        <div class="col-md-4 col-sm-4 col-xs-5">{{trans('fr_home.current_password')}}</div>
                                                        <div class="col-md-8 col-sm-8 col-xs-7">
                                                            <input type="password"
                                                                   class="form-control tx-general tx-hide prf-ip-pass bg-tr"
                                                                   placeholder="*********" id="current_password"
                                                                   name="current_password"/>
                                                        </div>
                                                    </div>
                                                    <!-- Mật khẩu mới -->
                                                    <div class="row prf-ipu-pass">
                                                        <div class="col-md-4 col-sm-4 col-xs-5">{{trans('fr_home.new_password')}}</div>
                                                        <div class="col-md-8 col-sm-8 col-xs-7">
                                                            <input type="password"
                                                                   class="form-control tx-general tx-hide prf-ip-pass bg-tr"
                                                                   placeholder="*********" id="password"
                                                                   name="password"/>
                                                        </div>
                                                    </div>
                                                    <!-- Nhập lại mật khẩu mới -->
                                                    <div class="row prf-ipu-pass">
                                                        <div class="col-md-4 col-sm-4 col-xs-5">{{trans('fr_home.confirm_new_password')}}</div>
                                                        <div class="col-md-8 col-sm-8 col-xs-7">
                                                            <input type="password"
                                                                   class="form-control tx-general tx-hide prf-ip-pass bg-tr"
                                                                   placeholder="*********"
                                                                   id="confirm_password"
                                                                   name="confirm_password"/>
                                                        </div>
                                                    </div>

                                                    <div class="row btt btt-tdmk prf-btt-pass">
                                                        <button type="submit"
                                                                class="btn btn-default btt-save-tdmk btt-save-change">{{trans('fr_home.save_change')}}
                                                        </button>
                                                        <button type="reset"
                                                                class="btn btn-default btt-cancel-tdmk">{{trans('fr_home.cancel')}}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <div id="buddy" class="tab-pane fade">
                                <h3>Menu 2</h3>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam.</p>
                            </div>
                            <!-- profile-content-3 -->
                            <div id="install" class="tab-pane fade">
                                <h3>{{trans('fr_home.setting')}}</h3>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam.</p>
                            </div>
                            <div id="notice" class="tab-pane fade">
                                <h3>{{trans('fr_home.notification')}}</h3>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cModalChangeAvatar" class="modal fade" role="dialog">
        <div class="modal-dialog centerDiv">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{{trans('fr_home.create_profile_picture')}}</h4>
                </div>
                <div class="modal-body">
                    <button style="margin: 10px 0; background-color: rgba(255, 255, 255, 0.1);" id="_pf-choose-image"><i
                                class="fa fa-picture-o"
                                aria-hidden="true"></i>{{trans('fr_home.choose_image')}}
                    </button>
                    <div class="image-editor">
                        <input type="file" class="cropit-image-input" style="display: none;" id="upload_avatar">
                        <div class="_pfcenter-parent" style="overflow: hidden;">
                            <div class="_pfcenter">
                                <div class="cropit-preview">
                                </div>
                            </div>
                        </div>
                        <div class="controls-wrapper">
                            <div class="rotation-btns">
                                <button class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                <button class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                            </div>
                            <div class="slider-wrapper">
                                <span class="icon icon-image small-image"><i class="fa fa-picture-o"
                                                                             aria-hidden="true"></i></span>
                                <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
                                <span class="icon icon-image large-image"><i class="fa fa-picture-o"
                                                                             aria-hidden="true"></i></span>
                            </div>
                        </div>

                    </div>
                    <div class="button-editor-img"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-popup-avatar"
                            data-dismiss="modal">{{trans('fr_home.close')}}</button>
                    <button type="button" class="btn btn-default export">{{trans('fr_home.save_changes')}}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js_wrapper')
    <script type="text/javascript">
        $('body').attr('id', 'bg-profile');
    </script>
    @include('default::modules.frontend.profile.profile_js')
@stop

