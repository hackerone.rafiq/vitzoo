<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="side-body">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">Vitzoo</a>
            </div>
            <ul class="nav navbar-nav navbar-right chat-menu">
                <li class="chat">
                    <a class="btn btn-danger btn-chat" href="./chat.html"><i class="fa fa-comment"
                                                                             aria-hidden="true"></i>Chat</a>
                    <a class="res-chat-tk" href="./chat.html"><i class="fa fa-comment" aria-hidden="true"></i></a>
                </li>
                <li class="tk">
                    <button type="button" class="btn btn-danger btn-taikhoan btn-tk-res"><i class="fa fa-user"></i>Tài
                        Khoản
                    </button>
                    <i class="fa fa-user btn-taikhoan res-chat-tk"></i>
                    <div class="ct-taikhoan">
                        <ul>
                            <li class="btn-bre-btm"><a href="#"><i class="fa fa-user"></i>Trang cá nhân</a></li>
                            <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Đăng xuất</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="side-menu-container">
            <ul class="nav navbar-nav navbar-right bg-color">
                <li class="paddingtop-responsive active">
                    <a href="index.html">Trang chủ</a>
                </li>
                <li>
                    <a href="about.html">Giới thiệu</a>
                </li>
                <li>
                    <a href="service.html">Dịch vụ</a>
                </li>
                <li>
                    <a href="faq.html">Hướng dẫn</a>
                </li>
                <li>
                    <a href="contact.html">Liên hệ</a>
                </li>
            </ul>
        </div>
    </div>
</nav>