<?php
/*    var_dump(Sentinel::check());
*/ ?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="side-body">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{Helper::url()}}">Vitzoo</a>
            </div>
            @if(!isset($sentinel) || empty($sentinel))
                <ul class="nav navbar-nav navbar-right chat-menu">
                    <li class="chat">
                        <a class="btn btn-danger" href="{{Helper::url('chat')}}">Chat</a>
                    </li>
                </ul>
            @else
                <ul class="nav navbar-nav navbar-right chat-menu">

                    {{--<li class="tk dropdown add-menu class-count-notice">
                        <a class="btn btn-danger btn-chat btn-notifi btn-bell-notifi" data-href="{{Helper::url('chat/loadNotification')}}" href="javascrip:void(0)" style="padding: 0 30px;"><i class="fa fa-bell"
                                                                                  aria-hidden="true"></i>
                            --}}{{--<span class="count-notifi">1</span>--}}{{--
                        </a>
                        <div class="dropdown-menu animated notifications notifi-box" style="left: 0;">
                            <div class="topnav-dropdown-header">
                                <span>{{trans('fr_home.notifications')}}</span>

                            </div>
                            <div class="scroll-pane has-scrollbar notice-nano">
                                <ul class="media-list scroll-content nano-content" tabindex="0" style="right: -17px;" data-url="{{Helper::url('chat/updateIsClick')}}">

                                </ul>
                            </div>
                        </div>
                    </li>--}}
                    <li class="chat class-count-message">
                        <a class="btn btn-danger btn-chat" href="{{Helper::url('chat')}}"><i class="fa fa-comment"
                                                                                             aria-hidden="true"></i>Chat</a>
                    </li>
                    <li class="tk">
                        <button type="button" class="btn btn-danger btn-taikhoan btn-tk-res truncate"><i
                                    class="fa fa-user"></i>{{$sentinel->last_name.' '.$sentinel->first_name}}
                        </button>
                        <i class="fa fa-user btn-taikhoan res-chat-tk"></i>
                        <div class="ct-taikhoan">
                            <ul>
                                <li class="btn-bre-btm"><a href="{{Helper::url('profile/'.$sentinel->id.'')}}"><i
                                                class="fa fa-user"></i>{{trans('fr_home.profile')}}</a></li>
                                <li><a href="{{Helper::url('logout')}}"><i class="fa fa-sign-out"
                                                                           aria-hidden="true"></i>{{trans('fr_home.sign_out')}}
                                    </a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            @endif
        </div>
        <div class="side-menu-container">
            <ul class="nav navbar-nav navbar-right bg-color">
                <li class="paddingtop-responsive active">
                    <a href="{{Helper::url()}}">{{trans('fr_home.home')}}</a>
                </li>
                <li class="about">
                    <a href="{{Helper::url('about')}}">{{trans('fr_home.about_us')}}</a>
                </li>
                {{--<li class="service">
                    <a href="{{Helper::url('service')}}">{{trans('fr_home.service')}}</a>
                </li>--}}
                <li class="faq">
                    <a href="{{Helper::url('faq')}}">{{trans('fr_home.guide')}}</a>
                </li>
                <li class="contact">
                    <a href="{{Helper::url('contact')}}">{{trans('fr_home.contact')}}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
