@extends($theme_layout)

@section('css_wrapper')
    @include('default::modules.admin.users.list_css')
@endsection

@section('content')
    <section class="content-header">
        <h1>{{ trans('admin.user_listing') }}</h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                </a>
            </li>
            <li class="active"><a href="/admin/user/list">{{ trans('admin.user_listing') }}</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if(session('action_success'))
                    <div class="callout callout-success">
                        {{session('action_success')}}
                    </div>
                @endif
                <div class="box box-info">
                    <div class="box-body">
                        <table id="user-list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="15px" class="text-center no-sort">
                                        <input type="checkbox" id="bulkDelete" class="minimal" />
                                    </th>
                                    <th class="no-sort">{{trans('user.avatar')}}</th>
                                    <th>{{ trans('user.email') }}</th>
                                    <th>{{ trans('user.first_name') }}</th>
                                    <th>{{ trans('user.last_name') }}</th>
                                    <th class="no-sort">{{ trans('user.role') }}</th>
                                    <th class="no-sort">{{ trans('site.created_date') }}</th>
                                    <th class="no-sort">{{ trans('site.status') }}</th>
                                    <th class="no-sort">{{ trans('site.action') }}</th>
                                </tr>
                            </thead>

                            <tbody>
                            @if (count($users))
                                @foreach ($users as $user)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>

                            @if($admin->hasAnyAccess(['admin.user_update','admin.user_delete']))
                            <tfoot>
                                <tr>
                                    <td>
                                        @if($admin->hasAnyAccess('admin.user_delete'))
                                            <button id="delete-checked" class="btn btn-danger">{{ trans('site.delete_ticked_users') }}</button>
                                        @endif
                                        @if($admin->hasAccess('admin.user_update'))
                                            <button id="create-new" class="btn btn-success" data-url="{{ url('admin/users/create_new') }}">
                                                {{ trans('site.create_new_user') }}
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('js_wrapper')
    @include('default::modules.admin.users.list_js')
@stop