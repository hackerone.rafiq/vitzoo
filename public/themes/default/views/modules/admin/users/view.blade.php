<div class="box box-widget widget-user-2">
    <div class="widget-user-header bg-aqua-active">
        <div class="widget-user-image">
            <img class="img-circle" src="" alt="{{$user->full_name}}'s avatar">
        </div>
        <h3 class="widget-user-username">{{$user->username}}</h3>
        <h5 class="widget-user-desc">{{$user->roles()->first()->name}}</h5>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-4 border-right">
                <div class="description-block">
                    <h5 class="description-header">3,200</h5>
                    <span class="description-text">POSTS</span>
                </div>
            </div>
            <div class="col-sm-4 border-right">
                <div class="description-block">
                    <h5 class="description-header">13,000</h5>
                    <span class="description-text">FOLLOWERS</span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="description-block">
                    <h5 class="description-header">35</h5>
                    <span class="description-text">REVIEWS</span>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li><a>Email : <strong>{{$user->email}}</strong></a></li>
            <li><a>First name : <strong>{{$user->first_name}}</strong></a></li>
            <li><a>Last name : <strong>{{$user->last_name}}</strong></a></li>
            <li><a>Phone : <strong>{{$user->mobile}}</strong></a></li>
            <li><a>Address : <strong>{{$user->address}}</strong></a></li>
            <li><a>Country : <strong>{{$user->country()->first()->Country or ''}}</strong></a></li>
            <li><a>State : <strong>{{$user->city()->first()->subdivision_1_name or ''}}</strong></a></li>
            <li><a>City : <strong>{{$user->city()->first()->City or ''}}</strong></a></li>
            <li><a>Group : <strong>{{$user->roles()->first()->name or ''}}</strong></a></li>
        </ul>
    </div>
</div>