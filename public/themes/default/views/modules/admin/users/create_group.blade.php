<link rel="stylesheet" href="/themes/default/plugins/simplecrop/css/style.css"/>
<style type="text/css">
    .clear {
        clear: both;
    }
</style>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li class="pull-left header text-blue"><i
                    class="fa fa-asterisk"></i> {{empty($group) ? trans('site.create_new_group') : trans('site.update_user_group')}}
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="normal-user">
            {!! Form::open(['url' => empty($group) ? Helper::url('admin/user/group/create') : Helper::url('admin/user/group/edit'), 'id' => 'create-group-form']) !!}
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{$title}}<i class="text-red">*</i></label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="title" placeholder="Group Title"
                                   value="{{!empty($group) ? $group->display_name : '' }}" required/>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">{{trans('site.role_permissions')}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                @foreach($perms as $perm)
                                    <div class="input-group col-md-6">
                                        <label>
                                            <input type="checkbox" class="minimal"
                                                   name="permission[{{$perm->name}}]"
                                                   {{!empty($group) && !empty($group->perms()->select('id')->where('name', $perm->name)->first())?'checked' : ''}}
                                                   value="{{$perm->id}}"/>
                                            {{trans('site.'.$perm->name.'')}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-md-12 text-center">
                    @if(!empty($group))
                        <input type="hidden" value="{{$group->_id}}" name="group_id"/>
                    @endif
                    <button type="submit" id="submit-form"
                            class="btn btn-success">{{empty($group) ? trans('site.add_group') : trans('site.update_group')}}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('#create-group-form').validate({
            ignore: [],
            rules: {
                title: "required"
            },
            messages: {
                title: "Please enter group title"
            },
            submitHandler: function (form) {
                var submit_url = $('#create-group-form').attr('action');
                $.ajax({
                    type: "POST",
                    url: submit_url,
                    data: $(form).serialize(),
                    dataType: "JSON",
                    success: function (result) {
                        if (result.success == false) {
                            modalDisplay("{{ trans('site.warning') }}", 'warning', result.message);
                        }
                        else
                            window.location.href = 'group';
                    },
                });
                return false;
            }
        });

    });
</script>