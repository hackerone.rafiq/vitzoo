@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('admin') }}">{{empty($user)? trans('site.add_new_user'): trans('site.edit_user')}}</a>
    </div>
    <div class="login-box-body">
        {!! Form::open(['url' => empty($user)?Helper::url('admin/users/new'):Helper::url('admin/users/edit'), 'id' => 'regForm']) !!}
        <div class="form-group has-feedback">
            <label for="email">{{ trans('site.email') }}</label>
            <input id="email" type="email" name="email" class="form-control"
                   placeholder="{{ trans('frontend::login.email') }}"
                   @if(!empty($user)) value="{{$user->email}}" @endif>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        @if(!empty($user))
            <input name="id" type="number" hidden value="{{$user->id}}">
        @endif
        <div class="form-group has-feedback">
            <label for="first_name">{{ trans('site.first_name') }}</label>
            <input id="first_name" type="text" name="first_name" class="form-control" placeholder="first name"
                   @if(!empty($user)) value="{{$user->first_name}}" @endif>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="last_name">{{ trans('site.last_name') }}</label>
            <input id="last_name" type="text" name="last_name" class="form-control" placeholder="last name"
                   @if(!empty($user)) value="{{$user->last_name}}" @endif>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="mobile">{{ trans('site.phone') }}</label>
            <input id="mobile" type="text" name="mobile" class="form-control" placeholder="phone number"
                   @if(!empty($user)) value="{{$user->mobile}}" @endif>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="address">{{ trans('site.address') }}</label>
            <input id="address" type="text" name="address" class="form-control" placeholder="address"
                   @if(!empty($user)) value="{{$user->address}}" @endif>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="countries">{{ trans('site.country') }}</label>
            <select id="countries" name="country_id" class="form-control">
                <option selected hidden disabled>Select country</option>
                @foreach($countries as $country)
                    <option @if(!empty($user)) @if($user->country_id===$country->id) selected
                            @endif @endif value="{{$country->id}}">{{$country->Country}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group has-feedback">
            <label for="states">{{ trans('site.state') }}</label>
            <select id="states" name="state_id" class="form-control">
                <option hidden disabled>Select state</option>

            </select>
        </div>

        <div class="form-group has-feedback">
            <label for="cities">{{ trans('site.city') }}</label>
            <select id="cities" name="city_id" class="form-control">
                <option hidden disabled>Select city</option>

            </select>
        </div>
        <div class="form-group has-feedback">
            @if(!empty($user))
                @if(!$user->inRoles(['owner','admin']))
                    <label for="role_id">{{trans('login.user_role') }}</label>
                    <select id="role_id" class="form-control" name="role_id">
                        <option selected hidden disabled>{{trans('login.choose_role') }}</option>
                        @foreach($roles as $role)
                            @if($role->slug != 'owner' && $role->slug != 'admin'))
                            <option value="{{$role->id}}"
                                    @if($user->inRoles($role->slug) || $role->slug==='user') selected @endif>{{$role->name}}</option>
                            @endif
                        @endforeach
                    </select>
                @endif
            @else
                <label for="role_id">{{trans('login.user_role') }}</label>
                <select id="role_id" class="form-control" name="role_id">
                    <option selected hidden disabled>{{trans('login.choose_role') }}</option>
                    @foreach($roles as $role)
                        @if($role->slug != 'owner')
                            @if($role->slug !== $admin->roles()->first()->slug)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @elseif(($role->slug ==='admin' && $admin->hasAccess('admin.add_new_admin')) || $admin->roles()->first()->slug == 'owner')
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endif
                        @endif
                    @endforeach
                </select>
            @endif
        </div>
        @if(!empty($user))
            @if(!$user->hasAccess('admin.access'))
        <div class="form-group has-feedback">
            <label for="status">{{ trans('site.status') }}</label>
            <select id="status" name="status" class="form-control">
                <option selected hidden disabled></option>
                <option value="0">Unactivated</option>
                <option value="1">Activated</option>
            </select>
        </div>
            @endif
            @else
            <div class="form-group has-feedback">
                <label for="status">{{ trans('site.status') }}</label>
                <select id="status" name="status" class="form-control">
                    <option selected hidden disabled>Choose a status</option>
                    <option value="0">Unactivated</option>
                    <option value="1">Activated</option>
                </select>
            </div>
        @endif
        <div class="form-group has-feedback">
            <label for="password">{{trans('login.password') }}</label>
            <input id="password" type="password" name="password" class="form-control"
                   placeholder="{{ trans('login.password') }}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="password_confirmation">{{trans('login.password_confirmation') }}</label>
            <input id="password_confirmation" type="password" name="password_confirmation" class="form-control"
                   placeholder="{{ trans('login.password') }}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                </div>
            </div>
            <div class="col-xs-4">
                <button type="submit"
                        class="btn btn-primary btn-block btn-flat">{{ trans('frontend::login.signin') }}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#regForm').on('submit', function () {
            event.preventDefault();
            var url = $(this).attr('action');
            sendFormByAjax(url, '{{Helper::url('admin/users/list')}}', $(this).serialize());
        });
        var countries = $('#countries');
        var states = $('#states');
        @if(!empty($user))
        if (countries.val() != '') {

            var current = '';
            @if(!empty($user)) current = '{{$user->state_id}}';
                    @endif
            var data = {id: countries.val(), current: current};
            loadCountryData(data, 'states', '{{Helper::url('admin/countrydata')}}');
        }
                @if($user->state_id && $user->city_id)
        var current = '';
        @if(!empty($user)) current = '{{$user->city_id}}';
        @endif
        if ($(this).val != '')
            var data = {state_id: {{$user->state_id}}, current: current};
        else var data = {id: countries.val(), current: current};
        loadCountryData(data, 'cities', '{{Helper::url('admin/countrydata')}}');
        @endif
    @endif

    countries.on('change', function () {
            states.html('<option hidden disabled>Select state</option>');
            $('#cities').html('<option hidden disabled>Select city</option>');
            var data = {id: $(this).val()};
            loadCountryData(data, 'states', '{{Helper::url('admin/countrydata')}}');
        });

        states.on('change', function () {
            $('#cities').html('<option hidden disabled>Select city</option>');
            if ($(this).val != '')
                var data = {state_id: $(this).val()};
            else var data = {id: $('#countries').val()};
            loadCountryData(data, 'cities', '{{Helper::url('admin/countrydata')}}');
        });
    });

</script>