<script src="{{Helper::getThemePlugins('datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{Helper::getThemePlugins('datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
    function remove_groups(ids_value) {
        $.ajax({
            type: "POST",
            url: "group/delete",
            data: {ids: ids_value, _token: '{{csrf_token()}}'},
            dataType: "JSON",
            beforeSend: function () {
                $('#loading').show();
            },
            success: function(result) {
                    $('#loading').hide();
                if (result.success == false) {
                    // open modal error message
                    modalDisplay( "{{trans('site.error') }}",'error',result.message);
                    return false;
                }
                // redirect to group list after delete
                window.location.replace("group");
            },
        });
    }



    function delete_group_popup(ids, message) {
        var dialog = new Messi(
            message,
            {
                modal: true,
                modalOpacity: 0.5,
                title: '{{trans('site.confirm')}}',
                titleClass: 'warning',
                buttons: [
                    {id: 0, label: 'Yes', val: 'Y'},
                    {id: 1, label: 'No', val: 'N'}
                ],
                callback: function(val) {
                    if (val == 'Y') {
                        // sent ajax to remove groups
                        remove_groups(ids);
                    }
                }
            }
        );
    }

    $(document).ready(function () {
        $("#user-list").DataTable({
            ordering: false
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('.group-delete').click(function(event){
            event.preventDefault();
            id = $(this).attr('id');
            delete_group_popup(id, '{{ trans('site.delete_this_group') }}');
        });
        $("#bulkDelete").on('ifClicked', function() {
            var status = this.checked;
            if (!status) {
                $(".deleteRow").iCheck('check');
            } else {
                $(".deleteRow").iCheck('uncheck');
            }
        });

        $('#delete-checked').click(function (event) {
            event.preventDefault();
            if ($('.deleteRow:checked').length > 0) {
//            var val=[];
//            $('.deleteRow:checked').each(function(){
//                val[i] = this.val();
//            });
                var val = $('.deleteRow:checked').map(function (_, el) {
                    return $(el).val();
                }).get();
                val = val.toString();
                delete_group_popup(val, '{{ trans('site.delete_checked_group') }}');
            }
            else {
                modalDisplay("{{ trans('site.error') }}",'error','{{trans('site.no_groups_ticked')}}');
            }
        });

        $('#create-new, .group-view, .group-edit').on('click', function (event) {
            event.preventDefault();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (e) {
                    $('#loading').hide();
                    if(e.success == false){
                        modalDisplay("{{ trans('site.warning') }}",'warning', e.message);
                        return false;
                    }
                    $('#pop-up .modal-content').html(e);
                    $('#pop-up').modal('show');

                }
            });
        });

    });
</script>