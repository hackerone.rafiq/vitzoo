<style>
    .ui-dialog { z-index: 1501 !important; }
    .avatar-list { list-style: none; padding-left: 0; overflow: hidden }
    .avatar-list li { float: left; margin: 0 1px; position: relative }
    .avatar-list img { margin: 0 3px 5px 0; padding: 2px; border: 1px solid #CCC; cursor: pointer }
    .avatar-list li .fa-times { position: absolute; top: 0; right: 0; cursor: pointer; color: red }
    .avatar-img { width: 43px; height: auto }
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
    }
    .cropit-preview-image-container {
        cursor: move;
    }
    .image-size-label {
        margin-top: 10px;
    }
</style>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li class="active"><a href="#tab_info" data-toggle="tab">{{trans('user.user_info')}}</a></li>
        <li><a href="#tab_avatar" data-toggle="tab">{{trans('user.avatar')}}</a></li>
        <li class="pull-left header">
            <i class="fa fa-th"></i>
            @if (!isset($user)) Thêm thành viên mới @else Thay đổi thông tin thành viên @endif
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_info">
            {!! Form::open(['url' => Helper::url('admin/users/do_update'), 'class' => 'form-horizontal', 'id' => 'memberForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">
                            Email
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" id="email" type="email" name="email" value="{{ $user->email or '' }}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">
                            Password
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" id="password" name="password" />
                            @if (isset($user))
                                <p class="text-blue">Để trống nếu bạn không thay đổi password!</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="col-sm-3 control-label">
                            First Name
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" id="first_name" name="first_name" value="{{ $user->first_name or '' }}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-sm-3 control-label">
                            Last Name
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input class="form-control" id="last_name" name="last_name" value="{{ $user->last_name or '' }}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-sm-3 control-label">Số điện thoại</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="mobile" name="mobile" value="{{ $user->mobile or '' }}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-sm-3 control-label">Giới tính</label>
                        <div class="col-sm-9">
                            {!! Form::select('gender',
                                ['male' => 'Male', 'female' => 'Female', 'unknow' => 'Unknow'],
                               (isset($user)) ? $user->gender : 'male',
                               ['class' => 'form-control', 'id' => 'status']
                            ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-3 control-label">Địa chỉ</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="address" name="address" value="{{ $user->address or '' }}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="language" class="col-sm-3 control-label">
                            Quốc gia
                        </label>
                        <div class="col-sm-9">
                            {!! Form::select('country',
                                ['vi' => 'Việt Nam'],
                               (isset($user)) ? $user->country : 'vi',
                               ['class' => 'form-control', 'id' => 'country']
                            ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="b_day" class="col-sm-3 control-label">
                            Sinh nhật
                        </label>
                        <div class="row col-sm-9">
                            <div class="col-sm-4">
                                Ngày
                                {!! Form::select('b_day',
                                [
                                    '01'    => '01',
                                    '02'    => '02',
                                    '03'    => '03',
                                    '04'    => '04',
                                    '05'    => '05',
                                    '06'    => '06',
                                    '07'    => '07',
                                    '08'    => '08',
                                    '09'    => '09',
                                    10      => 10,
                                    11      => 11,
                                    12      => 12,
                                    13      => 13,
                                    14      => 14,
                                    15      => 15,
                                    16      => 16,
                                    17      => 17,
                                    18      => 18,
                                    19      => 19,
                                    20      => 20,
                                    21      => 21,
                                    22      => 22,
                                    23      => 23,
                                    24      => 24,
                                    25      => 25,
                                    26      => 26,
                                    27      => 27,
                                    28      => 28,
                                    29      => 29,
                                    30      => 30,
                                    31      => 31
                                ],
                               (!empty($user) && !empty($birthday)) ? $birthday[0] : '15',
                               ['class' => 'form-control', 'id' => 'b_day']
                            ) !!}
                            </div>
                            <div class="col-sm-4">
                                Tháng
                                {!! Form::select('b_month',
                                    [
                                        '01'    => '01',
                                        '02'    => '02',
                                        '03'    => '03',
                                        '04'    => '04',
                                        '05'    => '05',
                                        '06'    => '06',
                                        '07'    => '07',
                                        '08'    => '08',
                                        '09'    => '09',
                                        10      => 10,
                                        11      => 11,
                                        12      => 12,
                                    ],
                                   (!empty($user) && !empty($birthday)) ? $birthday[1] : '06',
                                   ['class' => 'form-control', 'id' => 'b_month']
                                ) !!}
                            </div>
                            <div class="col-sm-4">
                                Năm
                                {!! Form::select('b_month',
                                    $years,
                                   (isset($user)) ? end($birthday) : '',
                                   ['class' => 'form-control', 'id' => 'b_month']
                                ) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group" class="col-sm-3 control-label">
                            Nhóm
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-9">
                            {!! Form::select('group',
                                $groups,
                               (isset($user)) ? $user->roles()->first()->id : 3,
                               ['class' => 'form-control', 'id' => 'group']
                            ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-sm-3 control-label">
                            Trạng thái
                            <span class="text-red">*</span>
                        </label>
                        <div class="col-sm-9">
                            {!! Form::select('status',
                                ['pending' => 'Pending', 'active' => 'Active', 'unactive' => 'Unactive'],
                               (isset($user)) ? $user->status : 'pending',
                               ['class' => 'form-control', 'id' => 'status']
                            ) !!}
                        </div>
                    </div>
                    <div class="form-group text-center">
                        @if (isset($user)) <input type="hidden" name="id" value="{{ $user->id }}" /> @endif
                        <input type="hidden" id="avatar" name="avatar" value="{{ $user->avatar or '' }}" />
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="tab-pane" id="tab_avatar">
            <div class="row box-body">
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="image-editor">
                            <input type="file" class="cropit-image-input">
                            <div class="cropit-preview"></div>
                            <div class="image-size-label">
                                Resize image
                            </div>
                            <input type="range" class="cropit-image-zoom-input" />
                            <button class="rotate-ccw">Rotate counterclockwise</button>
                            <button class="rotate-cw">Rotate clockwise</button>

                            <button class="export">Export</button>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ul class="avatar-list">
                            <li>
                                <img src="{{ url('media/avatars/no-avatar.jpg') }}" data-url="" class="avatar-img" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ Helper::getThemeJs("jquery.validate.min.js") }}"></script>
<script type="text/javascript" src="{{ Helper::getThemeJs("jquery.cropit.js") }}"></script>
<script>
    /**
     * add iCheck style for checkbox
     */
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $(document).ready(function() {
        $('.image-editor').cropit({
            imageState: {
                src: '@if (!empty($user) && !empty($user_avatar)) {{Helper::getAvatar($user->id.'/'.$user_avatar)}} @else {{url('media/avatars/no-avatar.jpg')}} @endif'
            }
        });

        $('.rotate-cw').click(function() {
            $('.image-editor').cropit('rotateCW');
        });

        $('.rotate-ccw').click(function() {
            $('.image-editor').cropit('rotateCCW');
        });

        $('.export').click(function() {
            var imageData = $('.image-editor').cropit('export');
            window.open(imageData);
        });
    });

    /**
     * event click on list avatars
     */
    $(document).on('click', '.avatar-img', function(e) {
        e.preventDefault();
        var img_url = $(this).attr('src');
        var data_url = $(this).attr('data-url');

        $('.avatar-img').removeClass('active');
        $(this).addClass('active');
        $('.picture-element-image').attr('src', img_url);
        $('#avatar').val(data_url);
    });

    /**
     * event click on remove avatar icon
     */
    $(document).on('click', '.remove-avatar', function(e) {
        e.preventDefault();
        var obj = $(this);
        var img_name = obj.prev().attr('data-url');
        var uid_id = '{{$user->id or csrf_token() }}';
        var avatar_display = $('.picture-element-image');

        new Messi(
            'Are you sure?',
            {
                modal: true,
                modalOpacity: 0.5,
                title: 'Confirmation',
                titleClass: 'warning',
                buttons: [
                    {id: 0, label: 'Yes', val: 'Y'},
                    {id: 1, label: 'No', val: 'N'}
                ],
                callback: function(val) {
                    if (val == 'Y') {
                        // sent ajax to remove
                        $.ajax({
                            type: "POST",
                            url: '{{ url('admin/users/avatar/remove') }}',
                            data: {avatar: img_name, uid: uid_id},
                            dataType: "JSON",
                            success: function () {
                                if (obj.prev().attr('src') == avatar_display.attr('src')) {
                                    avatar_display.attr('src', '{{ url('media/avatars/no-avatar.jpg') }}');
                                    $('#avatar').val('');
                                }
                                obj.parent().remove();

                                return false;
                            }
                        });
                    }
                }
            }
        );
    });

    /**
     * validate member form submit
     */
    $('#memberForm').validate({
        ignore: [],
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            @if (empty($user))
            password: {
                required: true,
                minlength: 6
            }
            @endif
        },
        messages: {
            first_name: {
                required: "Tên không được để trống!"
            },
            last_name: {
                required: "Họ không được để trống!"
            },
            email: {
                required: "Email không được để trống!"
            },
            @if (empty($user))
            password: {
                required: "Password không được để trống!"
            }
            @endif
        },
        submitHandler: function(form) {
            var obj = $(form);
            ajaxSubmit(obj.attr('action'), obj.serialize(), '{{Helper::url('admin/users/list')}}');
            return false;
        }
    });
</script>