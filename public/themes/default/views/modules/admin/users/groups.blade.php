@extends('default::modules.admin.layouts.master')

@section('css_wrapper')

@endsection

@section('content')
    @include('default::modules.admin.users.groups_css')
        <section class="content-header">
            <h1>{{ trans('site.group_list') }}</h1>
            <ol class="breadcrumb">
                <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                    </a>
                </li>
                <li class="active">{{ trans('site.group_list') }}</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @if(session('action_success'))
                        <div class="callout callout-success">
                            {{session('action_success')}}
                        </div>
                    @endif

                    <div class="box box-info">
                        <div class="box-body">
                            <table id="user-list" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="15px" class="text-center">
                                        <input type="checkbox" id="bulkDelete" class="minimal"/>
                                        @if($admin->hasAccess('admin.user_delete'))
                                            <button id="delete-checked"
                                                    class="btn btn-danger">{{ trans('site.delete_ticked_groups') }}</button>
                                        @endif
                                        @if($admin->hasAccess('admin.user_update'))
                                            <button id="create-new" class="btn btn-success"
                                                    data-url="{{Helper::url('admin/user/group/create')}}">
                                                {{ trans('site.create_new_group') }}
                                            </button>
                                        @endif
                                    </th>
                                    <th>{{ trans('site.title') }}</th>
                                    <th width="100px" class="text-center">{{ trans('site.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($groups))
                                    @foreach($groups as $group)
                                        <tr>
                                            <td class="text-center">
                                                <input class="deleteRow minimal" type="checkbox" name="group_id"
                                                       value="{{$group->id}}"/>
                                            </td>
                                            <td>
                                        <span class="{{(in_array($group->slug,['owner','admin']))?'text-red':(($group->slug=='mod')?'text-blue':"")}}">
                                            <strong>{{$group->name}}</strong>
                                        </span>
                                            </td>
                                            <td class="text-center">
                                        <span class="badge {{((!in_array($group->slug, ['owner', 'admin'])) && $admin->hasAccess('edit_group_permissions')) ? 'bg-light-blue group-edit' : 'bg-grey' }}"
                                              @if(!in_array($group->slug, ['owner', 'admin']))    data-url="{{\Helper::url('admin/user/group/edit')}}/{{$group->id}}
                                              @endif">
                                            {{ trans('site.edit') }}
                                        </span>&nbsp;&nbsp;
                                        <span class="badge {{(!in_array($group->slug, ['owner', 'admin', 'mod', 'user'])) ? 'bg-red group-delete' : 'bg-grey' }}"
                                              id="{{$group->id}}">
                                            {{ trans('site.delete') }}
                                        </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <div id="pop-up" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            </div>

        </div>
    </div>
@stop
@section('js_wrapper')
    @include('default::modules.admin.users.groups_js')
@stop
