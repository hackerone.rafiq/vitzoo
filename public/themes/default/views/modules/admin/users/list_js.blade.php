<script src="{{Helper::getThemePlugins('datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{Helper::getThemePlugins('datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#user-list");
        /**
         * generate datatables
         */
        table.DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ Helper::url('admin/users/list_search') }}",
            "dom": '<"top"li>rft<"bottom"p><"clear">',
            "columnDefs": [
                {
                    "targets": 'no-sort',
                    "orderable": false
                },
                /**
                 * render for checkbox column
                 */
                {
                    "targets": 0,
                    "className": "text-center",
                    "render": function (data) {
                        /**
                         * apply iCheck style for checkbox when render
                         */
                        $('input[type="checkbox"].minimal').iCheck({
                            checkboxClass: 'icheckbox_minimal-blue',
                            radioClass: 'iradio_minimal-blue'
                        });

                        return '<input class="deleteRow minimal" type="checkbox" name="user_id" value="'+ data + '" />';
                    }
                },
                /**
                 * render for avatar column
                 */
                {
                    "targets": 1,
                    "className": "text-center",
                    "render": function (data) {
                        return '<img src="' + data[0] + '" class="user-avatar" alt="' + data[1] + '" title="' + data[1] + '" />';
                    }
                },
                /**
                 * render created date column
                 */
                {
                    "targets": 6,
                    "className": "text-center",
                    "render": function (data) {
                        var d = data.date.slice(0, 10).split('-');
                        return d[2] + '-' + d[1] + '-' + d[0];
                    }
                },
                /**
                 * render for status column
                 */
                {
                    "targets": 7,
                    "className": "text-center",
                    "render": function (data) {
                        return '<span id="' + data[0] + '" class="btn label ' + data[1] + ' status">' + data[2] + '</span>';
                    }
                },
                /**
                 * render for actions column
                 */
                {
                    "targets": 8,
                    "className": "text-center",
                    "render": function (data) {
                        return '<span class="btn label bg-green user-view" data-url="{{ Helper::url('admin/users/details') }}/' + data + '">{{ trans('site.view') }}</span>' +
                                ' <span class="btn label {{Helper::statusClass('edit') }} edit" data-url="{{ Helper::url('admin/users/edit') }}/' + data + '">{{trans('site.edit')}}</span>' +
                                ' <span id="'+ data + '" class="btn label {{Helper::statusClass('delete') }} remove">{{trans('site.delete')}}</span>';
                    }
                }
            ]
        });

        /**
         * display create new user modal
         */
        table.on("click", '#create-new', function(e) {
            e.preventDefault();
            var obj = $(this);
            loadModalContent(obj);
            return false;
        });

        /**
         * display edit user modal
         */
        table.on("click", '.edit', function(e) {
            e.preventDefault();
            var obj = $(this);
            loadModalContent(obj);
            return false;
        });

        /**
         * change group status
         */
        table.on("click", '.status', function(e) {
            var obj = $(this);
            changeStatus("{{ Helper::url('admin/users/change_status') }}", obj);
            return false;
        });

        /**
         * open modal to confirm remove user(s)
         */
        function removeUsers(ids) {
            modalDelete(
                ids,
                'Xóa mục đã chọn',
                '{{ Helper::url('admin/users/remove') }}',
                false
            );
        }

        /**
         * delete a user
         */
        table.on("click", '.remove', function(e) {
            var id = $(this).attr('id');
            removeUsers(id);
            return false;
        });

        /**
         * delete all user has checked
         */
        table.on("click", '#delete-checked', function() {
            if ($('.deleteRow:checked').length > 0) {
                var ids = [];
                $('.deleteRow').each(function() {
                    if($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });
                var ids_value = ids.toString();
                removeUsers(ids_value);
            } else {
                /**
                 * open modal warning message
                 */
                modalWarning("Please choose group(s) to delete.");
            }
            return false;
        });

        /**
         * click to check all
         */
        table.on("ifClicked", '#bulkDelete', function() {
            var status = this.checked;
            if (!status) {
                $(".deleteRow").iCheck('check');
            } else {
                $(".deleteRow").iCheck('uncheck');
            }
        });

        $('.user-view').click(function () {
            url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (e) {
                    $('#loading').hide();
                    if (e.success == false) {
                        modalDisplay("{{ trans('site.warning') }}", 'warning', e.message);
                        return false;
                    }
                    $('#pop-up .modal-content').html(e);
                    $('#pop-up').modal('show');
                }
            });
        });
    });
</script>