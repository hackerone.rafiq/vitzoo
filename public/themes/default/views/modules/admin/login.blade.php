<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    {!! SEO::generate() !!}

    {!! Assets::css() !!}

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ Helper::url('admin') }}">Đăng nhập quản trị</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Hãy nhập đầy đủ thông tin vào form bên dưới!</p>
            {!! Form::open(['url' => Helper::url('admin/login'), 'id' => 'loginForm']) !!}
                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> Ghi nhớ
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Đăng nhập</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    {!! Assets::js() !!}

    <script>
        $(document).ready(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });

            $('#loginForm').on('submit', function() {
                var submit_url = $(this).attr('action');
                $.ajax({
                    url: submit_url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize(),
                    success: function(str) {
                        $('.form-group').removeClass('has-error');
                        if (!str.success) {
                            $('.login-box-msg').addClass('text-red').text(str.message);
                            $('#' + str.flag).parent().addClass('has-error');
                        } else {
                            $('.login-box-msg').addClass('text-green').text(str.message);
                            window.location.replace('{{ url('admin')  }}');
                        }
                        return false;
                    }
                });
                return false;
            });
        });
    </script>
</body>
</html>