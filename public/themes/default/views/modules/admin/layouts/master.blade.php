<!DOCTYPE html>
<html ng-app="VitzooAdmin">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="csrf-token" content="{{ csrf_token() }}" />

		{!! SEO::generate() !!}

		{!! Assets::css() !!}

		@yield('css_wrapper')

		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<script>

			var siteUrl = '{{ url('') }}';
		</script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			@include(Theme::getActive().'::modules.admin.partials.header')

			@include(Theme::getActive().'::modules.admin.partials.sidebar')

			<div class="content-wrapper">
				<div ng-view></div>

				@yield('content')
			</div>

			@include(Theme::getActive().'::modules.admin.partials.footer')
		</div>

		{!! Assets::js() !!}

		@yield('js_wrapper')
	</body>
</html>