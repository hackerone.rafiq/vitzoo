<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2016 <a href="http://www.vietprojectgroup.com">VITPR</a>.</strong> All rights reserved.
</footer>

<div class="modal fade">
    <div class="modal-dialog">
        <div class="box box-primary">
            <div class="modal-body"></div>
        </div>
    </div>
</div>