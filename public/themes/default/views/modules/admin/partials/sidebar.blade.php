<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Helper::getThemeImg('user2-160x160.jpg') }}" class="img-circle" alt="{{ $admin->fullname }}">
            </div>
            <div class="pull-left info">
                <p>{{ $admin->fullname }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> {{trans('user.online')}}</a>
            </div>
        </div>

        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>

        <ul class="sidebar-menu">
            <li class="header">{{trans('admin.navigation')}}</li>
            <li class="{{ Helper::activeSegments(3, '') }} treeview">
                <a href="{{ Helper::url('admin') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{trans('admin.dashboard')}}</span>
                </a>
            </li>

            <li class="{{ Helper::activeSegments(3, 'cms') }} treeview">
                <a href="{{ Helper::url('admin/cms') }}">
                    <i class="fa fa-trophy"></i> <span>{{trans('admin.cms_pages')}}</span>
                </a>
            </li>

            <li class="{{ Helper::activeSegments(3, 'users') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{trans('admin.user_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ Helper::activeRoutes('users/group') }}">
                        <a href="{{ Helper::url('admin/users/group') }}">
                            <i class="fa fa-circle-o text-red"></i> {{trans('admin.group_manage')}}
                        </a>
                    </li>
                    <li class="{{ Helper::activeRoutes('users/list') }}">
                        <a href="{{ Helper::url('admin/users/list') }}">
                            <i class="fa fa-circle-o text-yellow"></i> {{trans('admin.user_listing')}}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ Helper::activeSegments(3, 'chat') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{trans('admin.chat_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ Helper::activeRoutes('chat/rooms') }}">
                        <a href="{{ Helper::url('admin/chat/rooms') }}">
                            <i class="fa fa-circle-o text-red"></i> {{trans('admin.room_list')}}
                        </a>
                    </li>
                    <li class="{{ Helper::activeRoutes('chat/bots') }}">
                        <a href="{{ Helper::url('admin/chat/bots') }}">
                            <i class="fa fa-circle-o text-yellow"></i> {{trans('admin.bot_list')}}
                        </a>
                    </li>
                    <li class="{{ Helper::activeRoutes('chat/config') }}">
                        <a href="{{ Helper::url('admin/chat/config') }}">
                            <i class="fa fa-circle-o text-green"></i> {{trans('admin.config_chat')}}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ Helper::activeSegments(3, 'system') }} treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>{{trans('admin.config_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ Helper::activeRoutes('system/config') }}">
                        <a href="{{ Helper::url('admin/system/config') }}">
                            <i class="fa fa-circle-o text-red"></i> {{trans('admin.config_website')}}
                        </a>
                    </li>
                    <li class="{{ Helper::activeRoutes('system/cache') }}">
                        <a href="{{ Helper::url('admin/system/cache') }}">
                            <i class="fa fa-circle-o text-green"></i> {{trans('admin.config_cache')}}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ Helper::activeSegments(3, 'report') }} treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>{{trans('admin.report_manage')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                    <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                    <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                </ul>
            </li>

            <li>
                <a href="pages/mailbox/mailbox.html">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <small class="label pull-right bg-yellow">12</small>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>