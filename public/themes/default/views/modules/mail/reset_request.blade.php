You have requested to reset your account in global online studio, please the link click below to proceed.
@if(isset($email))
    <p>{{$email}}</p>
@endif
@if(isset($name))
    <p>{{$name}}</p>
@endif
<a href="{{Helpers::url('passwordreset').'?token='.$reset_token}}">Click here to change your password</a>
