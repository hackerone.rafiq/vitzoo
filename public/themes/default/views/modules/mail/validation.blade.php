Your email has been used to register in our site, please confirm if it really is you.
@if(isset($email))
    <p>{{$email}}</p>
@endif
@if(isset($name))
    <p>{{$name}}</p>
@endif
<a href="{{Helper::url('validate').'?cancel=false&token='.$registering_token}}">Click here to validate</a>
<p>Or</p>
<a href="{{Helper::url('validate').'?cancel=true&token='.$registering_token}}">Here to cancel</a>