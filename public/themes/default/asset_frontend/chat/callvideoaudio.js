var socketRtc = io.connect(helperRtc + '/easyrtc', {secure: true, query: "token=" + token});
var peers = {};
/* WebRTC call info */
var rtc = {};
/* Target user id (the other end) */
rtc.targetId = null;
/* Target easyrtc ids (the other end), when they accept the call */
rtc.currentCallee = [];
/* The room which the call is held on */
rtc.currentCallingRoom = null;
/* Some where someone calls */
rtc.otherRoom = null;
/* Call status, connected = true */
rtc.callConnected = false;
/* Call type, video/audio */
rtc.callType = 'video';
/* Return true when the call starts, false when the call ends (connection not required) */
rtc.doingCall = false;
/* Target easyrtc ids (the other end), all ids */
rtc.targetEasyrtc = [];
/* Current user easyrtc id */
rtc.selfId = null;
/* List of rooms that user joins */
rtc.roomJoined = new Set();
/* Sound element */
rtc.sound = {};
/* Notification sound file */
rtc.notification = urlGlobal + '/themes/default/asset_frontend/sound/shooting_start.mp3';
var inFullScreenMode = false;
var friend_list = (function () {
    var friends = {};
    $('#friend').find('li').each(function (i, v) {
        friends[v.dataset.id] = v.dataset.roomid;
    });
    return friends;
}());

socketRtc.on('connect', function () {
    /* Pre-load phone call sound */
    rtc.phoneSound = soundnotification(urlGlobal + '/themes/default/asset_frontend/sound/phone-calling.mp3', true, 50, true);
});

socketRtc.on('disconnect', function () {
});

socketRtc.on('joinOtherClient', function (data) {
    var room_id = data['room_id'];
    if (typeof data != 'undefined' && typeof room_id != 'undefined' && easyrtc.getRoomsJoined()[room_id] != true) {
        loadAjaxTabMessage({
            url: data['url'],
            id: data['user_id'],
            action: data['destination_id'],
            room_id: room_id,
            $tab_room: data['$tab_room'],
            status: 'no_socket_join'
        });
    }
});

socketRtc.on('toggle stream state', function (data) {
    /* Toggle video stream */
    if (data.type === 'video') {
        hideRemoteVideo(data.room_id, data.state)
    }
});

socketRtc.on('hang up', function (data) {
    !rtc.phoneSound.paused && rtc.phoneSound.pause();
    if (data.room_id === rtc.currentCallingRoom) {
        easyrtc.hangupAll();
        hangUp(rtc.currentCallingRoom);
        rtc.doingCall = false;
    }
});

socketRtc.on('sendAudio', function (data) {
    rtc.callType = data.type;
    if (data && sessionGlobal) {
        if (data.receiver_id === parseInt(sessionGlobal)) {
            rtc.otherRoom = data.room_id;
            if (!rtc.doingCall) {
                rtc.sound[data.room_id] = soundnotification(rtc.notification, true, localStorage['sound' + data.room_id]);
                rtc.targetId = data.initiator;
                rtc.currentCallingRoom = data.room_id;
            } else if (rtc.currentCallingRoom !== data.room_id) {
                socketRtc.emit('user busy', {id: sessionGlobal, friend_id: data.initiator, room_id: data.room_id});
            }
        } else {
            rtc.doingCall = true;
        }
    }
});

socketRtc.on('call accepted', function (data) {
    rtc.doingCall = true;
    !rtc.phoneSound.paused && rtc.phoneSound.pause();
    if (!rtc.callConnected) {
        if (rtc.sound[data.room_id] && !rtc.sound[data.room_id].paused) rtc.sound[data.room_id].pause();
        $('#cModalAcceptCall').modal('hide');
        // $('#callAcceptButton').trigger('click');
        easyrtc.closeLocalMediaStream(rtc.targetId);
        rtc.currentCallee = [];
    }
});

/****************************************/
/**********area easyrtc connect******/
/********author: vy le ****************/
/*************************************/

function connect() {
    easyrtc.enableDataChannels(false);
    easyrtc.enableVideo(false);
    easyrtc.enableAudio(false);
    easyrtc.setSocketUrl(":5555");
    easyrtc.setRoomOccupantListener(roomListener);
    easyrtc.connect("easyrtc.dataFileTransfer", loginSuccess, loginFailure);
}

function roomListener(roomName, occupants, isPrimary) {
    function buildDropDiv(easyrtcid) {
        if (easyrtc.getConnectStatus() === easyrtc.NOT_CONNECTED) {
        }
        var noDCs = {}; // which users don"t support data channels
        var fileSender = null;
    }
    
    for (var easyrtcid in occupants) {
        if (!peers[roomName]) {
            buildDropDiv(easyrtcid);
            peers[roomName] = true;
        }
    }
}
/* This runs when rtc call is connected */
easyrtc.setStreamAcceptor(function (easyrtcid, stream) {
    if (rtc.sound[rtc.currentCallingRoom] && !rtc.sound[rtc.currentCallingRoom].paused) rtc.sound[rtc.currentCallingRoom].pause();
    !rtc.phoneSound.paused && rtc.phoneSound.pause();
    setUpVideoCall(rtc.currentCallingRoom, stream);
    if (rtc.currentCallee.indexOf(easyrtcid) === -1)
        rtc.currentCallee.push(easyrtcid);
    rtc.callConnected = true;
});

/* This runs when rtc call is closed */
easyrtc.setOnStreamClosed(function (easyrtcid, stream, streamName) {
    !rtc.phoneSound.paused && rtc.phoneSound.pause();
    if (rtc.currentCallee.indexOf(easyrtcid) > -1) {
        hangUp(rtc.currentCallingRoom);
    }
});

/* This runs when rtc call is cancelled by the caller */
easyrtc.setCallCancelled(function (easyrtcid, explicitlyCancelled) {
    !rtc.phoneSound.paused && rtc.phoneSound.pause();
    rtc.doingCall = false;
    hangUp(rtc.currentCallingRoom);
    if (explicitlyCancelled) {
        $('#cModalAcceptCall').modal('hide');
        // console.log(easyrtc.idToName(easyrtcid) + " stopped trying to reach you");
    }
    else {
        // console.log("Implicitly called " + easyrtc.idToName(easyrtcid));
    }
});

/* This runs when target user get a call */
easyrtc.setAcceptChecker(function (easyrtcid, callback) {
    var acceptTheCall = function (wasAccepted, otherRoom) {
        if (wasAccepted && easyrtc.getConnectionCount() > 0) {
            easyrtc.hangupAll();
            /*setUpMirror();*/
        }
        if (wasAccepted && easyrtc.getConnectionCount() == 0) {
            
        }
        $('#cModalAcceptCall').modal('hide');
        if (rtc.sound[rtc.currentCallingRoom] && !rtc.sound[rtc.currentCallingRoom].paused) rtc.sound[rtc.currentCallingRoom].pause();
        if (wasAccepted) {
            easyrtc.enableVideo(true);
            easyrtc.enableAudio(true);
            easyrtc.initMediaSource(function () {
                rtc.doingCall = true;
                socketRtc.emit('call accepted', {room_id: rtc.currentCallingRoom, user_id: rtc.currentCallee});
                callback(wasAccepted, easyrtcid);
            }, function () {
            }, easyrtcid);
        } else {
            callback(wasAccepted, easyrtcid);
            if (otherRoom) {
                socketRtc.emit('hang up', {room_id: otherRoom, user_id: rtc.currentCallee});
                
            } else {
                socketRtc.emit('hang up', {room_id: rtc.currentCallingRoom, user_id: rtc.currentCallee});
            }
            
        }
    };
    
    /* Decline the call when closing the page */
    document.body.onbeforeunload = function () {
        acceptTheCall(false);
    };
    
    /* If user is in another call, decline the call */
    if (!rtc.doingCall) {
        /* Else show call request popup */
        var modal = $('#cModalAcceptCall');
        var user_name = modal.find('.rtc_user_name');
        user_name.html(easyrtc.idToName(easyrtcid) + ' ' + user_name.data('lang'));
        modal.modal({backdrop: 'static', keyboard: false});
        if (easyrtc.getConnectionCount() > 0) {
        }
        else {
            
        }
        document.getElementById("callAcceptButton").onclick = function () {
            acceptTheCall(true);
        };
        
        document.getElementById("callRejectButton").onclick = function () {
            acceptTheCall(false);
        };
    } else {
        /* If the call is in another browser tab, hide it */
        if (rtc.targetEasyrtc.indexOf(easyrtcid) !== -1) {
            $('#cModalAcceptCall').modal('hide');
            // acceptTheCall(true);
        } else {
            /* Else decline the call */
            acceptTheCall(false, rtc.otherRoom);
            // socket.emit('user busy', {user_id: sessionGlobal})
        }
    }
});
/* Remove debuggin notifications */
easyrtc.disconnect = easyrtc.debugPrinter = easyrtc.onError = function () {
};

easyrtc.enableDebug(false);


function loginSuccess(easyrtcid) {
    rtc.selfId = easyrtcid;
    socketRtc.emit('join', {easyrtcid: easyrtcid});
}

function loginFailure(errorCode, message) {
    console.log(errorCode, message);
}

/****************************************/
/**********area easyrtc send file******/
/********author: vy le ****************/
/*************************************/
var files = {};
var room_id = '';

$(document).on('change', '.upload-file', function () {
    files = this.files;
    var fileName = {};
    var fileSize = {};
    if (files) {
        for (var i = 0; i < files.length; i++) {
            fileName[i] = files[i]['name'];
            fileSize[i] = files[i]['size'];
        }
    }
});

$(document)
/*
 *   Toggle video on/off when in a call
 * */
    .on('click', '._video-control', function () {
        var room_id = $(this).closest('.video-call-wrapper').data('room-id');
        var src = easyrtc.getLocalStream();
        if (typeof src === 'object') {
            var j = 0;
            Object.keys(src).forEach(function (i) {
                toggleStream(room_id, src[i], 'video', j++);
            })
        } else {
            toggleStream(room_id, src, 'video');
        }
    })
    /*
     *   Toggle audio on/off when in a call
     * */
    .on('click', '._microphone-control', function () {
        var room_id = $(this).closest('.video-call-wrapper').data('room-id');
        var src = easyrtc.getLocalStream();
        if (typeof src === 'object') {
            var j = 0;
            Object.keys(src).forEach(function (i) {
                toggleStream(room_id, src[i], 'audio', j++);
            })
        } else {
            toggleStream(room_id, src, 'audio');
        }
    })
    /*
     *  Stop current call
     * */
    .on('click', '._hangup-control', function (e) {
        var audio = $(this).prev();
        var video = audio.prev();
        video.find('i').removeClass('slash');
        audio.find('i').removeClass('fa-microphone-slash');
        easyrtc.hangupAll();
        rtc.doingCall = false;
        !rtc.phoneSound.paused && rtc.phoneSound.pause();
        hangUp()
    })
    /*
     *   Resize local video element
     * */
    .on('mousedown', '.resize_video', function (e) {
        resize_video.call(this, e).resize(e);
    })
    /*
     *  Move local video element around
     * */
    .on('mousedown', '.resizable', function (e) {
        resize_video.call(this, e).startMoving(e);
    });
/*
 *   Leave pre-load rtc rooms when leaving the page
 * */
document.body.addEventListener('beforeunload', function () {
    rtc.roomJoined.forEach(function (room) {
        easyrtc.leaveRoom(room);
    });
    easyrtc.disconnect();
    easyrtc.hangupAll();
}, false);

/*
 *   Toggle stream on/off
 *   @param {string} room_id : current calling room id
 *   @param {object} stream : Media stream
 *   @param {string} type : audio or video
 *           {Array} type : recursively toggle stream if type is an array of string, i.e : ['audio', 'video']
 *   @next {Bool} next : change button icon if false
 * */
function toggleStream(room_id, stream, type, next) {
    if (!type) return;
    var videos, audios, localVideo, state = null;
    /* If type is string, toggle stream on off */
    if (typeof type === "string") {
        var videoCall = document.getElementById('call_' + room_id);
        if (type === "audio") {
            audios = stream.getAudioTracks();
            audios.forEach(function (audio) {
                state = audio.enabled = !audio.enabled;
            });
            if (!next) {
                var micro = videoCall.querySelector('.fa-microphone');
                micro.classList.toggle('fa-microphone-slash');
            }
        } else if (type === "video") {
            // easyrtc.enableVideo(true);
            videos = stream.getVideoTracks();
            videos.forEach(function (video) {
                state = video.enabled = !video.enabled;
            });
            if (!next) {
                var camera = videoCall.querySelector('.fa-video-camera');
                camera.classList.toggle('slash');
                localVideo = videoCall.querySelector('.video-content-myself');
                if (state) {
                    localVideo.style.display = 'block';
                } else {
                    localVideo.style.display = 'none';
                }
            }
        }
        if (state !== null && rtc.currentCallee.length && !next) {
            socketRtc.emit('toggle stream state', {
                room_id: room_id,
                type: type,
                state: state,
                easyrtcid: rtc.currentCallee
            })
        }
    } else if (Array.isArray(type)) {
        /* If type is an array of string, toggle multiple stream on/off recursively */
        var i = type.length;
        while (i--) {
            if (type[i]) {
                toggleStream(room_id, stream, type[i]);
            }
        }
    }
}


function returnTabRoom(data) {
    return $('#tab' + data['room_id']);
}

function returnTabUser(room_id) {
    return $('li[data-roomid="' + room_id + '"]');
}

function returnMainChat(room_id) {
    return $('#main_' + room_id);
}

/* Request an rtc call to another user
 *
 * */
function callRequest123(e) {
    e.preventDefault();
    /* Only allow calling if not currently in another call */
    if (!rtc.currentCallee.length || !rtc.doingCall) {
        rtc.doingCall = true;
        var url = $(this).data('url');
        // popup = PopupCenter(url, 'vitzoo', '1000', '700');
        //popup.data={ids : rtc.targetEasyrtc}
        //window.data
        
        var room_id = $(this).data('roomid');
        var receiver_id = +$(this).data('receiver');
        /* Only allow calling if the other user is online */
        if (!sortByName.online_users.has(receiver_id)) return showError(trans.not_online);
        rtc.currentCallingRoom = room_id;
        rtc.targetId = receiver_id;
        /* Get easyrtc id of all users in a specific room */
        rtc.targetEasyrtc = easyrtc.getRoomOccupantsAsArray(room_id);
        /* Only allow calling if the room exists */
        if (!rtc.targetEasyrtc) return;
        /* Get all easyrtc id of current user, an object is returned */
        var self = easyrtc.usernameToIds(easyrtc.idToName(easyrtc.myEasyrtcid), room_id);
        var selfIds = new Set();
        if (self.length) {
            /* Convert self object to array of ids */
            self.forEach(function (user) {
                selfIds.add(user.easyrtcid);
            });
        }
        easyrtc.enableVideo(true);
        easyrtc.enableAudio(true);
        // initcall(rtc.targetEasyrtc, selfIds);
        /* Start the call to all easyrtc id except the ones belong to the current user */
        rtc.targetEasyrtc.forEach(function (id) {
            if (!selfIds.has(id)) {
                setTimeout(function () {
                    performCall(id);
                }, 200);
            }
        });
        rtc.phoneSound.volume = +(localStorage['sound' + room_id] || localStorage.baseVolume) / 100;
        rtc.phoneSound.play();
        var type = $(this).data('type');
        setUpVideoCall(rtc.currentCallingRoom, false, type);
        socketRtc.emit('sendAudio', {
            room_id: room_id,
            receiver_id: receiver_id,
            type: type,
            easyrtc: rtc.selfId
        })
    } else {
        showError(trans.stop_current_call)
    }
}
function callRequest(e) {
    e.preventDefault();
    /* Only allow calling if not currently in another call */
    if (!rtc.currentCallee.length || !rtc.doingCall) {
        /*rtc.doingCall = true;*/
        var url = $(this).data('url');
        var room_id = $(this).data('roomid');
        popup = PopupCenter(url+'/'+room_id, 'vitzoo', '1000', '700');
        //popup.data={ids : rtc.targetEasyrtc}
        //window.data
        
        /*var room_id = $(this).data('roomid');
        var receiver_id = +$(this).data('receiver');
        /!* Only allow calling if the other user is online *!/
        if (!sortByName.online_users.has(receiver_id)) return showError(trans.not_online);
        rtc.currentCallingRoom = room_id;
        rtc.targetId = receiver_id;
        /!* Get easyrtc id of all users in a specific room *!/
        rtc.targetEasyrtc = easyrtc.getRoomOccupantsAsArray(room_id);
        /!* Only allow calling if the room exists *!/
        if (!rtc.targetEasyrtc) return;
        /!* Get all easyrtc id of current user, an object is returned *!/
        var self = easyrtc.usernameToIds(easyrtc.idToName(easyrtc.myEasyrtcid), room_id);
        var selfIds = new Set();
        if (self.length) {
            /!* Convert self object to array of ids *!/
            self.forEach(function (user) {
                selfIds.add(user.easyrtcid);
            });
        }
        easyrtc.enableVideo(true);
        easyrtc.enableAudio(true);
        // initcall(rtc.targetEasyrtc, selfIds);
        /!* Start the call to all easyrtc id except the ones belong to the current user *!/
        rtc.targetEasyrtc.forEach(function (id) {
            if (!selfIds.has(id)) {
                setTimeout(function () {
                    performCall(id);
                }, 200);
            }
        });
        rtc.phoneSound.volume = +(localStorage['sound' + room_id] || localStorage.baseVolume) / 100;
        rtc.phoneSound.play();
        var type = $(this).data('type');
        setUpVideoCall(rtc.currentCallingRoom, false, type);
        socketRtc.emit('sendAudio', {
            room_id: room_id,
            receiver_id: receiver_id,
            type: type,
            easyrtc: rtc.selfId
        })
    } else {
        showError(trans.stop_current_call)*/
    }
}

function initcall(targetIds, selfIds) {
    var i = 0;
    
    function call() {
        if (!~selfIds.indexOf(targetIds[i])) {
            var id = targetIds[i];
            easyrtc.enableVideo(true);
            easyrtc.enableAudio(true);
            setTimeout(function () {
                performCall(id);
                i++;
                call();
            }, 200);
        }
    }
    
    call();
}

$(document)
/* Start the call
 *  */
    .on('click', '.chat-video, .chat-audio', callRequest)
    /* Toggle fullscreen on/off
     *  */
    .on('click', '._fullscreen-control', fullScreenVideoCall);

function performCall(otherEasyrtcid, data) {
    // easyrtc.hangupAll();
    /*easyrtc.setVideoSource(otherEasyrtcid);*/
    var acceptedCB = function (accepted, easyrtcid) {
        if (!accepted) {
            easyrtc.hangupAll();
            hangUp(rtc.currentCallingRoom);
            // easyrtc.showError("CALL-REJECTED", "Sorry, your call to " + easyrtc.idToName(easyrtcid) + " was rejected");
        } else {
            // targetEasyrtcId = otherEasyrtcid;
        }
    };
    var successCB = function (easyrtcid, mediaType) {
        if (rtc.currentCallee.indexOf(otherEasyrtcid) === -1)
            rtc.currentCallee.push(otherEasyrtcid);
        // console.log("Got mediaType " + mediaType + " from " + easyrtc.idToName(easyrtcid));
    };
    var failureCB = function (errorCode, errMessage) {
        console.log("call to  " + easyrtc.idToName(otherEasyrtcid) + " failed:" + errMessage);
    };
    /*  Initialize stream elements
     * */
    easyrtc.initMediaSource(function () {
        /* After successfully initialize the stream, starts calling */
        easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB, [otherEasyrtcid]);
    }, function () {
    }, otherEasyrtcid);
}

/* Change video src, start it when playable
 * @param {DOM} video : video element
 * @param {string} src : link to the video
 * @return {DOM} video
 * */
function addVideoSrc(video, src) {
    if (src) {
        video.src = src;
        video.oncanplay = video.play;
    }
    return video;
}

/* Capital first letter of a string */
function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1).toLowerCase();
}

/* Get property value of an element
 * @param {DOM} elem : element
 * @param {string} prop : css name of the property, e.g: height, width...
 * */
function getStyle(elem, prop) {
    var res;
    if (window.getComputedStyle.getPropertyValue) {
        res = window.getComputedStyle(elem, null).getPropertyValue(prop)
    }
    else {
        res = window.getComputedStyle(elem)[prop]
    }
    if (res === 'auto') {
        if (prop === 'height' || prop === 'width') {
            prop = capitalizeFirstLetter(prop);
            res = elem['client' + prop];
        } else {
            res = 0;
        }
    }
    return textToNum(res);
}

/* Set up video call
 * @param {string} room_id : Calling room id
 * @param {object} remote_stream : remote stream object
 * @param {string} type : audio or video
 *
 * */
function setUpVideoCall(room_id, remote_stream, type) {
    room_id = room_id || rtc.currentCallingRoom || rtc.otherRoom;
    type = type ? type : rtc.callType;
    var main = document.getElementById('main_' + room_id);
    var li = document.querySelector('li[data-roomid="' + room_id);
    /* Hang up if caller is not friend */
    if (!li) return hangUp(room_id);
    var avatar = li.querySelector('.avatar img');
    var avatar_link;
    if (avatar) avatar_link = avatar.src;
    var a = li.querySelector('a');
    a.click();
    if (!main) {
        /* if chatbox is not loaded, wait until it is */
        var insertion = function (e) {
            if (e.animationName == "nodeInserted") {
                setUpVideoCall(room_id, remote_stream, type);
                document.removeEventListener("animationend", insertion, false);
                document.removeEventListener("webkitAnimationEnd", insertion, false);
            }
        };
        document.addEventListener("animationend", insertion, false);
        document.addEventListener("webkitAnimationEnd", insertion, false);
        return;
    }
    var isVideo = type === 'video';
    var videoCall = document.getElementById('call_' + room_id);
    var src = easyrtc.getLocalStream();
    if (typeof src === 'object') {
        src = src[Object.keys(src)[0]];
    }
    var user_name = li.querySelector('.user_name').innerHTML;
    var header = main.querySelector('header');
    var mobile_menu = document.querySelector('.menu-mobile');
    var mobile_m_height = 0;
    if (window.innerWidth <= 894) mobile_m_height = getStyle(mobile_menu, 'height');
    var chat_tab = main.querySelector('.chat-tab-container'),
        text_box = main.querySelector('.text-chat'),
        textboxHeight = getStyle(text_box, 'height'),
        headerHeight = getStyle(header, 'height'),
        m_height = 'calc(100vh - ' + (textboxHeight + headerHeight + mobile_m_height + 3) + 'px)';
    /* If video call element is created, proceed */
    if (videoCall) {
        var remote_src = remote_stream ? URL.createObjectURL(remote_stream) : "",
            connect = remote_src ? trans.connected : trans.connecting,
            reserve = !remote_src ? trans.connecting : trans.connected,
            local_src = src ? URL.createObjectURL(src) : '';
        /* Show the video call element */
        style(videoCall, {display: 'block', height: m_height});
        /* Show other user's video if it exists and call type is video */
        var display = remote_src && type === 'video' ? 'none' : 'block';
        var image = videoCall.querySelector('.video-calling');
        var status = videoCall.querySelector('.video-status');
        /* Change calling status */
        status.innerHTML = connect;
        status.dataset.connect = reserve;
        /* Show image if remote video is not shown */
        image.style.display = display;
        var remote = addVideoSrc(videoCall.querySelector('.remote-video'), remote_src);
        remote.style.display = remote_src && isVideo ? 'block' : 'none';
        !isVideo && videoCall.querySelector('.fa-video-camera').classList.add('slash');
        videoCall.querySelector('.video-content-myself').style.display = isVideo ? 'block' : 'none';
        addVideoSrc(videoCall.querySelector('.local-video'), local_src);
        style(remote, 'max-height', m_height);
        var chatOpen = header.querySelector('.open-chat-side-tab');
        chatOpen.style.display = 'block';
    }
    else {
        /* If video call element is not created, create it */
        var video = callStarts(room_id, user_name, src, remote_stream, type, avatar_link);
        if (!video) return hangUp(room_id);
        chat_tab.insertAdjacentHTML('beforebegin', video);
    }
    (function (videoCall) {
        var vid = videoCall, i = 0;
        /* Try showing local stream every 0.5s  */
        function retry() {
            i++;
            /* Run this function maximum 20 times */
            if (i > 20) return;
            setTimeout(function () {
                /* If wrapper element does not exist, find it then retry again */
                if (!vid) {
                    vid = document.getElementById('call_' + room_id);
                    return retry();
                }
                var src = easyrtc.getLocalStream();
                /* If local video src does not exist, retry */
                if (!Object.keys(src).length) {
                    retry();
                } else {
                    src = easyrtc.getLocalStream();
                    if (typeof src === 'object') {
                        src = src[Object.keys(src)[0]];
                    }
                    if (type !== 'video') {
                        /* If rtc call is audio only, hide local video */
                        var videos = src.getVideoTracks();
                        videos.forEach(function (video) {
                            video.enabled = false;
                        });
                    }
                    addVideoSrc(vid.querySelector('.local-video'), URL.createObjectURL(src));
                }
            }, 500);
        }
        
        /* If video wrapper or local stream src cannot be found, retry */
        if (!src || !vid) {
            retry();
        } else {
            /* Else show local video stream if calling type is video */
            if (type !== 'video') {
                var videos = src.getVideoTracks();
                videos.forEach(function (video) {
                    video.enabled = false;
                });
            }
            addVideoSrc(vid.querySelector('.local-video'), URL.createObjectURL(src));
        }
    })(videoCall);
    /* Change chatbox height to fit the page */
    style(chat_tab, {
        bottom: textboxHeight + 'px',
        height: m_height
    });
    $(chat_tab).addClass('minified-chat-tab');
}

/* Create video wrapper
 * @param {string} room_id : calling room id
 * @param {user_name} other user's username to be shown
 * @param {object} local_stream : local stream object
 * @param {object} remote_stream : remote stream object
 * @param {string} type : audio or video
 * @param {string} avatar_link : link to the other user's avatar
 * */
function callStarts(room_id, user_name, local_stream, remote_stream, type, avatar_link) {
    avatar_link = avatar_link || '/themes/default/asset_frontend/img/pf.jpg';
    var main = document.getElementById('main_' + room_id);
    if (!main) return;
    var header = main.querySelector('header');
    var mobile_menu = document.querySelector('.menu-mobile');
    var mobile_m_height = 0;
    var isVideo = type === 'video';
    var slash = isVideo ? '' : 'slash';
    var show_video = isVideo ? 'block' : 'none';
    if (window.innerWidth <= 894) mobile_m_height = getStyle(mobile_menu, 'height');
    var remote_src = remote_stream ? URL.createObjectURL(remote_stream) : '',
        remote_display = remote_src && isVideo ? 'block' : 'none',
        local_src = local_stream ? URL.createObjectURL(local_stream) : '',
        text_box = main.querySelector('.text-chat'),
        textboxHeight = getStyle(text_box, 'height'),
        headerHeight = getStyle(header, 'height'),
        display = remote_src && isVideo ? 'none' : 'block',
        connect = remote_src ? trans.connected : trans.connecting,
        reserve = !remote_src ? trans.connecting : trans.connected,
        m_height = 'calc(100vh - ' + (textboxHeight + headerHeight + mobile_m_height + 3) + 'px)';
    var side_chat_tab = '<div class="dropdown dot-menu open-chat-side-tab">' +
        '<button class="btn btn-menu" type = "button" onclick="openChatTab(\'' + room_id + '\')">' +
        '<i class="fa fa-comment">' +
        '</i></button></div>';
    header.insertAdjacentHTML('beforeEnd', side_chat_tab);
    return '<div style="height:' + m_height + '" class="video-call-wrapper clearfix" data-room-id="' + room_id + '" id="call_' + room_id + '">' +
        '<div class="video-content-caller">' +
        '<div class="video-calling" style="display: ' + display + '">' +
        '<img style="display:inline-block" class="video-avatar video-avatar-caller" src="' + avatar_link + '" >' +
        '<p class="video-name-connecting">' + user_name + '</p>' +
        '<p class="video-status" data-connect="' + reserve + '">' + connect + '</p></div>' +
        '<video src="' + remote_src + '" autoplay class="remote-video" style="display : ' + remote_display + ';max-height: ' + m_height + '; width: 100%"></video>' +
        '<div class="video-content-myself resizable" style="display:' + show_video + '"><div style="position: relative"><' +
        'video autoplay muted class="local-video" class="easyrtcMirror" src="' + local_src + '"></video>' +
        '<i class="fa fa-compress resize_video" style="position: absolute; bottom:0; right:0; transform:rotate(90deg);    font-size: 1.5rem; cursor:nwse-resize;"></i></div>' +
        '</div>' +
        '<div class="video-button-control">' +
        '<button role="button" title="Disable your video" class="js_5 _video-control"><i class="fa fa-video-camera ' + slash + '">' +
        '</i></button>' +
        '<button role="button" title = "Mute your microphone" class = "_microphone-control js_5" ><i class= "fa fa-microphone"></i></button >' +
        '<button role = "button" class= "js_5 _hangup-control" title = "hang up" >' +
        '<i class="fa fa fa-phone" style = "transform: rotate(135deg);" ></i></button>' +
        '<button role="button" title = "full screen" class="js_5 _fullscreen-control" >' +
        '<i class="fa fa-arrows-alt">' +
        '</i></button></div>' +
        '</div>';
}

/*
 *  Toggle chatbox when in rtc call
 *  @param {string} room_Id
 *  @param {bool} close : force chatbox closure if true
 * */
function openChatTab(room_id, close) {
    closeNav(room_id);
    var main = document.getElementById('main_' + room_id);
    if (main) {
        var mobile_menu = document.querySelector('.menu-mobile');
        var mobile_m_height = 0;
        if (window.innerWidth <= 894) mobile_m_height = getStyle(mobile_menu, 'height');
        var head = main.querySelector('.right-title'),
            textChat = main.querySelector('.text-chat'),
            h_height = getStyle(head, 'height'),
            text_height = getStyle(textChat, 'height'),
            m_height = 'calc(100vh - ' + (h_height + text_height + mobile_m_height + 3) + 'px)';
        var mainTab = document.getElementById('tab' + room_id);
        var chat_tab = $(mainTab.querySelector('.chat-tab-container'));
        var videoWrapper = mainTab.querySelector('.video-call-wrapper ');
        if (videoWrapper) videoWrapper.style.height = m_height;
        if (chat_tab.hasClass('opened') || close) {
            if (videoWrapper) videoWrapper.style.width = '';
            chat_tab.removeClass('opened');
            chat_tab.hasClass('minified-chat-tab') && chat_tab.css({transform: "translateX(100%)"});
        } else {
            if (videoWrapper) videoWrapper.style.width = 'calc(100% - ' + (chat_tab.outerWidth(true)) + 'px)';
            chat_tab.addClass('opened');
            chat_tab.hasClass('minified-chat-tab') && chat_tab.css({
                transform: "translateX(0)",
                bottom: (text_height + 2) + 'px',
                height: m_height
            });
            chat_tab.scrollTop(chat_tab[0].scrollHeight);
        }
    }
}
/*
 *  Stop an rtc call
 *  @param {string} room_id
 * */
function hangUp(room_id) {
    room_id = room_id || rtc.currentCallingRoom || rtc.otherRoom;
    if (room_id) {
        if (rtc.sound[room_id] && !rtc.sound[room_id].paused) rtc.sound[room_id].pause();
        var main = document.getElementById('main_' + room_id);
        var videoCall = document.getElementById('call_' + room_id);
        if (videoCall) {
            var chat_tab = main.querySelector('.chat-tab-container');
            var remote = videoCall.querySelector('.remote-video');
            var local = videoCall.querySelector('.local-video');
            var status = videoCall.querySelector('.video-status');
            status.innerHTML = trans.connecting;
            status.dataset.connect = trans.connected;
            remote.pause();
            local.pause();
            var header = main.querySelector('header');
            var chatOpen = header.querySelector('.open-chat-side-tab');
            chatOpen.style.display = 'none';
            style(chat_tab, {bottom: '', transform: ''});
            $(chat_tab).removeClass('minified-chat-tab');
            videoCall.style.display = 'none';
            videoCall.style.width = '';
            var fullScreenControl = videoCall.querySelector("._fullscreen-control");
            fullScreenVideoCall.call(fullScreenControl, fullScreenControl, true);
            remote.onpause = function () {
                this.src = '';
            };
            local.onpause = function () {
                this.src = '';
            };
        }
        !rtc.phoneSound.paused && rtc.phoneSound.pause();
        easyrtc.enableVideo(false);
        easyrtc.enableAudio(false);
        socketRtc.emit('hang up', {room_id: room_id, user_id: rtc.targetId});
        var src = easyrtc.getLocalStream();
        if (typeof src === 'object') {
            Object.keys(src).forEach(function (key) {
                easyrtc.closeLocalMediaStream(key);
            });
        } else {
            easyrtc.closeLocalMediaStream();
        }
        closeStream(src);
        $('#cModalAcceptCall').modal('hide');
        document.getElementById("callAcceptButton").onclick = function () {
        };
        
        document.getElementById("callRejectButton").onclick = function () {
        };
        rtc.callConnected = false;
        rtc.currentCallingRoom = null;
        rtc.currentCallee = [];
        rtc.doingCall = false;
    }
}

function foreachArray(array, status) {
    Array.prototype.forEach.call(array, function (i) {
        i.style.display = status;
    });
}

/*  Change element style
 *  @param {DOM} elem : element to be changed
 *          {collection} elem : element collection, allowing changes to multiple elements
 *  @param {string} name : css name
 *          {object} name : {css_name : css_value}, allowing multiple changes to the element
 *  @param {string} value : css value, not used if name is an object
 *
 *  */
function style(elem, name, value) {
    if (NodeList.prototype.isPrototypeOf(elem)) {
        for (var j = 0, length = elem.length; j < length; j++) {
            style(elem[j], name, value);
        }
    } else {
        if (typeof name === 'object') {
            Object.keys(name).forEach(function (i) {
                elem.style[i] = name[i];
            });
        } else {
            elem.style[name] = value;
        }
    }
}

/* Hide remote video when video stream is off */
function hideRemoteVideo(room_id, status) {
    var videoCall = document.getElementById('call_' + room_id);
    if (videoCall) {
        var remote = videoCall.querySelector('.remote-video'),
            imgStatus = remote.previousElementSibling,
            img = imgStatus.querySelector('img');
        if (status) {
            style(remote, 'display', 'block');
            style(imgStatus, 'display', 'none');
        } else {
            style(remote, 'display', 'none');
            style(imgStatus, 'display', 'block');
        }
    }
}

/****************************************/
/**********area upload file server******/
/********author: vy le ****************/
/*************************************/
/* Hidden input to house the file to be sent */
var hidden_input = document.createElement('input');
$(document).on('click', ".pseudo-file-upload", function () {
    // hidden_input = document.createElement('input');
    hidden_input.type = 'file';
    hidden_input.name = 'filez';
    hidden_input.click();
    var infomation_room = $(this).closest('.chatbox');
    var room_id = infomation_room.data('roomid');
    var data = {room_id: room_id,};
    hidden_input.onchange = function (e) {
        if (this.files[0].size > 26214400) {
            $('#errorModal p').html(trans.file_too_large);
            $('#errorModal').modal('show');
            return false;
        }
        UploadFile(this, data);
    };
});

/* Upload the file to send
 * @param {DOM} e : input element
 * @param {object} data : contains room_id
 * */
function UploadFile(e, data) {
    var files = e.files;
    if (!files.length)
        return;
    var room_id = data['room_id'];
    var relative = $('#voltaic_holder_' + room_id).find('.user-relative-chat').data('option');
    var tab_room = returnTabRoom(data);
    if (relative !== 'nonechat') {
        var file = files[0];
        data['date'] = Date.now();
        var uuid = UUID.generate();
        data['content'] = file.name.replace(/ /g, "_");
        data['htmlContent'] = selfMessage({
            uuid: uuid,
            content: data['content'],
            fullname: nameSession,
            time: getTimePmAm(new Date()),
            type: 'file',
            size: file.size
        });
        appendMessageFile(data, true);
        var location_file = tab_room.find('.chat-msg-file').last();
        var div = '<div class="progress progress-striped active" style="width: 20%;height: 10px;">' +
            '<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">' +
            '<span class= "sr-only"></span>' +
            '</div>' + '</div>';
        
        div += '<div class="cancel" style="float: left;margin-left: 10px;cursor: pointer;">' + trans.cancel + '</div>';
        location_file.append(div);
        var request = new XMLHttpRequest();
        var cancel = location_file.find('.cancel');
        
        request.upload.addEventListener('progress', function (e) {
            /* Show file upload progress */
            var percent = Math.round((e.loaded / e.total) * 100);
            location_file.find('.progress-bar').css('width', percent + '%');
            location_file.find('.sr-only').html(percent + '%');
            if (percent === 100) {
                var cancel = location_file.find('.cancel');
                cancel.text('100%');
                cancel.removeClass('cancel');
                cancel.addClass('uploaded');
            }
        }, false);
        
        request.addEventListener('load', function (e) {
            var response = JSON.parse(request.responseText);
            if (response.success) {
                var link = location_file.find('.download-file');
                link.attr('href', response.data.destinationPath);
                /* Remove adjacent elements when file send completes */
                link.next().remove();
                link.next().remove();
                link.next().remove();
            }
            hidden_input.value = null;
        }, false);
        
        request.addEventListener('abort', function (e) {
            hidden_input.value = null;
        }, false);
        
        request.addEventListener('error', function (e) {
            /* Show upload error */
            location_file.html(trans.error_during_upload);
            hidden_input.value = null;
        }, false);
        
        var formData = new FormData();
        formData.append('file', file);
        formData.append('room_id', room_id);
        // formData.append('receiver_id', data['receiver_id']);
        formData.append('id', uuid);
        request.open('post', '/chat/uploadFile');
        request.send(formData);
        cancel[0].addEventListener('click', function (e) {
            /* Abort file send process */
            request.abort();
            $(this).closest('.chat-msg-file').html(trans.upload_canceled);
        }, false);
    }
    else {
        showError(trans.cannot_upload_file);
    }
}


function getDate(times) {
    return addZero(times.getFullYear()) + '/' + addZero(times.getMonth() + 1) + '/' + addZero(times.getDate()) + ' 12:00:00';
}

function getTimePmAm(date) {
    var hours = date.getHours() % 24;
    var minutes = date.getMinutes();
    var mid = 'AM';
    if (hours == 0) { //At 00 hours we need to show 12 am
        hours = 12;
    }
    else if (hours > 12) {
        hours = hours % 12;
        mid = 'PM';
    }
    return addZero(hours) + ':' + addZero(minutes) + ' ' + mid;
}

/* Add this message to chatbox when receiving files
 * */
var contentMessage = function (data, size) {
    var st = '';
    var c_file = 'msg-text';
    if (data['type'] == 'file') c_file = 'msg-file';
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" id="' + data.uuid + '">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    st += '<div class="avatar">';
    if (data.url) {
        st += '<img class="user_avatar" width="100%" src="' + data.url + '"/>'
    } else {
        st += '<i class="fa fa-user"></i>';
    }
    st += '</div>';
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat">';
    st += '<div class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time><span>' + data['fullname'] + '</span><span class="clock-time">' + data.time + '</span></time>';
    st += '<div class="msg chat-' + c_file + ' ' + data['fileClass'] + '">';
    st += '<a class="download-file" download href="' + data.destinationPath + '"><p class="' + c_file + '">' + data['content'] + '</p></a>';
    if (data['type'] == 'file' && size)
        st += '<p class="msg-filesize">' + Math.round(data['size'] / 1024) + ' KB</p>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

/* Add this message to chatbox when sending files
 * */
var selfMessage = function (data, size) {
    var st     = '';
    var c_file = 'msg-text';
    if (data['type'] == 'file') c_file = 'msg-file';
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" id="' + data.uuid + '">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat">';
    st += '<div style="float:right" class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time><span>' + data['fullname'] + '</span><span class="clock-time"></span></time>';
    st += '<div class="msg chat-' + c_file + ' ' + data['fileClass'] + '">';
    if (data['type'] == 'file'){
        st += '<a class="download-file" download href="' + data.destinationPath + '"><p class="' + c_file + '">' + data['content'] + '</p></a>';
        st += '<p class="msg-filesize">' + Math.round(data.size / 1024) + ' KB</p>';
    } else {
        st += '<p class="' + c_file + '">' + data['content'] + '</p>';
    }
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

/* Resize and drag video */
function resize_video(e) {
    var self = {};
    var target = this;
    var node = $(this).closest('.resizable')[0];
    /* Move video element around */
    self.move = function (x, y) {
        node.style.left = x + 'px';
        node.style.top = y + 'px';
        node.style.opacity = 0.5;
    };
    
    self.resize = initDrag;
    var ratio;
    var startX, startY, startWidth, startHeight, video_wrapper, outerWidth, outerHeight;
    /*
     * Calculate video element dimensions and placement before resizing
     * */
    function initDrag(e) {
        video_wrapper = $(node).closest('.video-call-wrapper');
        outerWidth = video_wrapper.outerWidth(true);
        outerHeight = video_wrapper.outerHeight(true);
        node = $(target).closest('.resizable')[0];
        startX = e.clientX;
        startY = e.clientY;
        startWidth = parseInt(document.defaultView.getComputedStyle(node).width, 10);
        startHeight = parseInt(document.defaultView.getComputedStyle(node).height, 10);
        ratio = startWidth / startHeight;
        document.documentElement.addEventListener('mousemove', doDrag, false);
        document.documentElement.addEventListener('mouseup', stopDrag, false);
    }
    
    /*
     *  Calculate video element dimensions when resizing
     * */
    function doDrag(e) {
        var width = (startWidth + e.clientX - startX);
        width = width >= 0.66 * outerWidth ? 0.66 * outerWidth : width;
        width = width <= 100 ? 100 : width;
        var height = width / ratio;
        if (height >= 0.66 * outerHeight) {
            height = 0.66 * outerHeight;
            node.style.width = ((height * ratio >>> 0) / outerWidth * 100) + '%';
            node.style.height = '66%';
        }
        else {
            node.style.width = (width >>> 0) / outerWidth * 100 + '%';
            node.style.height = (height >>> 0) / outerHeight * 100 + '%';
        }
    }
    
    /* Remove element when resizing stops */
    function stopDrag(e) {
        document.documentElement.removeEventListener('mousemove', doDrag, false);
        document.documentElement.removeEventListener('mouseup', stopDrag, false);
    }
    
    var cWi, cHe, eWi, eHe, diffX, diffY;
    /*
     *  Calculate video element dimensions and placement before moving
     * */
    var selfMove = function (e) {
        video_wrapper = $(node).closest('.video-call-wrapper');
        cWi = video_wrapper.outerWidth(true);
        cHe = video_wrapper.outerHeight(true);
        var divTop = getStyle(node, 'top'),
            divLeft = getStyle(node, 'left');
        /*var divTop = offset.top,
         divLeft = offset.left;*/
        eWi = node.offsetWidth;
        eHe = node.offsetHeight;
        var posX = e.clientX,
            posY = e.clientY;
        document.body.style.cursor = 'move';
        diffX = posX - divLeft;
        diffY = posY - divTop;
        document.addEventListener('mousemove', mouseMove, false);
    };
    
    /* Calculate video element border when moving */
    var mouseMove = function (evt) {
        evt = evt || window.event;
        var posX = evt.clientX,
            posY = evt.clientY,
            aX = posX - diffX,
            aY = posY - diffY;
        if (aX < 0) aX = 0;
        if (aY < 0) aY = 0;
        if (aX + eWi > cWi) aX = cWi - eWi;
        if (aY + eHe > cHe) aY = cHe - eHe;
        self.move(aX, aY);
    };
    /* Remove moving events when drag stops
     * */
    var selfStop = function () {
        document.body.style.cursor = 'default';
        node.style.opacity = 1;
        document.removeEventListener('mousemove', mouseMove, false);
        // document.documentElement.removeEventListener('mousedown', selfMove, false);
        document.documentElement.removeEventListener('mouseup', selfStop, false);
    };
    
    /*
     *  This runs when video element is dragged
     * */
    self.startMoving = function (e) {
        if (!$(e.target).is('.resize_video')) {
            node = target;
            selfMove(e);
            // document.documentElement.addEventListener('mousedown', selfMove, false);
            document.documentElement.addEventListener('mouseup', selfStop, false);
        }
        return self;
    };
    
    return self;
}

function changeWindowSize(video) {
    video.addEventListener('play', function () {
        window.resizeTo(document.body.clientWidth, document.body.clientHeight + 50);
        formerHeight = window.innerHeight;
    });
}

document.fullscreenElement = document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement || false;
HTMLElement.prototype.requestFullscreen = HTMLElement.prototype.requestFullscreen
    || HTMLElement.prototype.webkitRequestFullscreen
    || HTMLElement.prototype.webkitRequestFullScreen
    || HTMLElement.prototype.mozRequestFullScreen;
document.exitFullscreen = document.exitFullscreen ||
    document.webkitExitFullscreen ||
    document.mozCancelFullScreen ||
    document.msExitFullscreen || false;
/*
 *  Toggle full screen on/off
 *  @param {event} e : un-used
 *  @param {Bool} forced : force exit full screen if true
 * */
function fullScreenVideoCall(e, forced) {
    var tab = $(this).closest('.right-content')[0];
    toggleFullscreen(tab, fullScreenBackground.bind(this), !!forced);
}

/*
 *  Change video call background when in full screen mode
 * */
function fullScreenBackground(e) {
    var style = e.style;
    if (e.dataset.fullscreen === 'full_screen') {
        $(this.querySelector('i')).addClass('fa-compress').removeClass('fa-arrows-alt');
        style.backgroundImage = window.getComputedStyle(document.body)['background-image'];
        style.backgroundAttachment = 'fixed';
        style.backgroundSize = 'cover';
    } else {
        $(this.querySelector('i')).removeClass('fa-compress').addClass('fa-arrows-alt');
        style.backgroundImage = '';
    }
}

/*
 *  Toggle fullscreen on/off
 *  @param {DOM} e : chat tab element
 *  @param {function} cb: callback
 *  @param {Bool} forced : force full screen exit if true
 * */
function toggleFullscreen(e, cb, forced) {
    if (forced) {
        inFullScreenMode = false;
        e.dataset.fullscreen = 'window';
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    } else {
        if (e.dataset.fullscreen !== 'full_screen') {
            inFullScreenMode = true;
            if (e.requestFullscreen) {
                e.requestFullscreen();
                e.dataset.fullscreen = 'full_screen';
            }
        } else {
            inFullScreenMode = false;
            e.dataset.fullscreen = 'window';
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
        }
    }
    cb && cb(e);
}
/*
 *  Pre-join to all rtc room
 * */
function joinRoom(users) {
    return null;
    /*var joinedRooms = easyrtc.getRoomsJoined();
    users.forEach(function (user) {
        var room_id = friend_list[user];
        if (!joinedRooms[room_id]) {
            easyrtc.joinRoom(friend_list[user], null, function (room_id) {
                rtc.roomJoined.add(room_id)
            });
        }
    });
    return friend_list;*/
}

/* Close media streams */
function closeStream(streams) {
    if (streams) {
        !rtc.phoneSound.paused && rtc.phoneSound.pause();
        if (typeof streams === 'object') {
            Object.keys(streams).forEach(function (i) {
                var stream = streams[i];
                audios = stream.getAudioTracks();
                audios.forEach(streamStop);
                videos = stream.getVideoTracks();
                videos.forEach(streamStop);
            });
        } else {
            audios = streams.getAudioTracks();
            audios.forEach(streamStop);
            videos = streams.getVideoTracks();
            videos.forEach(streamStop);
        }
    }
}

function streamStop(streamElement) {
    streamElement.stop();
}