var rtc = {};

rtc.sound = {};
/* Notification sound file */
rtc.notification = urlGlobal + '/themes/default/asset_frontend/sound/shooting_start.mp3';

var friend_list = (function () {
    var friends = {};
    $('#friend').find('li').each(function (i, v) {
        friends[v.dataset.id] = v.dataset.roomid;
    });
    return friends;
}());

/****************************************/
/**********area easyrtc send file******/
/********author: vy le ****************/
/*************************************/
var files = {};
var room_id = '';

$(document).on('change', '.upload-file', function () {
    files = this.files;
    var fileName = {};
    var fileSize = {};
    if (files) {
        for (var i = 0; i < files.length; i++) {
            fileName[i] = files[i]['name'];
            fileSize[i] = files[i]['size'];
        }
    }
});

function returnTabRoom(data) {
    return $('#tab' + data['room_id']);
}

function returnTabUser(room_id) {
    return $('li[data-roomid="' + room_id + '"]');
}

function returnMainChat(room_id) {
    return $('#main_' + room_id);
}

/* Request an rtc call to another user
 *
 * */
function callRequest(e) {
    e.preventDefault();
    var url = $(this).data('url');
    var room_id = $(this).data('roomid');
    var receiver_id = $(this).data('receiver');
    var version = detectIE();
    if (version === false) {
        console.log('is not IE/Edge');
    } else {
        $('#detectIE').modal('show');
        return false;
    }
    if (!sortByName.online_users.has(receiver_id)) return showError(trans.not_online);
    var type = $(this).data('type');
    socket.emit('user busy', {receiver_id: receiver_id, room_id: room_id, type: type});
}


$(document)
/* Start the call
 *  */
    .on('click', '.chat-video, .chat-audio', callRequest);

/****************************************/
/**********area upload file server******/
/********author: vy le ****************/
/*************************************/
/* Hidden input to house the file to be sent */
var hidden_input = document.createElement('input');
$(document).on('click', ".pseudo-file-upload", function () {
    // hidden_input = document.createElement('input');
    hidden_input.type = 'file';
    hidden_input.name = 'filez';
    hidden_input.click();
    var infomation_room = $(this).closest('.chatbox');
    var room_id = infomation_room.data('roomid');
    var data = {room_id: room_id,};
    hidden_input.onchange = function (e) {
        if (this.files[0].size > 26214400) {
            $('#errorModal p').html(trans.file_too_large);
            $('#errorModal').modal('show');
            return false;
        }
        UploadFile(this, data);
    };
});

/* Upload the file to send
 * @param {DOM} e : input element
 * @param {object} data : contains room_id
 * */
function UploadFile(e, data) {
    var files = e.files;
    if (!files.length)
        return;
    var room_id = data['room_id'];
    var relative = $('#voltaic_holder_' + room_id).find('.user-relative-chat').data('option');
    var tab_room = returnTabRoom(data);
    if (relative !== 'nonechat') {
        var file = files[0];
        data['date'] = Date.now();
        var uuid = UUID.generate();
        data['content'] = file.name.replace(/ /g, "_");
        data['htmlContent'] = selfMessage({
            uuid: uuid,
            content: data['content'],
            fullname: nameSession,
            time: getTimePmAm(new Date()),
            type: 'file',
            size: file.size
        });
        appendMessageFile(data, true);
        var location_file = tab_room.find('.chat-msg-file').last();
        var div = '<div class="progress progress-striped active" style="width: 20%;height: 10px;">' +
            '<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">' +
            '<span class= "sr-only"></span>' +
            '</div>' + '</div>';
        
        div += '<div class="cancel" style="float: left;margin-left: 10px;cursor: pointer;">' + trans.cancel + '</div>';
        location_file.append(div);
        var request = new XMLHttpRequest();
        var cancel = location_file.find('.cancel');
        
        request.upload.addEventListener('progress', function (e) {
            /* Show file upload progress */
            var percent = Math.round((e.loaded / e.total) * 100);
            location_file.find('.progress-bar').css('width', percent + '%');
            location_file.find('.sr-only').html(percent + '%');
            if (percent === 100) {
                var cancel = location_file.find('.cancel');
                cancel.text('100%');
                cancel.removeClass('cancel');
                cancel.addClass('uploaded');
            }
        }, false);
        
        request.addEventListener('load', function (e) {
            var response = JSON.parse(request.responseText);
            if (response.success) {
                var link = location_file.find('.download-file');
                link.attr('href', response.data.destinationPath);
                /* Remove adjacent elements when file send completes */
                link.next().remove();
                link.next().remove();
                link.next().remove();
            }
            hidden_input.value = null;
        }, false);
        
        request.addEventListener('abort', function (e) {
            hidden_input.value = null;
        }, false);
        
        request.addEventListener('error', function (e) {
            /* Show upload error */
            location_file.html(trans.error_during_upload);
            hidden_input.value = null;
        }, false);
        
        var formData = new FormData();
        formData.append('file', file);
        formData.append('room_id', room_id);
        // formData.append('receiver_id', data['receiver_id']);
        formData.append('id', uuid);
        request.open('post', '/chat/uploadFile');
        request.send(formData);
        cancel[0].addEventListener('click', function (e) {
            /* Abort file send process */
            request.abort();
            $(this).closest('.chat-msg-file').html(trans.upload_canceled);
        }, false);
    }
    else {
        showError(trans.cannot_upload_file);
    }
}


function getDate(times) {
    return addZero(times.getFullYear()) + '/' + addZero(times.getMonth() + 1) + '/' + addZero(times.getDate()) + ' 12:00:00';
}

function getTimePmAm(date) {
    var hours = date.getHours() % 24;
    var minutes = date.getMinutes();
    var mid = 'AM';
    if (hours == 0) { //At 00 hours we need to show 12 am
        hours = 12;
    }
    else if (hours > 12) {
        hours = hours % 12;
        mid = 'PM';
    }
    return addZero(hours) + ':' + addZero(minutes) + ' ' + mid;
}

/* Add this message to chatbox when receiving files
 * */
var contentMessage = function (data, size) {
    var st = '';
    var c_file = 'msg-text';
    if (data['type'] == 'file') c_file = 'msg-file';
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" id="' + data.uuid + '">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    st += '<div class="avatar">';
    if (data.url) {
        st += '<img class="user_avatar" width="100%" src="' + data.url + '"/>'
    } else {
        st += '<i class="fa fa-user"></i>';
    }
    st += '</div>';
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat">';
    st += '<div class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time><span>' + data['fullname'] + '</span><span class="clock-time">' + data.time + '</span></time>';
    st += '<div class="msg chat-' + c_file + ' ' + data['fileClass'] + '">';
    st += '<a class="download-file" download href="' + data.destinationPath + '"><p class="' + c_file + '">' + data['content'] + '</p></a>';
    if (data['type'] == 'file' && size)
        st += '<p class="msg-filesize">' + Math.round(data['size'] / 1024) + ' KB</p>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

/* Add this message to chatbox when sending files
 * */
var selfMessage = function (data, size) {
    var st = '';
    var c_file = 'msg-text';
    if (data['type'] == 'file') c_file = 'msg-file';
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" id="' + data.uuid + '">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat">';
    st += '<div style="float:right" class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time><span>' + data['fullname'] + '</span><span class="clock-time"></span></time>';
    st += '<div class="msg chat-' + c_file + ' ' + data['fileClass'] + '">';
    if (data['type'] == 'file') {
        st += '<a class="download-file" download href="' + data.destinationPath + '"><p class="' + c_file + '">' + data['content'] + '</p></a>';
        if (size) {
            st += '<p class="msg-filesize">' + Math.round(data.size / 1024) + ' KB</p>';
        }
    } else {
        st += '<p class="' + c_file + '">' + data['content'] + '</p>';
    }
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

$(document).ready(function () {
    $(document).on('click', '#callAcceptButton', function () {
        var url = fullUrl + '/chat/video';
        var parent = $(this).parent();
        var receiver_id = parent.data('caller');
        var room_id = parent.data('roomid');
        popup = PopupCenter(url + '/' + receiver_id + '/' + room_id, 'vitzoo', '1000', '700');
        popup.data = {
            callType: callType
        };
        !soundGlobal.calling.paused && soundGlobal.calling.pause();
    });
    
    
    $(document).on('click', '#callRejectButton', function () {
        var caller = $(this.parentElement).data('caller');
        var room_id = $(this.parentElement).data('roomid');
        var data = {
            receiver_id: caller,
        };
        var flagArray = {
            receiver_id: caller,
            room_id: room_id
        };
        updateFlagCall(flagArray, 'miss');
        socket.emit('hang up', data);
        !soundGlobal.calling.paused && soundGlobal.calling.pause();
    });
});

var contentMessageCallVideo = function (data) {
    var st = '';
    var user_id = data.user_id;
    var fullname = data.fullname;
    var isCurrentUser = user_id === parseInt(sessionGlobal);
    
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    
    if (!isCurrentUser) {
        st += '<div class="avatar">';
        if (data.url) {
            st += '<img class="user_avatar" width="100%" src="' + data.url + '"/>'
        } else {
            st += '<i class="fa fa-user"></i>';
        }
        st += '</div>';
    }
    
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat ">';
    st += '<div '
    if (isCurrentUser)
        st += 'style="float:right"'
    st += 'class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time datetime="2009-11-13T20:00"><span>' + fullname + '</span>' + data.time + '</time>';
    st += '<div class="msg">';
    st += '<p><i class="fa fa fa-phone" style="';
    if (data.is_file == 2) {
        st += 'color: red; margin-right: 10px;"></i>';
        if (!isCurrentUser)
            st += jsUcfirst(trans.you) + ' ' + trans.miss_call + ' ' + data.namereceiver;
        else
            st += data.namereceiver + ' ' + trans.miss_call + ' ' + trans.you;
    } else {
        st += 'color: blue; margin-right: 10px;"></i>';
        st += trans.call_end;
    }
    st += '</p>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;
    
    // Test values; Uncomment to check result …
    
    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
    
    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
    
    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
    
    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';
    
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }
    
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    
    // other browser
    return false;
}