var socketRtc = io.connect(helperRtc + '/easyrtc', {secure: true, query: "token=" + token});
/* WebRTC call info */
var rtc = {};
/* Target user id (the other end) */
rtc.targetId = null;
/* Call status, connected = true */
rtc.callConnected = false;
/* Call type, video/audio */
var callType = 'video';
if (window.data && typeof window.data.callType)
    callType = window.data.callType;
/* Target easyrtc ids (the other end), all ids */
rtc.targetEasyrtc = null;
/* Current user easyrtc id */
rtc.selfId = null;
/* Notification sound file */
rtc.notification = urlGlobal + '/themes/default/asset_frontend/sound/shooting_start.mp3';
var inFullScreenMode = false;
rtc.listened = false;
var room_caller = 1;
if (window.data)
    room_caller = window.data.room_caller;

var friend_list = (function () {
    var friends = {};
    $('#friend').find('li').each(function (i, v) {
        friends[v.dataset.id] = v.dataset.roomid;
    });
    return friends;
}());

socketRtc.on('connect', function () {
});

window.onload = connect;

socketRtc.on('disconnect', function () {
});

socketRtc.on('toggle stream state', function (data) {
    /* Toggle video stream */
    if (data.type === 'video') {
        hideRemoteVideo(data.state, 'socket');
    }
});

socketRtc.on('sendAudio', function (data) {
    rtc.callType = data.type;
    if (data && sessionGlobal) {
        if (data.receiver_id === parseInt(sessionGlobal)) {
            rtc.otherRoom = data.room_id;
            if (!rtc.doingCall) {
                rtc.sound[data.room_id] = soundnotification(rtc.notification, true, localStorage['sound' + data.room_id]);
                rtc.targetId = data.initiator;
                rtc.currentCallingRoom = data.room_id;
            } else if (rtc.currentCallingRoom !== data.room_id) {
                socketRtc.emit('user busy', {id: sessionGlobal, friend_id: data.initiator, room_id: data.room_id});
            }
        } else {
            rtc.doingCall = true;
        }
    }
});

socketRtc.on('call accepted', function (data) {
    rtc.doingCall = true;
    /*!rtc.phoneSound.paused && rtc.phoneSound.pause();*/
    if (!rtc.callConnected) {
        if (rtc.sound[data.room_id] && !rtc.sound[data.room_id].paused) rtc.sound[data.room_id].pause();
        $('#cModalAcceptCall').modal('hide');
        // $('#callAcceptButton').trigger('click');
        easyrtc.closeLocalMediaStream(rtc.targetId);
        rtc.currentCallee = [];
    }
});

socket.on('hang up', function (data) {
    window.close();
});

socket.on('callee connected', function (data) {
    var type = data.type;
    if (type == 'waiting') {
        soundGlobal.waiting = soundnotification(rtcwaitingSound, true, 50, false);
    }
});
/****************************************/
/**********area easyrtc connect******/
/********author: vy le ****************/
/*************************************/

function connect() {
    /*easyrtc.enableDataChannels(false);*/
    easyrtc.enableVideo(true);
    easyrtc.enableAudio(true);
    easyrtc.setSocketUrl(":5555");
    easyrtc.setRoomOccupantListener(roomListener);
    easyrtc.connect("easyrtc.dataFileTransfer", loginSuccess, loginFailure);
    addSlash(callType);
}

function roomListener(roomName, occupants, isPrimary) {
    if (rtc.listened) return;
    rtc.listened = true;
    setTimeout(function () {
        if (!rtc.doingCall){
            var flagArray = {
                receiver_id:receiver_idGlobal,
                room_id:room_idGlobal
            };
            updateFlagCall(flagArray,'miss');
            window.close();
        }
                
    }, 60000);
    // check if id is not myselft
    Object.keys(occupants).forEach(function (id) {
        if (id !== rtc.selfId) {
            performCall(id)
        }
    });
}
/* This runs when rtc call is connected */
easyrtc.setStreamAcceptor(function (easyrtcid, stream) {
    soundGlobal && soundGlobal.waiting && !soundGlobal.waiting.paused && soundGlobal.waiting.pause();
    if (!rtc.doingCall) {
        rtc.doingCall = true;
        setUpVideoCall(stream, callType);
        
    }
});

/* This runs when rtc call is closed */
easyrtc.setOnStreamClosed(function (easyrtcid, stream, streamName) {
    !rtc.phoneSound.paused && rtc.phoneSound.pause();
    if (rtc.currentCallee.indexOf(easyrtcid) > -1) {
        hangUp(rtc.currentCallingRoom);
    }
});

/* This runs when rtc call is cancelled by the caller */
easyrtc.setCallCancelled(function (easyrtcid, explicitlyCancelled) {
    rtc.doingCall = false;
    if (explicitlyCancelled) {
        console.log(easyrtc.idToName(easyrtcid) + " stopped trying to reach you");
    }
    else {
        console.log("Implicitly called " + easyrtc.idToName(easyrtcid));
    }
});

/* This runs when target user get a call */
easyrtc.setAcceptChecker(function (easyrtcid, callback) {
    easyrtc.enableVideo(true);
    easyrtc.enableAudio(true);
    easyrtc.initMediaSource(function () {
        callback(true, easyrtcid);
    }, function () {
    }, easyrtcid);
});
/* Remove debuggin notifications */
easyrtc.disconnect = easyrtc.debugPrinter = easyrtc.onError = function () {
};

easyrtc.enableDebug(false);


function loginSuccess(easyrtcid) {
    rtc.selfId = easyrtcid;
    joinRoom(easyrtcid);
}

function loginFailure(errorCode, message) {
    console.log(errorCode, message);
}
$(document)
/*
 *   Toggle video on/off when in a call
 * */
    .on('click', '._video-control', function () {
        var src = easyrtc.getLocalStream();
        if (typeof src === 'object') {
            var j = 0;
            Object.keys(src).forEach(function (i) {
                toggleStream(room_idGlobal, src[i], 'video', j++);
            })
        } else {
            toggleStream(room_idGlobal, src, 'video');
        }
    })
    /*
     *   Toggle audio on/off when in a call
     * */
    .on('click', '._microphone-control', function () {
        var src = easyrtc.getLocalStream();
        if (typeof src === 'object') {
            var j = 0;
            Object.keys(src).forEach(function (i) {
                toggleStream(room_idGlobal, src[i], 'audio', j++);
            })
        } else {
            toggleStream(room_idGlobal, src, 'audio');
        }
    })
    /*
     *  Stop current call
     * */
    .on('click', '._hangup-control', function (e) {
        var array = {
            receiver_id: receiver_idGlobal,
            type: 'hangup'
        };
        var flagArray = {
            receiver_id: receiver_idGlobal,
            room_id: room_idGlobal
        };
        if (!rtc.doingCall) {
            updateFlagCall(flagArray, 'miss');
        }else{
            flagArray.is_file = 3;
            updateFlagCall(flagArray, 'callsuccess');
        }
        socket.emit('hang up', array);
        window.close();
    })
    /*
     *   Resize local video element
     * */
    .on('mousedown', '.resize_video', function (e) {
        resize_video.call(this, e).resize(e);
    })
    /*
     *  Move local video element around
     * */
    .on('mousedown', '.resizable', function (e) {
        resize_video.call(this, e).startMoving(e);
    });


window.onbeforeunload = function (e) {
    var data = {
        receiver_id: receiver_idGlobal,
        type: 'beforeunload',
    };
    socket.emit('hang up', data);
};

/*
 *   Toggle stream on/off
 *   @param {string} room_id : current calling room id
 *   @param {object} stream : Media stream
 *   @param {string} type : audio or video
 *           {Array} type : recursively toggle stream if type is an array of string, i.e : ['audio', 'video']
 *   @next {Bool} next : change button icon if false
 * */
function toggleStream(room_id, stream, type, next) {
    if (!type) return;
    var videos, audios, localVideo, state = null;
    /* If type is string, toggle stream on off */
    if (typeof type === "string") {
        var videoCall = document.getElementById('call_room_id');
        if (type === "audio") {
            audios = stream.getAudioTracks();
            audios.forEach(function (audio) {
                state = audio.enabled = !audio.enabled;
            });
            if (!next) {
                var micro = videoCall.querySelector('.fa-microphone');
                micro.classList.toggle('fa-microphone-slash');
            }
        } else if (type === "video") {
            videos = stream.getVideoTracks();
            videos.forEach(function (video) {
                state = video.enabled = !video.enabled;
            });
            if (!next) {
                var camera = videoCall.querySelector('.fa-video-camera');
                camera.classList.toggle('slash');
                localVideo = videoCall.querySelector('.video-content-myself');
                var local = document.querySelector('.local-video');
                if (state) {
                    localVideo.style.display = 'block';
                } else {
                    localVideo.style.display = 'none';
                }
            }
        }
        if (state !== null && !next) {
            socketRtc.emit('toggle stream state', {
                room_id: room_id,
                type: type,
                state: state,
                target_id: +receiver_idGlobal
            })
        }
    } else if (Array.isArray(type)) {
        /* If type is an array of string, toggle multiple stream on/off recursively */
        var i = type.length;
        while (i--) {
            if (type[i]) {
                toggleStream(room_id, stream, type[i]);
            }
        }
    }
}


function returnTabRoom(data) {
    return $('#tab' + data['room_id']);
}

function returnTabUser(room_id) {
    return $('li[data-roomid="' + room_id + '"]');
}

function returnMainChat(room_id) {
    return $('#main_' + room_id);
}

/*function initcall(targetIds, selfIds) {
    var i = 0;
    
    function call() {
        if (!~selfIds.indexOf(targetIds[i])) {
            var id = targetIds[i];
            easyrtc.enableVideo(true);
            easyrtc.enableAudio(true);
            setTimeout(function () {
                performCall(id);
                i++;
                call();
            }, 200);
        }
    }
    
    call();
}*/

$(document)
/* Toggle fullscreen on/off
 *  */
    .on('click', '._fullscreen-control', fullScreenVideoCall);

function performCall(otherEasyrtcid, data) {
    var acceptedCB = function (accepted, easyrtcid) {
        if (!accepted) {
            /*easyrtc.hangupAll();*/
        }
    };
    var successCB = function (easyrtcid, mediaType) {
        console.log('successCB ', easyrtcid);
    };
    var failureCB = function (errorCode, errMessage) {
        console.log("call to  " + easyrtc.idToName(otherEasyrtcid) + " failed:" + errMessage);
    };
    /*  Initialize stream elements
     * */
    easyrtc.initMediaSource(function () {
        /* After successfully initialize the stream, starts calling */
        easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB, [otherEasyrtcid]);
    }, function () {
    }, otherEasyrtcid);
}

/* Change video src, start it when playable
 * @param {DOM} video : video element
 * @param {string} src : link to the video
 * @return {DOM} video
 * */
function addVideoSrc(video, src) {
    if (src) {
        video.src = src;
        video.oncanplay = video.play;
    }
    return video;
}

/* Capital first letter of a string */
function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1).toLowerCase();
}

/* Get property value of an element
 * @param {DOM} elem : element
 * @param {string} prop : css name of the property, e.g: height, width...
 * */
function getStyle(elem, prop) {
    var res;
    if (window.getComputedStyle.getPropertyValue) {
        res = window.getComputedStyle(elem, null).getPropertyValue(prop)
    }
    else {
        res = window.getComputedStyle(elem)[prop]
    }
    if (res === 'auto') {
        if (prop === 'height' || prop === 'width') {
            prop = capitalizeFirstLetter(prop);
            res = elem['client' + prop];
        } else {
            res = 0;
        }
    }
    return textToNum(res);
}


function textToNum(text) {
    if (!text) return 0;
    return +text.replace(/(\D+)?([\d.]+)(\D+)?/g, "$2") || 0;
}

/* Set up video call
 * @param {string} room_id : Calling room id
 * @param {object} remote_stream : remote stream object
 * @param {string} type : audio or video
 *
 * */
function setUpVideoCall(remote_stream, type) {
    type = type ? type : callType;
    var src = easyrtc.getLocalStream();
    if (typeof src === 'object') {
        src = src[Object.keys(src)[0]];
    }
    var remote_src = remote_stream ? URL.createObjectURL(remote_stream) : "",
        local_src = src ? URL.createObjectURL(src) : '';
    addVideoSrc(document.querySelector('.remote-video'), remote_src);
    addVideoSrc(document.querySelector('.local-video'), local_src);
    if (type != 'video') {
        /*$('video').css('display','none');*/
        hideRemoteVideo(false);
        var videos = src.getVideoTracks();
        videos.forEach(function (video) {
            video.enabled = false;
        });
    } else {
        hideRemoteVideo(true);
    }
    
    /* Change chatbox height to fit the page */
}

/* Hide remote video when video stream is off */
function hideRemoteVideo(status, type) {
    var remote = document.querySelector('.remote-video'),
        imgStatus = remote.previousElementSibling,
        img = imgStatus.querySelector('img');
    var local = document.querySelector('.video-content-myself');
    var videoStatus = document.querySelector('.video-status');
    var resize_video = document.querySelector('.resize_video');
    videoStatus.innerHTML = trans.connected;
    if (status) {
        style(remote, 'display', 'block');
        style(imgStatus, 'display', 'none');
        style(resize_video, 'display', 'block');
        !type && style(local, 'display', 'block');
    } else {
        style(remote, 'display', 'none');
        style(imgStatus, 'display', 'block');
        style(resize_video, 'display', 'none');
        !type && style(local, 'display', 'none');
    }
}


/* Resize and drag video */
function resize_video(e) {
    var self = {};
    var target = this;
    var node = $(this).closest('.resizable')[0];
    /* Move video element around */
    self.move = function (x, y) {
        node.style.left = x + 'px';
        node.style.top = y + 'px';
        node.style.opacity = 0.5;
    };
    
    self.resize = initDrag;
    var ratio;
    var startX, startY, startWidth, startHeight, video_wrapper, outerWidth, outerHeight;
    /*
     * Calculate video element dimensions and placement before resizing
     * */
    function initDrag(e) {
        video_wrapper = $(node).closest('.video-call-wrapper');
        outerWidth = video_wrapper.outerWidth(true);
        outerHeight = video_wrapper.outerHeight(true);
        node = $(target).closest('.resizable')[0];
        startX = e.clientX;
        startY = e.clientY;
        startWidth = parseInt(document.defaultView.getComputedStyle(node).width, 10);
        startHeight = parseInt(document.defaultView.getComputedStyle(node).height, 10);
        ratio = startWidth / startHeight;
        document.documentElement.addEventListener('mousemove', doDrag, false);
        document.documentElement.addEventListener('mouseup', stopDrag, false);
    }
    
    /*
     *  Calculate video element dimensions when resizing
     * */
    function doDrag(e) {
        var width = (startWidth + e.clientX - startX);
        width = width >= 0.66 * outerWidth ? 0.66 * outerWidth : width;
        width = width <= 100 ? 100 : width;
        var height = width / ratio;
        if (height >= 0.66 * outerHeight) {
            height = 0.66 * outerHeight;
            node.style.width = ((height * ratio >>> 0) / outerWidth * 100) + '%';
            node.style.height = '66%';
        }
        else {
            node.style.width = (width >>> 0) / outerWidth * 100 + '%';
            node.style.height = (height >>> 0) / outerHeight * 100 + '%';
        }
    }
    
    /* Remove element when resizing stops */
    function stopDrag(e) {
        document.documentElement.removeEventListener('mousemove', doDrag, false);
        document.documentElement.removeEventListener('mouseup', stopDrag, false);
    }
    
    var cWi, cHe, eWi, eHe, diffX, diffY;
    /*
     *  Calculate video element dimensions and placement before moving
     * */
    var selfMove = function (e) {
        video_wrapper = $(node).closest('.video-call-wrapper');
        cWi = video_wrapper.outerWidth(true);
        cHe = video_wrapper.outerHeight(true);
        var divTop = getStyle(node, 'top'),
            divLeft = getStyle(node, 'left');
        /*var divTop = offset.top,
         divLeft = offset.left;*/
        eWi = node.offsetWidth;
        eHe = node.offsetHeight;
        var posX = e.clientX,
            posY = e.clientY;
        document.body.style.cursor = 'move';
        diffX = posX - divLeft;
        diffY = posY - divTop;
        document.addEventListener('mousemove', mouseMove, false);
    };
    
    /* Calculate video element border when moving */
    var mouseMove = function (evt) {
        evt = evt || window.event;
        var posX = evt.clientX,
            posY = evt.clientY,
            aX = posX - diffX,
            aY = posY - diffY;
        if (aX < 0) aX = 0;
        if (aY < 0) aY = 0;
        if (aX + eWi > cWi) aX = cWi - eWi;
        if (aY + eHe > cHe) aY = cHe - eHe;
        self.move(aX, aY);
    };
    /* Remove moving events when drag stops
     * */
    var selfStop = function () {
        document.body.style.cursor = 'default';
        node.style.opacity = 1;
        document.removeEventListener('mousemove', mouseMove, false);
        // document.documentElement.removeEventListener('mousedown', selfMove, false);
        document.documentElement.removeEventListener('mouseup', selfStop, false);
    };
    
    /*
     *  This runs when video element is dragged
     * */
    self.startMoving = function (e) {
        if (!$(e.target).is('.resize_video')) {
            node = target;
            selfMove(e);
            // document.documentElement.addEventListener('mousedown', selfMove, false);
            document.documentElement.addEventListener('mouseup', selfStop, false);
        }
        return self;
    };
    
    return self;
}

function changeWindowSize(video) {
    video.addEventListener('play', function () {
        window.resizeTo(document.body.clientWidth, document.body.clientHeight + 50);
        formerHeight = window.innerHeight;
    });
}

document.fullscreenElement = document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement || false;
HTMLElement.prototype.requestFullscreen = HTMLElement.prototype.requestFullscreen
    || HTMLElement.prototype.webkitRequestFullscreen
    || HTMLElement.prototype.webkitRequestFullScreen
    || HTMLElement.prototype.mozRequestFullScreen;
document.exitFullscreen = document.exitFullscreen ||
    document.webkitExitFullscreen ||
    document.mozCancelFullScreen ||
    document.msExitFullscreen || false;
/*
 *  Toggle full screen on/off
 *  @param {event} e : un-used
 *  @param {Bool} forced : force exit full screen if true
 * */
function fullScreenVideoCall(e, forced) {
    var tab = $(this).closest('.video-call-wrapper')[0];
    toggleFullscreen(tab, fullScreenBackground.bind(this), !!forced);
}

/*
 *  Change video call background when in full screen mode
 * */
function fullScreenBackground(e) {
    var style = e.style;
    if (e.dataset.fullscreen === 'full_screen') {
        $(this.querySelector('i')).addClass('fa-compress').removeClass('fa-arrows-alt');
        style.backgroundImage = window.getComputedStyle(document.body)['background-image'];
        style.backgroundAttachment = 'fixed';
        style.backgroundSize = 'cover';
    } else {
        $(this.querySelector('i')).removeClass('fa-compress').addClass('fa-arrows-alt');
        style.backgroundImage = '';
    }
}

/*
 *  Toggle fullscreen on/off
 *  @param {DOM} e : chat tab element
 *  @param {function} cb: callback
 *  @param {Bool} forced : force full screen exit if true
 * */
function toggleFullscreen(e, cb, forced) {
    if (forced) {
        inFullScreenMode = false;
        e.dataset.fullscreen = 'window';
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    } else {
        if (e.dataset.fullscreen !== 'full_screen') {
            inFullScreenMode = true;
            if (e.requestFullscreen) {
                e.requestFullscreen();
                e.dataset.fullscreen = 'full_screen';
            }
        } else {
            inFullScreenMode = false;
            e.dataset.fullscreen = 'window';
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
        }
    }
    cb && cb(e);
}
/*
 *  Pre-join to all rtc room
 * */
var roomJoined = false;
function joinRoom(rtcid) {
    easyrtc.joinRoom(room_idGlobal, null, function (room_id) {
        //todo: send socket
        if (room_caller == 1 && !roomJoined) {
            roomJoined = true;
            var data = {
                easyrtc_id: rtcid,
                room_id: room_id,
                receiver_id: receiver_idGlobal,
                callType: callType
            };
            socket.emit('callee', data);
        }
        
    });
}

/*  Change element style
 *  @param {DOM} elem : element to be changed
 *          {collection} elem : element collection, allowing changes to multiple elements
 *  @param {string} name : css name
 *          {object} name : {css_name : css_value}, allowing multiple changes to the element
 *  @param {string} value : css value, not used if name is an object
 *
 *  */
function style(elem, name, value) {
    if (NodeList.prototype.isPrototypeOf(elem)) {
        for (var j = 0, length = elem.length; j < length; j++) {
            style(elem[j], name, value);
        }
    } else {
        if (typeof name === 'object') {
            Object.keys(name).forEach(function (i) {
                elem.style[i] = name[i];
            });
        } else {
            elem.style[name] = value;
        }
    }
}

function addSlash(status) {
    if (status == 'audio') {
        var video = document.querySelector('.fa-video-camera');
        video.classList.toggle('slash');
    }
}

function convertToLocalTime(date, type) {
    date = new Date(date);
    var local_date = new Date();
    var offset = date.getTimezoneOffset() * 60000;
    if (type == 'notice')
        offset = 0;
    var localOffset = local_date.getTimezoneOffset() * 60000;
    var localTime = date.getTime() + offset - localOffset;
    date = new Date(localTime);
    return date;
}

