var searchByName = (function () {
    var self = {};
    var timeout;
    var friend_tab = document.getElementById('tabaddfriend');
    
    function search() {
        var that = this;
        if (timeout) window.clearTimeout(timeout);
        timeout = window.setTimeout(function () {
            var value = that.value;
            value = value ? value.trim() : '';
            var url = fullUrl;
            url = url + '/chat/searchUser';
            // value = stripVowelAccent(value);
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {name: value},
                beforeSend: function (xhr) {
                    var spinner = '<div class="spin-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
                    friend_tab.querySelector('.append-all-friend div').innerHTML = spinner;
                    /*$('.search_more').css('display','none');*/
                    
                }
            }).done(returnResultSearchToAddfriend)
        }, 600);
        return self;
    }
    
    self.elasticSearch = function (elementSearch, elementChild) {
        // var $title = $('.tabaddfriend .titile-allfriend').find('p');
        // var title = $title.text;
        var elem = document.getElementById(elementSearch);
        if (elem) {
            elem.oninput = search;
            // elem.addEventListener('input', search, false);
        }
        return self;
    };
    
    self.search = search;
    
    var returnResultSearchToAddfriend = function (data) {
        // var friend_tab = $('.tabaddfriend');
        // friend_tab.find('.titile-allfriend').find('p').first().html(data.data.title);
        // friend_tab.find('.append-all-friend').find('div').first().html(data.data.htmlUser);
        if (friend_tab) {
            friend_tab.querySelector('.titile-allfriend p').innerHTML = data.data.title;
            friend_tab.querySelector('.append-all-friend div').innerHTML = data.data.htmlUser;
        }
        /*if(data.data.type == 'getfriend')
            $('.search_more').css('display', 'block');*/
        return self;
    };
    
    self.nanoScrollerFunction = function (element) {
        element.nanoScroller();
        return self;
    };
    
    self.calculatorNanoGroup = function (room_id) {
        var tab = returnTab(room_id);
        var tabgroup = tab.find('.voltaic-group');
        var height_header = tabgroup.find('.header-chat-profile').outerHeight(true);
        var he_body = tabgroup.find('.receive-tab').outerHeight(true);
        var hei = 'calc(100vh - ' + (height_header + he_body) + 'px)';
        $('.scroll-tab-group').css('height', hei);
        return self;
    };
    return self;
})();


var actionGroup = (function () {
    var self = {};
    
    self.deleteGroup = function (room_id) {
        
        return self;
    };
    
    self.updateWhenDeleteGroup = function (room_id) {
        /**
         * delete notice
         */
        var noticeLi = $('.notification-success[data-roomid="' + room_id + '"]');
        noticeLi.length && noticeLi.remove();
        /**
         * delete
         */
        var list_user = $('.list-user');
        var liGroup = list_user.find('li[data-roomid="' + room_id + '"]');
        
        var str = '<div class="error load-message-error"><span>' + trans.group_has_been_deleted + '</span></div>';
        var tab = returnTab(room_id);
        tab.length && tab.empty() && tab.html(str);
        tab.css('width', '100%');
        liGroup.remove();
        
        return self;
    };
    return self;
    
})();

var askBeforeAction = (function () {
    
    var self = {};
    self.yesNoFuntion = function (text, room_id) {
        var modalConfirm = $('#confirm-delete');
        modalConfirm.find('.modal-body p').text(text);
        room_id && modalConfirm.find('.btn-ok').data('roomid', room_id);
        modalConfirm.modal('show');
        return self;
    };
    return self;
    
})();


$(document).ready(function () {
    var tabFriend = $('.add-friend-chat');
    var idTabFriend = $('#tabaddfriend');
    
    tabFriend.click(function () {
        setTimeout(function () {
            searchByName.search('');
        }, 600);
        history.pushState(null, null, '/' + lang + '/chat/');
        active_tab(idTabFriend);
        sortByName.searchClient('search-friend-tabdashboard', '.user-afriend');
    });
    
    setTimeout(function () {
        if (idTabFriend.hasClass('active')) {
            searchByName.search('');
            sortByName.searchClient('search-friend-tabdashboard', '.user-afriend');
        }
    }, 600);
    
    $(document).on('input', '#search-addfriend', searchByName.search);
    
    $(document).on('click', '.ul-setting-group li[value="delete"]', function () {
        var room_id = $(this).data('roomid');
        askBeforeAction.yesNoFuntion(trans.are_you_sure_delete_group, room_id);
    });
    
    $(document).on('click', '.setting-group', function () {
        $('.ul-setting-group').slideToggle();
    });
    
    $(document).on('click', '#confirm-delete .btn-ok', function () {
        var room_id = $(this).data('roomid');
        var data = {
            room_id: room_id
        };
        room_id && actionGroup.updateWhenDeleteGroup(room_id);
        $('#confirm-delete').modal('hide');
        data && socket.emit('deleteGroup', data);
    });
    
    $(document).on('click', '.class-opentab-user .user-afriend', function () {
        var $friend_id = $(this).find('.user-chat').data('id');
        if ($friend_id)
            $('ul.list-user li[data-id="' + $friend_id + '"]').trigger('click');
    });
    var add_friend = $('#main_addfriend');
    add_friend.on('click', '.fa-user-plus', function () {
        var url = fullUrl + '/chat/addRemoveFriend';
        var id = $(this).closest('.icon-addfriend').siblings('.active-user-info').data('id');
        var self = $(this);
        var action = 'addfriend';
        var data = {id: id, action: action};
        addFriend(data, url, self);
        self.attr('title', trans.waiting_to_accept);
        self.addClass('fa-spinner').removeClass('fa-user-plus');
    });
    
    add_friend.on('click', '.chat-us-tt', function () {
        var friend_id = $(this.parentNode).data('id');
        var url = fullUrl + '/chat/getInfoUser';
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {friend_id: friend_id},
            beforeSend: function () {
            },
        }).done(function (str) {
            $('#voltaic_holder_addfriend').html(str);
            openNav('addfriend');
        }).fail(function (err) {
            
        })
    });
    
});

