"use strict";
var socket = io.connect(helperUrl + '/chat', {secure: true, query: {token: token, lang: lang}});
/****************************************/
/**********area socket.io******/
/********author: vy le ****************/
/*************************************/
var fullUrl = urlGlobal + '/' + lang;
var online_users = {};
var window_is_active = false;
var basicSound = urlGlobal + '/themes/default/asset_frontend/sound/message.mp3';
var rtccallingsound = urlGlobal + '/themes/default/asset_frontend/sound/shooting_start.mp3';
var rtcwaitingSound = urlGlobal + '/themes/default/asset_frontend/sound/phone-calling.mp3';
var soundGlobal = {};
var lastest_messages = new Map();
localStorage.baseVolume = localStorage.baseVolume || 100;
var $inputMessage = $('.input-message');
var user_id = sessionGlobal;
var popup;
var callType;
var isCalling = false;
var messageInActive = '';
var roomActive = '';
/*
 *  Window focus event, return true if this tab is the browser's active tab.
 *  Inactive tabs will receive notifications
 *
 * */
window.addEventListener('focus', function () {
    window_is_active = true;
});
window.addEventListener('blur', function () {
    window_is_active = false;
});
/*
 *  Change user status and favicon when socket is connected
 * */
socket.on('connect', socketConnection);
/*
 *  Change user status and show gray favicon when socket is disconnected
 * */
socket.on('disconnect', socketDisconnection);

/*
 *  Update message when url is successfully parsed and the site is scraped
 * */
socket.on('update message', function (data) {
    var msg = document.getElementById(data.uuid);
    if (msg) {
        var text = msg.querySelector('.msg-text');
        if (text) {
            text.innerHTML = data.content;
        }
    }
});

socket.on('lost messages', getLostMessages);

/*
 *  Append chat message when receiving one on socket
 * */
socket.on('socketSendText', appendMessageFile);

/*
 *  Show users' statuses when their statuses change
 * */
socket.on('change status', function (data) {
    if (data) {
        if (data.status === 'offline') {
            delete online_users[data.user_id];
        } else {
            online_users[data.user_id] = data;
        }
        sortByName.sort_all_users(online_users);
    }
});

/*
 *  Append file message when receiving one on socket. Sender will not process it.
 * */
socket.on('uploadFileServer', function (data) {
    /* If sender is found, return */
    if (document.getElementById(data.id)) return;
    if (data) {
        data.time = convertToLocalTime(data.date);
        data.time = getTimePmAm(data.time);
        if (data.user_id !== parseInt(sessionGlobal)) {
            /* If target is not sender, play a sound */
            soundnotification(basicSound, false, localStorage['sound' + data.room_id]);
            data.htmlContent = contentMessage(data);
            
        } else {
            /* Append content differently if target browser tab belongs to the sender */
            data.htmlContent = selfMessage(data);
        }
        data.type = 'file';
        if (data.is_file == 2 || data.is_file == 3) {
            data.htmlContent = contentMessageCallVideo(data);
            
        }
        window.data = data;
        appendMessageFile(data, true);
        // var tab_room = returnTabRoom(data);
        // var location_file = tab_room.find('.chat-msg-file').last();
        // location_file.find('.download-file').attr('href', data['destinationPath']);
    }
});

/*
 *  Show user's friends' statuses on socket connection
 * */
socket.on('userStatus', function (data) {
    if (data) {
        online_users = data;
    }
    if (typeof rtc !== 'undefined' && !data[rtc.targetId] && rtc.currentCallingRoom) hangUp(rtc.currentCallingRoom);
    sortByName.sort_all_users(online_users);
});

/**
 * Get user's mood when they change theirs
 * */
socket.on('change mood', function (data) {
    if (!data || typeof data !== 'object') return;
    $('.list-user').find('a[data-id="' + data.user + '"]').find('.active-user-info').attr('title', data.mood);
    $('#main_' + data.room_id).find('.user-mood-tt').html(data.mood);
});

/*
 *
 * */
socket.on('appendRelative', function (data) {
    if (data) {
        $('#voltaic_holder_' + data['room_id']).find('.user-relative-chat').data('option', data['relative']);
    }
});

/*
 *  Get new group picture when it changes
 * */
socket.on('change group picture', function (data) {
    var room_id = data.room_id;
    var url = data.url;
    $('#voltaic_holder_' + room_id).find('.hovereffect').css('background-image', 'url(' + url + ')');
    $('li[data-roomid="' + room_id + '"]')
        .find('.avatar')
        .html('<img src="' + url + '" alt="avatar" class="img-responsive center-block">');
});
/*
 *
 * */
socket.on('sendNotice', function (data) {
    var notifi = $('.notifi-box').find('ul');
    if (data.type == 'cancel-request' || data.type == 'block') {
        deleteNotice(data.notice_id);
        countNotifiMessage.countNotifi('countNotice');
    }
    if (data.type == 1) {
        var url = fullUrl + '/chat/loadNotification';
        loadNotification(url);
        soundnotification(basicSound, false, localStorage.baseVolume);
        countNotifiMessage.countNotifi('countNotice');
    } else if (data.type == 3 || data.type == 4) {
        var string = stringAppendNotice(data, 'socket');
        var childli;
        if ((childli = notifi.find('li'))) {
            childli.first().before(string);
        } else {
            notifi.append(string);
        }
        soundnotification(basicSound, false, localStorage.baseVolume);
        countNotifiMessage.countNotifi('countNotice');
    }
    if (data.type === 1 || data.type === 'cancel-request' || data.type == 3 || data.type == 4) {
        changeButtonLayout(data);
    }
});

/*
 *
 * */
socket.on('countMessage', function (data) {
    if (data) {
        var active = $(document.activeElement);
        var classCount;
        if (active.is('.input-message') && active.data('roomid') === data.room_id && window_is_active) {
            return countNotifiMessage.socketIsRead(room_id, 'message');
        } else if ($('.right-content.active').data('roomid') !== data.room_id || !window_is_active) {
            soundnotification(basicSound, false, localStorage['sound' + data.room_id]);
        }
        var count = $('#left-content-bar').find('#recent-active-room li[data-roomid="' + data.room_id + '"]');
        if (count.find(".count-message").length > 0) {
            count.find('.count-message').text(data.countMessage);
        } else {
            count.find('a').append('<span class="count-message">' + data.countMessage + '</span>');
            count.find('.user_name').css('font-weight', '700');
        }
        /*countNotifiMessage.countNotifi('countRecent');*/
        countNotifiMessage.countNotifi('countMessage');
    }
});

/*
 * */
socket.on('count notification message', function (data) {
    var option = data.type;
    var count = data.count;
    countNotifiMessage.appendNoticeMessage(option, count);
});

/*
 * */
socket.on('updateIsRead', function (data) {
    if (data) {
        var room_id = data.room_id;
        var type = data.type;
        if (type == 'message')
            countNotifiMessage.updateIsReadMessage(room_id, 0);
        else
            countNotifiMessage.appendNoticeMessage('notification', 0);
    }
    
});

socket.on('deleteGroup', function (data) {
    var room_id = data.room_id;
    room_id && actionGroup.updateWhenDeleteGroup(room_id);
    
});

socket.on('user busy', function (data) {
    if (!data) return;
    var user_value = data.user_value;
    var receiver_value = data.receiver_value;
    if (user_value)
        return showError(trans.close_window_before_call);
    if (receiver_value)
        return showError(trans.user_busy_call_later);
    var receiver_id = data.receiver_id;
    var room_id = data.room_id;
    var url = fullUrl + '/chat/video';
    var type = data.type;
    popup = PopupCenter(url + '/' + receiver_id + '/' + room_id, 'vitzoo', '1000', '700');
    popup.data = {
        room_caller: 1,
        receiver_id: receiver_id,
        callType: type
    };
    isCalling = true;
    
});

/**
 * get infomation from caller to show popup accept call audio, video
 * @param data
 */
socket.on('callee', function (data) {
    /**
     * take param from server transfer data include
     * name's caller, room id, id's caller, rtcid's caller
     * @type {string}
     */
    if (!isCalling) {
        var caller_name = data.name,
            caller_id = data.caller_id,
            room_id = data.room_id,
            rtc_idcaller = data.easyrtc_id,
            caller_url = data.url_caller;
        
        var modal = $('#cModalAcceptCall');
        var user_name = modal.find('.rtc_user_name');
        var button = modal.find('.button-accept-decline');
        button.data('easyrtc', rtc_idcaller);
        button.data('roomid', room_id);
        button.data('caller', caller_id);
        callType = data.callType;
        caller_url && modal.find('img').attr('src', caller_url);
        user_name.html(caller_name + ' ' + user_name.data('lang'));
        modal.modal({backdrop: 'static', keyboard: false});
        if (modal.length)
            soundGlobal.calling = soundnotification(rtccallingsound, true, 50, false);
    }
});


socket.on('hang up', function (data) {
    $('#cModalAcceptCall').modal('hide');
    soundGlobal && soundGlobal.calling && !soundGlobal.calling.paused && soundGlobal.calling.pause();
    isCalling = false;
});

socket.on('createGroup', function (data) {
    addGroupToAnother(data);
});
/****************************************/
/**********area function****************/
/********author: vy le ****************/
/*************************************/

/*
 *  SortByName object, return sorting functions
 * */
var sortByName = (function () {
    var friends = document.querySelector('.friend-listing');
    var groups = document.getElementById('group-listing');
    /*
     *  If neither friends nor groups element exists, the page is not chat page, return empty functions
     * */
    if (!friends || !groups) return {
        sort_all_users: function () {
        },
        sort_group_name: function () {
        },
        searchClient: function () {
        }
    };
    /* Object to be returned */
    var self = {};
    /* Private sorting function, convert node collection into array then sort by name */
    var sort = function (users) {
        var arr = [];
        if (users) {
            arr = Array.prototype.slice.call(users, 0);
            return arr.sort(function (a, b) {
                var string1 = a.dataset.name.trim(),
                    string2 = b.dataset.name.trim();
                return string1 > string2 ? 1 : -1;
            });
        }
        return arr;
    };
    
    /* Create a new set object for online users */
    self.online_users = new Set();
    /* Private variables */
    var friend_list = friends.getElementsByTagName('li'),
        friend_length = friend_list.length,
        users = sort(friend_list),
        group_list = groups.getElementsByTagName('li'),
        rooms, room_length = 0;
    var recent = document.getElementById('recent');
    /* Sort all users, utilizing flexbox css
     * @param {object} data : online users' data, including user id, status and mood
     * return {object} self
     * */
    self.sort_all_users = function (data) {
        /* If new friends are added or friends are removed/unfriended, sort friends by name again */
        if (friend_list.length !== friend_length) {
            friend_length = friend_list.length;
            users = sort(friend_list);
        }
        /* {int} v : order number for online users
         * {int} k : order number for offline users
         *  users are sorted by order numbers, higher ones are placed below lower ones
         * */
        var v = 0,
            k = friend_length;
        users.forEach(function (user) {
            var a = user.querySelector('a');
            /* Get user id, convert it into integer */
            var i = +a.dataset.id;
            /* Get user status element */
            var i_tag = $(a.querySelector('.status'));
            var info = $(a.querySelector('.active-user-info'));
            var recent_user = recent.querySelector('a[data-id="' + i);
            if (data[i] && data[i].status) {
                /* User is online */
                /* Flexbox css, order by number */
                user.style.order = v++;
                /* Add this user to online user list */
                self.online_users.add(i);
                /* Change user status in recent tab */
                $(recent_user).find('.status').html('<i class="' + data[i].status + '"></i>' + trans[data[i].status]);
                /* Change user mood in recent tab */
                $(recent_user).find('.active-user-info').attr('title', data[i].mood);
                /* Change user mood in friends tab */
                info.attr('title', data[i].mood);
                /* Change user status in friends tab */
                i_tag.html('<i class="' + data[i].status + '"></i>' + trans[data[i].status]);
            }
            else {
                /* User is offline */
                /* Flexbox css, order by number,
                 k is bigger than total number of users to make sure offline users are always below */
                user.style.order = k++;
                /* Remove this user from online user list */
                self.online_users.delete(i);
                /* Change user status in friends tab */
                $(recent_user).find('.status').html('<i class="offline"></i>' + trans.offline);
                i_tag.html('<i class="offline"></i>' + trans.offline);
            }
        });
        //todo
        /*joinRoom(self.online_users);*/
        return self;
    };
    /* Sort all group, utilizing flexbox css */
    self.sort_group_name = function () {
        /* If groups are sorted and no groups are added, stop */
        if (group_list.length === room_length) {
            return self;
        }
        /* Save group length to ram */
        room_length = group_list.length;
        /* Sort group by name in ascending order */
        rooms = sort(group_list);
        var i = 0;
        rooms.forEach(function (room) {
            /* User flexbox order number, lower is placed higher */
            room.style.order = i++;
        });
        return self;
    };
    
    /* Search by name client side */
    self.searchClient = function (elementSearch, elementChild) {
        var searchStyle = document.getElementById('search_style');
        var searchEl = document.getElementById(elementSearch);
        /*
         *  If either elements exists, return;
         * */
        if (!searchEl || !searchStyle) return self;
        searchEl.addEventListener('input', function (e) {
            e.preventDefault();
            if (!this.value) {
                searchStyle.innerHTML = "";
                return;
            }
            searchStyle.innerHTML = elementChild + ":not([data-name*=\"" + stripVowelAccent(this.value) + "\"]) { display: none!important; }";
        });
        return self;
    };
    return self;
})();

$(document).ready(function () {
    sessionStorage.clear();
    sortByName.sort_group_name();
    if ($('#search-list-user').length)
        sortByName.searchClient('search-list-user', '.new-friend');
    
    $(document).on('keydown', '.input-message', function (event) {
        if (!$(this).is('textarea')) return;
        var self = this;
        var room_id = $(this).data('roomid');
        /* If key press is enter, shift key is not pressed */
        if (event.keyCode === 13 && !event.shiftKey) {
            /* Check for "/alert on/off command" */
            var reg = /^\/((alert) (off|on))$/gi;
            if (self.value) {
                /* If text content is "alert on/off" replace it with empty string and run command */
                self.value = self.value.replace(reg, assessCommand.bind(self));
            }
            event.preventDefault();
            this.dataset.typed = 'false';
            sendMessage($(this));
            this.value = '';
            event.preventDefault();
        } else if (event.keyCode === 27) {
            self.value = '';
            this.dataset.typed = 'false';
        } else if (!event.shiftKey && this.dataset.typed !== 'true') {
            /* If shift key is not pressed and user did not type anything
             * Get previous content on key up and down
             * */
            if (event.keyCode === 38) {
                self.value = '';
                self.value = prevChatContent(room_id).getPrevTextContent() || '';
            } else if (event.keyCode === 40) {
                self.value = '';
                self.value = prevChatContent(room_id).getNextTextContent() || '';
            }
        }
        else if (event.keyCode === 13 && event.shiftKey) {
            /* If both shift key and enter are pressed
             * Add whitespace and linebreak to assure corrent url parsing
             * */
            event.preventDefault();
            // var text       = self.value;
            // var currentPos = self.selectionStart;
            // self.value     = text.substr(0, currentPos) + ' \n' + text.substr(currentPos);
            // select(self, currentPos + 2);
            self.insertAtCaret(' \n');
            // self.value = self.value + ' \n';
            this.dataset.typed = 'true';
            self.scrollTop = self.scrollTop + 20;
        }
        if (event.keyCode !== 38 && event.keyCode !== 40 && event.keyCode !== 13) {
            /*
             * textarea data-typed change to true when user press any key
             * */
            this.dataset.typed = 'true';
        }
    });
    var timeOut;
    /* Emit user typing on chatbox */
    $(document).on('input', '.input-message', function (event) {
        /* If timeout is not cleared or textarea is empty, return */
        if (timeOut || !this.value) return;
        var room_id = $(this).data('roomid');
        /* Emit typing message once every second at most */
        socket.emit('typing', {room_id: room_id});
        timeOut = setTimeout(function () {
            timeOut = false;
        }, 1000);
    });
    
    $(document).on('click', '.notifi-box ul li', function (e) {
        var notice_id = $(this).data('id');
        var $this = $(this);
        var url = $(this.parentNode).data('url');
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {notice_id: notice_id},
            beforeSend: function () {
            },
        }).done(function (str) {
            if (str)
                $this.removeClass('none_read');
        }).fail(function (err) {
            
        })
    });
    
    $(document).on('click', '.btn-delete-request', function (e) {
        var $this = $(this);
        setTimeout(function () {
            $this.parent().html('<button class="btn btn-notice btn-confirmed">' + trans.deleted_request + '</button>');
        }, 2000);
    });
    
    $(document).on('click', '.btn-confirm', function (e) {
        var $this = $(this);
        setTimeout(function () {
            $this.parent().html('<button class="btn btn-notice btn-confirmed">' + trans.confirmed + '</button>');
        }, 2000);
    });
    
    $(document).on('click', '.chatbutton', function (e) {
        e.preventDefault();
        var input = $(this).prev().find('textarea');
        sendMessage(input);
    });
    
    $('.btn-bell-notifi').click(function (e) {
        e.preventDefault();
        var url = $(this).data('href');
        var type = 'click_bell';
        if (sessionStorage['bellnotifi'] != 1)
            loadNotification(url);
        if ($(this).find('.count-notifi').length) {
            setTimeout(function () {
                countNotifiMessage.socketIsRead(0, 'notice');
            }, 1000);
        }
        
        /*countNotifi('countNotice', 'updateIsRead');*/
        /*countNotifiMessage.socketIsRead(0,'notice');*/
    });
})
;

function unescape(string) {
    return string.replace(/&nbsp;/g, " ").replace(/<br>/g, '\n')
}

function sendMessage(input) {
    var room_id = input.data('roomid');
    room_id = room_id ? room_id : (input.closest('.chatbox').data('roomid'));
    var text = input.val().trim();
    if (text && room_id) {
        prevChatContent(room_id).newTextMessage(text);
        if (!input.data('timeout')) {
            input.data('timeout', setTimeout(function () {
                input.data('timeout', 0);
            }, 100));
            var target_id = input.data('receiver');
            var datasocket = {
                room_id: room_id,
                content: text,
                user_id: user_id,
                token: token,
                target_id: target_id
            };
            socket.emit('socketSendText', datasocket);
            input.val('');
        } else {
            var data = {};
            data.room_id = room_id;
            data['date'] = Date.now();
            var uuid = UUID.generate();
            data['content'] = 'You are typing too fast, please slow down.';
            data.id = uuid;
            data['htmlContent'] = selfMessage({
                uuid: uuid,
                content: data['content'],
                fullname: nameSession,
                time: getTimePmAm(new Date()),
                type: 'message',
            });
            appendMessageFile(data);
        }
    }
}

/* Append message to chatbox */
function appendMessageFile(data, file, lost) {
    if (!lost) {
        var recent = document.getElementById('recent');
        var room_id = data.room_id;
        var li = '';
        //todo: error create recent when
        if (recent){
            li = recent.querySelector('#left-content-bar li[data-roomid="' + room_id + '"]');
        }
        if (li) {
            /* If recent item exists, bring it to the top of recent list */
            bottomToTop(li);
        } else {
            /* Else copy it from friend or group list and bring it to the top of recent list */
            li = document.querySelector('#left-content-bar li[data-roomid="' + room_id + '"]');
            if (recent)
                bottomToTop(li, recent.querySelector('ul'), true);
        }
    } else {
        lastest_messages.set(data.room_id, 0);
    }
    /*  scroll to the top of the list
     *  */
    $('.nano2').nanoScroller({scroll: 'top'});
    var $tabRoom = $('#tab' + data['room_id']);
    if ($tabRoom.length > 0) {
        /* Get the last article element */
        var article = $tabRoom.find('.article').last()[0];
        /* Initiate timeDistinct and test element, make sure that they're different */
        var timeDistinct = null;
        var test = NaN;
        /* Get chatbox */
        var chat_content = $tabRoom.find('.content-chat');
        if (chat_content[0]) {
            /* If chatbox exists, find typing element */
            var typing = chat_content[0].querySelector('.typing');
            /* Get user typing elements */
            if (!lost) {
                var users = typing.getElementsByClassName('appendage');
                if (users.length) {
                    /* Remove typing message when receiving message */
                    var user = typing.querySelector('[data-id="' + data.user_id);
                    if (user) {
                        user.parentElement.removeChild(user);
                        /* If no one is typing anymore, hide typing element */
                        if (!users.length) {
                            typing.style.display = 'none';
                        }
                    }
                }
            }
        }
        /* Convert to local time */
        var time = convertToLocalTime(data['date']);
        /* Convert time to 12h format */
        var clockTime = getTimePmAm(time);
        if (article) {
            /* If article exists, get its time */
            timeDistinct = article.dataset.time;
            test = toDMY(time);
        }
        /* If the message date is the same as article's one, append new message */
        if (timeDistinct === test) {
            $(typing).before(data.htmlContent);
        }
        else {
            /* Else, add new article for the current date */
            article = insertArticle(time, getDateTimeLang(lang));
            $(typing).before(article);
            $(typing).before(data.htmlContent);
        }
        if (!file) {
            /* Add timestamp to chat message */
            chat_content.find('#' + data.id).find('.clock-time').html('&nbsp;' + clockTime);
            scrollBottom(data);
        }
    }
}

function insertArticle(time, format) {
    var article = document.createElement('article'),
        div_time = document.createElement('div'),
        p = document.createElement('p'),
        div = document.createElement('div');
    article.dataset.time = toDMY(time);
    div_time.classList.add('date-time');
    div_time.dataset.item = 'a1';
    div_time.dataset.time = formatDateTime(convertToLocalTime(time));
    p.innerHTML = format;
    div.classList.add('row', 'row-append');
    div_time.appendChild(p);
    article.appendChild(div_time);
    article.appendChild(div);
    article.classList.add('clearfix', 'article');
    return article;
}

/* Being element to the top of parent
 * @param {DOM} el - Element
 * @param {DOM} parent - Assigned parent for the element, if parent is empty, get element's parent instead
 * @parant {Bool} html - If html is true, prepend el's outer html content to parent, else prepend el's inner html content to parent
 * return {DOM} parent
 * */
function bottomToTop(el, parent, html) {
    if (!el) return false;
    if (!parent) {
        parent = el.parentElement;
    }
    if (!parent) return false;
    if (html) {
        return parent.insertAdjacentHTML('afterbegin', el.outerHTML);
    }
    return parent.insertBefore(el, parent.firstElementChild);
}

function contentTime(data, lang) {
    var st = '';
    st += '<article>';
    st += '<div class="date-time" data-item="a1" data-time="' + data['date'] + '">';
    st += '<p>' + getDateTimeLang(lang) + '</p>';
    st += '</div>';
    st += '<div class="row row-append">';
    st += '</div>';
    st += '</article>';
    st += data['htmlContent'];
    return st;
}

function getTimePmAm(date) {
    var hours = date.getHours() % 24;
    var minutes = date.getMinutes();
    var mid = 'AM';
    if (hours == 0) {
        hours = 12;
    }
    else if (hours > 12) {
        hours = hours % 12;
        mid = 'PM';
    }
    return addZero(hours) + ':' + addZero(minutes) + ' ' + mid;
}

function getDateTimeLang(lang) {
    var time = new Date();
    var date = time.getDate();
    var day = time.getDay();
    var m = time.getMonth();
    var y = time.getFullYear();
    if (lang) {
        if (lang === 'en')
            return transDay(lang)[day] + ', ' + transMonth(lang)[m] + ' ' + date + ', ' + y;
        return transDay(lang)[day] + ', ngày ' + date + ' ' + transMonth(lang)[m] + ' năm ' + y;
        
    }
}

function transMonth(lang) {
    if (lang == 'en')
        return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return ['Tháng 01', 'Tháng 02', 'Tháng 03', 'Tháng 04', 'Tháng 05', 'Tháng 06', 'Tháng 07', 'Tháng 08', 'Tháng 09', 'Tháng 10', 'Tháng 11', 'Tháng 12'];
}

function transDay(lang) {
    if (lang == 'en')
        return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy'];
}

function scrollBottom(data) {
    var $tabRoom = $('#tab' + data['room_id']);
    var tab = $tabRoom.find('.chat-tab-container');
    tab = $('#' + tab.attr('id'));
    tab.data('bottom', true);
    var chatContent = tab.find('.content-chat');
    tab.nanoScroller();
    if (chatContent[0]) {
        chatContent[0].scrollTop = chatContent[0].scrollHeight;
    }
    tab.nanoScroller({scroll: 'bottom'});
}

function stringAppendNotice(data, option) {
    var url = data['url'];
    if(url)
        url = url.trim();
    var type = data['type'];
    if (!url || url == '' || url == false || url == null) {
        url = ( type == 3 || type == 'addgroup') ? 'avatar-group.png' : 'noti_avatar.jpg';
    }
    var id = data['friend_id'];
    var notice_id = data['id'];
    
    var dateTime = getDatetimeNotifi();
    
    var is_confirm = data['is_confirm'];
    
    var userName = !data['userName'] ? (data['last_name'] + ' ' + data['first_name']) : data['userName'];
    var content = '';
    
    if (option == 'php') {
        content = (type == 'addgroup' || type == '3') ? data.message + ' ' + '<a href="' + fullUrl + '/chat/room/g-' + data.room_id + '" style="color: #337ab7;">' + data.roomName + '</a>' : data['message'];
        dateTime = getDatetimeNotifi(convertToLocalTime(data['created_at'], 'notice'));
    } else {
        content = lang == 'en' ? data['message_en'] : data['message_en'];
        content = (type == 'addgroup' || type == '3') ? content + ' ' + '<a href="' + fullUrl + '/chat/room/g-' + data.room_id + '" style="color: #337ab7;">' + data.roomName + '</a>' : content;
    }
    
    var col = 12;
    if (type == 'addfriend' || type == 1)
        col = is_confirm == 0 ? 6 : 8;
    var st = '';
    
    if (data['is_click'] == 0)
        st += '<li class="media notification-success none_read" data-id=' + notice_id + ' data-roomid="' + data['room_id'] + '">';
    else
        st += '<li class="media notification-success" data-id=' + notice_id + ' data-roomid="' + data['room_id'] + '">';
    
    st += '<div class="notice-avatar col-md-2 col">';
    st += '<a href="' + fullUrl + '/profile/' + id + '">';
    st += '<img src="' + urlGlobal + '/themes/default/asset_frontend/img_avatar/' + url + '" alt="" style="width: 45px; height: 45px;">';
    st += '</a>';
    st += '</div>';
    
    st += '<div class="col-md-10 col">';
    st += '<div class="notice-username col-md-' + col + '">';
    st += '<p><a href="' + fullUrl + '/profile/' + id + '" style="color: #337ab7;">' + userName + '</a> ' + content + '</p>';
    st += '</div>';
    
    if (type == 'addfriend' || type == 1) {
        col = col == 12 ? 12 : (12 - col);
        st += '<div class="notice-btn-control col-md-' + col + '" data-url="' + fullUrl + '/chat/addRemoveFriend" data-id="' + id + '" data-type="noticebox">';
        if (is_confirm == 0) {
            st += '<button class="btn btn-notice btn-confirm ajax-relative" data-action="confirm">' + trans.confirm + '</button>';
            st += '<button class="btn btn-notice btn-delete-request ajax-relative" data-action="delete-request">' + trans.delete_request + '</button>';
        } else if (is_confirm == 1)
            st += '<button class="btn btn-notice btn-confirmed">' + trans.confirmed + '</button>';
        else
            st += '<button class="btn btn-notice btn-confirmed">' + trans.deleted_request + '</button>';
        
        st += '</div>';
    }
    
    st += '<div class="notice-time">' + dateTime + '</div>';
    st += '</div>';
    
    st += '</li>';
    return st;
}

function getDatetimeNotifi(times) {
    if (!times)
        times = new Date();
    return addZero(times.getFullYear()) + '/' + addZero(times.getMonth() + 1) + '/' + addZero(times.getDate()) + ' ' + getTimePmAm(times);
}

function loadNotification(url, type) {
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        beforeSend: function () {
        },
    }).done(function (str) {
        var string = '';
        if (str) {
            Object.keys(str).forEach(function (i) {
                string += stringAppendNotice(str[i], 'php');
            });
            
        }
        $('.notifi-box').find('ul').html(string);
        sessionStorage['bellnotifi'] = 1;
        /*if(type == 'updateIsRead')
         countNotifiMessage.socketIsRead(0, 'notice');*/
    }).fail(function (err) {
        
    })
}

function soundnotification(sound, replay, volume, stop) {
    var notif = new Audio(sound);
    if (status === 'busy') {
        notif.volume = 0;
    } else {
        volume = volume || localStorage.baseVolume;
        notif.volume = (+volume) / 100;
    }
    if (!stop) {
        notif.play();
    }
    notif.loop = !!replay;
    return notif;
}

function stripVowelAccent(str) {
    if (str) {
        str = str.toLowerCase();
        var rExps = [
            {re: /[\xC0-\xC6]/g, ch: 'A'},
            {re: /[\xE0-\xE6]/g, ch: 'a'},
            {re: /[\xC8-\xCB]/g, ch: 'E'},
            {re: /[\xE8-\xEB]/g, ch: 'e'},
            {re: /[\xCC-\xCF]/g, ch: 'I'},
            {re: /[\xEC-\xEF]/g, ch: 'i'},
            {re: /[\xD2-\xD6]/g, ch: 'O'},
            {re: /[\xF2-\xF6]/g, ch: 'o'},
            {re: /[\xD9-\xDC]/g, ch: 'U'},
            {re: /[\xF9-\xFC]/g, ch: 'u'},
            {re: /[\xD1]/g, ch: 'N'},
            {re: /[\xF1]/g, ch: 'n'},
            {re: /à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, ch: 'a'},
            {re: /è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, ch: 'e'},
            {re: /ì|í|ị|ỉ|ĩ/g, ch: 'i'},
            {re: /ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, ch: 'o'},
            {re: /ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, ch: 'u'},
            {re: /ỳ|ý|ỵ|ỷ|ỹ/g, ch: 'y'},
            {re: /đ/g, ch: 'd'}
        ];
        
        for (var i = 0, len = rExps.length; i < len; i++) {
            str = str.replace(rExps[i].re, rExps[i].ch);
        }
    }
    return str;
}

/* Convert time to day, month, year format (ddmmyyyy) */
function toDMY(time) {
    time = new Date(time);
    return addZero(time.getDate()) + addZero(time.getMonth() + 1) + String(time.getFullYear());
}
/* Change user mood */
function changeMood(mood_wrapper, input) {
    input.remove();
    var mood = input.val().trim();
    var i;
    if (mood) {
        /* If new mood is not empty */
        i = $('<i>', {
            'html': mood,
            'data-mood': '1',
            'class': 'truncate'
        });
    } else {
        i = $('<i>', {
            'html': trans.how_are_you,
            'data-mood': '0'
        });
    }
    mood_wrapper.append(i);
    
    socket.emit('change mood', input.val());
}

/*
 *  Preview image after finishing web scraping
 * */
var previewImage = (function () {
    return {
        /* If image cannot be shown, hide everything */
        imgError: function (e) {
            var img = $(e);
            var parent = img.parent();
            var title = parent.prev();
            var acc = title.closest('.accessory');
            title.css('position', 'relative');
            acc.show();
            parent.remove();
            var chatbox = acc.closest('.nanos');
            chatbox = $('#' + chatbox.attr('id'));
            if (chatbox.data('bottom') === false) {
                chatbox.nanoScroller();
            } else {
                chatbox.nanoScroller({scroll: 'bottom'});
                var content = chatbox.find('.content-chat');
                content[0].scrollTop = content[0].scrollHeight;
            }
        },
        /* If image is loading successfully, trim website description if need be */
        imgLoad: function (e) {
            var img = $(e);
            var acc = img.closest('.accessory');
            acc.show();
            var chatbox = acc.closest('.nanos');
            chatbox = $('#' + chatbox.attr('id'));
            if (chatbox.data('bottom') === false) {
                chatbox.nanoScroller();
            } else {
                chatbox.nanoScroller({scroll: 'bottom'});
                var content = chatbox.find('.content-chat');
                content[0].scrollTop = content[0].scrollHeight;
            }
            var max_height = img.parent().height();
            var title = acc.find('.preview-title');
            /* Only show description if image height is higher than 60px */
            if (max_height > 60) {
                /* Trim description if its height is higher than image height - 30px */
                while (title.outerHeight(true) > max_height - 30) {
                    title.text(function (index, text) {
                        return text.replace(/\W*\s(\S)*$/, '...');
                    });
                }
            } else {
                title.hide();
            }
        },
        /* This function runs if there's no image to be shown */
        showOnLoad: function (e) {
            var acc = $(e);
            /* Stop function if image is found */
            if (acc.find('img').length) return;
            e.style.display = 'block';
            var chatbox = acc.closest('.nanos');
            chatbox = $('#' + chatbox.attr('id'));
            if (chatbox.data('bottom') === false) {
                chatbox.nanoScroller();
            } else {
                chatbox.nanoScroller({scroll: 'bottom'});
                var content = chatbox.find('.content-chat');
                content[0].scrollTop = content[0].scrollHeight;
            }
        }
    }
}());
/* Pre-load offline favicon so it can be shown when disconnected */
var offline_img = new Image();
offline_img.src = '/themes/default/asset_frontend/img/offline-favicon.png';

/* Run this when socket is not connected */
function socketDisconnection(data) {
    var spinner = $('.offline-spinner').show();
    var status = spinner.next().hide();
    status.next().hide();
    $('.favicon').attr('href', offline_img.src);
    sortByName.sort_all_users({});
    $('#cDetectWifi').modal('show');
    /*// lastest_messages.forEach(function (v, i) {
     //     var chat_tab = document.getElementById('box' + i);
     //     if (chat_tab) {
     //         var mes = chat_tab.querySelectorAll('.msg_container[data-id]');
     //         var msg = mes[mes.length - 1];
     //         if (msg) {
     //             lastest_messages.set(i, +msg.dataset.id);
     //         }
     //     }
     // });*/
}

/* Run this function when socket is connected */
function socketConnection(data) {
    var spinner = $('.offline-spinner').hide();
    var status = spinner.next().show();
    status.next().show();
    $('.favicon').attr('href', '/themes/default/asset_frontend/img/favicon.ico');
    /*countNotifiMessage.countNotifi('reload');*/
    /*if (lastest_messages.size) {
     var lost_messages = {};
     lastest_messages.forEach(function (v, i) {
     lost_messages[i] = v;
     });
     socket.emit('lost messages', lost_messages);
     }*/
    var liactive = $('ul.list-user li.new-friend.active');
    var room_active = liactive.data('roomid');
    var tab_active = $('#main_' + room_active).find('textarea.input-message');
    var message_active = tab_active.val();
    if (tab_active)
        messageInActive = message_active;
    roomActive = room_active;
    sessionStorage.clear();
    liactive.length && liactive.click() && $('.right-content.active').css('width', '100%');
    $('#cDetectWifi').modal('hide');
}

/* Change volume icon */
function soundIcon(value, i) {
    if (value > 50) {
        i.removeClass('fa-volume-off fa-volume-down').addClass('fa-volume-up');
    } else if (value > 0) {
        i.removeClass('fa-volume-off fa-volume-up').addClass('fa-volume-down');
    } else {
        i.removeClass('fa-volume-down fa-volume-up').addClass('fa-volume-off');
    }
}

/* Show previous chat content when using up and down arrow key */
var prevChatContent;
(function () {
    var prevContent = {};
    var currentPointer = {};
    
    function prevText(room_id) {
        var self = {};
        if (!prevContent[room_id] || !prevContent[room_id].length) {
            prevContent[room_id] = [];
            currentPointer[room_id] = false;
        }
        
        function newTextMessage(text) {
            var textArr = prevContent[room_id];
            var len = textArr.length;
            if (len > 4) {
                textArr.shift(0);
            }
            var i = textArr.indexOf(text);
            if (~i) {
                prevContent[room_id] = textArr.remove(i);
            }
            prevContent[room_id].push(text);
            currentPointer[room_id] = textArr.length;
        }
        
        function getPrevTextContent() {
            var textArr = prevContent[room_id];
            var pos = currentPointer[room_id];
            if (pos === false) {
                var len = textArr.length;
                pos = len - 1 > 0 ? len - 1 : 0;
            } else {
                pos = pos - 1 > 0 ? pos - 1 : 0;
            }
            currentPointer[room_id] = pos;
            return textArr[pos] || '';
        }
        
        function getNextTextContent() {
            var textArr = prevContent[room_id];
            var pos = currentPointer[room_id];
            var len = textArr.length;
            if (!len) return;
            pos = pos + 1 >= len ? len - 1 : pos + 1;
            currentPointer[room_id] = pos;
            return textArr[pos] || '';
        }
        
        self.pointer = currentPointer[room_id];
        self.content = prevContent[room_id];
        self.newTextMessage = newTextMessage;
        self.getPrevTextContent = getPrevTextContent;
        self.getNextTextContent = getNextTextContent;
        return self;
    }
    
    prevChatContent = prevText;
    
    var timeout = {};
    /* Run this function when receiving typing notification from other users */
    function typing(data) {
        var room_id = data.room_id;
        var user_id = data.user_id;
        /* Check if room_id and user id are present */
        if (!room_id || !user_id) return;
        /* Check if chatbox is loaded */
        var chat_tab = document.getElementById('main_' + room_id);
        if (!chat_tab) return;
        /* Get typing elements */
        var typing = chat_tab.querySelector('.typing');
        var userTyping = typing.querySelector('.user_is_typing');
        var user = userTyping.querySelector('[data-id="' + user_id);
        var users = typing.getElementsByClassName('appendage');
        /* If typing notification for particular user is not already shown, show it */
        if (!user) {
            var name = data.name;
            var group = userTyping.querySelector('.group-typing');
            typing.style.display = '';
            group.insertAdjacentHTML('beforebegin', '<span class="appendage" data-id="' + user_id + '">' + name + '</span>');
            var p = typing.querySelector('.is-typing');
            p.innerHTML = ' ' + users.length < 2 ? trans.is_typing : trans.are_typing;
            var chat_content = chat_tab.querySelector('.content-chat');
            chat_content.scrollTop = chat_content.scrollHeight;
        }
        /* Remove notification removal when receiving new notification from specific user in specific room */
        if (timeout[user_id + room_id]) clearTimeout(timeout[user_id + room_id]);
        /* Remove typing notification after 5sec */
        (function (typing, users, id) {
            timeout[user_id + room_id] = setTimeout(function () {
                var user = typing.querySelector('[data-id="' + id);
                if (!user) return;
                user.parentElement.removeChild(user);
                if (!users.length) {
                    typing.style.display = 'none';
                } else if (users.length < 2) {
                    var p = typing.querySelector('.is-typing');
                    p.innerHTML = ' ' + trans.is_typing;
                }
            }, 5000);
        })(typing, users, user_id);
    }
    
    window.typingTimeout = typing;
})();

var countNotifiMessage = (function () {
    //todo: count message (11/09/2016 today)
    var self = {};
    var timeout;
    self.countNotifi = function (type) {
        if (type == 'reload') {
            socket.emit('count notification message', 'both');
        }
        if (type == 'countRecent') {
            if ($('.class-count-message').length)
                socket.emit('count notification message', 'message');
            if ($('.recent-notification').length) {
                var count = $('.list-user .count-message').length;
                self.appendNoticeMessage('message', count);
            }
        }
        if (type == 'countNotice') {
            socket.emit('count notification message', 'notice');
        }
        if (type == 'countMessage') {
            socket.emit('count notification message', 'message');
        }
    };
    
    self.countTile = function () {
        var regex = /^\s*\((.*?)\)\s*/g;
        var recent = $('.recent-notification');
        recent = recent.length ? recent.find('.count-notifi') : $('.class-count-message').find('.count-message-box');
        recent = parseInt(recent.text());
        var notice = parseInt($('.class-count-notice').find('.count-notifi').text());
        var count = recent || notice;
        count = (recent && notice) ? (recent + notice) : count;
        document.title = document.title.replace(regex, '');
        if (count > 0) {
            document.title = '(' + count + ') ' + document.title;
        }
    };
    
    self.aNoticeMessage = function (classNummber, count, classType) {
        var spantag = classNummber.find('a').find('.' + classType + '');
        if (spantag.length)
            spantag.remove();
        if (count > 0) {
            classNummber.find('a').first().append('<span class="' + classType + '">' + count + '</span>');
            /*runPausedAnimation('count-notifi', 'running');*/
        }
        self.countTile();
    };
    
    self.appendNoticeMessage = function (option, count) {
        var bell = $('.class-count-notice');
        var message = $('.class-count-message');
        var recent = $('.left-content .recent-notification');
        if (option == 'message') {
            if (message.length)
                self.aNoticeMessage(message, count, 'count-message-box');
            if (recent.length) {
                count = $('.list-user .count-message').length;
                self.aNoticeMessage(recent, count, 'count-notifi');
            }
        }
        
        if (option == 'notification') {
            self.aNoticeMessage(bell, count, 'count-notifi');
        }
        
        if (option == 'bothNoticeMessage') {
            self.aNoticeMessage(message, count, 'count-message-box');
            self.aNoticeMessage(bell, count, 'count-notifi');
        }
    };
    
    /**
     * update is read at list user recent, remove span count tag
     * @param room_id
     * @param count
     * @param classCount
     */
    self.updateIsReadMessage = function (room_id, count, classCount) {
        if (!classCount)
            classCount = $('#recent-active-room').find('li[data-roomid="' + room_id + '"]');
        var countMessage;
        if ((countMessage = classCount.find(".count-message")).length > 0) {
            countMessage.remove();
            self.countNotifi('countRecent');
            self.countTile();
        }
        if (count > 0) {
            classCount.append('<span class="count-message">' + count + '</span>');
        }
    };
    
    self.socketIsRead = function (room_id, type) {
        if (type == 'message') {
            var classCount = $('#recent-active-room').find('li[data-roomid="' + room_id + '"]');
            if (classCount.find(".count-message").length > 0) {
                socket.emit('updateIsRead', {room_id: room_id, type: 'message'});
                self.updateIsReadMessage(room_id, 0, classCount);
                classCount.find('.user_name').css('font-weight', '300');
            }
        } else if (type == 'notice') {
            socket.emit('updateIsRead', {room_id: room_id, type: 'notice'});
            self.appendNoticeMessage('notification', 0);
        }
        
    };
    return self;
})();

function assessCommand(a, command, type, status) {
    var chat_content = $(this).closest('.main-chat-content');
    var settings = chat_content.find('.basic-settings');
    var room_id = $(this).data('roomid');
    switch (type) {
        case 'alert' :
            var i = settings.find('.mute');
            var input = settings.find('.volume-level');
            switch (status) {
                case 'on' :
                    input.val(100);
                    soundIcon(100, i);
                    localStorage['sound' + room_id] = 100;
                    break;
                case 'off' :
                    input.val(0);
                    soundIcon(0, i);
                    localStorage['sound' + room_id] = 0;
                    break;
            }
            break;
        default:
            break;
    }
    return '';
}

socket.on('typing', typingTimeout);

/*
 *   Add remove function to Array. Indestructive function.
 *   Remove specified elements from index to index
 *   @param {int} from : starting index to be removed from array
 *   @param {int} to : ending index to be removed from array, can be left empty
 *   return Array
 * */
Array.prototype.remove = function (from, to) {
    to = to ? to : from;
    if (to < from) {
        var x = to;
        to = from;
        from = x;
    }
    return this.filter(function (v, i) {
        return i < from || i > to;
    });
};

/* Move your pointer around in textarea */
function select(el, start, end) {
    el.focus();
    if (!end) end = start;
    if (el.setSelectionRange) {
        el.setSelectionRange(start, end);
    }
    else {
        if (el.createTextRange) {
            var normalizedValue = el.value.replace(/\r\n/g, "\n");
            
            start -= normalizedValue.slice(0, start).split("\n").length - 1;
            end -= normalizedValue.slice(0, end).split("\n").length - 1;
            
            var range = el.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    }
}

function addFriendData(json) {
    var params = {
        id: json.data.friend_id,
        room_id: json.room_id,
        name: json.name,
        mood: json.mood,
        status: json.status,
        is_friend: json.is_friend,
        url: json.url
    };
    if(json.recent_flag)
        params.recent_flag = json.recent_flag;
    return createFriendTab(params)
}

function createFriendTab(data) {
    if (!data || !data.room_id || !data.id || !data.name) return;
    if ($('#f-' + data.room_id).length) return;
    var status = '<i class="' + data.status + '"></i>' + data.status + '</span >';
    var icon = data.url ? ('<img src="' + data.url + '" class="img-responsive center-block" alt="avatar"/>') : '<i class="fa ' + (!data.is_friend ? 'fa-question' : 'fa-user') + '" aria-hidden="true"></i>';
    var value_id = !data.recent_flag?'f-' + data.room_id:'rf-' + data.room_id;
    var str = '<li class="clearfix new-friend" data-name="' + stripVowelAccent(data.name) + '" data-roomid="' + data.room_id + '" id="'+value_id+'" data-id="'+ data.id+'">' +
        '<a class="clearfix" data-id="' + data.id + '" data-url="/chat/getMessage" data-action="one-to-one" data-roomid="' + data.room_id + '">' +
        '<div class="avatar">' +
        icon +
        '</div>' +
        '<div class="active-user-info" title="' + data.mood + '">' +
        '<span class="user_name">' + data.name + '</span>' +
        '<span class="status">' + status +
        '</div></a ></li >';
   
    if(data.recent_flag){
        var position = $('#recent').find('.list-user');
        if(data.position == 'last')
            position.last().append(str);
        else
            position.first().append(str);
    }else{
        $('#friend').find('.list-user').first().append(str);
        var recent = $('#rf-' + data.room_id);
        recent.find('.status').html(status);
        recent.find('.avatar').html(icon);
    }
}

function changeButtonLayout(data) {
    var side_tab = $('#voltaic_holder_' + data.room_id);
    if (!side_tab.length) return;
    var type = data.type === 1 ? 'confirm' : (data.type === 4 ? 'friend' : 'stranger');
    side_tab.find('.btn-chat-user').html(takeContentButtonRelative(type));
    if (type === 'friend') {
        var friend = $('#f-' + data.room_id).find('.avatar');
        var recent = $('#rf-' + data.room_id).find('.avatar');
        if (data.url) {
            var img = '<img src="' + data.url + '" alt = "avatar" class= "img-responsive center-block"/>';
            friend.html(img);
            recent.html(img);
        } else {
            friend.find('i.fa').removeClass('fa-question').addClass('fa-user');
            recent.find('i.fa').removeClass('fa-question').addClass('fa-user');
        }
        sortByName.sort_all_users(online_users);
    }
}

function getLostMessages(lost_messages) {
    if (Array.isArray(lost_messages)) {
        lost_messages.forEach(function (mes) {
            appendMessageFile(mes, false, true)
        })
    } else {
        appendMessageFile(lost_messages, false, true)
    }
}

function deleteNotice(notice_id) {
    var linotice = $('.notification-success[data-id="' + notice_id + '"]');
    linotice.length && linotice.remove();
}

function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
    
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var TheHeadHTML = '<link href="' + window.location.protocol + '//' + window.location.host + '/favicon.ico" rel="icon" type="image/x-icon">';
    
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    $(newWindow.document.head).html(TheHeadHTML);
    //newWindow.close();
    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
    newWindow.data = {};
    
    return newWindow;
}

function updateFlagCall(data, type) {
    if (type == 'miss')
        data.is_file = 2;
    data.flagcall = 0;
    socket.emit('update flagcall', data);
}

function addZero(num) {
    num = String(num);
    return num < 10 ? '0' + num : String(num);
}

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function detectInternet() {
    if (navigator.onLine) {
        socketConnection();
        socket.emit('detectOnline');
    } else
        socketDisconnection();
}



window.addEventListener("offline",detectInternet);
window.addEventListener("online", detectInternet);

function addGroupToAnother(data) {
    var title = data.title;
    var room_id = data.room_id;
    var action = data.action;
    
    var ul = $('#group').find('ul');
    if (action == 'changeTitle') {
        var tabUser = returnTabUser(room_id);
        if (tabUser.length) {
            tabUser.find('.user_name').text(title);
        }
    }
    if(action == 'friend'){
        ul.append(appendGroupForTabUser({room_id: room_id, title: title}));
        sortByName.sort_group_name();
    }
}



