/* load google font */
var checkDate       = lang == 'en' ? 'One of the three fields must not be empty!' : 'Không được bỏ trống một trong số ba trường này';
var checkChooseYear = lang == 'en' ? 'Please choose month and year before choose day!' : 'Vui lòng chọn năm và tháng trước khi chọn ngày';
/*WebFont.load({
 google: {
 families: ['Open Sans:300,400,600']
 }
 });*/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var global_information = '';
var status;
/* add active class to menu when visit the first time */
var hash = window.location.hash;
if (hash == '') hash = '#home';
var currentNav = $('.navbar-nav li a[href="' + hash + '"]');
$(document).prop('title', 'Vitzoo | ' + currentNav.text());
currentNav.parent().addClass('active');

$(document).ready(function () {
    $('.notice-nano').nanoScroller();
    var screen_height   = $(window).height();
    var nav_height      = $('.navbar').height() + 45;
    var footer_height   = $('footer').height() + 20;
    var register_height = $('.regform').height();
    /* add height for overflow */
    $('.home-form').css('height', register_height);
    /* display login form for slide */
    $('.logform, .forgotform').removeClass('hidden');
    /* slide register form */
    $(document).on('click', '#goLogin', function () {
        $('.rlForm').animate({
            top: -register_height + 'px'
        });
        return false;
    });
    
    status = $('.active-user-info').find('.user-status span').text().trim().toLowerCase();
    
    /* slide login form */
    $(document).on('click', '#goRegister, #returnRegister', function () {
        $('.rlForm').animate({
            top: 0
        });
        return false;
    });
    
    /* slide login form */
    $(document).on('click', '#forgotPass', function () {
        $('.rlForm').animate({
            top: -(register_height * 2) + 'px'
        });
        return false;
    });
    
    var ansTitle = $(".faq-accordian h3");
    /* FAQ menu accordian slide */
    ansTitle.on('click', function () {
        $('.faq-accordian h3').removeClass('bg-active').find('i').removeClass('fa-minus').addClass('fa-plus');
        $(".faq-accordian ul").slideUp();
        if (!$(this).next().is(":visible")) {
            $(this).removeClass('bottom-radius').addClass('bg-active').find('i').removeClass('fa-plus').addClass('fa-minus');
            $(this).next().slideDown();
        }
    });
    
    /* FAQ question hover animation */
    ansTitle.hover(
        function () {
            $(this).css('background-color', '#03304f');
        },
        function () {
            $(this).css('background-color', '');
        }
    );
    
    var questionTitle = $('.faq-accordian li a');
    /* FAQ title animate slide */
    questionTitle.hover(
        function () {
            $(this).stop().animate({
                'margin-left': '10px'
            });
        },
        function () {
            $(this).stop().animate({
                'margin-left': 0
            });
        }
    );
    
    questionTitle.on('click', function () {
        questionTitle.removeClass('active');
        $(this).addClass('active');
        
        var id = $(this).attr('href');
        return false;
    });
    
    /* js menu left*/
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);
    });
    
    /*Remove menu for searching*/
    $('#search-trigger').click(function () {
        $('.navbar-nav').removeClass('slide-in');
        $('.side-body').removeClass('body-slide-in');
    });
    
    /*JS iCheck*/
    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red',
        increaseArea : '20%' /*optional*/
    });
    
    /*close menu on click outside*/
    
    $(document).on("click", function (e) {
        var right_nav = $(".navbar-right");
        if ($(e.target).attr('class') != right_nav.attr('class') && $(e.target).attr('class') != $(".navbar-toggle").attr('class') && $(e.target).attr('class') != $(".icon-bar").attr('class')) {
            right_nav.removeClass("slide-in");
            $(".side-body").removeClass("body-slide-in");
        }
    });
    /*js tab*/
    $(".nav-tabs a").click(function () {
        $(this).tab('show');
    });
    /*js scroll menu faq home page*/
    questionTitle.click(function () {
        $('html, body').animate({
            scrollTop: $(".faq-tab-ct .active").offset().top - 20
        }, 500);
    });
    /*js click save and cancel*/
    /*js thong tin ca nhan*/
    
    $(".edit-ttcn").click(function () {
        
        var tx             = $(".ttcn .tx-general");
        var tx_general = $(".tx-general")
        if (!tx.hasClass("tx-show")) {
            global_information = $('#personal-information').clone();
            tx.addClass("tx-show");
            tx_general.removeAttr("disabled");
            $(".dropdown-abc").removeClass("disabled");
            $(".fa-hide-show").css("display", "block");
            /*js when click edit, hover input select background...*/
            tx_general.mouseover(function () {
                $(this).addClass("blue");
            });
            /*$(".tx-general").mouseout(function () {
             $(this).removeClass("blue");
             });*/
            $("#personal-information").find('input').attr("readonly", false);
            var nation          = $('#nation');
            var provinces       = $('#provinces');
            var value_nation    = getValueData(nation);
            var value_provinces = provinces.find('span').data('iso-code');
            
            if (nation.length == 0 || value_nation == 0 || nation.text().replace(/\s/g, '') == "") {
                $('.display-provinces').addClass('divNone');
                $('#display-city').addClass('divNone');
            }
            
            if (value_nation != 0) {
                ajaxProvinces(value_nation);
            }
            if (provinces.length == 0 || value_provinces == 0 || provinces.text().replace(/\s/g, '') == "") {
                $('#display-city').addClass('divNone');
            }
            if (value_provinces != 0) {
                ajaxCity(value_provinces, value_nation);
            }
        }
        else {
            tx.removeClass("tx-show");
            $(".date select").attr("disabled", "disabled");
            tx_general.addClass("disabled");
            $(".text-change input").attr("readonly", 'readonly');
            $(".fa-hide-show").css("display", "none");
            tx_general.removeClass("tx-form-css");
            /*js when click edit, hover input select background...*/
            tx_general.mouseover(function () {
                $(this).removeClass("blue");
            });
            $('.display-provinces').css('display', 'block');
            $('.btt-cancel-ttcn').click();
        }
        $(".btt-ttcn").slideToggle("slow");
        appendDay();
        
    });
    
    $(document).on("click", ".chat-dropdown-menu", function (e) {
        $('ul.list-user li.new-friend').removeClass('active');
    });
    
    $(document).on("click", ".btt-save-ttcn", function (e) {
        /*e.preventDefault();*/
        var $this = $(this);
        var tx    = $(".tx-general");
        if (!tx.hasClass("tx-show")) {
            
            $(".ttcn .tx-general").addClass("tx-show");
            tx.removeAttr("disabled");
            $(".dropdown-abc").removeClass("disabled");
            $(".fa-hide-show").css("display", "block");
            /*js when click edit, hover input select background...*/
            tx.mouseover(function () {
                $(this).addClass("blue");
            });
            tx.mouseout(function () {
                $(this).removeClass("blue");
            });
            $(".btt-ttcn").slideToggle("slow");
        }
        else {
            $("#editProfile").validate({
                ignore       : [],
                rules        : {
                    first_name  : {
                        required : true,
                        maxlength: 20
                    },
                    last_name   : {
                        required : true,
                        maxlength: 40
                    },
                    phone_number: {
                        maxlength: 20
                    },
                    address     : {
                        maxlength: 100
                    }
                    
                },
                submitHandler: function (form) {
                    editProfile($this);
                    if (!$('#date-error').length) {
                        $(".ttcn .tx-general").removeClass("tx-show");
                        $(".date select").attr("disabled", "disabled");
                        tx.addClass("disabled");
                        $(".text-change input").attr("readonly", 'readonly');
                        $(".fa-hide-show").css("display", "none");
                        tx.removeClass("tx-form-css");
                        /*js when click edit, hover input select background...*/
                        tx.mouseover(function () {
                            $(this).removeClass("blue");
                        });
                        $(".btt-ttcn").slideToggle("slow");
                    }
                }
            });
        }
    });
    
    $(document).on("click", ".btt-cancel-ttcn", function () {
        $('#personal-information').fadeOut(1000, function () {
            $('#personal-information').remove();
            $('.ttcn').append(global_information);
            global_information = '';
        });
    });
    
    /*js button tai khoan*/
    $(document).on('click', '.btn-taikhoan', function () {
        $(".ct-taikhoan").slideToggle("300");
    });
    
    /*js checkbox*/
    $(".date ul").on("click", ".init", function () {
        if (!$(".date ul").hasClass("disabled")) {
            $(this).closest("ul").children('li:not(.init)').slideToggle();
        }
    });
    
    /*js demo form select profile*/
    
    $(document).on('click', '.dropdown-abc dt a', function (e) {
        e.preventDefault();
        var dd = this.parentNode;
        $(".dropdown-abc dd ul").not($(dd).siblings('dd').find('ul')).hide();
        if (!$(".date dl").hasClass("disabled")) {
            
            $(dd).siblings('dd').find('ul').toggle();
        }
    });
    
    /*$(".dropdown-abc dd ul li a").click(function (e) {
     
     });*/
    
    $(document).on('click', '.dropdown-abc dd ul li a', function (e) {
        e.preventDefault();
        var text            = $(this).html();
        var value           = $(this).parent().data('value');
        var span_set_value  = $(this).closest('dd').siblings('dt').find('span');
        var span_set_status = $(this).closest('dd').siblings('dt').find('span.status');
        /*span_set_value.addClass('new-value-span');*/
        /*span_set_value.after(span_set_value.clone());*/
        
        if (span_set_value.text() != text)
            span_set_value.data('clicked', 'true');
        
        span_set_value.html(text);
        span_set_value.data('value', value);
        if (span_set_status.length > 0)
            span_set_status.data('value', $(this).data('status'));
        $(".dropdown-abc dd ul").hide();
    });
    
    $(document).on('click', '.sel-opt-status li', function (e) {
        var parent    = $(this).closest('ul').siblings('span');
        var attribute = $(this).data('attribute');
        var classname = $(this).find('i').attr('class');
        parent.html('<i class="' + classname + '"></i>' + classname);
        
        socket.emit('changeStatus', {status: attribute, name: classname});
        status = classname;
    });
    
    function getSelectedValue (id) {
        return $("#" + id).find("dt a span.value").html();
    }
    
    $(document).on('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dropdown"))
            $(".dropdown-abc dd ul").hide();
    });
    
    /*js when click outside status onl, off...*/
    $(document).click(function (e) {
        var status_down       = $('.status-down');
        var delete_group_down = $('.setting-group');
        if (!delete_group_down.is(e.target) && delete_group_down.has(e.target).length === 0) {
            $('.ul-setting-group').slideUp();
        }
        if (!status_down.is(e.target) && status_down.has(e.target).length === 0) {
            status_down.siblings("div").children('li').slideUp();
            status_down.siblings("ul").find('li').slideUp();
        }
    });
    
    /*$(document).click(function (e) {
     if (!$('#click_slideup').is(e.target) && $('#click_slideup').has(e.target).length === 0 && opened) {
     document.getElementById("voltaic_holder").style.width = "0";
     /!*document.getElementById("main").style.width = "0";*!/
     document.getElementById("main").style.marginRight = "0px";
     $("#main").removeClass("edit");
     }
     });*/
    /*js active menu*/
    var $link     = window.location.pathname;
    var $pathName = $link.substring(4);
    var sideMenu  = $('.side-menu-container ul li');
    switch ($pathName) {
        case 'about':
            sideMenu.removeClass("active");
            $('.about').addClass("active");
            break;
        case 'service':
            sideMenu.removeClass("active");
            $('.service').addClass("active");
            break;
        case 'faq':
            sideMenu.removeClass("active");
            $('.faq').addClass("active");
            break;
        case 'contact':
            sideMenu.removeClass("active");
            $('.contact').addClass("active");
            break;
    }
    
    /*js select options online, offline....*/
    $(document).on("click", ".user-status ul .status-down", function () {
        $(this).siblings("div").children('li').slideToggle();
    });
    $(document).on("click", ".active-user-info .status-down", function () {
        $(this).siblings("ul").find('li').slideToggle();
    });
    var allOptions = $(".user-status ul div").children('li');
    $(document).on("click", ".user-status ul li:not(.init)", function () {
        allOptions.removeClass('selected');
        $(this).addClass('selected');
        $(".user-status ul").children('.init').html($(this).html());
        allOptions.slideUp();
    });
    
    /*js star favorite*/
    $(document).on("click", ".chat-us-tt .fa-star-o", function () {
        $(this).css("display", "none");
        $('.chat-us-tt .fa-star').css("display", "block", "!important");
    });
    $(document).on("click", ".chat-us-tt .fa-star", function () {
        $(this).css("display", "none");
        $('.chat-us-tt .fa-star-o').css("display", "block", "!important");
    });
    
    /*js button addfriend, unfriend*/
    $(".btn-add-frd").click(function () {
        $(this).css("display", "none");
        $('.btn-un-frd').css("display", "block", "!important");
    });
    $(".btn-un-frd").click(function () {
        $(this).css("display", "none");
        $('.btn-add-frd').css("display", "block", "!important");
    });
    /*js button block, unblock*/
    $(".btn-bllock").click(function () {
        $(this).css("display", "none");
        $('.btn-unlock').css("display", "block", "!important");
    });
    $(".btn-unlock").click(function () {
        $(this).css("display", "none");
        $('.btn-bllock').css("display", "block", "!important");
    });
    
    $(document).on('click', '.date-sample', function () {
        var y    = $('#birth-year'), m = $('#birth-month');
        var date = $('.attribute-date');
        if (!y.text().length || !m.text().length) {
            $('#date-error').remove();
            date.find('fieldset.date-error').after('<label id="date-error" class="error" for="date">' + checkChooseYear + '</label>');
        }
    });
    $(document).on('click', '.y-change,.m-change', function () {
        $('#birth-day').text = '';
        appendDay();
    });
    
    
    $('.invite-friend-chat').click(function () {
        var tab = $("#tabinvite_friend");
        history.pushState(null, null, '/' + lang + '/chat/');
        
        active_tab(tab);
    });
    
});

function hideLoader () {
    $('#css-loading').hide();
}

function showLoader () {
    $('#css-loading').show();
}
function getAttribute (element, value) {
    return element.data(value);
}

function editProfile (e) {
    var $this      = e;
    var data       = {};
    var date       = $('.attribute-date');
    var url        = e.data('url');
    var gender     = $('#gender');
    var phone      = $('#phone-number');
    var address    = $('#address');
    var year       = $('#birth-year');
    var month      = $('#birth-month');
    var day        = $('#birth-day');
    var city       = $('#city');
    var nation     = $('#nation');
    var language   = $('#language');
    var provinces  = $('#provinces');
    var first_name = $('#first-name').val();
    var last_name  = $('#last-name').val();
    
    if (first_name && last_name) {
        data['first_name'] = first_name;
        data['last_name']  = last_name;
        if (gender.text())
            data['gender'] = {
                value : gender.text(),
                id    : getAttribute(gender, 'attribute'),
                status: getValueData($('.status-gender'))
            };
        
        data['phone_number'] = {
            value : (phone.val() || "").trim(), id: getAttribute(phone, 'attribute'),
            status: getValueData($('.status-phone-number'))
        };
        
        data['address'] = {
            value : (address.val() || "").trim(), id: getAttribute(address, 'attribute'),
            status: getValueData($('.status-address'))
        };
        
        if (year.text() && month.text() && day.text())
            data['birth_day'] = {
                value : (year.text().trim() + '/' + month.text().trim() + '/' + day.text().trim()),
                id    : getAttribute(date, 'attribute'),
                status: getValueData($('.status-birth-day'))
            };
    
        if (nation.text())
            data['nation'] = {
                value : getValueData(nation), id: getAttribute(nation, 'attribute'),
                status: getValueData($('.status-nation'))
            };
        if (provinces.text())
            data['provinces'] = {
                value : getValueData(provinces), id: getAttribute(provinces, 'attribute'),
                status: getValueData($('.status-provinces'))
            };
        if (city.text())
            data['city'] = {
                value : getValueData(city), id: getAttribute(city, 'attribute'),
                status: getValueData($('.status-city'))
            };
        if (language.text())
            data['language'] = {
                value : getValueData(language), id: getAttribute(language, 'attribute'),
                status: getValueData($('.status-language'))
            };
        var token = e.data('token');
        
        validaterDay();
        
        !$('#date-error').length && $.ajax({
            url     : url,
            type    : 'POST',
            dataType: 'JSON',
            data    : {data: data, _token: token, id: 2},
            success : function (str) {
                $('span.status').data('value', '');
            }
        }).always(function () {
            
        });
    }
    $('.profile-fullname span').text(last_name+' '+first_name);
    $('.btn-taikhoan').text(last_name + ' ' + first_name);
    
}

function formatDateTime (time) {
    time  = new Date(time);
    var y = time.getFullYear(),
        m = addZero(time.getMonth()),
        d = addZero(time.getDay());
    return y + '/' + m + '/' + d;
}

function getValueData (element) {
    var value = !element.data('value') ? element.find('span').data('value') : element.data('value');
    return !value ? 0 : value;
}

function textToNum (text) {
    if (!text) return 0;
    return +text.replace(/(\D+)?([\d.]+)(\D+)?/g, "$2") || 0;
}

function addZero (num) {
    num = String(num);
    return num < 10 ? '0' + num : String(num);
}
Array.prototype.remove = function (from, to) {
    to = to ? to : from;
    if (to < from) {
        var x = to;
        to    = from;
        from  = x;
    }
    return this.filter(function (v, i) {
        return i < from || i > to;
    });
};

function dataURItoBlob (dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = decodeURI(dataURI.split(',')[1]);
    
    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    
    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    
    return new Blob([ia], {type: mimeString});
}

var UUID = (function () {
    var self = {};
    var lut  = [];
    for (var i = 0; i < 256; i++) {
        lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
    }
    self.generate = function () {
        var d0 = Math.random() * 0xffffffff | 0;
        var d1 = Math.random() * 0xffffffff | 0;
        var d2 = Math.random() * 0xffffffff | 0;
        var d3 = Math.random() * 0xffffffff | 0;
        return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
            lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
            lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
            lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
    };
    return self;
})();

function showError (message) {
    $('#errorModal p').html(message);
    $('#errorModal').modal('show');
}

function showMessage (message) {
    $('#successModal p').html(message);
    $('#successModal').modal('show');
}

(function (exports) {
    'use strict';
    //shared pointer
    var i;
    //shortcuts
    var defineProperty = Object.defineProperty, is = function (a, b) {
        return (a === b) || (a !== a && b !== b)
    };
    
    //Polyfill global objects
    if (typeof WeakMap == 'undefined') {
        exports.WeakMap = createCollection({
            // WeakMap#delete(key:void*):boolean
            'delete': sharedDelete,
            // WeakMap#clear():
            clear   : sharedClear,
            // WeakMap#get(key:void*):void*
            get     : sharedGet,
            // WeakMap#has(key:void*):boolean
            has     : mapHas,
            // WeakMap#set(key:void*, value:void*):void
            set     : sharedSet
        }, true);
    }
    
    if (typeof Map == 'undefined' || typeof ((new Map).values) !== 'function' || !(new Map).values().next) {
        exports.Map = createCollection({
            // WeakMap#delete(key:void*):boolean
            'delete': sharedDelete,
            //:was Map#get(key:void*[, d3fault:void*]):void*
            // Map#has(key:void*):boolean
            has     : mapHas,
            // Map#get(key:void*):boolean
            get     : sharedGet,
            // Map#set(key:void*, value:void*):void
            set     : sharedSet,
            // Map#keys(void):Iterator
            keys    : sharedKeys,
            // Map#values(void):Iterator
            values  : sharedValues,
            // Map#entries(void):Iterator
            entries : mapEntries,
            // Map#forEach(callback:Function, context:void*):void ==> callback.call(context, key, value, mapObject) === not in specs`
            forEach : sharedForEach,
            // Map#clear():
            clear   : sharedClear
        });
    }
    
    if (typeof Set == 'undefined' || typeof ((new Set).values) !== 'function' || !(new Set).values().next) {
        exports.Set = createCollection({
            // Set#has(value:void*):boolean
            has     : setHas,
            // Set#add(value:void*):boolean
            add     : sharedAdd,
            // Set#delete(key:void*):boolean
            'delete': sharedDelete,
            // Set#clear():
            clear   : sharedClear,
            // Set#keys(void):Iterator
            keys    : sharedValues, // specs actually say "the same function object as the initial value of the values property"
            // Set#values(void):Iterator
            values  : sharedValues,
            // Set#entries(void):Iterator
            entries : setEntries,
            // Set#forEach(callback:Function, context:void*):void ==> callback.call(context, value, index) === not in specs
            forEach : sharedForEach
        });
    }
    
    if (typeof WeakSet == 'undefined') {
        exports.WeakSet = createCollection({
            // WeakSet#delete(key:void*):boolean
            'delete': sharedDelete,
            // WeakSet#add(value:void*):boolean
            add     : sharedAdd,
            // WeakSet#clear():
            clear   : sharedClear,
            // WeakSet#has(value:void*):boolean
            has     : setHas
        }, true);
    }
    
    /**
     * ES6 collection constructor
     * @return {Function} a collection class
     */
    function createCollection (proto, objectOnly) {
        function Collection (a) {
            if (!this || this.constructor !== Collection) return new Collection(a);
            this._keys      = [];
            this._values    = [];
            this._itp       = []; // iteration pointers
            this.objectOnly = objectOnly;
            
            //parse initial iterable argument passed
            if (a) init.call(this, a);
        }
        
        //define size for non object-only collections
        if (!objectOnly) {
            defineProperty(proto, 'size', {
                get: sharedSize
            });
        }
        
        //set prototype
        proto.constructor    = Collection;
        Collection.prototype = proto;
        
        return Collection;
    }
    
    /** parse initial iterable argument passed */
    function init (a) {
        var i;
        //init Set argument, like `[1,2,3,{}]`
        if (this.add)
            a.forEach(this.add, this);
        //init Map argument like `[[1,2], [{}, 4]]`
        else
            a.forEach(function (a) {
                this.set(a[0], a[1])
            }, this);
    }
    
    /** delete */
    function sharedDelete (key) {
        if (this.has(key)) {
            this._keys.splice(i, 1);
            this._values.splice(i, 1);
            // update iteration pointers
            this._itp.forEach(function (p) {
                if (i < p[0]) p[0]--;
            });
        }
        // Aurora here does it while Canary doesn't
        return -1 < i;
    };
    
    function sharedGet (key) {
        return this.has(key) ? this._values[i] : undefined;
    }
    
    function has (list, key) {
        if (this.objectOnly && key !== Object(key))
            throw new TypeError("Invalid value used as weak collection key");
        //NaN or 0 passed
        if (key != key || key === 0) for (i = list.length; i-- && !is(list[i], key);) {
        }
        else i = list.indexOf(key);
        return -1 < i;
    }
    
    function setHas (value) {
        return has.call(this, this._values, value);
    }
    
    function mapHas (value) {
        return has.call(this, this._keys, value);
    }
    
    /** @chainable */
    function sharedSet (key, value) {
        this.has(key) ?
            this._values[i] = value
            :
            this._values[this._keys.push(key) - 1] = value
        ;
        return this;
    }
    
    /** @chainable */
    function sharedAdd (value) {
        if (!this.has(value)) this._values.push(value);
        return this;
    }
    
    function sharedClear () {
        (this._keys || 0).length =
            this._values.length = 0;
    }
    
    /** keys, values, and iterate related methods */
    function sharedKeys () {
        return sharedIterator(this._itp, this._keys);
    }
    
    function sharedValues () {
        return sharedIterator(this._itp, this._values);
    }
    
    function mapEntries () {
        return sharedIterator(this._itp, this._keys, this._values);
    }
    
    function setEntries () {
        return sharedIterator(this._itp, this._values, this._values);
    }
    
    function sharedIterator (itp, array, array2) {
        var p = [0], done = false;
        itp.push(p);
        return {
            next: function () {
                var v, k = p[0];
                if (!done && k < array.length) {
                    v = array2 ? [array[k], array2[k]] : array[k];
                    p[0]++;
                } else {
                    done = true;
                    itp.splice(itp.indexOf(p), 1);
                }
                return {done: done, value: v};
            }
        };
    }
    
    function sharedSize () {
        return this._values.length;
    }
    
    function sharedForEach (callback, context) {
        var it = this.entries();
        for (; ;) {
            var r = it.next();
            if (r.done) break;
            callback.call(context, r.value[1], r.value[0], this);
        }
    }
    
})(typeof exports != 'undefined' && typeof global != 'undefined' ? global : window);

function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}

function validaterDay () {
    var date  = $('.attribute-date');
    var year  = $('#birth-year');
    var month = $('#birth-month');
    var day   = $('#birth-day');
    if ((year.text() || month.text() || day.text()) && (!year.text() || !month.text() || !day.text())) {
        date.find('fieldset.date-error').find('#date-error').remove();
        date.find('fieldset.date-error').after('<label id="date-error" class="error" for="date">' + checkDate + '</label>');
        return;
    }
    if (year.text() && month.text() && day.text()) {
        $('#date-error').remove();
    }
}

function appendDay () {
    var y = $('#birth-year'), m = $('#birth-month'), d = $('#birth-day');
    if (y.text().length > 0 && m.text().length > 0) {
        var daysInSelectedMonth = daysInMonth(m.text(), y.text());
        var st                  = '';
        $('#date-error').remove();
        for (var i = 1; i <= daysInSelectedMonth; i++) {
            st += '<li value="' + addZero(i) + '"><a href="javascript:void(0)">' + addZero(i) + '</a></li>';
        }
        $('.date-sample ul.select-day').html(st);
    }
}

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

function showImage (el, forced) {
    var image = document.getElementById('hovered_image');
    if (forced !== false) {
        var src    = el.src;
        var offset = el.getBoundingClientRect();
        if(el){
            var width = getStyle(el, 'width');
            var height = getStyle(el, 'height');
            var left = offset.left - width - 200;
            var top = offset.top - height - 200;
            if (top < 50) {
                top = offset.top + height;
            }
        }
    }
    if (!image) {
        if (forced !== false) {
            image = '<image src="' + src + '" id="hovered_image" style="top:' + top + 'px; left:' + left + 'px" />';
            document.body.insertAdjacentHTML('beforeend', image);
        }
    } else {
        var style = image.style;
        if (forced === false) {
            style.display = 'none';
        } else {
            image.src = src;
            image.setAttribute('style', 'top:' + top + 'px; left:' + left + 'px');
            style.display = 'block';
        }
    }
}

/* Add text in current pointer position in textarea - TextareaNode.insertAtCaret(text) */
HTMLTextAreaElement.prototype.insertAtCaret = function (text) {
    text = text || '';
    if (document.selection) {
        // IE
        this.focus();
        var sel  = document.selection.createRange();
        sel.text = text;
    } else if (this.selectionStart || this.selectionStart === 0) {
        // Others
        var startPos        = this.selectionStart;
        var endPos          = this.selectionEnd;
        this.value          = this.value.substring(0, startPos) +
            text +
            this.value.substring(endPos, this.value.length);
        this.selectionStart = startPos + text.length;
        this.selectionEnd   = startPos + text.length;
    } else {
        this.value += text;
    }
};