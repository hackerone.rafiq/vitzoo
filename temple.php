<div class="right-content tab-pane active" id="tabvideoAudio">
    <div class="rows">
        <div class="col-conservation tab-content">
            <div class="col-widthd clearfix" id="main_videoAudio">
                <header class="right-title clearfix" id="righttitle">
                    <div class="user-chat">
                        <div class="avatar"><i class="fa fa-user"></i></div>
                        <div class="active-user-info">
                            <div class="chat-us-tt clearfix">
                                <span class="color-text-ddd"></span>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown dot-menu">
                        <button class="btn btn-menu" type="button"
                                onclick="openNav('videoAudio')">
                            <i class="fa fa-info"></i>
                        </button>
                    </div>
                </header>
                <div class="">
                    <div class="clearfix">
                        <div class="video-content">
                            <div class="video-content-caller" style="">
                                <video id="callerVideo" style="height: 100%; width: 100%; display: none"></video>
                                <div class="video-calling">
                                    <img class="video-avata video-avata-caller"
                                         src="https://vitzoo.on/themes/default/asset_frontend/img/pf.jpg">
                                    <p class="video-name-connecting">Lê Thị Hồng Quý</p>
                                    <p class="video-status-connecting">connecting...</p>
                                    <p class="video-status-connected" style="display: none;">connected</p>
                                </div>
                                <div class="video-content-myself">
                                    <video id="selfVideo" class="easyrtcMirror" style="float:left; display: none;"
                                           width="150"
                                           height="150"></video>
                                    <img class="video-avata video-avata-myself"
                                         src="https://vitzoo.on/themes/default/asset_frontend/img/pf.jpg">
                                </div>
                                <div class="video-button-control">

                                    <button id="_video-control" role="button" title="Disable your video"
                                            class="js_5">
                                        <i class="fa fa-video-camera"></i></button>
                                    <button id="_microphone-control" role="button"
                                            title="Mute your microphone"
                                            class="js_5">
                                        <i class="fa fa-microphone"></i>
                                    </button>
                                    <button id="_hangup-control" role="button" class="js_5"
                                            title="hang up">
                                        <i class="fa fa fa-phone"
                                           style="transform: rotate(135deg);"></i>
                                    </button>
                                    <button id="_fullscreen-control" role="button"
                                            title="full screen"
                                            class="js_5">
                                        <i class="fa fa-arrows-alt"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-attach sidenav" id="voltaic_holder_videoAudio">
                <div class="header-chat-profile">
                    <h5>{{trans('fr_home.account')}}</h5>
                    <button class="btn btn-menu closebtn" type="button" onclick="closeNav('videoAudio')">
                        <a href="javascript:void(0)"><i
                                class="fa fa-remove"></i></a>
                    </button>
                    {{--<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i
                            class="fa fa-remove"></i></a>--}}
                </div>

                <div class="nanos scrollview" style="height: 237px;">
                    <div class="content-chat clearfix nano-content">
                        <article>
                            <div class="date-time" data-item="a1" data-time="2016/07/24 12:00:00">
                                <p>Sunday, July 24, 2016</p>
                            </div>
                            <div class="row row-append">
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    03:42 PM
                                                </time>
                                                <div class="msg">
                                                    That is mongodb looks good, huh?
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="date-time" data-item="a1" data-time="2016/07/25 12:00:00">
                                <p>Monday, July 25, 2016</p>
                            </div>
                            <div class="row row-append">
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    03:42 PM
                                                </time>
                                                <div class="msg">
                                                    yes. it is
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    03:44 PM
                                                </time>
                                                <div class="msg">
                                                    do you love me?
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    04:16 PM
                                                </time>
                                                <div class="msg">
                                                    yes, I do
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    04:49 PM
                                                </time>
                                                <div class="msg">
                                                    do you hate me?
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="date-time" data-item="a1" data-time="2016/07/26 12:00:00">
                                <p>Tuesday, July 26, 2016</p>
                            </div>
                            <div class="row row-append">
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    11:12 AM
                                                </time>
                                                <div class="msg">
                                                    no. I don't
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Minh Nhat</span>
                                                    11:18 AM
                                                </time>
                                                <div class="msg">
                                                    what can me help any something else?
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="date-time" data-item="a1" data-time="2016/08/02 12:00:00">
                                <p>Tuesday, August 02, 2016</p>
                            </div>
                            <div class="row row-append">
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    10:02 AM
                                                </time>
                                                <div class="msg">
                                                    no, tks you
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="date-time" data-item="a1" data-time="2016/08/08 12:00:00">
                                <p>Monday, August 08, 2016</p>
                            </div>
                            <div class="row row-append">
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    09:40 AM
                                                </time>
                                                <div class="msg">
                                                    no
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    09:59 AM
                                                </time>
                                                <div class="msg">
                                                    sdfsd
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    10:01 AM
                                                </time>
                                                <div class="msg">
                                                    hghgh
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article>
                            <div class="date-time" data-item="a1" data-time="2016/08/11 12:00:00">
                                <p>Thursday, August 11, 2016</p>
                            </div>
                            <div class="row row-append">
                                <div class="msg_container base_sent clearfix col-md-10 col-md-offset-1">
                                    <div class="col-md-1 col-xs-1 col-st">
                                        <div class="avatar">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-xs-11 ct-chat">
                                        <div class="chat-box">
                                            <div class="messages msg_sent">
                                                <time datetime="2009-11-13T20:00"><span>Lê Thị Hồng Quý</span>
                                                    08:23 AM
                                                </time>
                                                <div class="msg">
                                                    hghghghghg
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="text-chat" id="textchat">
                    <div class="rows clearfix">
                        <form method="POST" action="https://vitzoo.on/en/chat/getMessage" accept-charset="UTF-8"
                              onsubmit="event.preventDefault()"><input name="_token" type="hidden"
                                                                       value="EQj7QHTdigxzyRQWG7yvliK9bUu9wMW2GiKXjuSj">
                            <div class="chatbox">
                                <div class="input-group">
                                    <input type="text" class="form-control emojis-wysiwyg input-message" aria-label=""
                                           placeholder="Nhập tin nhắn" data-emojiable="true" data-receiver="2"
                                           data-roomid="cfcd208495d565ef66e7dff9f98764da">
                                    <div class="input-group-addon">
                                        <input type="file" id="upload_cfcd208495d565ef66e7dff9f98764da"
                                               class="upload-file" name="upload"
                                               data-roomid="cfcd208495d565ef66e7dff9f98764da" data-receiver="2"
                                               style="display: none" multiple="">
                                        <a onclick="document.getElementById('upload_cfcd208495d565ef66e7dff9f98764da').click(); return false"
                                           title="#">
                                            <i class="fa fa-paperclip"></i>
                                        </a>
                                        <a class="emoji-button" id="btn-smile" href="#" title="Emojis"
                                           onclick="document.getElementById('emotion').click(); return false">
                                            <i class="fa fa-smile-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="cModalAcceptCall" class="modal fade" role="dialog">
    <div class="modal-dialog centerDiv">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="gridSystemModalLabel">Vitzoo</h5>
            </div>
            <div class="modal-body panel-heading text-center">
                <div class="avatar"><i class="fa fa-user"></i></div>
                <div class="button" id="acceptCallBox">
                    <button type="button" class="btn btn-default accept" id="callAcceptButton" data-dismiss="modal">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-default hangup" id="callRejectButton" data-dismiss="modal">
                        <i class="fa fa fa-phone" style="transform: rotate(135deg);"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<!--hide-->

<!--show-->
<script>
    var socketRtc = io.connect(helperRtc + '/easyrtc', {secure: true, query: "token=" + token});
    var selfEasyrtcid = "";
    var peers = {};
    var disconnected = false;
    var list_peers = {};
    var haveSelfVideo = false;
    var $userRoomCallCurrent = {};
    var targetEasyrtcId = null;

    socketRtc.on('disconnect', function () {
        disconnected = true;
    });

    socketRtc.on('connect', function () {
        if (disconnected) {
            disconnected = false;
            /*connect();*/
            /*window.location.reload();*/
        }
    });

    socketRtc.on('joinOtherClient', function (data) {
        var room_id = data['room_id'];
        if (typeof data != 'undefined' && typeof room_id != 'undefined' && easyrtc.getRoomsJoined()[room_id] != true) {
            loadAjaxTabMessage({
                url: data['url'],
                id: data['user_id'],
                action: data['destination_id'],
                room_id: room_id,
                $tab_room: data['$tab_room'],
                status: 'no_socket_join'
            });
        }
    });

    document.body.onload = function () {
        connect();
    };

    /*function xxxx(fn) {
     var e = '123';
     fn(e)
     }

     function csl(e) {
     console.log(e);
     }

     xxxx(csl);*/


    function connect() {

        easyrtc.enableDataChannels(true);
        easyrtc.enableVideo(false);
        easyrtc.enableAudio(false);
        easyrtc.setAutoInitUserMedia(false);
        easyrtc.setSocketUrl(":5555");
        easyrtc.setRoomOccupantListener(roomListener);
        easyrtc.setDataChannelOpenListener(function (easyrtcid, usesPeer) {
        });
        easyrtc.setDataChannelCloseListener(function (easyrtcid) {
        });
        easyrtc.connect("easyrtc.dataFileTransfer", loginSuccess, loginFailure);
    }

    function roomListener(roomName, occupants, isPrimary) {
        var peerZone1 = document.getElementById("main_" + roomName);
        var peerZone = peerZone1.querySelector(".content-chat");
        /*var target = [];*/
        function buildDropDiv(easyrtcid) {
            if (easyrtc.getConnectStatus() === easyrtc.NOT_CONNECTED) {
                /*easyrtc.call(easyrtcid,
                 function (caller, mediatype) {
                 },
                 function (errorCode, errorText) {
                 noDCs[easyrtcid] = true;
                 },
                 function wasAccepted(yup) {
                 }
                 );*/
            }
            var noDCs = {}; // which users don"t support data channels

            var fileSender = null;

            /*function filesHandler(files) {
             // if we haven"t eastablished a connection to the other party yet, do so now,
             // and on completion, send the files. Otherwise send the files now.
             var timer = null;
             if (easyrtc.getConnectStatus(easyrtcid) === easyrtc.NOT_CONNECTED && noDCs[easyrtcid] === undefined) {
             //
             // calls between firefrox and chrome ( version 30) have problems one way if you
             // use data channels.
             //

             }
             else if (easyrtc.getConnectStatus(easyrtcid) === easyrtc.IS_CONNECTED || noDCs[easyrtcid]) {
             if (!fileSender) {
             fileSender = easyrtc_ft.buildFileSender(easyrtcid, updateStatusDiv);
             }
             fileSender(files, true /!* assume binary *!/);
             }
             else {
             easyrtc.showError("user-error", "Wait for the connection to complete before adding more files!");
             }
             }*/

        }

        for (var easyrtcid in occupants) {
            if (!peers[roomName]) {
                buildDropDiv(easyrtcid);
                peers[roomName] = true;
            }
        }


    }

    $(window).bind('beforeunload', function (e) {
        socketRtc.emit('leave', {easyrtcid: selfEasyrtcid});
    });

    easyrtc.disconnect = function () {


    };

    var files = {};
    var room_id = '';

    socketRtc.on('sendFiles', function (data) {
        if (files) {
            room_id = data.room_id;
            sendfile(data);
        }
    });

    $(document).on('change', '.upload-file', function () {
        files = this.files;
        var room_id = $(this).data('roomid');
        var receiver_id = $(this).data('receiver');
        var fileName = {};
        var fileSize = {};
        var d = new Date();
        var fileClass = d.getTime();
        if (files) {
            for (var i = 0; i < files.length; i++) {
                fileName[i] = files[i]['name'];
                fileSize[i] = files[i]['size'];
            }
            socketRtc.emit('sendFiles', {
                room_id: room_id,
                receiver_id: receiver_id,
                fileName: fileName,
                fileSize: fileSize,
                fileLength: files.length,
                fileClass: fileClass
            });
        }
    });

    var dataGlobal = {};
    function sendfile(data) {
        var easyrtcid = data.easyrtcidOfUser;
        dataGlobal = data;
        appendMessageFile(data);
        easyrtcid.forEach(function (easyrtcid) {
            var sender = easyrtc_ft.buildFileSender(easyrtcid, updateStatusDiv);
            sender(files, true);
        });
    }

    function updateStatusDiv(state) {
        var $tabRoom = returnTabRoom(dataGlobal);
        var $fileContent = $tabRoom.find('.content-chat').find('.' + dataGlobal['fileClass']);
        switch (state.status) {
            case "waiting":
                $fileContent.append('<p class="file-status">waiting</p>');
                break;
            case "started_file":
                $fileContent.find('.file-status').html('beging send');
                break;
            case "working":
                $fileContent.find('.file-status').html('sending');
                break;
            case "rejected":
                setTimeout(function () {
                    $fileContent.find('.file-status').html('cancel');
                }, 2000);
                break;
            case "done":
                setTimeout(function () {
                    $('.upload-file').val('');
                    $fileContent.find('.file-status').html('sent');
                }, 2000);
                files = '';
                break;
        }
        return true;
    }

    function acceptRejectCB(otherGuy, fileNameList, wasAccepted, data) {
        console.log(fileNameList);
        appendMessageFile(data);
        var abc = wasAccepted;
        var $tabRoom = returnTabRoom(data);
        if (data['fileLength'] > 1) {
            (function () {
                var $fileContent = $tabRoom.find('.content-chat').find('.msg').last()[0];
                $fileContent.innerHTML += '<p class="file-all"><i class="fa fa-download" aria-hidden="true"> All</i></p>';
                $fileContent.addEventListener('click', function () {
                    abc(true);
                });
            })()
        }
        else {
            var $fileContent = $tabRoom.find('.content-chat').find('.' + data['fileClass']);
            $fileContent.append('<p class="file-one"><i class="fa fa-download" aria-hidden="true"></i></p>');
            $fileContent[0].addEventListener('click', function () {
                abc(true);
            });
        }

    }

    socketRtc.on('receiveFiles', function (data) {
        /*var arg = Array.prototype.slice.call(arguments);
         var data = arg[0];*/
        easyrtc_ft.buildFileReceiver(
            function (otherGuy, fileNameList, wasAccepted) {
                acceptRejectCB(otherGuy, fileNameList, wasAccepted, data)
            },
            blobAcceptor,
            function (otherGuy, msg) {
                receiveStatusCB(otherGuy, msg, data)
            });
    });

    function receiveStatusCB(otherGuy, msg, data) {
        var $tabRoom = returnTabRoom(data)
        var $content = $tabRoom.find('.content-chat');
        var $fileContent = $content.find('.' + data['fileClass']);
        /*if (!receiveBlock) return;*/
        switch (msg.status) {
            case "started":
                var $fileAll = $content.find('.file-all');
                var $fileOne = $content.find('.file-one');
                if ($fileAll.length)
                    $fileAll.remove();
                if ($fileOne.length)
                    $fileOne.remove();
                /*$fileContent.append('<p class="status-receive">Receiving</p>');*/
                break;
            case "eof":
                $fileContent.append('<p class="status-receive">Received</p>');
                ;
                break;
            case "done":
                setTimeout(function () {
                }, 1000);
                break;
            case "started_file":
                break;
            case "progress":
                break;
            default:
                console.log("strange file receive cb message = ", JSON.stringify(msg));
        }
        return true;
    }

    function blobAcceptor(otherGuy, blob, filename) {
        easyrtc_ft.saveAs(blob, filename);
    }

    function loginSuccess(easyrtcid) {
        selfEasyrtcid = easyrtcid;
        socketRtc.emit('join', {easyrtcid: easyrtcid});
    }

    function loginFailure(errorCode, message) {
        easyrtc.showError(errorCode, message);
    }

    function returnTabRoom(data) {
        var $tabRoom = $('#tab' + data['room_id']);
        return $tabRoom;
    }

    $(document).on('click', '.chat-video', function (e) {
        var url = $(this).data('url');
        /*PopupCenter(url, 'vitzoo', '1000', '700');*/
        var room_id = $(this).data('roomid');
        var receiver_id = $(this).data('receiver');
        socketRtc.emit('sendAudio', {
            room_id: room_id,
            receiver_id: receiver_id
        });
    });

    function setUpMirror() {
        easyrtc.initMediaSource(
            function (stream) {
                // success callback
                var selfVideo = document.getElementById("selfVideo");
                easyrtc.setVideoObjectSrc(selfVideo, stream);
            },
            function (errorCode, errmesg) {
                easyrtc.showError("MEDIA-ERROR", errmesg);
            }
        );
    }

    function performCall(otherEasyrtcid, data) {
        easyrtc.hangupAll();
        var acceptedCB = function (accepted, easyrtcid) {
            if (!accepted) {
                easyrtc.showError("CALL-REJECTED", "Sorry, your call to " + easyrtc.idToName(easyrtcid) + " was rejected");
            }
            else
                targetEasyrtcId = otherEasyrtcid;
        };
        var successCB = function () {
            blockNoneVideo('whileCalling', 'video');
        };
        var failureCB = function () {
        };

        easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB);
    }

    var xy;
    easyrtc.setStreamAcceptor(function (easyrtcid, stream, streamName) {
        console.log(stream);
        xy = stream;
        var video = document.getElementById('callerVideo');
        easyrtc.setVideoObjectSrc(video, stream);
    });


    easyrtc.setOnStreamClosed(function (easyrtcid, stream, streamName) {
        /*easyrtc.clearMediaStream(document.getElementById("selfVideo"));*/

        /*var getLocalStream = easyrtc.getLocalStream().getTracks();

         getLocalStream.forEach(function (stream) {
         stream.stop();
         stream.muted = true;
         console.log(stream);
         });*/


        /*easyrtc.setVideoObjectSrc(document.getElementById("callerVideo"), "");
         easyrtc.setVideoObjectSrc(document.getElementById("selfVideo"), "");
         console.log(streamName);

         blockNoneVideo('hangup', 'video');*/
    });

    easyrtc.setAcceptChecker(function (easyrtcid, callback) {
        $('#cModalAcceptCall').modal({backdrop: 'static', keyboard: false});
        if (easyrtc.getConnectionCount() > 0) {
        }
        else {

        }
        var acceptTheCall = function (wasAccepted) {
            if (wasAccepted && easyrtc.getConnectionCount() > 0) {
                easyrtc.hangupAll();
            }
            callback(wasAccepted);
        };

        document.getElementById("callAcceptButton").onclick = function () {
            acceptTheCall(true);
            blockNoneVideo('whileCalling', 'video');
            setUpMirror();
            activeTab();
        };

        document.getElementById("callRejectButton").onclick = function () {
            acceptTheCall(false);
        };
    });

    socketRtc.on('sendAudio', function (data) {
        if (typeof data != 'undefined' && typeof sessionGlobal != 'undefined') {
            easyrtc.enableVideo(true);
            easyrtc.enableAudio(true);
            if (data.receiver_id != sessionGlobal) {
                activeTab();
                performCall(data['easyrtcidOfUser'][0], data);
                blockNoneVideo('waiting', 'video');
                setUpMirror();
            }
        }

    });

    function activeTab() {
        var $activeCurent = $('#chat-container .active.tab-pane').removeClass('active');
        $('#tabvideoAudio').addClass('active');
        return $activeCurent;
    }

    document.getElementById("_hangup-control").onclick = function () {
        /*easyrtc.closeLocalStream(namee);
         easyrtc.closeLocalMediaStream();
         easyrtc.hangupAll();
         console.log('xyz');*/
        easyrtc.hangupAll();
        easyrtc.getLocalStream().getAudioTracks()[0].stop();
        easyrtc.getLocalStream().getVideoTracks()[0].stop();
    };

    function foreachArray(array, status) {
        array.forEach(function (i) {
            i.style.display = status;
        });
    }

    function blockNoneVideo(status, method) {
        $img = document.querySelectorAll('.video-avata');
        $video = document.querySelectorAll('video');
        $videoCalling = document.querySelector('.video-calling');
        $avatarCaller = document.querySelector('.video-avata-caller');
        $avatarMyself = document.querySelector('.video-avata-myself');
        $callerVideo = document.getElementById('callerVideo');
        $myselfVideo = document.getElementById('selfVideo');

        if (method == 'video') {
            switch (status) {
                case 'waiting':
                    $avatarMyself.style.display = 'none';
                    $myselfVideo.style.display = 'block';
                    break;
                case 'whileCalling':
                    foreachArray($img, 'none');
                    foreachArray($video, 'block');
                    $videoCalling.style.display = 'none';
                    break;
                case 'hangup':
                    foreachArray($img, 'inline-block');
                    foreachArray($video, 'none');
                    $videoCalling.style.display = 'block';
                    break;
            }
        }
    }
</script>