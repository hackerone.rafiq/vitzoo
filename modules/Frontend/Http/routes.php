<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middlewareGroups' => ['localeSessionRedirect', 'localizationRedirect', 'web'],
    'middleware' => 'csrf'
], function () {
    //controller
    Route::get('/', 'Controller@index');
    Route::get('login', 'Controller@login');
    Route::get('about', 'Controller@about');
    Route::get('service', 'Controller@service');
    Route::get('contact', 'Controller@contact');
    Route::get('faq', 'Controller@faq');
    Route::get('profile/{id}', 'UserController@index');
    Route::get('logout', 'Controller@logout');
    Route::get('404', 'Controller@error_404');
    // register
    /*Route::get('doRegister', 'RegisterController@index');*/
    Route::post('doRegister', 'RegisterController@index');
    Route::get('logout', 'RegisterController@logout');
    //login
    Route::post('doLogin', 'LoginController@index');
    //Profile
    Route::post('getMonthYear', 'UserController@getMonthYear');
    
    Route::post('getCountry', 'UserController@getCountry');

    Route::post('getCity', 'UserController@getCity');
    Route::post('profile/edit', 'UserController@editProfile');
    Route::post('profile/editPassword', 'UserController@editProfilePassword');
    Route::post('profile/uploadAvatar', 'UserController@uploadAvatar');
    Route::get('/invite/{token}', 'Controller@invite');
    Route::get('suggestFriend', 'Controller@suggestFriend');
    Route::get('updateUrl', 'Controller@updateUrl');
    /*Route::get('test', function () {
        // create owner role
//		$ownerRole = Sentinel::getRoleRepository()->createModel()->create([
//			'name' => 'Owner',
//			'slug' => 'owner',
//		]);
        $perm = new \Modules\User\Entities\PermissionEntity();
        $data = [
            'system.config_update',
            'system.website_create',
            'system.website_edit',
            'system.website_delete',
            'system.store_create',
            'system.store_update',
            'system.store_delete',
            'system.trans_create',
            'system.trans_update',
            'system.trans_delete',
            'admin.user_create',
            'admin.user_update',
            'admin.user_delete',
            'admin.access',
            'user.update',
            'user.view'
        ];
        foreach ($data as $key => $value) {
            if ($value) {
                $p = $perm->find($key+1);
                $p->code = $value;
                $p->save();
            }
        }
        $permissions = $perm->get();
        $perms = [];
        foreach ($permissions as $permission) {
            $perms[$permission->code] = true;
        }
        $ownerRole = Sentinel::findRoleById(1);
        // assign permissions to owner role
        $ownerRole->permissions = $perms;
        $ownerRole->save();

        // create admin role
//		$adminRole = Sentinel::getRoleRepository()->createModel()->create([
//			'name' => 'Administrator',
//			'slug' => 'admin',
//		]);
        $adminRole = Sentinel::findRoleById(2);
        // assign permissions to owner role
        $adminRole->permissions = [
            'system.config_update' => true,
            'system.website_create' => false,
            'system.website_edit' => false,
            'system.website_delete' => false,
            'system.store_create' => true,
            'system.store_update' => true,
            'system.store_delete' => true,
            'system.trans_create' => false,
            'system.trans_update' => true,
            'system.trans_delete' => false,
            'admin.user_create' => true,
            'admin.user_update' => true,
            'admin.user_delete' => true,
            'admin.access' => true,
            'user.update' => true,
            'user.view' => true
        ];
        $adminRole->save();

        // create user role
//		$userRole = Sentinel::getRoleRepository()->createModel()->create([
//			'name' => 'User',
//			'slug' => 'user',
//		]);
        $userRole = Sentinel::findRoleById(3);
        // assign permissions to user role
        $userRole->permissions = [
            'system.config_update' => false,
            'system.website_create' => false,
            'system.website_edit' => false,
            'system.website_delete' => false,
            'system.store_create' => false,
            'system.store_update' => false,
            'system.store_delete' => false,
            'system.trans_create' => false,
            'system.trans_update' => false,
            'system.trans_delete' => false,
            'admin.user_create' => false,
            'admin.user_update' => false,
            'admin.user_delete' => false,
            'user.update' => true,
            'user.view' => true
        ];
        $userRole->save();

        // create admin account
//		$owner = Sentinel::registerAndActivate([
//			'website_id'	=> 1,
//			'email'			=> 'intersteller@gmail.com',
//			'password'		=> '123123',
//			'first_name'	=> 'Thuan',
//			'last_name'		=> 'Nguyen',
//			'mobile'		=> '(+84) 905930066',
//			'address'		=> '3B/109 Dang Huy Tru',
//			'status'		=> 'active'
//		]);
//
//		// create admin account
//		$admin = Sentinel::registerAndActivate([
//			'website_id'	=> 1,
//			'email'			=> 'admin@vietprojectgroup.com',
//			'password'		=> '123123',
//			'first_name'	=> 'Admin',
//			'last_name'		=> 'Testing',
//			'mobile'		=> '(+84) 123406789',
//			'address'		=> '23/33 Nguyen Truong To',
//			'status'		=> 'active'
//		]);
//
//		// create user account
//		$user = Sentinel::registerAndActivate([
//			'website_id'	=> 1,
//			'email'			=> 'user@vietprojectgroup.com',
//			'password'		=> '123123',
//			'first_name'	=> 'User',
//			'last_name'		=> 'Testing',
//			'mobile'		=> '(+84) 123456789',
//			'address'		=> '23/33 Nguyen Truong To',
//			'status'		=> 'active'
//		]);
//
//		// assign role to owner account
//		$ownerRole->users()->attach($owner);
//		// assign role to admin account
//		$adminRole->users()->attach($admin);
//		// assign role to user account
//		$userRole->users()->attach($user);

        dd('Done!');
    });*/
});
