<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Modules\Chat\Entities\RoomEntity;
use Request, SEOMeta, Theme, Helper, Assets, Hashids,Sentinel;
use DB;

class Controller extends BaseController
{
    public $session;

    public function __construct()
    {
        parent::__construct();

        $this->session = Helper::getSession();
        // set theme to use
        !empty($this->session['config']) ? Theme::setActive($this->session['config']->theme) : Theme::getActive();
        // set title suffix
        $titleSuffix = !empty($session['config']) ? $session['config']->title_suffix : 'ViTPR';
        SEOMeta::setTitleDefault($titleSuffix);
        Theme::setLayout('modules.frontend.layouts.master');

        Assets::add([
            /*'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js',*/
            Helper::getThemeJs('socket.io.min.js')
        ]);
    }

    public function index()
    {
        $sentinel = Sentinel::check();
        return Theme::view('modules.frontend.index',['sentinel'=> $sentinel]);
    }
    
    public function invite($token)
    {
        $sentinel = Sentinel::check();
        return Theme::view('modules.frontend.index', ['sentinel' => $sentinel,'token_invite'=>$token]);
    }

    public function error_404()
    {
        return Theme::view('modules.frontend.errors.404');
    }

    public function about()
    {
        return Theme::view('modules.frontend.about.about');
    }

    public function profile()
    {
        return Theme::view('modules.frontend.profile.profile');
    }
    public function service()
    {
        return Theme::view('modules.frontend.service.service');
    }
    public function faq()
    {
        return Theme::view('modules.frontend.faq.faq');
    }
    public function contact()
    {
        return Theme::view('modules.frontend.contact.contact');
    }
    
    function suggestFriend()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        var_dump($ip);
    }
    
    /**
     * set url equal empty
     */
    function updateUrl()
    {
        $room = RoomEntity::get();
        $i = 0;
        foreach ($room as $r) {
            $r->url = "";
            $r->save();
            var_dump($i++);
        }
    }
}