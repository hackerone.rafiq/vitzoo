<?php
/**
 * Created by PhpStorm.
 * User: NGOCTHINH
 * Date: 6/20/2016
 * Time: 11:28 AM
 */

namespace Modules\Frontend\Http\Controllers;

use Helper, Theme, SEOMeta, Assets, Sentinel, Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Modules\User\Models\AttributeModel;
use Modules\User\Models\LanguageModel;
use Modules\User\Models\UserAttributeModel;
use Modules\User\Models\UserModel;

class UserController extends BaseController
{
    private $UserModel;
    private $UserAttributeModel;
    private $AttributeModel;
    private $LanguageModel;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->session = Helper::getSession();
        // set theme to use
        !empty($this->session['config']) ? Theme::setActive($this->session['config']->theme) : Theme::getActive();
        // set title suffix
        $titleSuffix = !empty($session['config']) ? $session['config']->title_suffix : 'ViTPR';
        SEOMeta::setTitleDefault($titleSuffix);
        Theme::setLayout('modules.frontend.layouts.master');
        
        Assets::add([
            /*'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js',*/
            Helper::getThemeJs('socket.io.min.js')
        ]);
        $this->UserModel = new UserModel();
        $this->UserAttributeModel = new UserAttributeModel();
        $this->AttributeModel = new AttributeModel();
        $this->LanguageModel = new LanguageModel();
    }
    
    public function index($id)
    {
        if (!$this->isLoggedIn() && $id) {
            Assets::add([
                url("themes/default/asset_frontend/js/jquery.cropit.js")
            ]);
            $user = UserModel::getUserById($id);
            $country = $this->UserModel->getAllCountry();
            $user_attribute = $this->UserAttributeModel->getAllAtribute($id);
            $user_attribute_other = $this->UserAttributeModel->getAtributePublic($id, ['user_id', 'attribute_id', 'values', 'id', 'status']);
            $attribute = $this->AttributeModel->getAllAttribute();
            $language = $this->LanguageModel->getAllLanguage();
            //$city = $this->UserModel->getCity();
            $month = date('m', strtotime($user->birthday));
            $year = date('Y', strtotime($user->birthday));
            $date = $this->getDaysInMonth($month, $year);
            $data = [
                'user' => $user,
                'date' => $date,
                'country' => $country,
                'user_attribute' => $user_attribute,
                'attribute' => $attribute,
                'language' => $language,
                'user_attribute_other' => $user_attribute_other
            ];
            return Theme::view('modules.frontend.profile.profile', $data);
        }
        return redirect(Helper::url(''));
    }
    
    public function getDaysInMonth($month, $year)
    {
        $days = ($month === 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31));
        return $days;
    }
    
    public function getMonthYear()
    {
        if (Request::ajax() && Input::has('month', 'year')) {
            $month = Request::input('month');
            $year = Request::input('year');
            $date = $this->getDaysInMonth($month, $year);
            $data = "";
            for ($i = 1; $i <= $date; $i++) {
                $data .= '<li value="' . $i . '"><a href="">' . $i . '</a></li>';
            }
            return Helper::json($data);
        }
    }
    
    public function getCountry()
    {
        if (Request::ajax() && Input::has('idCountry')) {
            $country = Request::input('idCountry');
            $dataCountry = UserModel::getProvinces($country);
            $data = "";
            if ($dataCountry) {
                foreach ($dataCountry as $item) {
                    $data .= '<li value="' . $item->subdivision_1_iso_code . '" data-value=' . $item->id . '><a href="javascript:void(0)">' . $item->subdivision_1_name . '</a></li>';
                }
            }
            return Helper::json($data);
        }
    }
    
    public function getCity()
    {
        if (Request::ajax() && Input::has('idProvinces') && Input::has('idCountry')) {
            $idProvinces = Request::input('idProvinces');
            $idCountry = Request::input('idCountry');
            $dataCity = UserModel::getCity($idProvinces, $idCountry);
            $data = "";
            if ($dataCity) {
                foreach ($dataCity as $item) {
                    $data .= '<li value="' . $item->id . '" data-value=' . $item->id . '><a href="javascript:void(0)">' . $item->city . '</a></li>';
                }
            }
            return Helper::json($data);
        }
    }
    
    public function editProfile()
    {
        //todo: edit profile
        $result = '';
        if (Request::ajax()) {
            $data = Request::all();
            $user = Sentinel::check();
            if ($user)
                $result = $this->UserAttributeModel->editProfile($data, $user->id);
        }
        return Helper::json($result);
    }
    
    public function editProfilePassword()
    {
        if (Request::ajax() &&
            ($current_password = Request::input('current_password')) &&
            ($password = Request::input('password'))
        ) {
            $user = Sentinel::getUser();
            $user_new = [
                'email' => $user->email,
                'password' => $current_password
            ];
            if (Sentinel::validateCredentials($user, $user_new)) {
                if ($current_password !== $password) {
                    $user_new['password'] = $password;
                    $result = Sentinel::update($user, $user_new);
                    unset($result['password']);
                    $result = Helper::resultData(true, trans('site.action_succeeded'), $result);
                } else {
                    $result = Helper::resultData(false, trans('site.new_password_unchanged'));
                }
            } else {
                $result = Helper::resultData(false, trans('site.action_failed'));
            }
        } else {
            $result = Helper::resultData(false, trans('site.error_occured'));
        }
        return Helper::json($result);
    }
    
    public function uploadAvatar()
    {
        if (Request::ajax() && Request::hasFile('data')) {
            $user_id = Sentinel::check()->id;
            $data = Request::file('data');
            $result = Helper::resultData(false, trans('site.update_fail'));
            if (filesize($data) > 1048576) {
                $result = Helper::resultData(false, trans('site.size_large'));
                return Helper::json($result);
            }
            
            $img = Image::make($data->getRealPath());
            if ($img->width() >Helper::getWidthHeighAvatar())
                $img->resize(Helper::getWidthHeighAvatar(), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            if ($img->height() > Helper::getWidthHeighAvatar())
                $img->resize(null, Helper::getWidthHeighAvatar(), function ($constraint) {
                    $constraint->aspectRatio();
                });
            $destinationPath = public_path() . Helper::getLinkAvatar();
//            $file_name = $user_id.md5($user_id).'.jpg';
            $file_name = strtolower(uniqid($user_id . '_') . '.jpg');
            $img->save($destinationPath . $file_name);
            
            if ($user_id) {
                $data = UserModel::updateAvatar(['user_id' => $user_id, 'avatarName' => $file_name]);
                if ($data) {
                    $result = Helper::resultData(true, trans('site.update_success'), ['url' => $file_name]);
//                    \LRedis::publish('addfriend', json_encode(['user_id' => $user_id, 'target_users' => $room_users]));
                }
                
            }
            return Helper::json($result);
        }
    }
}