<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Frontend\Http\Controllers;

use Request, SEOMeta, Theme, Helper, Assets, Hashids, Sentinel;
use DB;
use Modules\User\Models\UserModel;
use Modules\User\Models\StatusModel;

class RegisterController extends BaseController
{
    private $session;
    private $UserModel;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->session = Helper::getSession();
        // set theme to use
        !empty($this->session['config']) ? Theme::setActive($this->session['config']->theme) : Theme::getActive();
        // set title suffix
        $titleSuffix = !empty($session['config']) ? $session['config']->title_suffix : 'ViTPR';
        SEOMeta::setTitleDefault($titleSuffix);
        Theme::setLayout('modules.frontend.layouts.master');
        
        Assets::add([
            /*'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js',*/
            Helper::getThemeJs('socket.io.js')
        ]);
        $this->UserModel = new UserModel();
    }
    
    public function index()
    {
        if (Request::ajax()) {
            $post_data = Request::all();
            unset($post_data['_token']);
            $validator = $this->UserModel->validatorUser($post_data, 'register');
            if ($validator->fails()) {
                $result = Helper::getValidatorError($validator);
                return Helper::json($result, 422);
            }
            unset($post_data['retype-password']);
            $post_data['group'] = 3;
            $post_data['role'] = 'user';
            $result = UserModel::createUser($post_data);
            
            if ($result['success'] && !empty($result) && !empty($result['data'] && !empty($result['data']['id']))) {
                $data = [
                    'user_id' => $result['data']['id'],
                    'status' => 'online'
                ];
                StatusModel::createOrUpdateStatus($data);
                if ($post_data && isset($post_data["token_invite"])) {
                    $invite_token = $post_data["token_invite"];
                    $this->UserModel->incrementInvite($invite_token);
                }
            }
            // push message to flash session
            Helper::setSessionMessage($result);
            return Helper::json($result);
        } else {
            die();
        }
    }
    
    public function logout()
    {
        Sentinel::logout();
        session()->forget('jwt_token');
        return redirect(Helper::url(''));
    }
    
    
    /** Facebook login redirection
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToProvider()
    {
        //if ($this->checkLogin()) return redirect()->back();
        return \Socialize::driver('facebook')->scopes(['email', 'public_profile'])->redirect();
    }
    
    //Twitter
    public function rediretTwitter()
    {
        return \Socialize::driver('twitter')->redirect();
    }
    
    //google
    public function rediretGoogle()
    {
        return \Socialize::driver('google')->redirect();
    }
    
    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        //if ($this->checkLogin()) return redirect()->back();
        //$user = \Socialize::driver('facebook')-
        $driver = \Socialize::driver('facebook')->fields
        ([
            'name',
            'first_name',
            'last_name',
            'email',
            'gender',
            'verified',
        ]);
// retrieve the user
        $fb = $driver->user();
        if (!$fb) return redirect(url());
        $user = $fb->user;
        $user['avatar'] = $fb->avatar;
//        echo "<pre>";
//        print_r($user); exit();
        $user['group'] = 3;
        $user['role'] = 'user';
        //session()->put('user', $user);
        $data = ['email', 'first_name', 'last_name', 'id', 'role', 'group', 'avatar'];
        $data = array_flip($data);
        $data = array_intersect_key($user, $data);
        $result = (new UserModel)->createUserSocial($data);
        try {
            if (!empty($locale = $user['user']->language_code))
                \LaravelLocalization::setLocale($locale);
        } catch (\Exception $e) {
            
        }
        return redirect(\Helper::url());
    }
    
    //Twitter
    public function handleTwitterCallback()
    {
        try {
            $user = \Socialize::driver('twitter')->user();
            echo "<pre>";
            print_r($user);
        } catch (Exception $e) {
            return redirect('auth/twitter');
        }
//        $authUser = $this->findOrCreateUser($user);
//        Auth::login($authUser, true);
        
        return redirect(\Helpers::url());
    }
    
    //Google
    public function handleGoogleCallback()
    {
        try {
            $user = \Socialize::driver('google')->user();
            echo "<pre>";
            print_r($user);
        } catch (Exception $e) {
            return redirect('auth/google');
        }
//        $authUser = $this->findOrCreateUser($user);
//        Auth::login($authUser, true);
        
        return redirect(\Helpers::url());
    }
    
}