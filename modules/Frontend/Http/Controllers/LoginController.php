<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Frontend\Http\Controllers;

use Request, SEOMeta, Theme, Helper, Assets, Hashids, Sentinel, Response;
use Authorizer;
use DB;
use Modules\User\Models\UserModel;
use Modules\User\Models\StatusModel;


class LoginController extends BaseController
{
    public $session;
    private $UserModel;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->session = Helper::getSession();
        // set theme to use
        !empty($this->session['config']) ? Theme::setActive($this->session['config']->theme) : Theme::getActive();
        // set title suffix
        $titleSuffix = !empty($session['config']) ? $session['config']->title_suffix : 'ViTPR';
        SEOMeta::setTitleDefault($titleSuffix);
        Theme::setLayout('modules.frontend.layouts.master');
        
        Assets::add([
            /*'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js',*/
            Helper::getThemeJs('socket.io.min.js')
        ]);
        $this->UserModel = new UserModel();
    }
    
    public function index()
    {
        if (Request::ajax()) {
            $post_data = Request::all();
            unset($post_data['_token']);
            $validator = $this->UserModel->validatorUser($post_data, 'login');
            if ($validator->fails()) {
                return Helper::getValidatorError($validator);
            }
            $result = Helper::resultData(true, trans('login.login_success'));
            if (!UserModel::loginUser($post_data)) {
                $result = Helper::resultData(false, trans('login.login_fail'));
            }
            Helper::setSessionMessage($result);
            return Helper::json($result);
        }
        die();
    }
}