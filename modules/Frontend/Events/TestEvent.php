<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: vitzoo
 * Date: 6/2/16
 */

namespace Modules\Frontend\Events;

use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class TestEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $data;

    public function __construct()
    {
        $this->data = array(
            'power'=> '20'
        );
    }

    public function broadcastOn()
    {
        return ['test-channel'];
    }
}