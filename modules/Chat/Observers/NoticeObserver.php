<?php
/**
 * Created by PhpStorm.
 * User: levy
 * Date: 6/14/2016
 * Time: 9:41 AM
 */
namespace Modules\Chat\Observers;

use Modules\Chat\Entities\IncrementIdEntity;

class NoticeObserver
{
    public function saving($notice)
    {
    }

    public function saved($notice)
    {
    }

    public function creating($notice)
    {
        // auto add increment id when create new user
        $notice_id = IncrementIdEntity::getIncrementId('chat__notices');
        $notice->_id = $notice_id;
    }

    public function created($notice)
    {
    }

    public function updating($notice)
    {
    }

    public function updated($notice)
    {
    }

    public function deleting($notice)
    {
    }

    public function deleted($notice)
    {
    }

    public function restoring($notice)
    {
    }

    public function restored($notice)
    {
    }
}