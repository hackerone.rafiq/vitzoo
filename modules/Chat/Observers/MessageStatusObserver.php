<?php
/**
 * Created by PhpStorm.
 * User: levy
 * Date: 6/14/2016
 * Time: 9:41 AM
 */
namespace Modules\Chat\Observers;

use Modules\Chat\Entities\IncrementIdEntity;

class MessageStatusObserver
{
    public function saving($message_status)
    {
    }

    public function saved($message_status)
    {
    }

    public function creating($message_status)
    {
        // auto add increment id when create new user
        $message_status_id = IncrementIdEntity::getIncrementId('chat__message_status');
        $message_status->_id = $message_status_id;
    }

    public function created($message_status)
    {
    }

    public function updating($message_status)
    {
    }

    public function updated($message_status)
    {
    }

    public function deleting($message_status)
    {
    }

    public function deleted($message_status)
    {
    }

    public function restoring($message_status)
    {
    }

    public function restored($message_status)
    {
    }
}