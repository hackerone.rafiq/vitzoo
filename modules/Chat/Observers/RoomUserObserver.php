<?php
/**
 * Created by PhpStorm.
 * User: levy
 * Date: 6/14/2016
 * Time: 9:41 AM
 */
namespace Modules\Chat\Observers;

use Modules\Chat\Entities\IncrementIdEntity;

class RoomUserObserver
{
    public function saving($room_user)
    {
    }

    public function saved($room_user)
    {
    }

    public function creating($room_user)
    {
        // auto add increment id when create new user
        $room_user_id = IncrementIdEntity::getIncrementId('chat__room_users');
        $room_user->_id = $room_user_id;
    }

    public function created($room_user)
    {
    }

    public function updating($room_user)
    {
    }

    public function updated($room_user)
    {
    }

    public function deleting($room_user)
    {
    }

    public function deleted($room_user)
    {
    }

    public function restoring($room_user)
    {
    }

    public function restored($room_user)
    {
    }
}