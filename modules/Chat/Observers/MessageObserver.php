<?php
/**
 * Created by PhpStorm.
 * User: levy
 * Date: 6/14/2016
 * Time: 9:41 AM
 */
namespace Modules\Chat\Observers;

use Modules\Chat\Entities\IncrementIdEntity;

class MessageObserver
{
    public function saving($message)
    {
    }

    public function saved($message)
    {
    }

    public function creating($message)
    {
        // auto add increment id when create new user
        $message_id = IncrementIdEntity::getIncrementId('chat__messages');
        $message->_id = $message_id;
    }

    public function created($message)
    {
    }

    public function updating($message)
    {
    }

    public function updated($message)
    {
    }

    public function deleting($message)
    {
    }

    public function deleted($message)
    {
    }

    public function restoring($message)
    {
    }

    public function restored($message)
    {
    }
}