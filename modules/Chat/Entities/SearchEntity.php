<?php
namespace Modules\Chat\Entities;


use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;

class SearchEntity extends Model
{
    use Searchable;
    protected $table = 'chat__search';

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'email'
    ];

    public $searchable = ['id', 'first_name', 'last_name', 'email'];

    public function buildDocument()
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' =>$this->last_name,
            'email' => $this->email
        ];
    }

    public $documentType = 'search';
    public $documentIndex = 'search_index';
    //a
}