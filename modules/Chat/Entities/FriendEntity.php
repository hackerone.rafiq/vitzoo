<?php
namespace Modules\Chat\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\UserEntity;

class FriendEntity extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'chat__friends';
    public $timestamps = true;
    public $primaryKey = 'id';
    protected $fillable = ['id','user_id','friend_id','status'];
    /**
     * Get models
     */
    public function joinUser()
    {
        return $this->belongsTo(new UserEntity(), 'friend_id');
    }

    public function user(){
        return $this->belongsTo(new UserEntity(), 'friend_id', 'id');
    }

    public function joinMessageStatus($room_id = 0, $user_id = 0){
        return MessageStatusEntity::where('user_id','=',$user_id)->where('room_id','=',$room_id)->first();
    }
}