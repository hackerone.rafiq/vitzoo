<?php
namespace Modules\Chat\Entities;

use Jenssegers\Mongodb\Model as Eloquent;

class MessageStatusEntity extends Eloquent
{
    protected $collection = 'chat__message_statuses';
    protected $connection = 'mongodb';
    protected $primaryKey = '_id';

    public static function boot()
    {
        parent::boot();
        MessageStatusEntity::observe(new \Modules\Chat\Observers\MessageStatusObserver());
    }

}