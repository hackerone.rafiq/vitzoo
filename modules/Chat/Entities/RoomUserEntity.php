<?php
namespace Modules\Chat\Entities;

use Jenssegers\Mongodb\Model as Eloquent;
use Modules\User\Entities\UserEntity;

class RoomUserEntity extends Eloquent
{
    /**
     * {@inheritDoc}
     */
    protected $collection = 'chat__room_users';
    protected $connection = 'mongodb';
    protected $primaryKey = '_id';
    protected $fillable = ['_id','room_id','user_id','created_at','updated_at'];
    /**
     * Get models
     */
    public static function boot()
    {
        parent::boot();
        RoomUserEntity::observe(new \Modules\Chat\Observers\RoomUserObserver());
    }

    public function joinRoom()
    {
        return $this->belongsTo(new RoomEntity(),'room_id', '_id');

    }

    public function joinUser(){
        return $this->belongsTo(new UserEntity(), 'user_id', 'id');
    }

    public function countMessageIsRead($room_id = 0, $user_id = 0)
    {
        return MessageStatusEntity::where('user_id', '=', $user_id)->where('room_id', '=', $room_id)->where('is_read', '>', 0)->select(['_id', 'is_read', 'message_id'])->first();
    }
}