<?php
namespace Modules\Chat\Entities;

use \Jenssegers\Mongodb\Model as Eloquent;

class IncrementIdEntity extends Eloquent
{
    protected $collection = 'identitycounters';
    public $timestamps = false;
    protected $connection = 'mongodb';

    public static function getIncrementId($name, $field = '_id',$condition = 'model')
    {
        $seq = IncrementIdEntity::where($condition,$name)->first();
        if (empty($seq)) {
            $seq = new IncrementIdEntity();
            $seq->model = $name;
            $seq->field = $field;
            $seq->count = 0;
            $seq->__v = 0;
        }
        $seq->count += 1;
        $seq->save();
        return $seq->count;
    }
}