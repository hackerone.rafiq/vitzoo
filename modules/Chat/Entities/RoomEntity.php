<?php
namespace Modules\Chat\Entities;

use Jenssegers\Mongodb\Model as Eloquent;

class RoomEntity extends Eloquent
{
    /**
     * {@inheritDoc}
     */
    protected $collection = 'chat__rooms';
    protected $connection = 'mongodb';
    protected $primaryKey = '_id';
    protected $fillable = ['_id','title','is_public','room_admin','created_by','updated_by','created_at','updated_at'];
    /**
     * Get models
     */
    public function joinRoomUser()
    {
        return $this->hasMany(new RoomUserEntity(), 'room_id', 'id');
    }
    
    public function user($id = false) {
        if(!$id) return false;
        return $this->hasMany(new RoomUserEntity(), 'room_id', 'id')->where('user_id', '!=', $id)->first()->joinUser();
    }
}