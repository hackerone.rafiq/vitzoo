<?php
namespace Modules\Chat\Entities;

use Jenssegers\Mongodb\Model as Eloquent;
use Modules\User\Entities\UserEntity;

class MessageEntity extends Eloquent
{
    protected $collection = 'chat__messages';
    protected $connection = 'mongodb';
    protected $primaryKey = '_id';
    protected $dates = ['date'];

    public static function boot()
    {
        parent::boot();
        MessageEntity::observe(new \Modules\Chat\Observers\MessageObserver());
    }

    function joinUser(){
        return $this->belongsTo(new UserEntity(), 'user_id', 'id');
    }
}