<?php
namespace Modules\Chat\Entities;


use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\UserEntity;

class NoticeEntity extends Model
{
    protected $table = 'chat__notices';

    protected $fillable = [
        'id',
        'user_id',
        'friend_id',
        'is_read',
        'room_id',
        'type',
        'is_click',
        'is_confirm'
    ];
    /**
     * Get models
     */
    public function joinUser()
    {
        return $this->belongsTo(new UserEntity(), 'user_id','id');
    }

    public function joinNoticeLanguage(){
        return $this->hasMany(new NoticeLanguageEntity(),'notice_id','id');
    }
}