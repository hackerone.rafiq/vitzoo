<?php
namespace Modules\Chat\Entities;


use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\UserEntity;

class NoticeLanguageEntity extends Model
{
    protected $table = 'chat__notices_language';

    protected $fillable = [
        'id',
        'notice_id',
        'message',
        'language_code'
    ];
    public $timestamps = false;


    public function joinNotice(){
        return $this->belongsTo(new NoticeEntity(),'notice_id','id');
    }
}