<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Chat\Http\Controllers;

use App\Item;
use Modules\Chat\Entities\FriendEntity;
use Modules\Chat\Entities\NoticeLanguageEntity;
use Modules\Chat\Entities\SearchEntity;
use Modules\Chat\Models\ElasticSearchModel;
use Modules\Chat\Models\MessageModel;
use Modules\Chat\Models\RoomModel;
use Modules\Chat\Models\RoomUserModel;
use Modules\User\Entities\UserEntity;
use Modules\User\Models\StatusModel;
use Modules\User\Models\UserAttributeModel;
use Request, SEOMeta, Theme, Helper, Assets, Sentinel, LRedis;
use DB;
use Modules\User\Models\UserModel;
use Modules\Chat\Models\FriendModel;
use Modules\Chat\Models\NoticeModel;
use Sleimanx2\Plastic\Facades\Plastic;

class ElasticSearchController extends BaseController
{
    private $UserModel;
    private $user;
    private $SearchModel;
    private $StatusModel;
    
    public function __construct()
    {
        parent::__construct();
        // set title suffix
        $titleSuffix = !empty($session['config']) ? $session['config']->title_suffix : 'ViTPR';
        SEOMeta::setTitleDefault($titleSuffix);
        Theme::setLayout('modules.frontend.layouts.master');
        
        $this->UserModel = new UserModel();
        if ($this->isLoggedIn() == null) {
            $this->user = Sentinel::check();
        }
        $this->SearchModel = new ElasticSearchModel();
        $this->StatusModel = new StatusModel();
        
    }
    
    /**
     * @param name , value of input search
     * @return json array user when user search for name
     */
    function index()
    {
        if (Request::ajax() && $this->user) {
            $user_id = $this->user->id;
            $name = Request::input('name');
            if($name)
                $name= trim($name);
            $type = 'addfriend';
            if(Request::input('type'))
                $type = Request::input('type');
            $htmlUser = '';
            $checkEmpty = true;
            
            if ($name) {
                $users = $this->SearchModel->searchUser($name, ['id', 'first_name', 'last_name', 'url','email']);
                if (count($users)) {
                    foreach ($users as $user) {
                        $friend_id = $user->id;
                        if ($user_id != $friend_id) {
                            $checkEmpty = false;
                            $friend = StatusModel::getMoodById($friend_id);
                            $relative = FriendModel::getStatusRelate($user_id, $friend_id)['status'];
                            $type = $relative != 'friend' ? 'nonefriend' : 'addfriend';
                            if($relative == 'waiting')
                                $type = 'waiting';
                            $htmlUser .= Theme::view('modules.frontend.chat.chat_afriend', ['friend' => $friend, 'user' => $user, 'type' => $type])->render();
                        }
                    }
                }
                
                if ($checkEmpty) {
                    $htmlUser = trans('fr_home.did_not_find_results');
                }
                $result = Helper::resultData(true, trans('fr_home.result'), ['htmlUser' => $htmlUser, 'title' => trans('fr_home.the_search_results'),'type' => 'search']);
            } else {
                $chatController = new ChatController();
                $htmlUser = $chatController->getFriend($user_id, $type);
                $result = Helper::resultData(true, trans('fr_home.result'), ['htmlUser' => $htmlUser, 'title' => trans('fr_home.all_friend'),'type' => 'getfriend']);
            }
            return Helper::json($result);
        }
        return Helper::json('', 422);
    }
    
    function loadMoreFriend(){
        
    }
}