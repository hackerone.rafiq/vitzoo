<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Chat\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Modules\Chat\Entities\FriendEntity;
use Modules\Chat\Entities\MessageStatusEntity;
use Modules\Chat\Models\MessageModel;
use Modules\Chat\Models\RoomModel;
use Modules\Chat\Models\RoomUserModel;
use Modules\User\Models\UserAttributeModel;
use Request, SEOMeta, Theme, Helper, Assets, Sentinel, LRedis;
use DB;
use Modules\User\Models\UserModel;
use Modules\User\Models\StatusModel;
use Modules\Chat\Models\FriendModel;
use Modules\Chat\Models\NoticeModel;

class ChatController extends BaseController
{
    private $session;
    private $UserModel;
    private $user;
    private $friend;
    private $notice;
    private $userAttribute;
    private $room;
    private $roomUser;
    private $message;
    private $limit = 50;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->session = Helper::getSession();
        // set theme to use
        !empty($this->session['config']) ? Theme::setActive($this->session['config']->theme) : Theme::getActive();
        // set title suffix
        $titleSuffix = !empty($session['config']) ? $session['config']->title_suffix : 'ViTPR';
        SEOMeta::setTitleDefault($titleSuffix);
        Theme::setLayout('modules.frontend.layouts.master');
        $this->isLoggedIn();
        $this->UserModel = new UserModel();
        $this->user = Sentinel::check();
        $this->friend = new FriendModel();
        $this->notice = new NoticeModel();
        $this->userAttribute = new UserAttributeModel();
        $this->room = new RoomModel();
        $this->roomUser = new RoomUserModel();
        $this->message = new MessageModel();
        Assets::add([
            url("themes/default/asset_frontend/chat/search.js")
        ]);
    }
    
    public function index($request_user_id = false,$room = false)
    {
        if (!$this->isLoggedIn()) {
            $user = Sentinel::check();
            $status = StatusModel::getStatusById($user->id);
            $friend = $this->friend->getFriend($user->id, ['friend', 'waiting']);
            $roomUser = $this->room->getAllRoomUserContainer($user->id);
            $recent = $this->room->recentActiveRoom($user->id, false);
            $allRecent = $this->room->recentActiveRoom($user->id, true);
            if ($room) {
                $acronym = substr($room, 0, 2);
            } else {
                $acronym = '';
            }
            $max_page = ceil($allRecent / 10) - 1;
            $data = [
                'status' => $status,
                'username' => $user->last_name . ' ' . $user->first_name,
                'listuser' => $friend,
                'session' => $user,
                'roomUser' => $roomUser,
                'recent' => $recent,
                'acronym' => $acronym,
                'list_friend_global' => [],
                'max_page' => $max_page,
                'show_max' => $max_page > 1,
                'page' => 2,
            ];
            Assets::add([
                url("themes/default/asset_frontend/css/emoji.min.css"),
                url("themes/default/asset_frontend/css/jquery.scrollbar.min.css"),
                url("themes/default/asset_frontend/js/jquery.scrollbar.min.js"),
                url("themes/default/asset_frontend/js/jquery.nanoscroller.min.js"),
                url("themes/default/asset_frontend/js/emojis.min.js"),
                url("themes/default/asset_frontend/js/gnmenu.min.js"),
                url("themes/default/asset_frontend/js/jquery.cropit.min.js"),
            ]);
            
            return Theme::view('modules.frontend.chat.chat_index', $data);
        }
        return $this->isLoggedIn();
    }
    
    public function moreRecent()
    {
        if (!$this->isLoggedIn()) {
            $page = (int) request('page');
            $max_page = (int) request('max');
            if (!is_numeric($page) || !is_numeric($max_page)) return Helper::resultData(false);;
            $user = Sentinel::check();
            $offset = 10 * $page;
            $recent = $this->room->recentActiveRoom($user->id, false, $offset, 10);
            $data = [
                'user_session' => Sentinel::check(),
                'recent' => $recent,
                'show_max' => $page !== $max_page && count($recent),
                'page' => ++$page,
                'max_page' => $max_page,
            ];
            $view = Theme::view('modules.frontend.chat.chat_recent_rooms', $data)->render();
            $result = Helper::resultData(true, $view);
            return $result;
        }
    }
    
    public function group()
    {
        if ($this->isLoggedIn() == null) {
            $user = Sentinel::check();
            $status = StatusModel::getStatusById($user->id);
            $data = [
                'status' => $status,
                'username' => $user->last_name . ' ' . $user->first_name,
                'listuser' => UserModel::getAllUsers(),
                'session' => $user
            ];
            Assets::add([
                url("themes/default/asset_frontend/chat/easyrtc.min.css"),
                url("themes/default/asset_frontend/css/emoji.min.css"),
                url("themes/default/asset_frontend/css/jquery.scrollbar.min.css"),
                url("themes/default/asset_frontend/js/jquery.nanoscroller.min.js"),
                url("themes/default/asset_frontend/js/tether.min.js"),
                url("themes/default/asset_frontend/js/util.min.js"),
                url("themes/default/asset_frontend/js/classie.min.js"),
                url("themes/default/asset_frontend/js/gnmenu.min.js"),
                url("themes/default/asset_frontend/chat/easyrtc.min.js"),
                url("themes/default/asset_frontend/chat/easyrtc_ft.min.js"),
                url("themes/default/asset_frontend/chat/send_file.js"),
                url("themes/default/asset_frontend/js/jquery.scrollbar.min.js"),
            ]);
            
            return Theme::view('modules.frontend.chat.chat_addgroup', $data);
        }
        return $this->isLoggedIn();
    }
    
    public function getUserById()
    {
        if (Request::ajax() && Input::has('id')) {
            $id = Request::input('id');
            $user = UserModel::getUserById($id, ['id', 'first_name', 'last_name', 'address']);
            return Helper::json($user);
        }
    }
    
    /**
     * add friend, unfriend, block user, unblock, cancel request, cofirm request, delete request
     * @param: friend_id
     * @param: action
     * @return mixed
     */
    function addRemoveFriend()
    {
        if (Request::ajax() && Input::has('id') && Input::has('action')) {
            $friend_id = (int) Request::input('id');
            $user_id = (int) $this->user->id;
            $user_id_be = $user_id;
            $friend_id_be = $friend_id;
            $user_name = $this->user->last_name . ' ' . $this->user->first_name;
            $action = Request::input('action');
            // check relative between friend and user
            $data = [
                'user_id' => $user_id,
                'friend_id' => $friend_id,
                'action' => $action
            ];
            
            $result = FriendModel::addRemoveFriend($data);
            $room_id = Helper::uniqueRoom($friend_id, $user_id);
            /**
             * if you cancel the request and block the account, delete announcement this before
             */
            if ($result['success'] && ($action == 'cancel-request' || $action == 'block')) {
                $notice_id = $this->notice->deleteNotice($friend_id, $user_id);
                if ($action == 'block' && $notice_id == 0) {
                    $notice_id = $this->notice->deleteNotice($user_id, $friend_id);
                    $friend_id = $user_id;
                }
                if ($notice_id)
                    \LRedis::publish('addfriend', json_encode(['type' => $action, 'user_id' => $friend_id, 'notice_id' => $notice_id, 'room_id' => $room_id]));
                
            }
            if ($result['success'] && $action == 'delete-request') {
                $this->notice->updateIsConfirm($user_id, $friend_id, -1);
            }
            
            //send notice and user of room for nodejs by redis
            if ($result['success'] && ($action == 'addfriend' || $action == 'confirm')) {
                $message1 = '';
                $message2 = '';
                $type = '';
                $data = [
                    'user_id' => $friend_id,
                    'friend_id' => $user_id,
                    'is_read' => 0,
                ];
                if ($action == 'addfriend') {
                    $message2 = 'sent you a request';
                    $message1 = 'gửi cho bạn yêu cầu';
                    $type = 1;
                    $this->notice->deleteNotice($friend_id, $user_id);
                }
                
                if ($action == 'confirm') {
                    $message2 = 'accepted your friend request';
                    $message1 = 'chấp nhận lời mời kết bạn.';
                    $type = 4;
                    $this->notice->updateIsConfirm($user_id, $friend_id, 1);
                    \LRedis::publish('friend_added', json_encode(['user_id' => $user_id, 'target_id' => $friend_id]));
                }
                $data['type'] = $type;
                $notice = $this->notice->insertNotice($data, [$message1, $message2]);
                if ($notice) {
                    $data['id'] = $notice->id;
                    $data['message_vi'] = $message1;
                    $data['message_en'] = $message2;
                    $data['type'] = $notice->type;
                    $data['confirm'] = trans('fr_home.confirm');
                    $data['url'] = $this->user->url ? url('themes/default/asset_frontend/img_avatar/' . $this->user->url) : false;
                    $data['delete_request'] = trans('fr_home.delete_request');
                    $data['userName'] = $user_name;
                    if ($action == 'confirm' || $action == 'addfriend') {
                        $data['room_id'] = $room_id;
                        $status = $this->user->status()->first();
                        if ($status) {
                            $data['mood'] = $status->mood;
                            $data['status'] = $status->status;
                        }
                    }
                    \LRedis::publish('addfriend', json_encode($data));
                }
            }
            
            $roomUser = $this->roomUser->getAllUserByRoomId($room_id, ['user_id']);
            
            if ($action === 'confirm' || $action === 'addfriend') {
                $users = $this->UserModel;
                $friend = $users::getUserById($friend_id);
                if ($friend) {
                    $result['room_id'] = $room_id;
                    $result['name'] = $friend->last_name . ' ' . $friend->first_name;
                    $result['status'] = 'offline';
                    $result['mood'] = '';
                    $result['is_friend'] = false;
                    if ($action === 'confirm') {
                        $status = $friend->status()->first();
                        if ($status) {
                            $result['status'] = $status->status;
                            $result['mood'] = $status->mood;
                        }
                        $result['is_friend'] = true;
                        $result['url'] = $friend->url ? url('themes/default/asset_frontend/img_avatar/' . $friend->url) : false;
                    }
                }
            }
            /**
             *
             */
            $relative = FriendModel::getStatusRelate($user_id_be, $friend_id_be);
            if ($relative && $roomUser) {
                $relative = $relative['option'];
                $dataSocket = [
                    'room_id' => $room_id,
                    'user_id' => $roomUser,
                    'relative' => $relative,
                    'user_id_send' => $user_id
                ];
                $result['data']['option'] = $relative;
                \LRedis::publish('saveUserOfRoom', json_encode($dataSocket));
            }
            return Helper::json($result);
        }
    }
    
    /**
     * get tab messager when click to user
     *
     * @return mixed
     */
    function ajaxDisplayMessage()
    {
        if (Request::ajax() && Input::has('id') && $this->user) {
            $id = (int) Request::input('id');
            $timezone = (int) Request::input('timezone');
            $timezone = is_int($timezone) ? $timezone : 0;
            $user = $this->UserModel->getUserById($id, ['first_name', 'last_name', 'id', 'email', 'url']);
            $status = StatusModel::getStatusById($id);
            $status_relative = FriendModel::getStatusRelate($this->user->id, $id);
            $room_id = Helper::uniqueRoom($id, $this->user->id);
            $data = [
                '_id' => $room_id,
                'title' => '',
                'is_public' => 1,
                'room_admin' => '',
                'type' => 'personal'
            ];
            $this->room->insertRoom($data, [$this->user->id, $id]);
            $roomUser = $this->roomUser->getAllUserByRoomId($room_id, ['user_id']);
            if ($roomUser && $status_relative) {
                $data = [
                    'room_id' => $room_id,
                    'user_id' => $roomUser,
                    'relative' => $status_relative['option']
                ];
                \LRedis::publish('saveUserOfRoom', json_encode($data));
            }
            $message = $this->message->getMessage($room_id, [], $this->limit);
            $count = count($message);

            $output = [
                'session' => $this->user,
                'status' => $status,
                'user' => $user,
                'status_relative' => $status_relative,
                'room_id' => $room_id,
                'message' => $message,
                'timezone' => $timezone,
                'is_group' => false,
            ];
            $html_message = Theme::view('modules.frontend.chat.chat_tab', $output)->render();
            $result = [
                'id' => $id,
                'room_id' => $room_id,
                'html_message' => $html_message,
                'page' => 1
            ];
            $result = Helper::resultData(true, trans('site.register_success'), $result, $count === $this->limit);
            return Helper::json($result);
        }
        return Helper::json([], 400);
    }
    
    function scrollUpMessages()
    {
        if (Request::ajax() && Input::has('page') && Input::has('room_id')) {
            $page = request('page');
            $id = (int) Request::input('id');
            $user_id = (int) $this->user->id;
            $room_id = request('room_id');
            $created_day = 0;
            if ($id) {
                $rooms = new RoomUserModel();
                $user_ids = $rooms->getAllUserByRoomId($room_id, ['user_id']);
                $temp_arr = [];
                foreach ($user_ids as $user) {
                    $temp_arr[] = $user->user_id;
                }
                $_test_arr = array_intersect($temp_arr, [$id, $user_id]);
                if (count($_test_arr) < 2) return ['successs' => false];
            } else {
                $rooms = new RoomModel();
                $is_in_room = $rooms->getUserInRoom($user_id, $room_id);
                if (!$is_in_room) return ['successs' => false];
                $created_day = $is_in_room->created_at;
            }
            $messages = $this->message->getMessage($room_id, [], $this->limit, $page, false, $created_day);
            $count = count($messages);
            if (!$count) return ['success' => false];
            $timezone = (int) Request::input('timezone');
            $users = [];
            foreach ($messages as &$message) {
                if (isset($users[$message->user_id])) {
                    $user_join = $users[$message->user_id];
                } else {
                    $user_join = $message->joinUser()->first();
                    $users[$message->user_id] = $user_join;
                }
                $message->content = $this->messageToHTML($message, $timezone, $user_join);
                $message->format = Helper::getDatetimeChat(strtotime($message->created_at) + (-60) * $timezone);
            }
            $result = Helper::resultData(true, trans('site.register_success'), $messages, $count === $this->limit);
            return Helper::json($result);
        }
        return Helper::json([], 400);
    }
    
    function messageToHTML($message, $timezone, $user_join)
    {
        if (!$user_join) return '';
        $avatar = '';
        $style = '';
        if ($user_join->id !== $this->user->id) {
            $img = $user_join->url ? '<img class="user_avatar" width="100%"  src="' . url('themes/default/asset_frontend/img_avatar/' . $user_join->url) . '"/>' :
                '<i class="fa fa-user"></i>';
            $avatar = '<div class="avatar">' . $img . '</div>';
        } else {
            $style = 'style="float: right"';
        }
        $decrypted = Helper::decrypt($message->content);
        $mes = $message->is_file ? '<a href="' . url('themes/default/asset_frontend/img_file/' . Helper::stripVowelAccent($user_join->id . $user_join->first_name) . '/' . $decrypted) . '" download>' . $decrypted . '</a>'
            : $decrypted;
        return '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1"><div class="col-md-1 col-xs-1 col-st">
                       ' . $avatar . '</div>
        <div class="col-md-11 col-xs-11 ct-chat">
                        <div ' . $style . 'class="chat-box">
                            <div class="messages msg_sent">
                                <time datetime="2009-11-13T20:00">
                                    <span>' . $user_join->last_name . ' ' . $user_join->first_name . '</span>'
        . Helper::getHourMinutes(strtotime($message->created_at), $timezone) . '</time>
                                <div class="msg">' . $mes . '</div>
                            </div>
                        </div>
                    </div>
                </div>';
    }
    
    function ajaxDisplayMessageGroup()
    {
        if (Request::ajax() && $this->user) {
            $timezone = (int) Request::input('timezone');
            $room_id = Request::input('room_id');
            $rooms = new RoomModel();
            $is_in_room = $rooms->getUserInRoom($this->user->id, $room_id);
            if (!$is_in_room) return ['successs' => false];
            $created_day = $is_in_room->created_at;
            $roomUser = $this->roomUser->getAllUserByRoomId($room_id, ['user_id']);
            $roomInfo = $this->room->getAllInfoRoom($room_id, ['title', 'room_admin', 'url']);
            $isAdmin = $this->user->id === $roomInfo->room_admin;
            $message = $this->message->getMessage($room_id, [], $this->limit, 0, $created_day);

            $count = count($message);
            $listFriend = $this->friend->getFriendSelect($this->user->id, ['friend_id', 'status']);
            $output = [
                'is_group' => true,
                'isAdmin' => $isAdmin,
                'roomInfo' => $roomInfo,
                'room_picture' => Helper::getLinkAvatar() . $roomInfo->url,
                'room_id' => $room_id,
                'roomMember' => $roomUser,
                'session' => $this->user,
                'listFriend' => $listFriend,
                'message' => $message,
                'timezone' => $timezone,
            ];
            $html_message = Theme::view('modules.frontend.chat.chat_group', $output)->render();
            if ($roomUser) {
                $data = [
                    'room_id' => $room_id,
                    'user_id' => $roomUser,
                    'relative' => 'chat'
                ];
                \LRedis::publish('saveUserOfRoom', json_encode($data));
            }
            $result = [
                'room_id' => $room_id,
                'html_message' => $html_message,
                'page' => 1
            ];
            
            $result = Helper::resultData(true, trans('site.register_success'), $result, $count === $this->limit);
            return Helper::json($result);
        }
        return Helper::json([], 400);
    }
    
    function getFriend($user_id, $type)
    {
        $friend = $this->friend->getFriend($user_id, 'friend');
        $data = trans('fr_home.do_not_have_friend');
        if ($friend && count($friend) > 0) {
            $data = '';
            foreach ($friend as $item) {
                $user = $item->joinUser()->select(['first_name', 'last_name', 'id', 'url'])->first();
                $mood = StatusModel::getMoodById($item->friend_id);
                $data .= Theme::view('modules.frontend.chat.chat_afriend', ['friend' => $mood, 'user' => $user, 'type' => $type])->render();
            }
        }
        return $data;
    }
    
    function ajaxGetFriend()
    {
        if (Request::ajax() && $this->user) {
            $user_id = (int) $this->user->id;
            $friend = $this->friend->getFriend($user_id, 'friend');
            $type = Request::input('type');
            $data = $this->getFriend($user_id, $type);
            $result = Helper::resultData(true, '', $data);
            return Helper::json($result);
        }
        return Helper::json([], 422);
    }
    
    function ajaxAddGroup()
    {
        if (Request::ajax()) {
            $ajax = Request::all();
            $ajax['user_id'] = $this->user->id;
            $ajax['name_user'] = $this->user->last_name . ' ' . $this->user->first_name;
            $result = $this->room->addRemoveChangeTitleRoom($ajax);
            return Helper::json($result);
        }
    }
    
    function ajaxGetFriendNotice()
    {
        if (Request::ajax() && Input::has('type')) {
            $type = Request::input('type');
            $room_id = Request::input('room_id');
            $st = '';
            if ($type == 'friend' && ($friend = $this->friend->getFriend($this->user->id, 'friend')) && $room_id) {
                $userRoom = $this->friend->getUserNotInRoom($room_id);
                foreach ($friend as $i) {
                    $check = $this->friend->checkUserExistsRoom($userRoom, $i->friend_id);
                    if (!$check && ($joinUser = $i->joinUser()->select(['id', 'first_name', 'last_name'])->first())) {
                        $status = 'offline';
                        if ($joinStatus = $joinUser->status()->select('status')->first())
                            $status = $joinStatus->status;
                        $name = $joinUser->last_name . ' ' . $joinUser->first_name;
                        $st .= '<li class="media notification-success addli-togroup" data-name="' . Helper::stripVowelAccent($name) . '">' .
                            '<a href="javascript:void(0)">' .
                            '<div class="active-user-info" data-id="' . $i->friend_id . '">' .
                            '<span class="">' .
                            $name .
                            '</span>' .
                            '</div>' .
                            '<div class="icon-addfriend icon-addgroup-box" style="display:none;margin-right: 20px;">' .
                            '<i class="fa fa-user-plus" aria-hidden="true"></i>' .
                            '</div></a></li>';
                    }
                }
                
            }
            $ajax['htmlMessage'] = $st;
            $ajax['type'] = $type;
            $result = Helper::resultData(true, trans('site.delete_success'), $ajax);
            return Helper::json($result);
        }
    }
    
    function getContentMessage()
    {
        Assets::add([
            url("themes/default/asset_frontend/chat/easyrtc.min.css"),
            url("themes/default/asset_frontend/css/emoji.min.css"),
            url("themes/default/asset_frontend/css/nanoscroller.min.css"),
            url("themes/default/asset_frontend/js/jquery.nanoscroller.min.js"),
            url("themes/default/asset_frontend/js/tether.min.js"),
            url("themes/default/asset_frontend/js/config.min.js"),
            url("themes/default/asset_frontend/js/util.min.js"),
            url("themes/default/asset_frontend/js/jquery.emojiarea.min.js"),
            url("themes/default/asset_frontend/js/emoji-picker.min.js"),
            url("themes/default/asset_frontend/js/classie.min.js"),
            url("themes/default/asset_frontend/js/gnmenu.min.js"),
            url("themes/default/asset_frontend/chat/easyrtc.min.js"),
            url("themes/default/asset_frontend/chat/easyrtc_ft.min.js"),
            url("themes/default/asset_frontend/chat/send_file.js")
        ]);
        return Theme::view('modules.frontend.chat.chat_addgroup');
    }
    
    function videoPage()
    {
        Assets::add([
            url("themes/default/asset_frontend/chat/easyrtc.min.css"),
            url("themes/default/asset_frontend/chat/easyrtc.min.js"),
            url("themes/default/asset_frontend/chat/easyrtc_ft.min.js"),
            url("themes/default/asset_frontend/chat/send_file.js"),
            url("themes/default/asset_frontend/chat/prettify.min.js"),
            url("themes/default/asset_frontend/chat/loadAndFilter.min.js"),
            url("themes/default/asset_frontend/chat/landing.min.css"),
            url("themes/default/asset_frontend/chat/prettify.min.css"),
        ]);
        return Theme::view('modules.frontend.chat.video');
    }
    
    /**
     * receiver file from client after add file to server by ajax
     * @param file
     * @return mixed
     */
    function uploadFile()
    {
        $result = null;
        if (Input::hasFile('file') && $this->user) {
            $file = Input::file('file');
            if (File::size($file) < 26214400) {
                //remove accent of full name
                $folder = Helper::stripVowelAccent($this->user->id . $this->user->first_name);
                $destinationPath = '/themes/default/asset_frontend/img_file/' . $folder . '/';
                $data = Request::all();
                $room_id = $data['room_id'];
                $rooms = new RoomModel();
                // check user belongs to group
                $is_in_room = $rooms->checkUserExistsRoom($this->user->id, $room_id);
                // take all users into group to get infomation about user_id save to array
                if ($is_in_room) {
                    $room = new RoomUserModel();
                    $room = $room->getAllUserByRoomId($room_id);
                    $users = [];
                    foreach ($room as $user) {
                        $users[] = $user->user_id;
                    }
                    if ($this->user->url) {
                        $data['url'] = Helper::getDataURI(url('themes/default/asset_frontend/img_avatar/' . $this->user->url));
                    }
                    $data['fullname'] = $this->user->last_name . ' ' . $this->user->first_name;
                    // upload file on server, after send array user_id to node.js by redis
                    $result = $this->message->uploadFile($file, $destinationPath, $this->user->id, $users, $data);
                }
            }
        }
        /*return Helper::json(334,200);*/
        return Helper::json($result);
    }
    
    function ajaxCountNotifi()
    {
        if (Request::ajax() && $this->user) {
            $type = Request::input('type');
            if ($type == 'updateIsRead')
                $this->notice->updateIsRead($this->user->id);
            $count = $this->notice->countIsNotRead($this->user->id);
            $message = $this->message->countIsRead($this->user->id);
            return Helper::json(['notice' => $count, 'message' => $message]);
        }
    }
    
    /**
     * display all messages of myselft
     * @return mixed
     */
    function ajaxLoadAllNotification()
    {
        if (Request::ajax() && $this->user) {
            $user_id = $this->user->id;
            $lang = \LaravelLocalization::getCurrentLocale();
            $notice = $this->notice->getAllNotifi($user_id, $lang);
            if ($notice)
                //save name of rom, url of room after data transmission to client
                foreach ($notice as $value) {
                    $room_id = $value->room_id;
                    if ($room_id != '0') {
                        $room = $this->room->getRoomInfo($value->room_id);
                        if($room){
                            $value->roomName = $room->title;
                            $value->url = $room->url; 
                        }   
                    }
                }
            
            return Helper::json($notice);
        }
    }
    
    function updateIsClick()
    {
        if (Request::ajax()) {
            $notice_id = Request::input('notice_id');
            $notice = $this->notice->updateIsClick($notice_id);
            return Helper::json($notice);
        }
        die();
    }
    
    function updateIsReadMessage()
    {
        if (Request::ajax() && $this->user) {
            $room_id = Request::input('room_id');
            $user_id = $this->user->id;
            $message = $this->message->updateIsRead($user_id, $room_id);
            return Helper::json($message);
        }
        die();
    }
    
    function updateGroupPicture()
    {
        if (Request::ajax() && Request::hasFile('data')) {
            $user_id = Sentinel::check()->id;
            $room_id = request('room_id');
            $is_admin = $this->room->checkIsAdminRoom($user_id, $room_id);
            if (!$is_admin) return;
            $file = Request::file('data');
            if (!exif_imagetype($file)) {
                return Helper::errors('invalid_request', 'The file you uploaded is not an image.', 422);
            }
            $result = Helper::resultData(false, trans('site.update_fail'));
            if (filesize($file) > 1048576) {
                $result = Helper::resultData(false, trans('site.size_large'));
                return Helper::json($result);
            }
            $arr_name = explode('.', $file->getClientOriginalname());
            $extension = end($arr_name);
            $file_name = rand(1, 100) . $room_id . rand(100, 10000) . '.' . $extension;
            $data = [
                'file_name' => $file_name,
                'user_id' => $user_id,
                'room_id' => $room_id
            ];
            $update_data = $this->room->updateRoomPicture($file, $data);
            if ($update_data) {
                $result = Helper::resultData(true, trans('site.update_success'), ['url' => $file_name]);
                $room_users = $this->room->getRoomUsers($room_id);
                $url = Helper::getLinkAvatar() . $file_name;
                \LRedis::publish('updateAvatar', json_encode(['url' => $url, 'user_id' => $user_id, 'room_id' => $room_id, 'target_users' => $room_users]));
            }
            return Helper::json($result);
        }
        exit;
    }
    
    /**
     * get information of friend
     * @param friend_id
     * @return mixed
     */
    function getInfoUser()
    {
        if (Request::ajax()) {
            $friend_id = Request::input('friend_id');
            $user = $this->UserModel->getUserById($friend_id, ['first_name', 'last_name', 'id', 'email', 'url']);
            $status = StatusModel::getStatusById($friend_id);
            $user_attribute = $this->userAttribute->getAtributePublic($friend_id, ['user_id', 'attribute_id', 'values', 'id']);
            $data = [
                'user' => $user,
                'status' => $status,
                'user_attribute' => $user_attribute,
            ];
            $result = Theme::view('modules.frontend.chat.chat_left_info', $data)->render();
            return Helper::json($result);
        }
    }
    
    function callvideo($receiver_id,$room_id){
        if (Sentinel::check()) {
            if($this->room->checkUserExistsRoom((int)$receiver_id, $room_id,'personal') && $this->room->checkTypeRoom($room_id)){
                Assets::reset()->addCss([
                    Helper::getThemeJs('socket.io.min.js'),
                    url("themes/default/asset_frontend/css/font-awesome.min.css"),
                    url("themes/default/asset_frontend/css/bootstrap.min.css"),
                    url("themes/default/asset_frontend/css/nanoscroller.min.css"),
                    url("themes/default/asset_frontend/css/styleVideo.css"),
                ]);
                 Assets::addJs([
                    Helper::getThemeJs('socket.io.min.js'),
                    url("themes/default/asset_frontend/js/jquery.min.js"),
                    url("themes/default/asset_frontend/js/bootstrap.min.js"),
                    url("themes/default/asset_frontend/chat/easyrtc.min.js"),
                    url("themes/default/asset_frontend/chat/easyrtc_ft.min.js"),
                    url("themes/default/asset_frontend/js/jquery.nanoscroller.min.js"),
                    url("themes/default/asset_frontend/chat/send_file.js"),
                    url("themes/default/asset_frontend/chat/send_message.min.js").'?v='.Helper::version(),
                ]);
                $receiver = $this->UserModel->getUserById((int)$receiver_id, ['first_name', 'last_name', 'id', 'email', 'url']);
                $data=[
                    'receiver' => $receiver,
                    'room_id' => $room_id
                ];
                $user_id = $this->user->id;
                $flag = $this->message->checkFlagCall($user_id,$receiver_id, $room_id);
                if(!$flag){
                    $redis = [
                        'is_file' => 3,
                        'room_id' => $room_id,
                        'user_id' => $this->user->id,
                        'content' => 'call',
                        'flagcall' => 1,
                        'room_users' => array($user_id, $receiver_id)
                    ];
                    \LRedis::publish('saveFile', json_encode($redis));
                }
                return Theme::view('modules.frontend.chat.video', $data);
            }
            die();
        }
        return $this->isLoggedIn();
    }
    
    function create_recent(){
        if (Request::ajax()) {
            $friend_id = (int)Request::input('user_id');
            $user_id = (int) $this->user->id;
            $room_id = Helper::uniqueRoom($friend_id, $user_id);
            $friend = $this->UserModel->getUserById($friend_id, ['first_name', 'last_name']);
            $data = [
                'friend_id' => $friend_id,
                'room_id' => $room_id,
                'status' => 'offline'
            ];
            if($friend)
                $data['name'] = $friend->last_name.' '.$friend->first_name;
            return Helper::json($data);
        }
    }
    
}