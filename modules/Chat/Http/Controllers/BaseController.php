<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Chat\Http\Controllers;

use App\Helpers\JWT;
use Illuminate\Routing\Controller;
use Modules\Admin\Models\SystemModel;
use Helper, Theme, SEOMeta, Assets, Sentinel, Activation, LRedis;

class BaseController extends Controller
{
    public $config;
    
    public function __construct()
    {
        // load store config to use
        $this->setConfig();
        $this->assets();
        if ($session = Sentinel::check()) {
            $user = [
                'user_id' => $session->id,
                'first_name' => $session->first_name,
                'last_name' => $session->last_name,
                'email' => $session->email,
                'salt' => uniqid('Vitzoo', true),
                'time' => date('D M d Y H:i:s O')
            ];
            if($session->url) {
                $user['url'] = url('themes/default/asset_frontend/img_avatar/' . $session->url);
            }
            $this->create_jwttoken($user);
            \LRedis::publish('user', json_encode($user));
        }
    }
    
    /**
     * get current config
     */
    private function setConfig()
    {
        // load default config to use
        if (empty($this->config)) {
            $systemModel = new SystemModel;
            $this->config = $systemModel->getConfig();
        }
    }
    
    private function assets()
    {
        //Global theme
        // set assets
        Assets::add([
            // add css
            url("themes/default/asset_frontend/css_ajax/gooey-dots.min.css"),
            url("themes/default/asset_frontend/plugins/icheck/skins/minimal/red.min.css"),
            url("themes/default/asset_frontend/css/font-awesome.min.css"),
            url("themes/default/asset_frontend/css/bootstrap.min.css"),
            url("themes/default/asset_frontend/css/nanoscroller.min.css"),
            url("themes/default/asset_frontend/css/style.css"),
            url("themes/default/asset_frontend/plugins/messi/messi.min.css"),
            url("themes/default/asset_frontend/css/'jquery.scrollbar.min.css'"),
            //add js
            url("themes/default/asset_frontend/js/jquery.min.js"),
            url("themes/default/asset_frontend/chat/jquery.form.min.js"),
            url("themes/default/asset_frontend/js/jquery.validate.min.js"),
            url("themes/default/asset_frontend/js/bootstrap.min.js"),
            url("themes/default/asset_frontend/js/webfont.min.js"),
            url("themes/default/asset_frontend/js/vz.min.js"),
            url("themes/default/asset_frontend/plugins/icheck/icheck.min.js"),
            url("themes/default/asset_frontend/plugins/messi/messi.min.js"),
            url("themes/default/asset_frontend/js/'jquery.scrollbar.min.js'"),
        ]);
        if (Sentinel::check()) {
            Assets::addJs([
                Helper::getThemeJs('socket.io.min.js'),
                url("themes/default/asset_frontend/chat/send_message.min.js").'?v='.Helper::version(),
                url("themes/default/asset_frontend/chat/send_file.js"),
            ]);
        }
        if (\LaravelLocalization::getCurrentLocale() == 'vi') {
            Assets::add(url("themes/default/asset_frontend/js/messages_vi.min.js"));
        }
    }
    
    public function isLoggedIn()
    {
        $user = Sentinel::check();
        if (!$user) {
            $result = Helper::resultData(false, trans('login.login_before_chat'), null, 1);
            Helper::setSessionMessage($result);
            return redirect(Helper::url(''));
        }
        /*if(!Activation::completed($user)){
            $result = Helper::resultData(false, trans('login.please_active_mail'),null,1);
            Helper::setSessionMessage($result);
            return redirect(Helper::url(''));
        }*/
        return null;
    }
    
    function create_jwttoken($ar)
    {
        session()->set('jwt_token', JWT::encode($ar, Helper::getEncodeKey()));
    }
    
    
}