<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group([
	'prefix' => LaravelLocalization::setLocale() . '/chat',
	'middlewareGroups' => ['localeSessionRedirect', 'localizationRedirect', 'web', 'csrf']
], function () {
	Route::get('/', 'ChatController@index');
    Route::get('/room/{user_id}/{id}', 'ChatController@index');
    Route::get('/room/{id}', 'ChatController@index');
	Route::post('getUserById', 'ChatController@getUserById');
	Route::post('sendInvitation', 'ChatController@sendInvitation');
	Route::get('getMessage', 'ChatController@ajaxDisplayMessage');
	Route::post('addRemoveFriend', 'ChatController@addRemoveFriend');
	Route::post('uploadFile', 'ChatController@uploadFile');
    Route::get('createRoom', 'ChatController@createRoom');
    Route::get('group', 'ChatController@group');
    Route::get('getMessageRoom', 'ChatController@ajaxDisplayMessageGroup');
    Route::post('getFriend', 'ChatController@ajaxGetFriend');
    Route::post('addgroup', 'ChatController@ajaxAddGroup');
    Route::post('getFriendNotice', 'ChatController@ajaxGetFriendNotice');
    Route::post('loadNotification', 'ChatController@ajaxLoadAllNotification');
    Route::post('countNotifi', 'ChatController@ajaxCountNotifi');
    Route::post('updateIsClick', 'ChatController@updateIsClick');
    Route::post('olderMessages', 'ChatController@scrollUpMessages');
    Route::post('updateIsRead', 'ChatController@updateIsReadMessage');
    Route::post('group_picture', 'ChatController@updateGroupPicture');
    Route::post('searchUser', 'ElasticSearchController@index');
    Route::post('getInfoUser', 'ChatController@getInfoUser');
    Route::get('deleteOrCreate/{type}', 'ElasticSearchController@deleteOrCreate');
    Route::get('video/{receiver_id}/{room_id}', 'ChatController@callvideo');
    Route::get('removepersonalroom', 'ChatController@removePersonalRoom');
    Route::get('recent', 'ChatController@create_recent');
    Route::post('recent_rooms', 'ChatController@moreRecent');
});
