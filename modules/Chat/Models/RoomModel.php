<?php
    namespace Modules\Chat\Models;
    
    use Illuminate\Support\Facades\Validator;
    use Helper;
    use Modules\Chat\Entities\NoticeEntity;
    use Modules\Chat\Entities\RoomEntity;
    use Modules\Chat\Entities\RoomUserEntity;
    
    class RoomModel
    {
        function insertRoom($data, $user_id = [])
        {
            if (empty($data)) {
                // return error message
                return false;
            }
            $room = new RoomEntity();
            if ($data['_id'] != '0' && !$result = RoomEntity::find($data['_id'])) {
                $room = Helper::mapdata($room, $data);
                $room->save();
                $room_id = $room->id;
                if ($room_id && $user_id) {
                    $roomUser = new RoomUserModel();
                    foreach ($user_id as $i) {
                        $roomUser->insertRoom(['room_id' => $room_id, 'user_id' => $i]);
                    }
                }
                return $room;
            }
            return false;
        }
        
        /* Get recently active rooms
            @param {int} $user_id: user id
            @param {Bool} $get_all : get total number of rooms if true
            @param {int} offset : how many
         */
        function recentActiveRoom($user_id, $get_all = false, $offset = 0, $limit = 20)
        {
            $roomUser = RoomUserEntity::where('user_id', $user_id);
            if ($get_all) {
                return $roomUser->count();
            }
            return $roomUser->orderBy('new_message_date', 'DESC')->orderBy('created_at', 'DESC')->skip($offset)->take($limit)->get();
        }
        
        function addRemoveChangeTitleRoom($data)
        {
            try {
                if (empty($data) || !isset($data['action']) || !isset($data['user_id']) || !isset($data['room_id'])) {
                    // return error message
                    return false;
                }
                
                $room = new RoomEntity();
                $action = $data['action'];
                $user_id = (int) $data['user_id'];
                $room_id = $data['room_id'];
                $dataUser = [
                    'room_id' => $room_id,
                    'user_id' => $user_id
                ];
                $roomUser = new RoomUserModel();
                
                if (isset($data['title']))
                    $title = $data['title'];
                
                if ($action == 'changeTitle' && isset($title) && $this->checkIsAdminRoom($user_id, $room_id)) {
                    $room = RoomEntity::find($room_id);
                    $room->title = $title;
                    $room->save();
                    return Helper::resultData(true, trans('site.change_success'), $data);
                }
                if ($action == 'friendAndGroup' && !RoomEntity::find($room_id)) {
                    if (!isset($title))
                        $title = 'no title';
                    $room_id = Helper::uniqueRoomGroup();
                    $room->_id = $room_id;
                    $room->title = $title;
                    $room->is_public = 1;
                    $room->room_admin = $user_id;
                    $room->type = 'room';
                    $room->url = '';
                    $room->save();
                    $dataUser['room_id'] = $room_id;
                    $data['room_id'] = $room_id;
                    $roomUser->insertRoom($dataUser);
                }
                
                if (isset($data['friend_id']) && $data['friend_id'] != 0) {
                    $friend_id = (int) $data['friend_id'];
                    if ($action == 'deleteUser' && $this->checkIsAdminRoom($user_id, $room_id)) {
                        $check = $roomUser->deleteUserRoom(['room_id' => $room_id, 'user_id' => $friend_id]);
                        $notice = new NoticeModel();
                        $notice->deleteNotice($friend_id, $user_id,'addgroup',$room_id);
                        $data['checkDelete'] = $check;
                        return Helper::resultData(true, trans('site.delete_success'), $data);
                    }
                    
                    if (!$this->checkUserExistsRoom($friend_id, $room_id)) {
                        $dataUser['user_id'] = $friend_id;
                        $check = $roomUser->insertRoom($dataUser);
                        $data['checkAddUser'] = $check;
                        if ($check) {
                            $dataNotice = [
                                'user_id'   => $friend_id,
                                'friend_id' => $user_id,
                                'is_read'   => 0,
                                'room_id'   => $room_id,
                                'type'      => 3
                            ];
                            $message2 = 'added you to a group ';
                            $message1 = 'đã mời bạn vào nhóm ';
                            $notice = new NoticeModel();
                            $notice = $notice->insertNotice($dataNotice, [$message1, $message2]);
                            $roomInfo = $this->getRoomInfo($room_id);
                            if ($roomInfo) {
                                $dataNotice['roomName'] = $roomInfo->title;
                                $dataNotice['url'] = $roomInfo->url;
                                $data['title'] = $roomInfo->title;
                            }
                            
                            $dataNotice['message_vi'] = $message1;
                            $dataNotice['message_en'] = $message2;
                            $dataNotice['id'] = $notice->_id;
                            $dataNotice['userName'] = $data['name_user'];
                            \LRedis::publish('addfriend', json_encode($dataNotice));
                        }
                    }
                }
                return Helper::resultData(true, trans('site.change_success'), $data);
                
            } catch (\Exception $e) {
                return Helper::resultData(false, $e);
            }
        }
        
        function getUserInRoom($id, $room_id)
        {
            return RoomUserEntity::where('user_id', $id)->where('room_id', $room_id)->first();
        }
        
        function getRoomUsers($room_id)
        {
            return RoomUserEntity::where('room_id', $room_id)->select('user_id')->get();
        }
        
        function checkUserExistsRoom($id, $room_id)
        {
            return RoomUserEntity::where('user_id', $id)->where('room_id', $room_id)->exists();
        }
        
        function checkIsAdminRoom($id, $room_id)
        {
            return RoomEntity::where('room_admin', '=', $id)->where('_id', $room_id)->exists();
        }
        
        function checkTypeRoom($room_id,$type='personal'){
            return RoomEntity::where('_id', '=', $room_id)->where('type','=',$type)->exists();
        }
        
        function getAllRoomUserContainer($user_id)
        {
            return RoomUserEntity::where('user_id',(int)$user_id)->select(['room_id'])->get();
        }
        
        function getAllInfoRoom($room_id, $field = [])
        {
            if (!$room_id)
                return false;
            $room = RoomEntity::where('_id', '=', $room_id);
            if ($field)
                $room->select($field);
            return $room->first();
        }
        
        function getRoomInfo($room_id)
        {
            return RoomEntity::find($room_id);
        }
        
        function getAllRoom($fields = [])
        {
            $room = new RoomEntity();
            if (count($fields)) {
                foreach ($fields as $field => $query) {
                    $room = $room->where($field, $query);
                }
            }
            return $room;
        }
        
        function updateRoomInfo($room_id, $data, $changeAvatar = false, $folder = '')
        {
            if ($changeAvatar) {
                $room = RoomEntity::where('_id', $room_id)->first();
                $room->url && file_exists($folder . $room->url) && unlink($folder . $room->url);
                $room = Helper::mapData($room, $data);
                return $room->save();
            }
            return RoomEntity::where('_id', $room_id)->update($data, ['upsert' => true]);
        }
        
        function updateRoomPicture($file, $data)
        {
            $img = \Image::make($file->getRealPath());
            $height = Helper::getWidthHeighAvatar();
            $width = Helper::getWidthHeighAvatar();
            if ($img->width() > $width)
                $img->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            if ($img->height() > $height)
                $img->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            $destinationPath = public_path() . Helper::getLinkAvatar();
            $img->save($destinationPath . $data['file_name']);
            $update_data = ['url' => $data['file_name']];
            return $this->updateRoomInfo($data['room_id'], $update_data, true, $destinationPath);
        }
    }