<?php
namespace Modules\Chat\Models;

use Illuminate\Support\Facades\Validator;
use Modules\Chat\Entities\NoticeEntity;
use Helper,DB;
use Modules\Chat\Entities\NoticeLanguageEntity;

class NoticeModel
{
    function insertNotice($data,$message = []){
        if (empty($data)) {
            // return error message
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }
        $notice = new NoticeEntity();
        $notice = Helper::mapdata($notice,$data);
        $check = $notice->save();
        if($check && $notice && $message){
            //todo: check insert success
            $data_language = [];
            $data_language['notice_id'] = $notice->id;
            $data_language['message'] = $message[0];
            $data_language['language_code'] = 'vi';
            $this->insertNoticeLanguage($data_language);
            $data_language['message'] = $message[1];
            $data_language['language_code'] = 'en';
            $this->insertNoticeLanguage($data_language);
        }
        return $notice;
    }

    function insertNoticeLanguage($data){
        if(!$data)
            return false;
        $noticeLanguage = new NoticeLanguageEntity();
        $noticeLanguage->notice_id = $data['notice_id'];
        $noticeLanguage->message = $data['message'];
        $noticeLanguage->language_code = $data['language_code'];
        $noticeLanguage->save();
        return $noticeLanguage;
    }

    function getAllNotifi($user_id, $lang,$field = []){
        if(!$user_id)
            return false;
        /*$notice = NoticeEntity::where('user_id',$user_id);
        if($field)
            $notice = $notice->select($field);
        return $notice->orderBy('created_at', 'desc')->get();*/
        $notice = DB::table('chat__notices_language as no2')
            ->join('chat__notices as no1', 'no1.id', '=', 'no2.notice_id')
            ->join('user__users as us', 'us.id', '=', 'no1.friend_id')
            ->where('no2.language_code',$lang)
            ->where('no1.user_id',$user_id)
            ->select('no1.*','no2.message','us.url','us.first_name','us.last_name')
            ->offset(0)
            ->limit(100)
            ->orderBy('created_at', 'desc')
            ->get();
        return $notice;
    }

    function countIsNotRead($user_id){
        return NoticeEntity::where('user_id','=',$user_id)->where('is_read','=',0)->count();
    }

    function updateIsRead($user_id){
        $notice = NoticeEntity::where('user_id','=',$user_id)->where('is_read', '=', 0)->update(['is_read' => 1]);
        return $notice;
    }

    function updateIsClick($notice_id){
        if(!$notice_id)
            return false;
        return NoticeEntity::find((int)$notice_id)->update(['is_click' => 1]);
    }

    function updateIsConfirm($user_id,$friend_id,$value)
    {
        if (!$user_id || !$friend_id)
            return false;
        return NoticeEntity::where('user_id','=',(int)$user_id)->where('friend_id','=',(int)$friend_id)->where('type','=','addfriend')->update(['is_confirm' => $value]);
    }

    function deleteNotice($user_id,$friend_id,$type = 'addfriend',$room_id = 0){
        if (!$user_id || !$friend_id)
            return false;
        $notice = NoticeEntity::where('user_id','=',$user_id)->where('friend_id','=',$friend_id)->where('type','=',$type);
        if($room_id != 0)
            $notice = $notice->where('room_id','=',$room_id);
        $notice = $notice->get();
        $notice_id = 0;
        if($notice)
            foreach ($notice as $item){
                $this->deleteNoticeLanguage($item->id);
                $item->delete();
                $notice_id = $item->id;
            }
        return $notice_id;
    }

    function deleteNoticeLanguage($notice_id){
        if (!$notice_id)
            return false;
        return NoticeLanguageEntity::where('notice_id',(int)$notice_id)->delete();
    }

}