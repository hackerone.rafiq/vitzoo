<?php
namespace Modules\Chat\Models;

use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Validator;
use Modules\Chat\Entities\MessageEntity;
use Helper;
use Modules\Chat\Entities\MessageStatusEntity;

class MessageModel
{
    function dateTimeDistinct($room_id, $limit = 1, $offset = 0, $first = false)
    {
        if (!$room_id)
            return false;
        if ($first) $limit = $first;
        $skip = $offset * $limit;
        return MessageEntity::where('room_id', $room_id)->select('date')->orderBy('created_at', 'DESC')->skip($skip)->take($limit)->groupBy('date')->get();
    }
    
    function getMessage($room_id, $field = [], $limit = 1, $offset = 0, $fromDate = 0, $fromDateapi = 0)
    {
        if (!$room_id)
            return false;
        $skip = $offset * $limit;
        $message = MessageEntity::where('room_id', $room_id);
        if ($field)
            $message = $message->select($field);
        if ($fromDate) {
            $message = $message->where('created_at', '>=', $fromDate);
        }
        if($fromDateapi)
            $message = $message->where('created_at', '>=', $fromDateapi);
        return $message->orderBy('created_at', 'DESC')->skip($skip)->take($limit)->get();
    }
    
    function giveANameFile($file, $destinationPath)
    {
        $i = 1;
        $filename = pathinfo($file, PATHINFO_FILENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        while (true) {
            if (file_exists($destinationPath . $file)) {
                $file = $filename . '(' . $i . ').' . $extension;
                $i += 1;
            } else {
                break;
            }
        }
        return $file;
    }
    
    function uploadFile($file, $des, $user_id, $users, $data)
    {
        if ($file && $des && $user_id) {
            $destinationPath = public_path() . $des;
            //$file_name = $this->giveANameFile($file->getClientOriginalName(), $destinationPath);
            $file_name = $file->getClientOriginalName();
            $file_name = str_replace(' ', '_', Helper::stripVowelAccent($file_name));
            $file->move($destinationPath, $file_name);
            $data['user_id'] = $user_id;
            unset($data['file']);
            $data['content'] = $file_name;
            $data['room_users'] = $users;
            $data['destinationPath'] = url($des . $file_name);
            $data['is_file'] = 1;
            \LRedis::publish('saveFile', json_encode($data));
            return Helper::resultData(true, trans('site.upload_success'), $data);
        }
        return Helper::resultData(false, trans('site.upload_error'));
    }

    function updateIsRead($user_id,$room_id){
        if(!$user_id || !$room_id)
            return false;
        $messageStatus = MessageStatusEntity::where('user_id','=',$user_id)->where('room_id','=',$room_id)->first();
        if($messageStatus){
            $messageStatus->is_read = 0;
            $messageStatus->message_id = 0;
            $messageStatus->save();
        }
        return $messageStatus;
    }

    function countIsRead($user_id){
        if(!$user_id)
            return false;
        return MessageStatusEntity::where('user_id','=',$user_id)->where('is_read','>',0)->count();
    }
    
    function checkFlagCall($user_id,$receiver_id,$room_id,$flagCall = 1){
        return MessageEntity::where('room_id','=',$room_id)->where(function ($query) use ($user_id,$receiver_id) {
            $query->where('user_id', '=', $user_id)
                ->orWhere('user_id', '=', (int)$receiver_id);
        })->where('flagcall','=',$flagCall)->exists();
        /*return MessageEntity::where('room_id', '=', $room_id)->where('user_id', '=', (int)$receiver_id)->where('flagcall', '=', $flagCall)->exists();*/
    }
}