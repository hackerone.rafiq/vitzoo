<?php
namespace Modules\Chat\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Chat\Entities\FriendEntity;
use Helper;
use Modules\Chat\Entities\RoomUserEntity;
use Modules\Chat\Entities\SearchEntity;
use Modules\User\Entities\UserEntity;
use Searchy;

/**
 * Class ElasticSearchModel
 * class
 * @package Modules\Chat\Models
 */
class ElasticSearchModel
{
    /**
     * search user in table either empty user then return all record in table or user can be find
     * @param $name
     * type string, name is a string inlcude first_name, last_name or email
     * @return array user of user__users table
     */
    function searchUser($name, $fields = [])
    {
        /*$name = e($name);*/
        $name = Helper::stripVowelAccent($name);
        $user = UserEntity::where(DB::raw('concat(last_name," ",first_name)'), 'LIKE', '%'.$name.'%');
        if (!empty($fields)) {
            $user->select($fields);
        }
        return $user->limit(50)->get();
    }
    
    /**
     * create index all user has into table, function only call the first time
     * @return the user array of database
     */
    function createIndex()
    {
        $user = UserEntity::get();
        if ($user) {
            foreach ($user as $item) {
                $item->document()->save();
            }
        }
        return true;
        /*$result = UserEntity::search()->get();*/
        /*return $result->hits()->toArray();*/
    }
    
    /**
     * @param $id , primary key of table users, type: int
     * @return bool
     */
    function deleteIndex($id)
    {
        if (!$id)
            return false;
        return UserEntity::find($id)->document()->delete();
    }
    
    /**
     * @param $id , primary key of table, type: int
     * @return bool
     */
    function updateIndex($id)
    {
        if (!$id)
            return false;
        return UserEntity::find($id)->document()->update();
    }
    
    /**
     * @return the user array of database
     */
    function deleteAllIndex()
    {
        $user = SearchEntity::get();
        if ($user) {
            foreach ($user as $item) {
                $item->document()->delete();
            }
        }
        $result = UserEntity::search()->get();
        return $result->hits()->toArray();
    }
}