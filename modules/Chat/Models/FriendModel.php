<?php
namespace Modules\Chat\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Chat\Entities\FriendEntity;
use Helper;
use Modules\Chat\Entities\RoomUserEntity;

class FriendModel
{
    /**
     * update status for one user
     * @param $data
     * @return bool
     */
    static function update_status_friend($data)
    {
        if (empty($data)) {
            return false;
        }
        $friend = FriendEntity::where('user_id', $data['user_id'])->where('friend_id', $data['friend_id'])->first();
        $friend->status = $data['status'];
        $result = $friend->save();
        return $result;
    }

    /**
     * create a new friend
     * @param $data
     * @return bool
     */
    static function createFriend($data)
    {
        if (empty($data)) {
            return false;
        }
        $friend = new FriendEntity();
        $result = $friend->insert($data);
        return $result;
    }
    
    /**
     * @param $id  (myselft)
     * @param $status (Value default is friend)
     * @return bool
     */
    function getFriend($id, $status, $get_all = false, $offset = 0, $limit = 20,$getlist = true)
    {
        if (empty($id)) {
            // return error message
            return false;
        }
        $friend = FriendEntity::where('user_id', $id);
        if (is_array($status)) {
            $friend = $friend->whereIn('status', $status);
        } else {
            $friend = $friend->where('status', $status);
        }
        
        if ($get_all) {
            return $friend->count();
        }
        if($getlist)
            return $friend->get();
        return $friend->skip($offset)->take($limit)->get();
    }

    /**
     * get status relative between user and friend
     * @param $user_id
     * @param $friend_id
     * @return array|bool
     */
    static function getStatusRelate($user_id, $friend_id)
    {
        
        if (empty($user_id) || empty($friend_id)) {
            // return error message
            return false;
        }
        $relate_me = FriendEntity::where('user_id',(int) $user_id)->where('friend_id', (int)$friend_id)->select('status')->first();
        $relate_friend = FriendEntity::where('user_id',(int) $friend_id)->where('friend_id', (int)$user_id)->select('status')->first();
        $result = [];
        if (!$relate_me && !$relate_friend) {
            $result['status'] = 'stranger';
            $result['option'] = 'chat';
            return $result;
        }
        switch (true) {
            case ($relate_me->status == 'waiting' && $relate_friend->status == 'stranger'):
                $result['status'] = 'waiting';
                $result['option'] = 'chat';
                break;
            case ($relate_me->status == 'stranger' && $relate_friend->status == 'waiting'):
                $result['status'] = 'confirm';
                $result['option'] = 'chat';
                break;
            case ($relate_me->status == 'friend'):
                $result['status'] = 'friend';
                $result['option'] = 'chat';
                break;
            case ($relate_me->status == 'blocked'):
                $result['status'] = 'blocked';
                $result['option'] = 'nonechat';
                break;
            case ($relate_me->status == 'unfriended' && $relate_friend->status == 'blocked'):
                $result['status'] = 'unfriended_blocked';
                $result['option'] = 'nonechat';
                break;
            case ($relate_me->status == 'unfriended' && $relate_friend->status == 'unfriended'):
                $result['status'] = 'unfriended';
                $result['option'] = 'chat';
                break;
            default:
                $result['status'] = 'nothing';
                $result['option'] = 'nothing';
                break;
        }
        return $result;
    }

    /**
     * update status for two user to use when change status for friend
     * @param $data
     * @param $other
     * @return mixed
     */
    static function updateStatusTwoUser($data, $other)
    {
        if (!$data && !$other)
            return Helper::resultData(false, trans('site.data_empty'));
        $array1 = [
            'user_id' => $data['user_id'],
            'friend_id' => $data['friend_id'],
            'status' => $data['status_me']
        ];
        $array2 = [
            'user_id' => $data['friend_id'],
            'friend_id' => $data['user_id'],
            'status' => $data['status_friend']
        ];
        if (self::update_status_friend($array1) && self::update_status_friend($array2)) {
            $array1['status_friend'] = $other['status_friend'];
            $array1['status_me'] = $other['status_me'];
            unset($array1['status']);
            return Helper::resultData(true, $other['message_success'], $array1);
        }
        return Helper::resultData(false, $other['message_error']);
    }

    /**
     * function: add friend, unfriend, block user, unblock, cancel request, cofirm request, delete request
     * @param $data
     * @return mixed
     */
    static function addRemoveFriend($data)
    {
        if (!$data)
            return Helper::resultData(false, trans('site.data_empty'));
        $user_id = $data['user_id'];
        $friend_id = $data['friend_id'];
        $action = $data['action'];
        $relative = self::getStatusRelate($user_id, $friend_id);

        if ($relative)
            $relative = $relative['status'];
        $array = [
            'friend_id' => $friend_id,
            'user_id' => $user_id
        ];

        if ($action == 'addfriend') {
            if ($relative == 'stranger') {
                $mytime = Carbon::now();
                $array = [
                    [
                        'user_id' => $user_id,
                        'friend_id' => $friend_id,
                        'status' => 'waiting',
                        'created_at' => $mytime->toDateTimeString(),
                        'updated_at' => $mytime->toDateTimeString()
                    ],
                    [
                        'user_id' => $friend_id,
                        'friend_id' => $user_id,
                        'status' => 'stranger',
                        'created_at' => $mytime->toDateTimeString(),
                        'updated_at' => $mytime->toDateTimeString()
                    ],
                ];

                $result = self::createFriend($array);
                if ($result)
                    return Helper::resultData(true, trans('site.addfriend_success'), ['user_id' => $user_id, 'friend_id' => $friend_id, 'status_me' => 'waiting', 'status_friend' => 'confirm']);
                return Helper::resultData(false, trans('site.error_addffriend'));
            }
            if ($relative == 'unfriended') {
                $array['status_me'] = 'waiting';
                $array['status_friend'] = 'stranger';
                $other = [
                    'message_success' => trans('site.addfriend_success'),
                    'message_error' => trans('site.addfriend_error'),
                    'status_friend' => 'confirm',
                    'status_me' => 'waiting'
                ];
                return self::updateStatusTwoUser($array, $other);
            }
            if ($relative == 'unfriended_blocked') {
                return Helper::resultData(false, trans('site.cannot_send_request_people'));
            }
        }
        if ($action == 'confirm' && $relative == 'confirm') {
            $array['status_me'] = 'friend';
            $array['status_friend'] = 'friend';
            $other = [
                'message_success' => trans('site.you_cofirm_request'),
                'message_error' => trans('site.you_cofirm_request_error'),
                'status_friend' => 'friend',
                'status_me' => 'friend'
            ];
            return self::updateStatusTwoUser($array, $other);
        }
        if ($action == 'cancel-request' || $action == 'delete-request' || $action == 'unfriend')
            if ($relative == 'waiting' || $relative == 'confirm' || $relative == 'friend') {
                $array['status_me'] = 'unfriended';
                $array['status_friend'] = 'unfriended';
                $other = [
                    'message_success' => trans('site.action_success'),
                    'message_error' => trans('site.action_error'),
                    'status_friend' => 'unfriended',
                    'status_me' => 'unfriended'
                ];
                return self::updateStatusTwoUser($array, $other);
            }
        if ($action == 'block'){
            if ($relative == 'friend' || $relative == 'waiting' || $relative == 'confirm' || $relative == 'unfriended') {

                $array['status_me'] = 'blocked';
                $array['status_friend'] = 'unfriended';
                $other = [
                    'message_success' => trans('site.you_blocked'),
                    'message_error' => trans('site.you_blocked_error'),
                    'status_friend' => 'unfriended_blocked',
                    'status_me' => 'blocked'
                ];
                return self::updateStatusTwoUser($array, $other);
            }
            if($relative == 'unfriended_blocked'){
                $array['status'] = 'blocked';
                if (self::update_status_friend($array))
                    return Helper::resultData(true, trans('site.block_success'), ['user_id' => $user_id, 'friend_id' => $friend_id, 'status_me' => 'blocked']);
                return Helper::resultData(false, trans('site.block_error'));
            }
        }
        if ($action == 'unblock' && $relative == 'blocked') {
            $array['status'] = 'unfriended';
            if(self::update_status_friend($array))
                return Helper::resultData(true, trans('site.unblock_success'), ['user_id' => $user_id, 'friend_id' => $friend_id, 'status_me' => 'unfriended']);
            return Helper::resultData(false, trans('site.unblock_error'));
        }
        return Helper::resultData(false, trans('site.error_system'));
    }

    function getUserNotInRoom($room_id)
    {
        return RoomUserEntity::where('room_id', '=', $room_id)->get(['user_id']);
    }
    
    /**
     * check user_id exists in array
     * @param $array (containing user information)
     * @param $user_id
     * @return bool
     */
    function checkUserExistsRoom($array,$user_id){
        if($array && $user_id){
            foreach ($array as $item){
                if($item['user_id'] == $user_id)
                    return true;
            }
        }
        return false;
    }

    function getFriendSelect($id,$field = []){
        if (empty($id)) {
            // return error message
            return false;
        }
        $result = FriendEntity::where('user_id', $id)->Where(function ($query) {
            $query->where('status','=', 'friend')
                ->orWhere('status', '=', 'waiting');
        });
        /*$result = FriendEntity::where('user_id', $id)->where('status', 'friend');*/
        if($field)
            $result = $result->select($field);
        return $result->get();
    }
}