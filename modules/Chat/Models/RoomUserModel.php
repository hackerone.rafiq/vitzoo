<?php
namespace Modules\Chat\Models;

use Illuminate\Support\Facades\Validator;
use Helper;
use Modules\Chat\Entities\RoomEntity;
use Modules\Chat\Entities\RoomUserEntity;

class RoomUserModel
{
    function insertRoom($data){
        if (empty($data)) {
            // return error message
            return false;
        }
        $room = new RoomUserEntity();
        $room = Helper::mapdata($room, $data);
        return $room->save();
    }

    function deleteUserRoom($data){
        if (empty($data)) {
            // return error message
            return false;
        }
        return RoomUserEntity::where('room_id','=',$data['room_id'])->where('user_id','=',(int)$data['user_id'])->delete();
    }

    function getAllUserByRoomId($id,$fields = []){
        if (empty($id)) {
            return false;
        }
        $roomUser = RoomUserEntity::where('room_id',$id);
        if (!empty($fields)) {
            $roomUser->select($fields);
        }
        return $roomUser->get();
    }
    
    function getAllRoomWithUsers($room_id, $fields = [])
    {
        $room = new RoomUserEntity();
        if (count($fields)) {
            foreach ($fields as $field => $query) {
                $room = $room->where($field, '=', $query);
            }
        }
        return $room->where('room_id', $room_id);
    }
}