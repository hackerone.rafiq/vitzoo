<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('api', 'ApiController@baseRoutes');

/*Route::group(['prefix' => 'api'], function () {
    Route::post('authenticate', 'ApiController@apiAuthentication');
});*/

Route::group(['prefix' => 'api',
    'middlewareGroups' => ['api'],
], function () {
    Route::post('apiregister', 'ApiController@apiregister');
    Route::post('authenticate', 'ApiController@apiAuthentication');
    Route::post('checklogin', 'ApiController@checklogin');
    Route::post('register', 'ApiController@register');
    Route::get('languages', 'ApiController@languages');
});



Route::group(
    [
        'prefix' => 'api',
        'middlewareGroups' => ['api'],
        'middleware' => 'oauth'
    ], function() {
    Route::get('countries', 'ApiController@getAllCountries');
    Route::get('states', 'ApiController@getStatesByCountry');
    Route::get('cities', 'ApiController@getCitiesByState');
    Route::get('user', 'ApiController@userRoutes');
    Route::get('logout', 'ApiController@logOut');
    Route::post('group', 'MessageApiController@functionsgroup');
    Route::get('profile', 'ApiController@profile');
    Route::post('profile', 'ApiController@editProfile');
    Route::post('friendrequest', 'ApiController@addRemoveFriends');
    Route::get('chattoken', 'ApiController@getJWT');
    Route::post('avatar', 'ApiController@avatarChange');
    Route::post('group_picture', 'ApiController@groupPictureChange');
    Route::post('friends', 'ApiController@getFriends');
    Route::post('chat_rooms', 'ApiController@getUserRooms');
    Route::post('recent', 'ApiController@getRecentActiveRoom');
    Route::post('chat_messages', 'MessageApiController@getChatMessages');
    Route::post('content_messages', 'MessageApiController@getContentMessage');
    Route::post('notifications', 'MessageApiController@notifications');
    Route::post('count_notifications', 'MessageApiController@countNotifications');
    Route::post('count_message', 'MessageApiController@countMessageOfRoom');
    Route::post('unread_notifications', 'MessageApiController@unreadNotification');
    Route::post('set_message_as_read', 'MessageApiController@setMessageAsRead');
    Route::post('getInfomationGroup', 'MessageApiController@getGroupInformation');
    Route::post('send_file', 'MessageApiController@uploadFile');
    Route::post('search', 'ApiController@search');
    Route::post('updateHistory', 'MessageApiController@saveHistoryVideoCalling');
    Route::get('uniqueRoom', 'MessageApiController@uniqueRoom');
    Route::get('list_confirm', 'MessageApiController@listConfirm');
    Route::get('list_block', 'MessageApiController@listBlocked');
    Route::post('change_password', 'ApiController@changePassword');
});
