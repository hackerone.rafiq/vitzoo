<?php
/**
 * Created by PhpStorm.
 * User: Yanaro
 * Date: 2/11/2016
 * Time: 10:09 AM
 */

namespace Modules\Api\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Modules\Api\Models\ApiModel;
use Modules\Chat\Entities\RoomUserEntity;
use Modules\Chat\Models\FriendModel;
use Modules\Chat\Models\MessageModel;
use Modules\Chat\Models\NoticeModel;
use Modules\Chat\Models\RoomModel;
use Modules\Chat\Models\RoomUserModel;
use Helper;
use Authorizer, Request, Validator;
use Modules\User\Models\UserModel;

class MessageApiController extends Controller
{
    /**
     * @var int
     */
    private $limit = 100;
    
    public function __construct()
    {
        $this->apimodel = new ApiModel();
    }
    
    /** Get chat history
     * @param {int} first : Get (first) amount of messages, must be a multiple of 50
     * @param {int} timezone : User timezone
     * @param {string} room_id : required, room from which to get messages
     * @return mixed
     */
    public function getChatMessages()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $room_id = request('room_id');
        $rooms = new RoomModel();
        $is_in_room = $rooms->getUserInRoom($user_id, $room_id);
        if (!$is_in_room) return Helper::errors('invalid_request', 'Your request cannot be proceeded.', 422);
        $messages = new MessageModel();
        $created_day = $is_in_room->created_at;
        $roomUsers = new RoomUserModel();
        $page = (int) request('page') ?: 1;
        $roomUser = $roomUsers->getAllUserByRoomId($room_id, ['user_id']);
        $roomInfo = $rooms->getAllInfoRoom($room_id, ['title', 'room_admin', 'url']);
        $isAdmin = $user_id === $roomInfo->room_admin;
        $message = $messages->getMessage($room_id, [], $this->limit, $page - 1, false, $created_day);
        foreach ($message as $item) {
            $user_join = $item->joinUser()->first();
            $item->content = $item->is_file ? url('themes/default/asset_frontend/img_file/' . Helper::stripVowelAccent($user_join->id . $user_join->first_name) . '/' . Helper::decrypt($item->content)) : Helper::decrypt($item->content);
        }
        $count = count($message);
        if ($roomUser) {
            $data = [
                'room_id' => $room_id,
                'user_id' => $roomUser,
                'relative' => 'chat'
            ];
            \LRedis::publish('saveUserOfRoom', json_encode($data));
        }
        
        $result = [
            'isAdmin' => $isAdmin,
            'roomInfo' => $roomInfo,
            'room_id' => $room_id,
            'room_members' => $roomUser,
            'page' => $page + 1,
            'last' => !$count || $count !== $this->limit
        ];
        if ($roomInfo->title) {
            $result['room_picture'] = Helper::getLinkAvatar() . $roomInfo->url;
        }
        $result['messages'] = $message;
        return $this->apimodel->jsondata($result);
    }
    
    /**
     * @return mixed
     */
    public function notifications()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $lang = request('language');
        $notification = new NoticeModel();
        $notice = $notification->getAllNotifi($user_id, $lang);
        $rooms = new RoomModel();
        if ($notice) {
            foreach ($notice as $value) {
                $room_id = $value->room_id;
                if ($room_id) {
                    $room = $rooms->getRoomInfo($value->room_id);
                    $value->roomName = $room->title;
                    $value->url = $room->url;
                }
            }
        }
        return $this->apimodel->jsondata($notice);
    }
    
    /**
     * @return mixed
     */
    function countNotifications()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $type = request('type');
        $notification = new NoticeModel();
        $messages = new MessageModel();
        if ($type == 'updateIsRead')
            $notification->updateIsRead($user_id);
        $count = $notification->countIsNotRead($user_id);
        $message = $messages->countIsRead($user_id);
        return $this->apimodel->jsondata(['notice' => $count, 'message' => $message]);
    }
    
    function countMessageOfRoom()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $room_id = request('room_id');
        $room = new RoomUserEntity();
        $count = $room->countMessageIsRead($room_id,$user_id);
        $count = !$count?0:$count->is_read;
        return $this->apimodel->jsondata(['room_id'=>$room_id,'count'=>$count]);
    }
    /**
     * @return mixed
     */
    public function unreadNotification()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $notification = new NoticeModel();
        $notice_id = request('notice_id');
        $notice = $notification->updateIsClick($notice_id);
        return Helper::json($notice);
    }
    
    /**
     * @return mixed
     */
    public function setMessageAsRead()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $room_id = request('room_id');
        $messages = new MessageModel();
        $message = $messages->updateIsRead($user_id, $room_id);
        return Helper::json($message);
    }
    
    /**
     * @return mixed
     */
    public function uploadFile()
    {
        $result = null;
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $data = request()->all();
        
        $rules = [
            'room_id' => 'required|max:100',
            'img' => 'required',
            'file_name' => 'required'
        ];
        $apiController = new ApiController();
        $validator =$apiController->validater($data, $rules);
        if($validator)
            return Helper::errors('invalid_request', $validator, 422);
        
        $room_id = $data['room_id'];
        $name = $data['file_name'];
        /**
         * check user exists in room
         */
        $rooms = new RoomModel();
        $is_in_room = $rooms->checkUserExistsRoom($user_id, $room_id);
        if ($is_in_room) {
            $users = new UserModel();
            /**
             * get infomation myself
             */
            $user = $users::getUserById($user_id);
            /**
             * vowel accent of folder
             */
            $folder = Helper::stripVowelAccent($user_id . $user->first_name);
            $destinationPath = '/themes/default/asset_frontend/img_file/' . $folder . '/';
            if (!File::exists(public_path() . $destinationPath)) {
                // path does not exist
                File::makeDirectory(public_path() . $destinationPath, $mode = 0777, true, true);
            }
            /**
             * get all user in room
             */
            $room = new RoomUserModel();
            $room = $room->getAllUserByRoomId($room_id);
            $users = [];
            foreach ($room as $participant) {
                $users[] = $participant->user_id;
            }
            /**
             * change name of image
             */
            $file_name = str_replace(' ', '_', Helper::stripVowelAccent($name));
            $filepath = $destinationPath . $file_name;
            /**
             * upload image format base64
             */
            $apiModel = new ApiModel();
            $resultUpload = $apiModel->uploadImageBase64($data, public_path(). $filepath, $rules);
            
            if($resultUpload != 'success')
                return $resultUpload;
            /**
             * transfer data to nodejs
             */
            $data['fullname'] = $user->last_name . ' ' . $user->first_name;
            if ($user->url) {
                $data['url'] = Helper::getDataURI(url(Helper::getLinkAvatar(). $user->url));
            }
            
            $data['user_id'] = $user_id;
            unset($data['img']);
            $data['content'] = $file_name;
            $data['room_users'] = $users;
            $data['destinationPath'] = url($destinationPath . $file_name);
            $data['is_file'] = 1;
            \LRedis::publish('saveFile', json_encode($data));
            
            //$result = $messages->uploadFile($file, $destinationPath, $user->id, $users, $data);
            unset($data['url']);
            return $this->apimodel->jsondata($data);
        }
        return Helper::errors('invalid_request', 'Please make sure that the data you enter is valid.', 400);
    }
    
    function getContentMessage()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $room_id = request('room_id');
        $fromDateapi = request('created_at');
        
        if(gettype($fromDateapi) == 'string'){
            $fromDateapi = Helper::convertDatetimeObject($fromDateapi);
        }
        $rooms = new RoomModel();
        $is_in_room = $rooms->getUserInRoom($user_id, $room_id);
        if (!$is_in_room) return Helper::errors('invalid_request', 'Your request cannot be proceeded.', 422);
        $messages = new MessageModel();
        $created_day = $is_in_room->created_at;
        $roomUsers = new RoomUserModel();
        $page = (int) request('page') ?: 1;
        $roomUser = $roomUsers->getAllUserByRoomId($room_id, ['user_id']);
        $roomInfo = $rooms->getAllInfoRoom($room_id, ['title', 'room_admin', 'url', 'type']);
        $isAdmin = $user_id === $roomInfo->room_admin;
        
        $message = $messages->getMessage($room_id, [], $this->limit, $page - 1, $created_day, $fromDateapi);
        $count = count($message);
        if ($count)
            foreach ($message as $item) {
                $user_join = $item->joinUser()->first();
                $item->content = $item->is_file ? url('themes/default/asset_frontend/img_file/' . Helper::stripVowelAccent($user_join->id . $user_join->first_name) . '/' . Helper::decrypt($item->content)) : Helper::decrypt($item->content);
            }
        
        if ($roomUser) {
            $data = [
                'room_id' => $room_id,
                'user_id' => $roomUser,
                'relative' => 'chat'
            ];
            \LRedis::publish('saveUserOfRoom', json_encode($data));
        }
        
        $result = [
            'isAdmin' => $isAdmin,
            'roomInfo' => $roomInfo
        ];
        if ($roomInfo->title) {
            $result['room_picture'] = Helper::getLinkAvatar() . $roomInfo->url;
        }
        $result['messages'] = $message;
        if($count)
            return $this->apimodel->jsondata($result);
        return $this->apimodel->jsondata($result,'History Messages do not have change',false);
    }
    
    function getGroupInformation(){
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $room_id = request('room_id');
        $rooms = new RoomModel();
        $is_in_room = $rooms->getUserInRoom($user_id, $room_id);
        if (!$is_in_room) return Helper::errors('invalid_request', 'Your request cannot be proceeded.', 422);
        $roomInfo = $rooms->getAllInfoRoom($room_id, ['title', 'room_admin', 'url','type']);
        $users = $rooms->getRoomUsers($room_id);
        $user_list = [];
        if($users)
            foreach ($users as $user){
                $user_list[] = $user->joinUser()->select(['id','first_name','last_name','url'])->first();
            }
        $isAdmin = $user_id === $roomInfo->room_admin;
        $result = [
            'isAdmin' => $isAdmin,
            'roomInfo' => $roomInfo,
            'users' => $user_list
        ];
        return $this->apimodel->jsondata($result);
    }
    
    /**
     * save history video calling : miss or successfull
     * @return mixed
     */
    function saveHistoryVideoCalling(){
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $receiver_id = request('receiver_id');
        $room_id = request('room_id');
        $rules = [
            'receiver_id' => 'required',
            'room_id' => 'required',
        ];
        $apiController = new ApiController();
        if ($validater = $apiController->validater([
            'receiver_id' => $receiver_id,
            'room_id' => $room_id
        ], $rules))
            return Helper::errors('invalid_request', $validater, 422);
        
        $roomModel = new RoomModel();
        if ($roomModel->checkUserExistsRoom((int) $receiver_id, $room_id, 'personal') && $roomModel->checkTypeRoom($room_id)) {
            $messageModel = new MessageModel();
            $flag = $messageModel->checkFlagCall($user_id, $receiver_id, $room_id);
            $redis = [];
            if (!$flag) {
                $redis = [
                    'is_file' => 3,
                    'room_id' => $room_id,
                    'user_id' => $user_id,
                    'content' => 'call',
                    'flagcall' => 1,
                    'room_users' => array($user_id, $receiver_id)
                ];
                \LRedis::publish('saveFile', json_encode($redis));
            }
            return $this->apimodel->jsondata($redis,'save successfull');
        }
        return Helper::errors('invalid_request', 'User is out of the room.', 400);
    }
    
    /**
     * create unique room_id when chat
     * @return mixed
     */
    function uniqueRoom(){
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $friend_id = (int)request('friend_id');
        if (!$friend_id) {
            return Helper::errors('invalid_request', 'friend_id is required.', 400);
        }
        if($user_id == $friend_id)
            return Helper::errors('invalid_request', 'Friend code is not identical to the user code', 400);
        $room_id = Helper::uniqueRoom($user_id,$friend_id);
        if(!$room_id)
            return Helper::errors('system_error', 'The system error occurred during operation', 400);
        return $this->apimodel->jsondata($room_id);
    }
    
    function listConfirm(){
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $list = $this->apimodel->getListFriendStatus($user_id, 'stranger', 'waiting');
        $list = $this->apimodel->listInformationUser($list,'friend_id');
        return $this->apimodel->jsondata($list);
    }
    
    function listBlocked()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $friendModel = new FriendModel();
        $list = $friendModel->getFriend($user_id, 'blocked');
        $list = $this->apimodel->listInformationUser($list, 'friend_id');
        return $this->apimodel->jsondata($list);
    }
    
    /**
     * create, edit name of group, add user to group, remove user from group
     * @return mixed
     */
    function functionsgroup(){
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $rules = [
            'action' => ['required', 'regex:/^(friendAndGroup|changeTitle|friend|deleteUser)$/'],
            'room_id' => 'required',
            'friend_id' => 'required',
            'title' => 'required|max:20',
        ];
        $apiController = new ApiController();
        $data = request()->all();
        
        if ($validater = $apiController->validater($data, $rules))
            return Helper::errors('invalid_request', $validater, 422);
        $roomModel = new RoomModel();
        $data['user_id'] = $user_id;
        $user = UserModel::getUserById($user_id, ['first_name', 'last_name']);
        $user && $data['name_user'] = $user->last_name.' '. $user->last_name;
        $result = $roomModel->addRemoveChangeTitleRoom($data);
        if($result['success'])
            return $this->apimodel->jsondata($result['data']);
        return Helper::errors('system_error', 'The system error occurred during operation', 400);
    }
}