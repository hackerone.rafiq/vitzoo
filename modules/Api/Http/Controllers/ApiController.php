<?php
namespace Modules\Api\Http\Controllers;

use App\Helpers\JWT;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Modules\Api\Models\ApiModel;
use Modules\Chat\Models\ElasticSearchModel;
use Modules\Chat\Models\FriendModel;
use Modules\Chat\Models\NoticeModel;
use Modules\Chat\Models\RoomModel;
use Modules\Chat\Models\RoomUserModel;
use Modules\User\Models\AttributeModel;
use Modules\User\Models\LanguageModel;
use Modules\User\Models\UserAttributeModel;
use Request, SEOMeta, Theme, Helper, Assets, Sentinel, Response, Validator, LRedis;
use Authorizer;
use DB;
use Modules\User\Models\UserModel;
use Modules\User\Models\StatusModel;

class ApiController extends Controller
{
    /** Current API version
     * @var string
     */
    private $version;
    /** URL to APIs
     * @var array
     */
    private $links;
    /*
     * HTTP status codes
     * 404: not found
     * 400: bad request
     * 422: Unprocessable entity
     * */
    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->version = env('VERSION', '1.0');
        $this->apimodel = new ApiModel();
        $this->links = [
            'user_registration' => [
                'href' => '/register',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'client_credentials'
            ],
            'user_logout' => [
                'href' => '/logout',
                'rel' => 'self',
                'method' => 'post',
                'type' => 'password'
            ],
            'user_routes' => [
                'href' => '/user',
                'rel' => 'routes',
                'method' => 'get',
                'type' => 'client_credentials'
            ],
            'self_profile' => [
                'href' => '/profile',
                'rel' => 'self',
                'method' => 'get',
                'type' => 'password'
            ],
            'user_profile' => [
                'href' => '/profile/id',
                'rel' => 'user',
                'method' => 'get',
                'type' => 'password'
            ],
            'room_list' => [
                'href' => '/chat_room',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'recent_active_room_list' => [
                'href' => '/recent',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'friend_list' => [
                'href' => '/friends',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'update_group_picture' => [
                'href' => '/group_picture',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'edit_profile' => [
                'href' => '/profile',
                'rel' => 'edit',
                'method' => 'post',
                'type' => 'password'
            ],
            'friend_request' => [
                'href' => '/friendrequest',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'jwt_token' => [
                'href' => '/chattoken',
                'rel' => 'chat',
                'method' => 'get',
                'type' => 'password'
            ],
            'edit_avatar' => [
                'href' => '/avatar',
                'rel' => 'edit',
                'method' => 'post',
                'type' => 'password'
            ],
            'countries' => [
                'href' => '/countries',
                'rel' => 'list',
                'method' => 'get',
                'type' => 'client_credentials'
            ],
            'states' => [
                'href' => '/states',
                'rel' => 'list',
                'method' => 'get',
                'type' => 'client_credentials'
            ],
            'cities' => [
                'href' => '/cities',
                'rel' => 'list',
                'method' => 'get',
                'type' => 'client_credentials'
            ],
            'chat_messages' => [
                'href' => '/chat_messages',
                'rel' => 'list',
                'method' => 'get',
                'type' => 'password'
            ],
            'all_notification' => [
                'href' => '/notification',
                'rel' => 'list',
                'method' => 'get',
                'type' => 'password'
            ],
            'count_notification' => [
                'href' => '/count_notification',
                'rel' => 'list',
                'method' => 'get',
                'type' => 'password'
            ],
            'set_unread_message_as_read' => [
                'href' => '/set_message_as_read',
                'rel' => 'user',
                'method' => 'get',
                'type' => 'password'
            ],
            'send_file_in_room' => [
                'href' => '/send_file',
                'rel' => 'user',
                'method' => 'get',
                'type' => 'password'
            ],
        ];
    }
    
    /** Get usable routes for logged in users
     * @return mixed
     */
    public function userRoutes()
    {
        $data = [
            'version' => $this->version,
            'base_url' => url('api'),
            'links' => $this->links
        ];
        return Helper::json($data);
    }
    
    /**
     * @param null $field_name
     * @param $field_data
     * @param null $link
     * @return array
     */
    public function restfulAPI($field_name = null, $field_data, $link = null)
    {
        $data = [
            'version' => $this->version,
            'base_url' => url('api'),
        ];
        if ($field_name) {
            $data[$field_name] = $field_data;
        }
        if ($link) {
            $data['link'] = $link;
        }
        return $data;
    }
    
    /**  Register new user account
     * @param {string} email : unique email address
     * @param {string} first_name : User first name
     * @param {string} last_name : User last name
     * @param {password}
     * @return mixed
     */
    public function register()
    {
        $rules = [
            'email' => 'required|max:100|unique:user__users,email|email',
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:40',
            'password' => 'required|min:5'
        ];
        $req = request()->all();
        $validator = Validator::make($req, $rules);
        if ($validator->fails()) {
            $result = [];
            $errors = $validator->errors();
            foreach ($errors->all() as $error) {
                if (is_array($error)) {
                    foreach ($error as $item) {
                        $result[] = $item;
                    }
                } else
                    $result[] = $error;
            }
            return Helper::errors('invalid_request', $result, 422);
        }
        $data = [
            'first_name', 'last_name', 'password', 'email'
        ];
        $data = array_flip($data);
        $data = array_intersect_key($req, $data);
        $data['group'] = 3;
        $data['role'] = 'user';
        $result = UserModel::createUser($data);
        
        if ($result['success'] && !empty($result) && !empty($result['data'] && !empty($result['data']['id']))) {
            $data = [
                'user_id' => $result['data']['id'],
                'status' => 'online'
            ];
            StatusModel::createOrUpdateStatus($data);
        }
        if ($result['success']) {
            $link = [
                [
                    'self_profile' => [
                        'href' => '/profile',
                        'rel' => 'self',
                        'method' => 'get',
                        'type' => 'password'
                    ],
                    'edit_profile' => [
                        'href' => '/profile',
                        'rel' => 'edit',
                        'method' => 'post',
                        'type' => 'password'
                    ],
                ]
            ];
            return $this->apimodel->jsondata($result['data'], 'Register credentials are successfully!', true, $link);
        }
        return Helper::errors('request_failed', $result['message'], 422);
    }
    
    /**
     * @return mixed
     */
    public function logOut()
    {
        $id = Authorizer::getResourceOwnerId();
        if ($id) {
            $api = new ApiModel();
            $api->removeApiSession($id);
            return $this->baseRoutes();
        }
        return Helper::errors('invalid_request', 'User does not exist', 404);
    }
    
    /** Get base API routes for all users
     * @return mixed
     */
    public function baseRoutes()
    {
        $data = [
            'version' => $this->version,
            'base_url' => url('api'),
            'links' => [
                'api_authentication' => [
                    'href' => '/checklogin',
                    'rel' => 'user',
                    'method' => 'post',
                    'grant_type' => [
                        'password',
                        'refresh_token',
                        'client_credentials',
                    ]
                ]
            ]
        ];
        return Helper::json($data);
    }
    
    function checklogin()
    {
        $email = request('username');
        $password = request('password');
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        if (Sentinel::authenticate($credentials)) {
            $data = [
                'user' => ['id' => Sentinel::check()->id,
                    'email' => $email,
                    'first_name' => Sentinel::check()->first_name,
                    'last_name' => Sentinel::check()->last_name
                ]
            ];
            $author = Authorizer::issueAccessToken();
            if ($author)
                $data['token'] = $author;
            return $this->apimodel->jsondata($data, 'Login successful');
        }
        return Helper::errors('invalid_request', 'Login credentials are wrong. Please try again!', 404);
    }
    /* OAuth2 Authentication
        @param {String} grant_type : password || refresh_token || client_credentials
        @param {String} client_id : client public key
        @param {String} client_secret : client secret key
           password grant type :
                @param {String} email : User email
                @param {String} password: User password
           refresh token grant type:
                @param {String} refresh_token : Refresh token
           client credentials grant type :
                No additional params required
        @return access token
    */
    
    /**
     * @return mixed
     */
    public function apiAuthentication()
    {
        return Response::json(Authorizer::issueAccessToken());
    }
    
    /*
     *  Verification function used by Api Authentication
     * @return bool
     */
    /**
     * @return bool
     */
    public function verify()
    {
        $email = request('username');
        $password = request('password');
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        if (Sentinel::authenticate($credentials)) {
            return Sentinel::check()->id;
        }
        return false;
    }
    
    /** Show user profile
     * @param {String} access_token : OAuth2 password access token
     * @param {int} id : user id
     * @return mixed
     */
    public function profile()
    {
        $id = request('id');
        
        if (!$user_id = Authorizer::getResourceOwnerId()) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        if (empty($id)) {
            $id = $user_id;
        }
        $users = new UserModel();
        $user_attr = new UserAttributeModel();
        $attr = new AttributeModel();
        $user = $users::getUserById($id);
        if (!$user) {
            return Helper::errors('invalid_request', 'User does not exist', 404);
        }
        $user_attribute = $user_attr->getAllAtribute($id, $id !== $user_id);
        $attribute = $attr->getAllAttribute();
        foreach ($user_attribute as $att) {
            $name = $att->joinAttribute()->first()->attribute_name;
            $att['attribute_name'] = $name;
        }
        $request = [
            'id', 'first_name', 'last_name', 'email', 'url'
        ];
        $request = array_flip($request);
        $user = array_intersect_key($user->toArray(), $request);
        $data = [
            'base_avatar' => Helper::getLinkAvatar(),
            'user' => $user,
            'user_attributes' => $user_attribute->toArray(),
            'available_attributes' => $attribute->toArray(),
            'status' => StatusModel::getStatusById($id),
        ];
        $links = ['edit_profile' => [
            'href' => '/profile',
            'rel' => 'edit',
            'method' => 'post',
            'type' => 'password'
        ],
        ];
        return $this->apimodel->jsondata($data, 'Get Data Successly!', true, $links);
    }
    
    /** Edit user profile
     * @param {String} access_token : OAuth2 password access token
     * @return mixed
     */
    public function editProfile(Request $r)
    {
        $user_id = Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $user = UserModel::getUserById($user_id);
        if (empty($user)) {
            return Helper::errors('invalid_request', 'User does not exist', 404);
        }
        $this->user = $user;
        $rules = [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:40',
            'stt_gender' => 'numeric',
            'stt_phone' => 'numeric',
            'stt_address' => 'numeric',
            'stt_birthday' => 'numeric',
            'stt_nation' => 'numeric',
            'stt_provinces' => 'numeric',
            'stt_city' => 'numeric',
            'stt_language' => 'numeric',
            'birthday' => 'date'
        ];
        $req = $r::all();
        if ($validater = $this->validater($req, $rules))
            return Helper::errors('invalid_request', 'The data is invalid', 422);
        $data = [
            'first_name' => request('first_name'),
            'last_name' => request('last_name')
        
        ];
        $attribute = [
            'gender' => [
                'value' => request('gender'),
                'id' => 1,
                'status' => request('stt_gender'),
            ],
            'phone_number' => [
                'value' => request('phone'),
                'id' => 2,
                'status' => request('stt_phone'),
            ],
            'address' => [
                'value' => request('address'),
                'id' => 3,
                'status' => request('stt_address'),
            ],
            'birth_day' => [
                'value' => request('birthday'),
                'id' => 4,
                'status' => request('stt_birthday'),
            ],
            'nation' => [
                'value' => request('nation'),
                'id' => 5,
                'status' => request('stt_nation'),
            ],
            'provinces' => [
                'value' => request('provinces'),
                'id' => 6,
                'status' => request('stt_provinces'),
            ],
            'city' => [
                'value' => request('city'),
                'id' => 7,
                'status' => request('stt_city'),
            ],
            'language' => [
                'value' => request('language'),
                'id' => 8,
                'status' => request('stt_language'),
            ],
        ];
        //return ['a'=>$attribute];
        $dataconvert = $this->convertArrayEmpty($attribute, $data);
        if ($dataconvert == false)
            return Helper::errors('invalid_request', 'The data is invalid', 404);
        $data = [
            'data' => $dataconvert
        ];
        $User = new UserAttributeModel();
        $result = $User->editProfile($data, $user->id);
        if ($result['success']) {
            return $this->apimodel->jsondata('', 'The information was successfully updated', true, [
                'self_profile' => [
                    'href' => '/profile',
                    'rel' => 'self',
                    'method' => 'get',
                    'type' => 'password'
                ],
            ]);
        }
        return Helper::errors('request_failed', $result['message'], 422);
    }
    
    /**
     * filter element of the value array is empty
     * @param array $data
     * @param array $arr
     * @return array|bool
     */
    function convertArrayEmpty($data = [], $arr = [])
    {
        
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if ($value['value'])
                    $arr[$key] = $value;
                if ($value['value'] && (!in_array($value['status'], [0, 1]) || $value['status'] == null))
                    return false;
            }
        }
        return $arr;
    }
    
    /**
     * validator array
     * @param $data
     * @param $rules
     * @return array|bool
     */
    function validater($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $result = [];
            $errors = $validator->errors();
            foreach ($errors->all() as $error) {
                if (is_array($error)) {
                    foreach ($error as $item) {
                        $result[] = $item;
                    }
                } else
                    $result[] = $error;
            }
            return $result;
            
        }
        return false;
    }
    
    /**
     * @param {String} access_token : OAuth2 client credentials access token
     * @return mixed
     */
    public function ApiRegister(Request $post_data)
    {
        $user_id = Authorizer::getResourceOwnerId();
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $rules = ['name' => 'required|max:100|unique:oauth_clients,name'];
        $validator = Validator::make($post_data::all(), $rules);
        if ($validator->fails()) {
            $result = [];
            $errors = $validator->errors();
            foreach ($errors->all() as $error) {
                if (is_array($error)) {
                    foreach ($error as $item) {
                        $result[] = $item;
                    }
                } else {
                    $result[] = $error;
                }
            }
            return Helper::errors('invalid_request', $result, 422);
        }
        $name = $post_data::input('name');
        $secret = Helper::create_unique();
        $public = uniqid();
        $api = new ApiModel();
        $result = $api->ApiCreation($public, $secret, $name);
        if (!$result) {
            return Helper::errors('request_failed', 'Failed to create new Vitzoo API ID.', 400);
        } else {
            return Helper::json([
                'name' => $name,
                'id' => $public,
                'secret' => $secret
            ]);
        }
    }
    
    /** Get list of countries, get a specific country data if country id is present
     * @param {String} access_token : OAuth2 client credentials access token
     * @param {int} country_id : not required
     * @return mixed
     */
    public function getAllCountries()
    {
        $country_id = request('country_id');
        $api = new ApiModel();
        $countries = $api->loadCountryData(['id' => $country_id]);
        if ($countries) {
            return $this->apimodel->jsondata($countries->toArray(), '', true, [
                'states' => [
                    'href' => '/states',
                    'rel' => 'list',
                    'method' => 'get',
                    'type' => 'client_credentials'
                ],
                'cities' => [
                    'href' => '/cities',
                    'rel' => 'list',
                    'method' => 'get',
                    'type' => 'client_credentials'
                ]
            ]);
        } else {
            return Helper::errors('request_failed', 'Failed to retrieve required data.', 404);
        }
    }
    
    /** Get list of states by a specific country
     * @param {String} access_token : OAuth2 client credentials access token
     * @param {int} country_id
     * @param {int} state_id : not required
     * @return mixed
     */
    public function getStatesByCountry()
    {
        $id = request('country_id');
        if ($id) {
            $state_id = request('state_id');
            $api = new ApiModel();
            $data = [
                'id' => $id,
                'state_id' => $state_id
            ];
            $states = $api->loadCountryData($data, 'states');
            if ($states) {
                return $this->apimodel->jsondata($states->toArray(), '', true, [
                    'countries' => [
                        'href' => '/countries',
                        'rel' => 'list',
                        'method' => 'get',
                        'type' => 'client_credentials'
                    ],
                    'cities' => [
                        'href' => '/cities',
                        'rel' => 'list',
                        'method' => 'get',
                        'type' => 'client_credentials'
                    ]
                ]);
            } else {
                return Helper::errors('request_failed', 'Failed to retrieve required data.', 404);
            }
        }
        return Helper::errors('request_failed', 'The field "country_id" cannot be empty.', 400);
    }
    
    /** Get a list of cities by a specific country or state
     * @param {String} access_token : OAuth2 client credentials access token
     * @return mixed
     */
    public function getCitiesByState()
    {
        $id = request('country_id');
        $state_id = request('state_id');
        $city_id = request('city_id');
        if ($id || $state_id || $city_id) {
            $api = new ApiModel();
            $data = [
                'id' => $id,
                'state_id' => $state_id,
                'city_id' => $city_id
            ];
            $cities = $api->loadCountryData($data, 'cities');
            if ($cities) {
                return $this->apimodel->jsondata($cities->toArray(), '', true, [
                    'countries' => [
                        'href' => '/countries',
                        'rel' => 'list',
                        'method' => 'get',
                        'type' => 'client_credentials'
                    ],
                    'states' => [
                        'href' => '/states',
                        'rel' => 'list',
                        'method' => 'get',
                        'type' => 'client_credentials'
                    ]
                ]);
            } else {
                return Helper::errors('request_failed', 'Failed to retrieve required data.', 404);
            }
        }
        return Helper::errors('request_failed', 'At least one of the "country_id" ,"state_id" or "city_id" fields must be present.', 400);
    }
    
    /** Add or remove friend
     * @param {String} access_token : OAuth2 password access token
     * @param {int} friend_id : required
     * @param action : required
     *                 - addfriend: send a friend request
     *                 - confirm : accept friend request
     *                 - cancel-request : stop sending friend request
     *                 - delete-request : deny a friend request
     *                 - unfriend : remove friend from friend list
     *                 - block : block targeted user from interacting with user
     *                 - unblock : remove targeted user from block list
     * @return mixed
     */
    public function addRemoveFriends()
    {
        $friend_id = (int) request('friend_id');
        $action = request('action');
        $user_id = (int) Authorizer::getResourceOwnerId();
        $user_id_be = $user_id;
        $friend_id_be = $friend_id;
        //return $user_id;
        if (!$user_id) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $user = UserModel::getUserById($friend_id);
        $myself = UserModel::getUserById($user_id);
        
        if (empty($user)) {
            return Helper::errors('invalid_request', 'User does not exist', 404);
        }
        if (empty($myself)) {
            return Helper::errors('invalid_request', 'User does not exist', 404);
        }
        if ($user_id === $friend_id) {
            return Helper::errors('invalid_request', 'You cannot befriend yourself!', 422);
        }
        $user_name = $myself->last_name . ' ' . $myself->first_name;
        $rules = [
            'friend_id' => 'integer|required',
            'action' => ['required', 'regex:/^(addfriend|unfriend|confirm|cancel-request|delete-request|block|unblock)$/']
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $result = [];
            $errors = $validator->errors();
            foreach ($errors->all() as $error) {
                if (is_array($error)) {
                    foreach ($error as $item) {
                        $result[] = $item;
                    }
                } else {
                    $result[] = $error;
                }
            }
            return Helper::errors('invalid_request', $result, 422);
        }
        $noticeModel = new NoticeModel();
        
        $room = new RoomUserModel();
        $data = [
            'user_id' => $user_id,
            'friend_id' => $friend_id,
            'action' => $action
        ];
        $result = FriendModel::addRemoveFriend($data);
        $room_id = Helper::uniqueRoom($friend_id, $user_id);
        /**
         * if you cancel the request and block the account, delete announcement this before
         */
        if ($result['success'] && ($action == 'cancel-request' || $action == 'block')) {
            $notice_id = $noticeModel->deleteNotice($friend_id, $user_id);
            if ($action == 'block' && $notice_id == 0) {
                $notice_id = $noticeModel->deleteNotice($user_id, $friend_id);
                $friend_id = $user_id;
            }
            if ($notice_id)
                \LRedis::publish('addfriend', json_encode(['type' => $action, 'user_id' => $friend_id, 'notice_id' => $notice_id, 'room_id' => $room_id]));
            
        }
        if ($result['success'] && $action == 'delete-request') {
            $noticeModel->updateIsConfirm($user_id, $friend_id, -1);
        }
        
        //send notice and user of room for nodejs by redis
        if ($result['success'] && ($action == 'addfriend' || $action == 'confirm')) {
            $message1 = '';
            $message2 = '';
            $type = '';
            $data = [
                'user_id' => $friend_id,
                'friend_id' => $user_id,
                'is_read' => 0,
            ];
            if ($action == 'addfriend') {
                $message2 = 'sent you a request';
                $message1 = 'gửi cho bạn yêu cầu';
                $type = 1;
                $noticeModel->deleteNotice($friend_id, $user_id);
            }
            
            if ($action == 'confirm') {
                $message2 = 'accepted your friend request';
                $message1 = 'chấp nhận lời mời kết bạn.';
                $type = 4;
                $noticeModel->updateIsConfirm($user_id, $friend_id, 1);
                \LRedis::publish('friend_added', json_encode(['user_id' => $user_id, 'target_id' => $friend_id]));
            }
            $data['type'] = $type;
            $notice = $noticeModel->insertNotice($data, [$message1, $message2]);
            
            if ($notice) {
                $data['id'] = $notice->id;
                $data['message_vi'] = $message1;
                $data['message_en'] = $message2;
                $data['type'] = $notice->type;
                $data['confirm'] = trans('fr_home.confirm');
                $data['url'] = $myself->url ? url('themes/default/asset_frontend/img_avatar/' . $myself->url) : false;
                $data['delete_request'] = trans('fr_home.delete_request');
                $data['userName'] = $user_name;
                if ($action == 'confirm' || $action == 'addfriend') {
                    $data['room_id'] = $room_id;
                    $status = $myself->status()->first();
                    if ($status) {
                        $data['mood'] = $status->mood;
                        $data['status'] = $status->status;
                    }
                }
                \LRedis::publish('addfriend', json_encode($data));
            }
        }
        
        /**
         *
         */
        $roomUser = $room->getAllUserByRoomId($room_id, ['user_id']);
        $relative = FriendModel::getStatusRelate($user_id_be, $friend_id_be);
        if ($relative && $roomUser) {
            $relative = $relative['option'];
            $dataSocket = [
                'room_id' => $room_id,
                'user_id' => $roomUser,
                'relative' => $relative,
                'user_id_send' => $user_id
            ];
            $result['data']['option'] = $relative;
            \LRedis::publish('saveUserOfRoom', json_encode($dataSocket));
        }
        
        if ($result['success']) {
            return $this->apimodel->jsondata($result);
        }
        return Helper::errors('friend_request_failed', $result['message'], 422);
    }
    
    /** Get chat token necessary to pass socket
     * @param {String} access_token : OAuth2 password access token
     * @return mixed
     */
    public function getJWT()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (empty($user_id)) {
            return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        }
        $user = UserModel::getUserById($user_id);
        if (empty($user)) {
            return Helper::errors('invalid_request', 'User does not exist', 404);
        }
        $user = [
            'user_id' => $user_id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'salt' => uniqid('Vitzoo', true),
            'time' => date('D M d Y H:i:s O')
        ];
        
        $jwt = JWT::encode($user, Helper::getEncodeKey());
        return $this->apimodel->jsondata($jwt, '', true, [
            'chat_server' => [
                'href' => url('') . ':3030/chat',
                'rel' => 'chat',
                'transport' => 'websocket',
                'params' => [
                    'secure' => true,
                    'token' => 'token'
                ]
            ],
            'voip_server' => [
                'href' => url('') . ':5555/easyrtc',
                'rel' => 'VOIP',
                'transport' => 'websocket',
                'params' => ['secure' => true,
                    'token' => 'token'
                ]
            ]
        ]);
    }
    
    /**
     * @param {String} access_token : OAuth2 password access token
     * @return mixed
     */
    public function avatarChange()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $rules = [
            'img' => 'required'
        ];
        
        $file_name = strtolower(uniqid($user_id . '_') . '.jpg');
        $filepath = public_path() . Helper::getLinkAvatar() . $file_name;
        $apiModel = new ApiModel();
        $resultUpload = $apiModel->uploadImageBase64(['img' => request('img')], $filepath, $rules);
        if ($resultUpload != 'success')
            return $resultUpload;
        
        $sizeimg = getimagesize($filepath);
        if ($sizeimg[0] > Helper::getWidthHeighAvatar() || $sizeimg[1] > Helper::getWidthHeighAvatar()) {
            file_exists($filepath) && unlink($filepath);
            return Helper::errors('invalid_request', 'Image too large', 422);
        }
        $data = UserModel::updateAvatar(['user_id' => $user_id, 'avatarName' => $file_name]);
        if ($data)
            return $this->apimodel->jsondata(['img_url' => url(Helper::getLinkAvatar() . $file_name)]);
        file_exists($filepath) && unlink($filepath);
        return Helper::errors('invalid_request', 'the system error occurred', 422);
    }
    
    /**
     * Change group picture
     * @param {String} access_token : OAuth2 password access token
     * @param {file} image : Image to update into
     * @param {string} room_id :
     * @return mixed
     */
    public function groupPictureChange()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $room_id = request('room_id');
        $file = request('img');
        $rules = [
            'img' => 'required',
            'room_id' => 'required'
        ];
        $apiController = new ApiController();
        if ($rules && $validater = $apiController->validater(['img' => $file, 'room_id' => $room_id], $rules))
            return Helper::errors('invalid_request', $validater, 422);
        /**
         * check is admin of room
         */
        $room = new RoomModel();
        $is_admin = $room->checkIsAdminRoom($user_id, $room_id);
        if (!$is_admin) return Helper::errors('invalid_request', 'You don\'t have the authority for this request.', 400);
        /**
         * upload image basse64
         */
        $img = base64_decode($file);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $img, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);
        $file_name = rand(1, 100) . $room_id . rand(100, 10000) . '.' . $mime_type;
        if ($validater = $apiController->validater(['extension' => $mime_type], ['extension' => ['regex:/^(jpg|jpeg|png|gif)$/']]))
            return Helper::errors('invalid_request', $validater, 422);
        /**
         * input content base64 into file_path
         */
        $filepath = public_path() . Helper::getLinkAvatar() . $file_name;
        try {
            file_put_contents($filepath, $img);
        } catch (Exception $e) {
            return Helper::errors('system_error', $e->getMessage());
        }
        /**
         * check width height of image
         */
        $sizeimg = getimagesize($filepath);
        if ($sizeimg[0] > Helper::getWidthHeighAvatar() || $sizeimg[1] > Helper::getWidthHeighAvatar()) {
            file_exists($filepath) && unlink($filepath);
            return Helper::errors('invalid_request', 'Image too large', 422);
        }
        /**
         * save url into db
         */
        $data = [
            'file_name' => $file_name,
            'user_id' => $user_id,
            'room_id' => $room_id
        ];
        $update_data = ['url' => $file_name];
        $roomModel = new RoomModel();
        $destinationPath = public_path() . Helper::getLinkAvatar();
        $update_data = $roomModel->updateRoomInfo($data['room_id'], $update_data, true, $destinationPath);
        
        /**
         * send redis
         */
        if ($data) {
            $room_users = $room->getRoomUsers($room_id);
            $url = Helper::getLinkAvatar() . $file_name;
            $result = [
                'url' => $url,
                'room_id' => $room_id
            ];
            \LRedis::publish('updateAvatar', json_encode(['url' => $url, 'user_id' => $user_id, 'room_id' => $room_id, 'target_users' => $room_users]));
            return $this->apimodel->jsondata($result);
        }
        file_exists($filepath) && unlink($filepath);
        return Helper::errors('invalid_request', 'There was an error during update process, please try again later.', 400);
    }
    
    /**
     * @return mixed
     */
    public function getFriends()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $friends = new FriendModel();
        $friends = $friends->getFriend($user_id, 'friend');
        $friend_list = [];
        if (count($friends)) {
            foreach ($friends as $friend) {
                $friend_list[] = $friend->joinUser()->select(['first_name', 'last_name', 'id', 'url'])->first()->toArray();
            }
        }
        $links = [
            'room_list' => [
                'href' => '/chat_room',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'recent_active_room_list' => [
                'href' => '/recent',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
        ];
        return $this->apimodel->jsondata($friend_list, '', true, $links);
    }
    
    /**
     * @return mixed
     */
    public function getUserRooms()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $rooms = new RoomModel();
        $api = new ApiModel();
        $roomUser = $rooms->getAllRoomUserContainer($user_id);
        $room_list = [];
        if (count($roomUser)) {
            foreach ($roomUser as $room) {
                $room_info = $api->getRoomInfo($room->room_id);
                if ($room_info && $room_info->type == "room") {
                    $room_info['users'] = $rooms->getRoomUsers($room->room_id);
                    $room_list[] = $room_info;
                }
            }
        }
        return $this->apimodel->jsondata($room_list, '', true, [
            'recent_active_room_list' => [
                'href' => '/recent',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
            'friend_list' => [
                'href' => '/friends',
                'rel' => 'user',
                'method' => 'post',
                'type' => 'password'
            ],
        ]);
    }
    
    /**
     * @return mixed
     */
    public function getRecentActiveRoom()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $rooms = new RoomModel();
        $roomUser = $rooms->recentActiveRoom($user_id);
        $room_list = [];
        if (count($roomUser)) {
            foreach ($roomUser as $room) {
                $room_info = $rooms->getRoomInfo($room->room_id);
                $room_info['users'] = $rooms->getRoomUsers($room->room_id);
                $room_list[] = $room_info;
            }
        }
        $data = $this->restfulAPI('room_list', $room_list,
            [
                'room_list' => [
                    'href' => '/chat_room',
                    'rel' => 'user',
                    'method' => 'post',
                    'type' => 'password'
                ],
                'friend_list' => [
                    'href' => '/friends',
                    'rel' => 'user',
                    'method' => 'post',
                    'type' => 'password'
                ],
            ]);
        return Helper::json($data);
    }
    
    /**
     * @return mixed
     */
    public function search()
    {
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $user = UserModel::getUserById($user_id);
        if (empty($user)) {
            return Helper::errors('invalid_request', 'User does not exist', 404);
        }
        $search = new ElasticSearchModel();
        $name = request('name');
        $users = $search->searchUser($name, ['id', 'first_name', 'last_name', 'url']);
        $result = [];
        if (count($users)) {
            foreach ($users as $user) {
                if ($user_id !== ($friend_id = $user->id)) {
                    $relative = FriendModel::getStatusRelate($user_id, $friend_id)['status'];
                    $friend = [
                        'user' => $user,
                        'relation' => $relative
                    ];
                    $result[] = $friend;
                }
            }
        }
        return $this->apimodel->jsondata($result);
    }
    
    function languages()
    {
        $languageModel = new LanguageModel();
        return $languageModel->getAllLanguage();
    }
    
    function changePassword(){
        $user_id = (int) Authorizer::getResourceOwnerId();
        if (!$user_id) return Helper::errors('invalid_request', 'Please use password token to proceed.', 400);
        $rules = [
            'new_password' => 'required',
            'current_password' => 'required'
        ];
        $data = request()->all();
        $apiController = new ApiController();
        if ($validater = $apiController->validater($data, $rules))
            return Helper::errors('invalid_request', $validater, 422);
        $user = Sentinel::findById($user_id);
        $current_password = $data['current_password'];
        $password = $data['new_password'];
        
        $user_new = [
            'email' => $user->email,
            'password' => $current_password
        ];
        if (Sentinel::validateCredentials($user, $user_new)) {
            if ($current_password !== $password) {
                $user_new['password'] = $password;
                $result = Sentinel::update($user, $user_new);
                unset($result['password']);
                return $this->apimodel->jsondata($result);
            } else {
                return Helper::errors('invalid_request', 'Your new and current password should not be the same.', 400);
            }
        }
        return Helper::errors('invalid_request', 'The password is incorrect', 400);
    }
}