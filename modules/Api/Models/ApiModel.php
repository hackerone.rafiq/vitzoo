<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: vitpr
 * Date: 4/12/16
 */

namespace Modules\Api\Models;

use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Modules\Api\Entities\OauthSessionEntity;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Chat\Entities\FriendEntity;
use Modules\Chat\Entities\RoomEntity;
use Modules\User\Entities\CityEntity;
use Modules\User\Entities\CountryEntity;
use Modules\User\Entities\UserEntity;
use Modules\User\Models\UserModel;
use Validator, Helper, Sentinel;
use Modules\Api\Entities\OauthClientEntity;

class ApiModel
{
    public function ApiCreation($public, $secret, $name)
    {
        $client = new OauthClientEntity();
        $client->id = $public;
        $client->secret = $secret;
        $client->name = $name;
        if (!$client->save()) return false;
        return $client;
    }
    
    public function removeApiSession($id)
    {
        return OauthSessionEntity::where('owner_id', $id)->delete();
    }
    
    
    public function loadCountryData($data = [], $division = '')
    {
        if (empty($division)) {
            if (!empty($data['id']))
                return CountryEntity::select('locale_code', 'continent_name', 'country_iso_code', 'Country')
                    ->find($data['id']);
            return CountryEntity::select('id', 'locale_code', 'continent_name', 'country_iso_code', 'Country')
                ->orderBy('Country')->get();
        }
        $country = null;
        if (!empty($data['id']))
            $country = CountryEntity::find($data['id']);
        if ($division === 'states') {
            if (!$country) return null;
            if (empty($data['state_id']))
                return $country->states()
                    ->select('locale_code', 'continent_name', 'country_iso_code', 'country_name', 'subdivision_1_name', 'subdivision_id', 'time_zone')
                    ->orderBy('subdivision_1_name', 'asc')->get();
            return $country->states()
                ->where('subdivision_id', $data['state_id'])
                ->orderBy('subdivision_1_name', 'asc')
                ->first(['locale_code', 'continent_name', 'country_iso_code', 'country_name', 'subdivision_1_name', 'subdivision_id', 'time_zone']);
        }
        if ($division === 'cities') {
            $select = ['id', 'locale_code', 'country_iso_code', 'country_name', 'subdivision_1_name', 'subdivision_id', 'City', 'time_zone'];
            if (empty($data['state_id'])) {
                if (!$country) return null;
                $cities = $country->cities()->select($select);
                if (empty($data['city_id']))
                    return $cities->orderBy('City', 'asc')->get();
                return $cities->find($data['city_id']);
            } elseif (empty($data['city_id'])) {
                return CityEntity::where('subdivision_id', $data['state_id'])->orderBy('City', 'asc')->get($select);
            }
            if (!$country) return null;
            return $country->cities()->select($select)->find($data['city_id']);
        }
        return null;
    }
    
    public function jsondata($data = NULL, $message = '', $success = true, $link = '')
    {
        $result = [
            'version' => env('VERSION', '1.0'),
            'base_url' => url('api'),
            'status' => $success,
            'msg' => $message,
            'data' => $data,
            'link' => $link
        ];
        return Helper::json($result);
    }
    
    public function uploadImageBase64($data, $filepath, $rules = null)
    {
        $file = $data['img'];
        $apiController = new ApiController();
        
        if ($rules && $validater = $apiController->validater($data, $rules))
            return Helper::errors('invalid_request', $validater, 422);
        $img = base64_decode($file);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $img, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);
        
        if ($validater = $apiController->validater(['extension' => $mime_type], ['extension' => ['regex:/^(jpg|jpeg|png|gif)$/']]))
            return Helper::errors('invalid_request', $validater, 422);
        try {
            file_put_contents($filepath, $img);
        } catch (Exception $e) {
            return Helper::errors('system_error', $e->getMessage());
        }
        return 'success';
    }
    
    function getListFriendStatus($user_id, $status_me, $status_friend)
    {
        $user = DB::select('SELECT `friend_id` FROM `chat__friends` WHERE `user_id`=? AND `status`=? AND `friend_id` IN (SELECT `user_id` FROM `chat__friends` WHERE `friend_id`=? AND status=?)',
            [$user_id,$status_me,$user_id,$status_friend]);
        return $user;
    }
    
    function getRoomInfo($room_id)
    {
        return RoomEntity::where('_id','=',$room_id)->where('type','=','room')->first();
    }
    
    function listInformationUser($listUser,$field_name = null){
        $list= [];
        if($listUser)
            foreach ($listUser as $user){
                $i = UserModel::getUserById($user->$field_name,['id','first_name','last_name','url']);
                $i && $list[] = $i;
            }
        return $list;
    }
}