<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: Test Online
 * Date: 3/29/16
 */

namespace Modules\User\Models;

use Modules\Admin\Entities\LanguageEntity;
use Helper, Validator, Sentinel;

class LanguageModel
{
    function getAllLanguage(){
        return LanguageEntity::get();
    }
}