<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: Test Online
 * Date: 3/29/16
 */

namespace Modules\User\Models;

use Helper, Validator, Sentinel;
use Modules\User\Entities\CountryEntity;
use Modules\User\Entities\UserAttributeEntity;
use Modules\User\Entities\UserEntity;

class UserAttributeModel{

    function getAtributePublic($user_id, $fields = []){
        $user_attrubute = UserAttributeEntity::where('user_id', $user_id)->where('status', '=','public');
        if (!empty($fields)) {
            $user_attrubute->select($fields);
        }
        return $user_attrubute->get();
    }

    function getAllAtribute($user_id, $isPublic = false, $fields = [])
    {
        $user_attrubute = UserAttributeEntity::where('user_id', $user_id);
        if($isPublic) {
            $user_attrubute = $user_attrubute->where('status', '=', 'public');
        }
        if (!empty($fields)) {
            $user_attrubute->select($fields);
        }
        return $user_attrubute->get();
    }

    function editProfile($data,$user_id){
        if(!$data || empty($data['data']) || !$user_id){
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }
        try {
            $attr = $data['data'];
            $user = UserEntity::where('id',$user_id)->first();
            if($attr['first_name'] && $attr['last_name']){
                $user->first_name = $attr['first_name'];
                $user->last_name = $attr['last_name'];
                $user->save();
                unset($attr['first_name']);
                unset($attr['last_name']);
            }
            if(count($attr)){
                if(isset($attr['nation'])){
                    if(isset($attr['provinces']) && !isset($attr['city'])){
                        UserAttributeEntity::where('user_id', $user_id)->where('attribute_id',7)->delete();
                    }
                    if (!isset($attr['provinces'])) {
                        UserAttributeEntity::where('user_id', $user_id)->whereIn('attribute_id', [6,7])->delete();
                    }
                }
                foreach ($attr as $item){
                    $attri = UserAttributeEntity::where('user_id', $user_id)->where('attribute_id',(int)$item['id'])->first();
                    if(!$attri){
                        $attri_new = new UserAttributeEntity();
                        $attri_new->user_id = $user_id;
                        $attri_new->attribute_id = (int)$item['id'];
                        $attri_new->values = $item['value'];
                        if($item['status'] && $item['status']!=0)
                            $attri_new->status = $item['status'];
                        $attri_new->save();
                    }
                    else{
                        $attri->values = $item['value'];
                        if ($item['status'] && $item['status'] != 0)
                            $attri->status = $item['status'];
                        $attri->save();
                    }
                }
            }
            $result = Helper::resultData(true, trans('site.register_success'), $user);
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage(), null, 1);
        }
        return $result;
    }



}