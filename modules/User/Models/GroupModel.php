<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: Test Online
 * Date: 3/29/16
 */

namespace Modules\User\Models;

use \Modules\User\Entities\RoleEntity;
use Helper, Validator, Sentinel;

class GroupModel
{
    public function getAllGroups()
    {
        $groups = RoleEntity::all();
        return $groups;
    }

    /**
     * @return mixed
     */
    public static function getGroups()
    {
        $groups = RoleEntity::all();
        return $groups;
    }

    /**
     * get user data by id
     * @param $id
     * @return mixed
     */
    public static function getGroupById($id, $fields = [])
    {
        $group = RoleEntity::where('id', $id);
        if (!empty($fields)) {
            $group->select($fields);
        }

        return $group->first();
    }

    /**
     * create new or update group
     * @param $data
     * @return mixed
     */
    public static function saveGroup($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }

        // validate the info, create rules for the inputs
        $rules = [
            'name'          => 'required',
            'display_name'  => 'required'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make($data, $rules);

        // if the validator fails, return error message
        if ($validator->fails()) {
            // return validate error message
            $result = Helper::resultData(false, $validator->errors()->first());
            return $result;
        }

        $currentAdmin = Sentinel::check();
        // check user permission before update
        if (!$currentAdmin->inRoles(['owner', 'admin'])) {
            $result = Helper::resultData(false, trans('site.no_permission'), null, 1);
            return $result;
        }

        try {
            // save permission to database
            // if edit group
            if (!empty($data['id'])) {
                // check group is exist or not
                $model = Sentinel::findRoleById($data['id']);
                // remove data not in use
                unset($data['id']);

                if (empty($model)) {
                    // return error message
                    $result = Helper::resultData(false, trans('user.group_not_exist'), null, 1);
                    return $result;
                }

                $model->update($data);

                // if create new group
            } else {
                $model = Sentinel::getRoleRepository()->createModel()->create($data);
            }

            // return success data
            $result = Helper::resultData(true, trans('site.update_success'), $model);
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage(), null, 1);
        }

        return $result;
    }

    /**
     * remove roles
     * @param $data
     * @return array
     */
    public static function removeGroup($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }

        // check user permission before delete role
        $currentAdmin = Sentinel::check();
        if (!$currentAdmin->inRoles(['owner', 'admin'])) {
            $result = Helper::resultData(false, trans('site.no_permission'), null, 1);
            return $result;
        }

        // make sure all id values are integers
        $ids = Helper::stringToInt($data['ids']);

        try {
            // remove user group from database
            RoleEntity::destroy($ids);

            // return success data
            $result = Helper::resultData(true, trans('site.delete_success'));
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage());
        }

        return $result;
    }
}