<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: Test Online
 * Date: 3/29/16
 */

namespace Modules\User\Models;

use \Modules\User\Entities\StatusEntity;
use Helper, Validator, Sentinel;

class StatusModel
{
    /**
     * create or update status chat of user, include 3 status: online, offline, visible
     * @param $data
     * @return bool|StatusEntity
     */
    public static function createOrUpdateStatus($data)
    {
        // check data submit is empty or not

        if (empty($data)) {
            // return error message
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }
        else if(!empty($data['user_id'])){

            if($status = self::checkUserExists($data['user_id'])){
                $status->status = $data['status'];
                return $status->save();
            }
            else{
                $status = new StatusEntity();
                Helper::mapData($status,$data);
                $status->save();
            }
            return $status;
        }
        return false;
    }

    /**
     * check user exists in db. if exists to update status
     * @param $user_id
     * @return bool
     */
    public static function checkUserExists($user_id)
    {
        if (!empty($user_id)){
            $status = StatusEntity::where('user_id',$user_id)->first();
            return $status;
        }
        return false;
    }

    public static function getStatusById($id){
        if(!empty($id)){
            $status = StatusEntity::select('status', 'mood')->find($id);
            return $status;
        }
        return false;
    }

    public static function getMoodById($id)
    {
        if (!empty($id)) {
            $mood = StatusEntity::select('mood')->find($id);
            return $mood;
        }
        return false;
    }
}