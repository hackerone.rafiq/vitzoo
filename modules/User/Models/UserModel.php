<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: vitpr
 * Date: 4/12/16
 */

namespace Modules\User\Models;

use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Exception\NotWritableException;
use Modules\Chat\Entities\FriendEntity;
use Modules\Chat\Models\ElasticSearchModel;
use Modules\User\Entities\CityEntity;
use Modules\User\Entities\CountryEntity;
use Modules\User\Entities\UserEntity;
use Validator, Helper, Sentinel, Image;

class UserModel
{
    /**
     * get user data by id
     * @param $id
     * @return mixed
     */
    public static function getUserById($id, $fields = [])
    {
        if (!$id)
            return false;
        
        $user = UserEntity::where('id', $id);
        if (!empty($fields)) {
            $user->select($fields);
        }
        $user = $user->first();
        unset($user['password'], $user['permissions'], $user['facebook_id']);
        return $user;
    }
    
    /**
     * get user data by email
     * @param $email
     * @return mixed
     */
    public static function getUserByEmail($email, $fields = [])
    {
        $user = UserEntity::where('email', $email);
        if (!empty($fields)) {
            $user->select($fields);
        }
        
        return $user->first();
    }
    
    public static function getAvatar($id)
    {
        $user = self::getUserById($id);
        $avatars = $user->avatars()->get(['avatar', 'status']);
        if (count($avatars)) {
            foreach ($avatars as $avatar) {
                if ($avatar->status == 'active') {
                    return $avatar->avatar;
                }
            }
        }
        
        return false;
    }
    
    /**
     * check user login
     * @param $data
     * @return array
     */
    public static function checkLogin($data)
    {
        // validate the info, create rules for the inputs
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];
        
        // run the validation rules on the inputs from the form
        $validator = Validator::make($data, $rules);
        
        // if the validator fails, return error message
        if ($validator->fails()) {
            $result = [
                'success' => false,
                'message' => trans('login.login_incorrect')
            ];
            return $result;
        }
        
        $remember = empty($data['remember']) ? null : $data['remember'];
        unset($data['remember']);
        
        $user = empty($remember) ? Sentinel::authenticate($data) : Sentinel::authenticateAndRemember($data);
        if (!$user) {
            Sentinel::bypassCheckpoints(false, ['throttle']);
            
            $result = [
                'success' => false,
                'message' => trans('login.login_incorrect')
            ];
            return $result;
        }
        
        $result = [
            'success' => true,
            'message' => trans('login.login_success'),
            'data' => $user
        ];
        return $result;
    }
    
    public static function checkAdminLogin($data)
    {
        $userLogged = self::checkLogin($data);
        $userLogged['success'] = true;
        if ($userLogged['success']) {
            $user = Sentinel::getUser();
            
            $check = function () use ($user) {
                return $user->inRoles(['admin', 'owner']) || $user->hasAccess('admin.access');
            };
            Sentinel::bypassCheckpoints($check, ['throttle']);
            
            if (!$check) {
                $result = [
                    'success' => false,
                    'message' => trans('login.login_incorrect')
                ];
                Sentinel::logout($user);
                return $result;
            }
            
        }
        
        return $userLogged;
    }
    
    public static function getUsers($data = [])
    {
        $totalRows = UserEntity::count();
        
        switch ($data['order_by']) {
            case 1:
                $orderName = 'email';
                break;
            case 2:
                $orderName = 'first_name';
                break;
            case 3:
                $orderName = 'last_name';
                break;
            default:
                $orderName = 'id';
        }
        
        $sql = UserEntity::select('id', 'email', 'first_name', 'last_name', 'created_at', 'status');
        if (!empty($data['search_value'])) {
            $sql = $sql->where('email', 'like', "%{$data['search_value']}%")
                ->orWhere('first_name', 'like', "%{$data['search_value']}%")
                ->orWhere('last_name', 'like', "%{$data['search_value']}%");
        }
        $query = $sql->skip($data['start'])
            ->take($data['end'])
            ->orderBy($orderName, $data['order_sort'])
            ->get();
        
        $dataReturn = [];
        if (count($query)) {
            $dataReturn['recordsTotal'] = $totalRows;
            $dataReturn['recordsFiltered'] = count($query);
            foreach ($query as $user) {
                $avatar = self::getAvatar($user->id) ? $user->id . '/' . self::getAvatar($user->id) : "default.png";
                $group = $user->roles()->first();
                $groupName = !empty($group) ? $group->name : '';
                
                $dataReturn['data'][] = [
                    0 => $user->id,
                    1 => [
                        url('media/avatars/' . $avatar),
                        $user->first_name . " " . $user->last_name
                    ],
                    2 => $user->email,
                    3 => $user->first_name,
                    4 => $user->last_name,
                    5 => $groupName,
                    6 => $user->created_at,
                    7 => [$user->id, Helper::statusClass($user->status), ucfirst($user->status)],
                    8 => $user->id
                ];
            }
        }
        
        return $dataReturn;
    }
    
    public static function getAllUsers()
    {
        return UserEntity::select(['id', 'email', 'first_name', 'last_name', 'status', 'created_at', 'url'])->get();
    }
    
    public static function updateStatus($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }
        
        // check user exist
        $user = self::getUserById($data['id']);
        if (empty($user)) {
            $result = Helper::resultData(false, trans('site.user_not_exist'));
            return $result;
        }
        
        // save new status to database
        $currentAdmin = Sentinel::check();
        $status = ($user->status == 'active') ? 'unactive' : 'active';
        $user->status = $status;
        $user->updated_by = $currentAdmin->id;
        try {
            $user->save();
            // return success data
            $result = Helper::resultData(true, trans('site.update_success'), ucfirst($status));
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage());
        }
        
        return $result;
    }
    
    /**
     * validator data when login or register
     * @param $data
     * @param $type
     * @return mixed
     */
    function validatorUser($data, $type)
    {
        $rules = [
            'email' => 'required|max:100',
            'password' => 'required|min:5',
        ];
        $more_rules = [];
        if ($type == 'register') {
            $more_rules = [
                'email' => 'required|max:100|unique:user__users,email|email',
                'first_name' => 'required|max:20',
                'last_name' => 'required|max:40',
                'retype-password' => 'required|min:5|same:password'
            ];
        }
        $rules = array_merge($rules, $more_rules);
        $validator = Validator::make($data, $rules);
        return $validator;
    }
    
    /**
     * active mail when register
     * @param $data
     * @param $template
     * @return bool|string
     */
    public static function sendMail($data, $template)
    {
        try {
            Mail::queue($template, $data, function ($message) use ($data) {
                $message->from("lethihongquy24@gmail.com", "vitzoo")->to($data['email'], $data['name'])->subject($data['subject']);
            });
            return true;
        } catch (\Swift_TransportException $ex) {
            return $ex->getMessage();
        } catch (\Swift_RfcComplianceException $ex) {
            return $ex->getMessage();
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
    
    /**
     * login for user
     * @param $data
     * @return mixed
     */
    public static function loginUser($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helpers::resultData(false, trans('site.data_empty'));
            return $result;
        }
        $credentials = [
            'email' => $data['email'],
            'password' => $data['password']
        ];
//        if (empty($data['remember'])) {
//            return Sentinel::authenticate($credentials);
//        }
        return Sentinel::authenticateAndRemember($credentials);
    }
    
    /**
     * check user activation
     * @param $data
     * @return mixed
     */
    public static function checkActivation($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helpers::resultData(false, trans('site.data_empty'));
            return $result;
        }
        if (empty($data['id'])) {
            $user = self::getUserByEmail($data['email'], 'id');
            $data['id'] = $user == null ? null : $user->id;
            
        }
        $users = Sentinel::findById($data['user_id']);
        return Activation::completed($users);
    }
    
    /**
     * register user frontend
     * @param $data request from register form
     * @return mixed
     */
    public static function createUser($data = [])
    {
        // check if submitted data is empty or not
        if (empty($data)) {
            // return error message
            $result = Helpers::resultData(false, trans('site.data_empty'));
            return $result;
        }
        try {
            // if edit role
            if (isset($data['id']) && !empty($data['id'])) {
                // check user is exist or not
                $model = self::getUserById($data['id']);
                // remove data not in use
                unset($data['id']);
                
                if (empty($model)) {
                    // return error message
                    $result = Helper::resultData(false, trans('site.user_not_exist'), null, 1);
                    return $result;
                }
                $user = Sentinel::update($model, $data);
                $result = Helper::resultData(true, trans('site.update_success'), $user);
                // if create new user
            } else {
                $user = Sentinel::registerAndActivate($data);
                $role = Sentinel::findRoleBySlug($data['role']);
                $role->users()->attach($user);
                Sentinel::loginAndRemember($user);
                $result = Helper::resultData(true, trans('site.register_success'), $user);
            }
            // return success data
            
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage(), null, 1);
        }
        return $result;
        
    }
    
    /**
     * create new or update member
     * @param $data
     * @return mixed
     */
    public static function saveUser($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }
        
        // validate the info, create rules for the inputs
        $rules = [
            'group' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'status' => 'required'
        ];
        
        if (empty($data['id'])) {
            $more_rules = [
                'email' => 'required|email|unique:users,email',
                'password' => 'required'
            ];
        } else {
            $more_rules = [
                'email' => 'required|email',
            ];
        }
        $rules = array_merge($rules, $more_rules);
        
        // run the validation rules on the inputs from the form
        $validator = Validator::make($data, $rules);
        
        // if the validator fails, return error message
        if ($validator->fails()) {
            // return validate error message
            $result = Helper::resultData(false, $validator->errors()->first());
            return $result;
        }
        
        // remove data not in use
        unset($data['_token']);
        $group_id = $data['group'];
        unset($data['group']);
        
        try {
            // if edit role
            if (!empty($data['id'])) {
                // check user is exist or not
                $model = self::getUserById($data['id']);
                // remove data not in use
                unset($data['id']);
                
                if (empty($model)) {
                    // return error message
                    $result = Helper::resultData(false, trans('site.user_not_exist'), null, 1);
                    return $result;
                }
                
                $user = Sentinel::update($model, $data);
                // update user group
                if ($group_id != $user->roles()->first()->id) {
                    // remove old group
                    $user->roles()->detach();
                    // assign user to new group
                    $group = GroupModel::getGroupById($group_id);
                    $group->users()->attach($user);
                }
                // if create new user
            } else {
                $user = (in_array($group_id, [1, 2])) ? Sentinel::register($data) : Sentinel::registerAndActivate($data);
                // assign user to group
                $group = GroupModel::getGroupById($group_id);
                $group->users()->attach($user);
            }
            
            // return success data
            $result = Helper::resultData(true, trans('site.update_success'), $user);
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage(), null, 1);
        }
        
        return $result;
    }
    
    /**
     * remove members data
     * @param $data
     * @return array
     */
    public static function removeUser($data)
    {
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helper::resultData(false, trans('site.data_empty'));
            return $result;
        }
        
        // make sure all id values are integers
        $ids = Helper::stringToInt($data['ids']);
        
        try {
            $users = UsersEntity::find($ids);
            if (count($users)) {
                foreach ($users as $user) {
                    // remove avatars
                    $avatars = $user->avatars()->get(['avatar']);
                    if (count($avatars)) {
                        foreach ($avatars as $avatar) {
                            $avatarFile = public_path("media/avatars/{$user->id}/{$avatar}");
                            // if avatar image is exist, remove it
                            if (file_exists($avatarFile)) {
                                unlink($avatarFile);
                            }
                        }
                    }
                    
                    // remove user
                    $user->delete();
                }
                
                // return success data
                return Helper::resultData(true, trans('site.update_success'));
            }
            
            return Helper::resultData(false, trans('site.user_not_exist'));
        } catch (\Exception $e) {
            return Helper::resultData(false, $e->getMessage());
        }
    }
    
    /*
     * Check if it's appropriate to delete one or more users
     * */
    public static function canBeDeleted($arr = [])
    {
        //Check if the input variable is an array (of id's)
        if (is_array($arr) || is_object($arr)) {
            foreach ($arr as $id) {
                //Return false if any of the user is the current logged in admin or they're either owner or admin
                $user = is_array($arr) ? Sentinel::findById($id) : $id;
                if (!empty($user) && ($user->id === Sentinel::getUser()->id || $user->inRoles(['owner', 'admin']))) {
                    return false;
                }
            }
        }
        
        //if nothing can be returned, set default output to true
        return true;
    }
    
    
    /*public static function createUserSocial($data = [])
    {
        $user = new UserEntity();
        $user->email = $data['email'];
        $user->last_name = $data['last_name'];
        $user->first_name = $data['first_name'];
        $user->facebook_id = $data['id'];
        $user->save();
        $avata = new AvatarEntity();
        $avata->user_id = $user->id;
        $avata->url = $data['avatar'];
        $avata->save();
        //
        Activation::create($user);
        $role = Sentinel::findRoleBySlug($data['role']);
        $role->users()->attach($user);
        return ;
        // check data submit is empty or not
        if (empty($data)) {
            // return error message
            $result = Helpers::resultData(false, trans('site.data_empty'));
            return $result;
        }
        try {
            // if edit role
            if (!empty($data['id'])) {
                // check user is exist or not
                $model = self::getUserById($data['id']);
                // remove data not in use
                unset($data['id']);

                if (empty($model)) {
                    // return error message
                    $result = Helper::resultData(false, trans('site.user_not_exist'), null, 1);
                    return $result;
                }
                $user = Sentinel::update($model, $data);
                $result = Helper::resultData(true, trans('site.update_success'), $user);
                // if create new user
            } else {
                $user = Sentinel::register($data);
                Activation::create($user);
                $role = Sentinel::findRoleBySlug($data['role']);
                $role->users()->attach($user);
                $result = Helper::resultData(true, trans('site.register_success'), $user);
            }
            // return success data

        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage(), null, 1);
        }
        return $result;

    }*/
    
    public function getUserByFbId($id)
    {
        $user = UserEntity::where('facebook_id', $id);
        return ($user);
    }
    
    //Create User Facebook
    public function createUserSocial($data = [])
    {
        if ($authUser = $this->getUserByFbId($data['id'])) {
            if (!empty($authUser->email))
                return $authUser;
            if (!empty($fbUser['email'])) {
                if ($authUser->email == $fbUser['email'])
                    return $authUser;
                else {
                    $authUser->email = $fbUser['email'];
                    $authUser->save();
                    return $authUser;
                }
            }
            return $authUser;
        }
        
        if (!empty($fbUser['email']) && $user = $this->getUserByEmail($fbUser['email'])) {
            if (isset($user->deleted_at)) {
                session()->flash('action_unsuccess', trans('home::home.email_acc_failed'));
                return redirect('/');
            }
            if ($user->hasRoles(['admin', 'owner', 'mod']))
                return redirect()->back();
            if (!empty($user->facebook_id)) {
                return $user;
            }
            $user->facebook_id = $fbUser['id'];
            $user->avatar = "http://graph.facebook.com/" . $fbUser['id'] . "/picture?type=normal";
            $user->save();
            return $user;
        }
        $user = new UserEntity();
        $new_user = clone $user;
        $user->full_name = $fbUser->name;
        $user->email = $fbUser->email;
        $user->facebook_id = $fbUser->id;
        $user->avatar = "http://graph.facebook.com/" . $fbUser->id . "/pciture?type=normal";
        $user->created_by = 'facebook';
        $user->language_code = \LaravelLocalization::getCurrentLocale();
        $user->status = 'active';
        $new_user->unset('id');
        $user->save();
        $id = 4;
        if (!$this->getRoleById($id)) $id += 79;
        $user->roles()->attach($id);
        return $user;
    }
    
    //get all user__country
    public static function getAllCountry()
    {
        return CountryEntity::orderBy('Country', 'ASC')->get(['id', 'CountryId', 'locale_code', 'continent_code',
            'continent_name',
            'country_iso_code', 'Country']);
    }
    
    public static function getCity($idProvinces, $idCountry)
    {
        return CityEntity::where('subdivision_1_iso_code', $idProvinces)->where('country_iso_code', $idCountry)->get(['id', 'city']);
    }
    
    public static function getProvinces($countycity)
    {
        return CityEntity::where('country_iso_code', $countycity)->groupBy('subdivision_1_iso_code')->get(['id', 'countryid', 'locale_code', 'country_iso_code', 'country_name', 'city', 'subdivision_1_iso_code', 'subdivision_1_name']);
    }
    
    public static function updateAvatar($data)
    {
        if (empty($data))
            return Helper::resultData(false, trans('site.data_empty'));
        try {

            if (isset($data['user_id']) && ($user = UserEntity::find($data['user_id']))) {
                $old_img = public_path() . Helper::getLinkAvatar() . $user->url;
                $user->url && file_exists($old_img) && unlink($old_img);
                $user->url = $data['avatarName'];
                if ($user->save())
                    return Helper::resultData(true, trans('site.update_avatar_success'), $user);
                return Helper::resultData(false, trans('site.update_avatar_fail'));
            }
            return Helper::resultData(false, trans('site.user_not_exist'));
        } catch (\Exception $e) {
            $result = Helper::resultData(false, $e->getMessage());
            return $result;
        }
    }
    
    public function uploadAvatar($data, $user_id)
    {
        $result = Helper::resultData(false, trans('site.update_fail'));
        if (filesize($data) > 1048576) {
            return Helper::resultData(false, trans('site.size_large'));
        }
        
        $img = Image::make($data->getRealPath());
        $base_ratio = Heper::getWidthHeighAvatar() / Heper::getWidthHeighAvatar();
        $width = $img->width();
        $height = $img->height();
        $ratio = $width / $height;
        if ($ratio > $base_ratio) {
            $width = $height * $base_ratio;
        } else if ($ratio < $base_ratio) {
            $height = $width / $base_ratio;
        }
        $img->crop($width, $height);
        if ($width > Heper::getWidthHeighAvatar()) {
            $img->resize(Heper::getWidthHeighAvatar(), null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        if ($height > Heper::getWidthHeighAvatar()) {
            $img->resize(null, Heper::getWidthHeighAvatar(), function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $destinationPath = base_path() . Helper::getLinkAvatar();
        $file_name = strtolower(uniqid($user_id . '_') . '.jpg');
        try {
            $img->save($destinationPath . $file_name);
        } catch (\Exception $e) {
            return $result;
        }
        if ($user_id) {
            $data = UserModel::updateAvatar(['user_id' => $user_id, 'avatarName' => $file_name]);
            if ($data['success'])
                $result = Helper::resultData(true, trans('site.update_success'), ['url' => $file_name]);
            else {
                $result = Helper::resultData(false, $data['message']);
            }
        }
        return $result;
    }
    
    function incrementInvite($token){
        if (!$token)
            return false;
        $id = base64_decode($token);
        return UserEntity::where('id',"=",$id)->increment('invite_mark');
    }
    
}