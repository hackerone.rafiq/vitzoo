<?php

/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel
 * @version    2.0.9
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class CityEntity extends Model
{

    /**
     * {@inheritDoc}
     */
    protected $table = 'user__city';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'countryid',
        'locale_code',
        'continent_code',
        'continent_name',
        'country_iso_code',
        'country_name',
        'city'
    ];
    public $timestamps = false;

    /**
     * {@inheritDoc}
     */

    public function country()
    {
        return $this->belongsTo(new CountryEntity(), 'country_iso_code', 'country_iso_code');
    }

    public function state()
    {
        return $this->groupBy('subdivision_1_name');
    }
}