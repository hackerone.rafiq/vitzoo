<?php

/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel
 * @version    2.0.9
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Modules\User\Entities;

use Cartalyst\Sentinel\Throttling\EloquentThrottle;
use Cartalyst\Sentinel\Throttling\ThrottleRepositoryInterface;
use Cartalyst\Sentinel\Users\UserInterface;

class ThrottleEntity extends EloquentThrottle implements ThrottleRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'user__throttle';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'ip',
        'type',
    ];

    public function globalDelay()
    {
        // TODO: Implement globalDelay() method.
    }

    public function ipDelay($ipAddress)
    {
        // TODO: Implement ipDelay() method.
    }

    public function userDelay(UserInterface $user)
    {
        // TODO: Implement userDelay() method.
    }

    public function log($ipAddress = null, UserInterface $user = null)
    {
        // TODO: Implement log() method.
    }

    public function setDelay()
    {

    }
}
