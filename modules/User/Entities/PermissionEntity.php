<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class PermissionEntity extends Model
{
    public $table = 'user__permissions';
    protected $fillable = ['code'];
    public $timestamps = false;
}