<?php

/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel
 * @version    2.0.9
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class CountryEntity extends Model
{

    /**
     * {@inheritDoc}
     */
    protected $table = 'user__country';

    protected $fillable = [
        'id',
        'countryid',
        'locale_code',
        'continent_code',
        'continent_name',
        'country_iso_code',
        'country'
    ];
    public $timestamps = false;

    public function cities()
    {
        return $this->hasMany(new CityEntity(),'country_iso_code','country_iso_code');
    }

    public function states()
    {
        return $this->hasMany(new CityEntity(),'country_iso_code', 'country_iso_code')->groupBy('subdivision_1_name');
    }
}