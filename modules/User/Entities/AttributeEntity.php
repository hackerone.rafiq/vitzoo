<?php

/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel
 * @version    2.0.9
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class AttributeEntity extends Model
{

    /**
     * {@inheritDoc}
     */
    protected $table = 'user__attributes';
    public $timestamps = false;
    public $primaryKey = 'id';

    protected $fillable = [
        'id',
        'attribute_name'
    ];

    public function joinUserAttribute($id)
    {
        return $this->hasMany(new UserAttributeEntity(), 'attribute_id', 'id')->where('user_id',$id);
    }
}