<?php

/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel
 * @version    2.0.9
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\LanguageEntity;

class UserAttributeEntity extends Model
{

    /**
     * {@inheritDoc}
     */
    protected $table = 'user__attribute_users';
    public $timestamps = false;
    public $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'attribute_id',
        'values',
        'status'
    ];

    public function joinUser(){
        return $this->belongsTo(new UserEntity(),'user_id','id');
    }

    public function joinAttribute()
    {
        return $this->belongsTo(new AttributeEntity(), 'attribute_id','id');
    }

    public function joinCountry()
    {
        return $this->belongsTo(new CountryEntity(), 'values', 'country_iso_code');
    }

    public function joinCity()
    {
        return $this->belongsTo(new CityEntity(), 'values', 'id');
    }

    public function joinLanguage()
    {
        return $this->belongsTo(new LanguageEntity(), 'values', 'id');
    }
}