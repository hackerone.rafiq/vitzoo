<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\Admin\Models;

use Modules\Admin\Entities\WebsiteEntity;
use Modules\Admin\Entities\StoreEntity;
use Modules\Admin\Entities\ConfigEntity;
use Helper;

class SystemModel
{
    public static function getConfig()
    {
        $config = [];
        $system = ConfigEntity::get();
        foreach ($system as $sys) {
            $config[$sys->title] = $sys->value;
        }

        return $config;
    }
}