<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class ConfigEntity extends Model
{
    public $table = 'system__config';
    public $timestamps = false;
    protected $fillable = ['store_id', 'title', 'value'];

    public function website()
    {
        return $this->belongsTo('Modules\Admin\Entities\WebsiteEntity', 'website_id');
    }
}