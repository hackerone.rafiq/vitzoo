<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class StoreEntity extends Model
{
    public $table = 'system__store';
    protected $fillable = ['website_id', 'code', 'name', 'language_id', 'is_default', 'status', 'created_by', 'updated_by'];

    public function website()
    {
        return $this->belongsTo('Modules\Admin\Entities\WebsiteEntity', 'website_id');
    }

    public function config()
    {
        return $this->hasMany('Modules\Admin\Entities\ConfigEntity', 'store_id');
    }
}