<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class LanguageEntity extends Model
{
    //use Translatable;

    public $table = 'system__languages';
    public $timestamps = false;
    public $translatedAttributes = ['name'];
    protected $fillable = ['code', 'name'];
}