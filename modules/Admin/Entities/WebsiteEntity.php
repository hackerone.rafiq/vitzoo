<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class WebsiteEntity extends Model
{
    public $table = 'system__website';
    protected $fillable = ['code', 'name', 'domain', 'status', 'created_by', 'updated_by'];

    public function stores()
    {
        return $this->hasMany('Modules\Admin\Entities\StoreEntity', 'website_id');
    }

    public function users()
    {
        return $this->hasMany('Modules\User\Entities\UserEntity', 'website_id');
    }
}