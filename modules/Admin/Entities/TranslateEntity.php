<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/10/16
 */

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class TranslateEntity extends Model
{
    public $table = 'system__translates';
    public $timestamps = false;
    protected $fillable = ['name'];
}