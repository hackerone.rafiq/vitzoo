<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale().'/admin',
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'web', 'auth.admin']
], function () {
    Route::get('/', 'Controller@index');
    Route::get('login', 'Controller@login');
    Route::post('login', ['uses' => 'AjaxController@doLogin']);
    Route::get('logout', 'Controller@logout');

    Route::get('users/list', 'Controller@userListing');
    Route::get('users/list_search', 'AjaxController@userList');
    Route::get('users/create_new', ['uses' => 'AjaxController@newUser']);
    Route::get('users/details/{id}', ['uses' => 'AjaxController@detailUser'])->where('id', '[0-9]+');
    Route::get('users/edit/{id}', ['uses' => 'AjaxController@editUser'])->where('id', '[0-9]+');
    Route::post('users/do_update', ['uses' => 'AjaxController@saveUser']);
    Route::post('users/change_status', ['uses' => 'AjaxController@statusUser']);
    Route::post('users/remove', ['uses' => 'AjaxController@removeUser']);
    Route::get('countrydata', 'AjaxController@loadCountryData');

    Route::get('users/group', ['uses' => 'Controller@userGroups']);
    Route::get('users/group/create_new', ['uses' => 'AjaxController@newGroup']);
    Route::get('users/group/edit/{id}', ['uses' => 'AjaxController@editGroup'])->where('id', '[0-9]+');
    Route::post('users/group', ['uses' => 'AjaxController@userGroup']);
    Route::post('users/group/do_update', ['uses' => 'AjaxController@saveGroup']);
    Route::post('users/group/change_status', ['uses' => 'AjaxController@statusGroup']);
    Route::post('users/group/remove', ['uses' => 'AjaxController@removeGroup']);

    Route::get('system/config', 'Controller@systemConfig');
    Route::get('system/cache', 'Controller@systemCache');
});