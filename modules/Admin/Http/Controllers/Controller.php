<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Admin\Http\Controllers;

use Modules\User\Models\GroupModel;
use \Modules\User\Models\UserModel;
use Request, SEOMeta, Theme, Assets, Hashids, Helper, Sentinel;

class Controller extends BaseController
{
    private $titleSuffix;
    private $user;
    private $session;

    public function __construct()
    {
        parent::__construct();

        $this->user = Sentinel::check();
        // get session admin
        $this->session = Helper::getSession('admin');
        // set title suffix
        $this->titleSuffix = !empty($this->config) ? $this->config['title_suffix'] : 'Vitzoo';
        SEOMeta::setTitleDefault($this->titleSuffix);

        Assets::add([
            Helper::getThemePlugins('bootstrap/css/bootstrap.min.css'),
            Helper::getThemePlugins('font-awesome/css/font-awesome.min.css'),
            Helper::getThemePlugins('ionicons/css/ionicons.min.css'),
            Helper::getThemeCss('AdminLTE.min.css'),
            Helper::getThemePlugins('iCheck/square/blue.css'),
            Helper::getThemePlugins('jQuery/jQuery-2.2.0.min.js'),
            Helper::getThemePlugins('bootstrap/js/bootstrap.min.js'),
            Helper::getThemePlugins('fastclick/fastclick.min.js'),
            Helper::getThemePlugins('iCheck/icheck.min.js')
        ]);

        if (!empty($this->user)) {
            Theme::setLayout('modules.admin.layouts.master');

            Assets::add(Helper::getThemeCss('skin-blue.min.css'));
            Assets::add(Helper::getThemePlugins('iCheck/all.css'));
            Assets::add(Helper::getThemePlugins('messi/messi.min.css'));
            Assets::add(Helper::getThemeCss('style.css'));
            Assets::add(Helper::getThemePlugins('messi/messi.min.js'));
            Assets::add(Helper::getThemeJs('app.min.js'));
            Assets::add(Helper::getThemeJs('admin.js'));
        }
    }

    public function index()
    {
        // set title
        SEOMeta::setTitle(trans('admin.dashboard'));

        $data = [
            'admin'     => $this->user
        ];
        return Theme::view('modules.admin.index', $data);
    }

    public function login()
    {
        // check admin logged in or not
        if (!empty($this->user)) {
            // redirect to admin control panel if logged in
            Helper::redirect('admin')->send();
        }
        // set title
        SEOMeta::setTitle(trans('login.login_admincp'));

        return Theme::view('modules.admin.login');
    }

    public function logout()
    {
        // Logs the current user out
        Sentinel::logout($this->user);
        // remove only admin session
        Helper::setSession([], 'admin');

        // redirect admin login page
        return Helper::redirect('admin/login');
    }

    public function userListing()
    {
        $users = new UserModel;
        SEOMeta::setTitle(trans('admin.user_listing'));
        $users = $users->getAllUsers();
        $data = [
            'users' => $users,
            'admin' => $this->user,
        ];
        return Theme::view('modules.admin.users.list', $data);
    }

    public function newUser()
    {
        $user = new UserModel();
        $roles = GroupModel::getGroups();
        $countries=$user->loadCountryData();
        $data = [
            'countries' => $countries,
            'roles' => $roles,
            'current_admin' => $this->user,
        ];
        return Theme::view('modules.admin.users.add', $data)->render();
    }

    public function editUser($id)
    {
        $users = new UserModel();
        $roles = GroupModel::getGroups();
        $user = $users->getUserById($id);
        $countries = $users->loadCountryData();
        $data = [
            'countries' => $countries,
            'roles' => $roles,
            'current_admin' => $this->user,
            'user' => $user
        ];
        return Theme::view('modules.admin.users.add', $data)->render();
    }

    public function userDetails($id)
    {
        if(!Request::ajax() || $this->user->hasAccess('admin.user_update')){
            $result = \Helper::resultData(false, trans('site.no_authorization'));
            return \Helper::ajaxOutput($result);
        }
        $users = new UserModel();
        $user = $users->getUserById($id);
        $data = [
            'current_admin' => $this->user,
            'user' => $user
        ];
        return Theme::view('modules.admin.users.view', $data)->render();
    }

    public function userGroups()
    {
        $users = new UserModel;
        SEOMeta::setTitle('User Groups');
        $roles = GroupModel::getGroups();
        $data = [
            'groups' => $roles,
            'admin' => $this->user,
        ];
        return Theme::view('modules.admin.users.groups', $data);
    }

    public function notFound()
    {

    }
}