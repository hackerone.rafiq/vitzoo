<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/9/16
 */

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Models\SystemModel;
use Request, Helper, Sentinel;

class BaseController extends Controller
{
    public $config;

    public function __construct()
    {
        // load store config to use
        $this->setConfig();
    }

    /**
     * get current config
     */
    private function setConfig()
    {
        // load default config to use
        if (empty($this->config)) {
            $this->config = SystemModel::getConfig();
        }
    }
}