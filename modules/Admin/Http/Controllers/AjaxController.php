<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: VietProjectGroup
 * Date: 4/12/16
 */

namespace Modules\Admin\Http\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Modules\User\Models\UserModel;
use Modules\User\Models\GroupModel;

use Request, Helper, Input, Theme;

class AjaxController extends BaseController
{
    private $user;
    private $session;
    private $userModel;

    public function __construct()
    {
        if (!Request::ajax()) {
            echo '404 not found';
            exit;
        }

        parent::__construct();

        $this->user = Sentinel::check();
        // get session admin
        $this->session = Helper::getSession('admin');

        $this->userModel = new UserModel;
    }

    public function doLogin()
    {
        $post_data = Input::all();
        $result = UserModel::checkAdminLogin($post_data);
        return Helper::json($result);
    }

    /**
     * ajax display create new group form
     * @return mixed
     */
    public function newGroup()
    {
        return Theme::view('modules.admin.users.group_update');
    }

    /**
     * ajax display edit group form
     * @param $id
     * @return mixed
     */
    public function editGroup($id)
    {
        if (empty($id) || !is_numeric($id)) {
            $result = Helper::resultData(false, trans('site.data_empty'));
            return Helper::json($result);
        }

        $data = [
            'user'      => $this->user,
            'groups'    => GroupModel::getGroupById($id)
        ];

        return Theme::view('modules.admin.users.group_update', $data);
    }

    /**
     * ajax create new/update group
     * @return mixed
     */
    public function saveGroup()
    {
        $post_data = Input::all();
        $result = GroupModel::saveGroup($post_data);

        // push message to flash session
        Helper::setSessionMessage($result);

        return Helper::json($result);
    }

    /**
     * ajax delete group(s)
     * @return mixed
     */
    public function removeGroup()
    {
        $post_data = Input::all();
        $result = GroupModel::removeGroup($post_data);

        // push message to flash session
        Helper::setSessionMessage($result);

        return Helper::json($result);
    }

    /**
     * ajax update user status
     * @return mixed
     */
    public function statusUser()
    {
        $post_data = Input::all();
        $result = UserModel::updateStatus($post_data);

        return $result;
    }

    /**
     * ajax get user list data to generate
     * @return mixed
     */
    public function userList()
    {
        $data_get = Input::get();
        $request = Helper::convertDataTables($data_get);
        $response = $this->userModel->getUsers($request);

        return Helper::json($response);
    }

    /**
     * ajax display create new member form
     * @return mixed
     */
    public function newUser()
    {
        $groupUser = [];
        $groups = GroupModel::getGroups();
        foreach ($groups as $group) {
            $groupUser[$group->id] = $group->title;
        }

        $data = [
            'groups'   => $groupUser
        ];

        return Theme::view('modules.admin.users.user_update', $data);
    }

    /**
     * ajax display edit member form
     * @param $id
     * @return mixed
     */
    public function editUser($id)
    {
        if (empty($id) || !is_numeric($id)) {
            $result = Helper::resultData(false, trans('site.data_empty'));
            return Helper::json($result);
        }

        $user = UserModel::getUserById($id);
        if (empty($user)) {
            $result = Helper::resultData(false, trans('site.user_not_exist'));
            return Helper::json($result);
        }

        $groupUser = [];
        $groups = GroupModel::getGroups();
        foreach ($groups as $group) {
            $groupUser[$group->id] = $group->name;
        }

        $years = [];
        $thisYear = date('Y', time());
        for ($i = ($thisYear - 10); $i > 1976; $i--) {
            $years[$i] = $i;
        }

        $data = [
            'user'      => $user,
            'user_avatar'    => UserModel::getAvatar($id),
            'groups'    => $groupUser,
            'birthday'  => explode('-', $user->birthday),
            'years'     => $years
        ];

        return Theme::view('modules.admin.users.user_update', $data);
    }

    /**
     * ajax create new/update user
     * @return mixed
     */
    public function saveUser()
    {
        $post_data = Input::all();
        $result = UserModel::saveUser($post_data);

        // push message to flash session
        Helper::setSessionMessage($result, 'admin');

        return Helper::json($result);
    }

    /**
     * ajax delete user(s)
     * @return mixed
     */
    public function removeUser()
    {
        $post_data = Input::all();
        $result = UserModel::removeUser($post_data);

        // push message to flash session
        Helper::setSessionMessage($result);

        return Helper::json($result);
    }

    function loadCountryData(Request $r)
    {
        $data = $r::input('data');
        $division = $r::input('division');
        return Helper::json($this->userModel->loadCountryData($data,$division));
    }
}