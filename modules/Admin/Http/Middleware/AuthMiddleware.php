<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: gsi
 * Date: 5/16/16
 */

namespace Modules\Admin\Http\Middleware;

use Closure, Request, Sentinel, Helper;

class AuthMiddleware
{
    /**
     * Handle user access admin dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Sentinel::check();
        // check admin logged in
        if ((empty($user) || (!$user->inRoles(['admin', 'owner']) && !$user->hasAccess('admin.access'))) && Request::segment(3) != 'login') {
            Helper::redirect('admin/login');
        }

        return $next($request);
    }
}