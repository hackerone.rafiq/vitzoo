<?php

namespace App\Helpers\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class HelpersServiceProvider extends ServiceProvider
{
    public function register()
    {
        App::bind("helpers", function() {
            return new App\Helpers\Helpers;
        });
    }
}