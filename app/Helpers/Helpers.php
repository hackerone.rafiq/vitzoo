<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: Test Online
 * Date: 3/28/16
 */

namespace App\Helpers;

use Faker\Provider\DateTime;
use Request;

class Helpers
{
    public function version(){
        return 100;
    }

    public function getport($port)
    {
        return $port;
    }
    
    public function geturl($port)
    {
        return secure_url('') . ':' . $this->getport($port);
    }
    
    public function currentDomain($suffix = true)
    {
        if (empty($_SERVER["SERVER_NAME"])) {
            return false;
        }
        
        $currentDomain = $_SERVER["SERVER_NAME"];
        $exDomain = explode('.', $currentDomain);
        $total = count($exDomain);
        $domain = $exDomain[$total - 2];
        
        if ($suffix) {
            $domain .= '.' . end($exDomain);
        }
        
        return $domain;
    }
    
    /**
     * show result data in array
     * @param bool|false $success
     * @param string $message
     * @param null $data
     * @param int $flag
     * @return array
     */
    public function resultData($success = FALSE, $message = '', $data = NULL, $flag = 0)
    {
        $result = [
            'success' => $success,
            'message' => $message,
            'data' => $data,
            'flag' => $flag
        ];
        
        return $result;
    }
    
    /**
     * pretty json encode
     * @param $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function response($data, $statusCode = 200)
    {
        return response()->json($data, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }
    
    public function json($data, $statusCode = 200)
    {
        if (0 === strpos(request()->headers->get('Content-Type'), 'application/xml')) {
            function array_to_xml($data, \SimpleXMLElement &$xml_data)
            {
                foreach ($data as $key => $value) {
                    if (is_array($value)) {
                        if (is_numeric($key)) {
                            $key = 'item_' . ++$key;
                        }
                        $subnode = $xml_data->addChild($key);
                        array_to_xml($value, $subnode);
                    } else {
                        $xml_data->addChild($key, $value);
                    }
                }
            }
            
            $xml_data = new \SimpleXMLElement('<?xml version="' . env('VERSION') . '" encoding="UTF-8"?><root></root>');
            array_to_xml($data, $xml_data);
            return $xml_data->asXML();
        }
        return $this->response($data, $statusCode);
    }
    
    /**
     * redirect url with localization
     * @param $url
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function redirect($url)
    {
        return redirect(\LaravelLocalization::getCurrentLocale() . '/' . $url)->send();
    }
    
    /**
     * get url with localization
     * @param $url
     * @return string
     */
    public function url($url = '', $secure = false)
    {
        $url = \LaravelLocalization::getCurrentLocale() . '/' . $url;
        return $secure ? secure_url($url) : url($url);
    }
    
    /**
     * data mapping array to object
     * @param $obj
     * @param array $data
     * @return mixed
     */
    public function mapData($obj, $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $obj->{$key} = $value;
            }
        }
        return $obj;
    }
    
    /**
     * remove folder and all sub-folders or files in it
     * @param $dir
     */
    function removeFolder($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->removeFolder($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
    
    /** Map string array to Int array.
     * @param array $arr
     * @return array
     */
    public function stringToInt($array = [], $ex = ',')
    {
        if (!is_array($array)) {
            $array = explode($ex, $array);
        }
        
        $int = [];
        foreach ($array as $arr) {
            $int[] = (int) $arr;
        }
        return $int;
    }
    
    public function getThemeAssets($asset)
    {
        return \Theme::asset(\Theme::getActive() . '::' . $asset);
    }
    
    public function getThemeCss($css)
    {
        return $this->getThemeAssets('css/' . $css);
    }
    
    public function getThemeJs($js)
    {
        return $this->getThemeAssets('js/' . $js);
    }
    
    public function getThemePlugins($plugin)
    {
        return $this->getThemeAssets('plugins/' . $plugin);
    }
    
    public function getThemeImg($img)
    {
        return $this->getThemeAssets('img/' . $img);
    }
    
    public function getAvatar($img)
    {
        return url('media/avatars/' . $img);
    }
    
    public function getPostImg($img)
    {
        return url('media/news/' . $img);
    }
    
    public function getPostThumbImg($img)
    {
        return url('media/news/thumbs/' . $img);
    }
    
    /**
     * compares given route name with current route name
     * @param $routes
     * @param string $output
     * @return string
     */
    public function activeRoutes($routes, $output = 'active')
    {
        $path = \Route::getCurrentRoute()->getPath();
        if (!is_array($routes)) {
            return (ends_with($path, $routes)) ? $output : '';
        }
        
        foreach ($routes as $route) {
            if (ends_with($path, $route)) return $output;
        }
        
        return '';
    }
    
    /**
     * compare given segment with values
     * @param $segment
     * @param $value
     * @return string
     */
    public function activeSegments($segment, $values, $output = 'active')
    {
        if (!is_array($values)) {
            return \Request::segment($segment) == $values ? $output : '';
        }
        
        foreach ($values as $value) {
            if (\Request::segment($segment) == $values) return $output;
        }
        
        return '';
    }
    
    /**
     * detects if the given string is found in the current URL.
     * @param  string $string
     * @param  string $output
     * @return boolean
     */
    public function activeMatch($string, $output = 'active')
    {
        return (\Request::is($string)) ? $output : '';
    }
    
    public function getSession($session = 'frontend')
    {
        if (!session()->has($session)) {
            session($session, []);
        }
        
        return session($session);
    }
    
    public function setSession($data = [], $sessName = 'frontend')
    {
        if (!empty($data)) {
            $session = $this->getSession($sessName);
            foreach ($data as $key => $value) {
                if (!empty($session[$key])) {
                    unset($session[$key]);
                }
                $session[$key] = $value;
            }
        } else {
            $session = $data;
        }
        
        // remove all sessions
        session()->forget($sessName);
        // set sessions to new data
        session()->put($sessName, $session);
    }
    
    /**
     * set result message to flash session
     * @param $result
     */
    public function setSessionMessage($result)
    {
        // push message to flash session
        if ($result['success']) {
            $message = ['success' => $result['message']];
        } else {
            if ($result['flag']) {
                $message = ['error' => $result['message']];
            } else {
                $message = ['warning' => $result['message']];
            }
        }
        session()->flash('message', $message);
    }
    
    public function adminLoginRedirect()
    {
        $session = $this->getSession('admin');
        $url = !empty($session['redirect']) ? $session['redirect'] : $this->url('admin');
        $this->setSession(['redirect' => null], 'admin');
        
        return $url;
    }
    
    /**
     * protect email from spam and crawler
     * @param $email
     * @return string
     */
    public function hideEmail($email, $mailto = false)
    {
        $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
        $key = str_shuffle($character_set);
        $cipher_text = '';
        $id = 'e' . rand(1, 999999999);
        
        for ($i = 0; $i < strlen($email); $i += 1) $cipher_text .= $key[strpos($character_set, $email[$i])];
        
        $script = 'var a="' . $key . '";var b=a.split("").sort().join("");var c="' . $cipher_text . '";var d="";';
        $script .= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
        
        if ($mailto) {
            $script .= 'document.getElementById("' . $id . '").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
        } else {
            $script .= 'document.getElementById("' . $id . '").innerHTML=d';
        }
        
        $script = "eval(\"" . str_replace(array("\\", '"'), array("\\\\", '\"'), $script) . "\")";
        $script = '<script type="text/javascript">/*<![CDATA[*/' . $script . '/*]]>*/</script>';
        
        return '<span id="' . $id . '"></span>' . $script;
    }
    
    /**
     * get current admin logged-in
     * @return bool
     */
    public function currentAdmin()
    {
        $session = session('admin');
        if (!empty($session['user'])) {
            return $session['user'];
        }
        
        return false;
    }
    
    /**
     * get current user logged-in
     * @return bool
     */
    public function currentUser()
    {
        $session = session('frontend');
        if (!empty($session['user'])) {
            return $session['user'];
        }
        
        return false;
    }
    
    public function shortContent($content, $length = 200)
    {
        $content = strip_tags($content);
        if (strlen($content) > $length) {
            $ex = explode(' ', $content);
            $short = '';
            foreach ($ex as $string) {
                $short .= $string;
                if (strlen($short) >= $length) break;
                $short .= ' ';
            }
            
            return $short . '...';
        }
        
        return $content;
    }
    
    /**
     * get status class by status code
     * @param $status
     * @return string
     */
    public function statusClass($status)
    {
        switch ($status) {
            case 'delete':
            case 'unactive':
            case 'disabled':
                return 'label-danger';
            case 'edit':
            case 'pending':
                return 'label-warning';
            case 'active':
            default:
                return 'label-success';
        }
    }
    
    /**
     * display admin message
     * @param $message
     * @return string
     */
    public function adminMessage($message)
    {
        $display = '';
        foreach ($message as $key => $value) {
            if ($key == 'success') {
                $display = '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i> ' . $value . '
                </div>';
            } elseif ($key == 'error') {
                $display = '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-ban"></i> ' . $value . '
                </div>';
            } else {
                $display = '<div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-warning"></i> ' . $value . '
                </div>';
            }
            break;
        }
        
        return $display;
    }
    
    /**
     * remap datatables data to correct array
     * @param array $data
     * @return array
     */
    public function convertDataTables($data = [])
    {
        $result = [];
        if (!empty($data)) {
            $result = [
                'draw' => $data['draw'],
                'start' => (int) $data['start'],
                'end' => (int) $data['length'],
                'order_by' => $data['order'][0]['column'],
                'order_sort' => $data['order'][0]['dir'],
                'search_value' => $data['search']['value'],
                'search_regex' => $data['search']['regex']
            ];
        }
        
        return $result;
    }
    
    function identifier($str, $separator = 'dash', $lowercase = false)
    {
        $foreign_characters = [
            '/ä|æ|ǽ/' => 'ae',
            '/ö|œ/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/À|Á|Â|Ã|Ä|Å|Ǻ|Ā|Ă|Ą|Ǎ|А/' => 'A',
            '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª|а/' => 'a',
            '/Б/' => 'B',
            '/б/' => 'b',
            '/Ç|Ć|Ĉ|Ċ|Č|Ц/' => 'C',
            '/ç|ć|ĉ|ċ|č|ц/' => 'c',
            '/Ð|Ď|Đ|Д/' => 'D',
            '/ð|ď|đ|д/' => 'd',
            '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě|Е|Ё|Э/' => 'E',
            '/è|é|ê|ë|ē|ĕ|ė|ę|ě|е|ё|э/' => 'e',
            '/Ф/' => 'F',
            '/ф/' => 'f',
            '/Ĝ|Ğ|Ġ|Ģ|Г/' => 'G',
            '/ĝ|ğ|ġ|ģ|г/' => 'g',
            '/Ĥ|Ħ|Х/' => 'H',
            '/ĥ|ħ|х/' => 'h',
            '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ|И/' => 'I',
            '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı|и/' => 'i',
            '/Ĵ|Й/' => 'J',
            '/ĵ|й/' => 'j',
            '/Ķ|К/' => 'K',
            '/ķ|к/' => 'k',
            '/Ĺ|Ļ|Ľ|Ŀ|Ł|Л/' => 'L',
            '/ĺ|ļ|ľ|ŀ|ł|л/' => 'l',
            '/М/' => 'M',
            '/м/' => 'm',
            '/Ñ|Ń|Ņ|Ň|Н/' => 'N',
            '/ñ|ń|ņ|ň|ŉ|н/' => 'n',
            '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ|О/' => 'O',
            '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º|о/' => 'o',
            '/П/' => 'P',
            '/п/' => 'p',
            '/Ŕ|Ŗ|Ř|Р/' => 'R',
            '/ŕ|ŗ|ř|р/' => 'r',
            '/Ś|Ŝ|Ş|Š|С/' => 'S',
            '/ś|ŝ|ş|š|ſ|с/' => 's',
            '/Ţ|Ť|Ŧ|Т/' => 'T',
            '/ţ|ť|ŧ|т/' => 't',
            '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ|У/' => 'U',
            '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ|у/' => 'u',
            '/В/' => 'V',
            '/в/' => 'v',
            '/Ý|Ÿ|Ŷ|Ы/' => 'Y',
            '/ý|ÿ|ŷ|ы/' => 'y',
            '/Ŵ/' => 'W',
            '/ŵ/' => 'w',
            '/Ź|Ż|Ž|З/' => 'Z',
            '/ź|ż|ž|з/' => 'z',
            '/Æ|Ǽ/' => 'AE',
            '/ß/' => 'ss',
            '/Ĳ/' => 'IJ',
            '/ĳ/' => 'ij',
            '/Œ/' => 'OE',
            '/ƒ/' => 'f',
            '/Ч/' => 'Ch',
            '/ч/' => 'ch',
            '/Ю/' => 'Ju',
            '/ю/' => 'ju',
            '/Я/' => 'Ja',
            '/я/' => 'ja',
            '/Ш/' => 'Sh',
            '/ш/' => 'sh',
            '/Щ/' => 'Shch',
            '/щ/' => 'shch',
            '/Ж/' => 'Zh',
            '/ж/' => 'zh'
        ];
        
        $str = preg_replace(array_keys($foreign_characters), array_values($foreign_characters), $str);
        
        $replace = ($separator == 'dash') ? '-' : '_';
        
        $trans = [
            '&\#\d+?;' => '',
            '&\S+?;' => '',
            '\s+' => $replace,
            '[^a-z0-9\-\._]' => '',
            $replace . '+' => $replace,
            $replace . '$' => $replace,
            '^' . $replace => $replace,
            '\.+$' => ''
        ];
        
        $str = strip_tags($str);
        
        foreach ($trans as $key => $val) {
            $str = preg_replace("#" . $key . "#i", $val, $str);
        }
        
        if ($lowercase === true) {
            if (function_exists('mb_convert_case')) {
                $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
            } else {
                $str = strtolower($str);
            }
        }
        
        $str = preg_replace('#[^a-z 0-9~%.:_\-]#i', '', $str);
        
        return trim(stripslashes($str));
    }
    
    
    public function getStrDoubleQuote($str)
    {
        if (preg_match_all('~(["\'])([^"\']+)\1~', $str, $arr)) {
            return $arr[2];
        }
        
        return [];
    }
    
    public function configInput($input = 'text', $name = '', $val = '', $id = '', $class = 'form-control')
    {
        switch ($input) {
            case 'radio':
                $data = '<input class="' . $class . '" type="radio" name="' . $name . '" value="' . $val . '" />';
                break;
            case 'language':
                $data = \Form::select($name, ['vn' => 'VietNam', 'en' => 'English'], $val, ['class' => $class, 'id' => $id]);
                break;
            case 'cache':
                $yInput = Form::radio($name, 'yes', ($val == 'yes') ? true : false);
                $yInput = Form::radio($name, 'no', ($val == 'no') ? true : false);
                $data = '<div class="field switch">
                    <input type="radio" id="' . $id . '_enable" name="' . $name . '" value="yes" checked />
                    <input type="radio" id="' . $id . '_disable" name="' . $name . '" value="no" />
                    <label for="' . $id . '_enable" class="cb-enable selected"><span>Enable</span></label>
                    <label for="' . $id . '_disable" class="cb-disable"><span>Disable</span></label>
                </div>';
                break;
            default:
                $data = '<input class="' . $class . '" name="' . $name . '" value="' . $val . '" />';
        }
        
        return $data;
    }
    
    public function getValidatorError($validator)
    {
        $html = '';
        $errors = $validator->errors();
        foreach ($errors->all() as $error) {
            if (is_array($error)) {
                foreach ($error as $item) {
                    $html .= '<p>' . $item . '</p>';
                }
            } else
                $html .= '<p>' . $error . '</p>';
        }
        return $this->resultData(false, $html);
    }
    
    public function create_unique()
    {
        $agent = Request::header('User-Agent');
        $ip = Request::ip();
        // Read the user agent, IP address, current time, and a random number:
        
        $data = $agent . $ip . time() . rand();
        // Return this value hashed via sha1
        return strtolower(sha1($data));
    }
    
    /**
     * Escape special character
     * @param array $data
     * @return array
     */
    function arrayEscape($data = [])
    {
        foreach ($data as $k => $v) {
            $data[$k] = e($v);
        }
        return $data;
    }
    
    /**
     * decrypt text from nodejs
     * @param $data
     * @return string
     */
//    public function decrypt($data)
//    {
//        $encryptKey = 'hn6IMw1XW3O6KXmf';
//        $iv = 'HHF]8gM<5iyoZyOT';
//        $blocksize = 16;
//
//        return $this->unpad(mcrypt_decrypt("rijndael-128",
//            $encryptKey,
//            hex2bin($data),
//            MCRYPT_MODE_CBC, $iv), $blocksize);
//    }
    
    public static function decrypt($message)
    {
        $key = 'hn6IMw1XW3O6KXmfhn6IMw1XW3O6KXmf';
//        $ivsize = openssl_cipher_iv_length('aes-256-cbc');
        $iv = 'HHF]8gM<5iyoZyOT';
//        $iv = mb_substr($message, 0, $ivsize, '8bit');
//        $ciphertext = mb_substr($message, $ivsize, null, '8bit');
        
        return openssl_decrypt($message, 'aes-256-cbc', $key, 0, $iv);
    }
    
    private function unpad($data, $blocksize)
    {
        $len = mb_strlen($data);
        $pad = ord($data[$len - 1]);
        if ($pad && $pad < $blocksize) {
            $pm = preg_match('/' . chr($pad) . '{' . $pad . '}$/', $data);
            if ($pm) {
                return mb_substr($data, 0, $len - $pad);
            }
        }
        return $data;
    }
    
    function cantonPairing($k1, $k2)
    {
        return (($k1 + $k2) * ($k1 + $k2 + 1) + $k2) / 2;
    }
    
    function uniqueRoom($x, $y)
    {
        if ($x < $y) {
            return md5($this->cantonPairing($x, $y));
        }
        if ($x > $y) {
            return md5($this->cantonPairing($y, $x));
        }
        return '';
    }
    
    function getDatetimeChat($intDatetime)
    {
        $lang = \LaravelLocalization::getCurrentLocale();
        //$intDatetime = strtotime($intDatetime);
        if ($intDatetime) {
            if ($lang == 'en') {
                return trans('vitzoo.' . date("l", $intDatetime)) . ', ' . trans('vitzoo.' . date("F", $intDatetime)) . ' ' . date("d, Y", $intDatetime);
            }
            return trans('vitzoo.' . date("l", $intDatetime)) . ', ngày ' . date("d", $intDatetime) . ' ' . trans('vitzoo.' . date("F", $intDatetime)) . ' năm ' . date("Y", $intDatetime);
        }
        return false;
    }
    
    function getHourMinutes($intDatetime, $timezone)
    {
        $time = $intDatetime + (-60) * $timezone;
        return date("h", $time) . ':' . date("i", $time) . ' ' . date("A", $time);
    }
    
    public function getDaysInMonth($month, $year)
    {
        $days = ($month === 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31));
        return $days;
    }
    
    public function getNameFile($string, $condition)
    {
        if (!$condition)
            $condition = " ";
        return explode($condition, $string);
    }
    
    public function uniqueRoomGroup()
    {
        $time = time();
        $time = md5('Vitpr' . $time);
        return $time;
    }
    
    public function errors($error, $message, $code)
    {
        $error = [
            'error' => $error,
            'error_description' => $message
        ];
        return self::json($error, $code);
    }
    
    function stripVowelAccent($str)
    {
        $foreign_characters = [
            '/ä|æ|ǽ/' => 'ae',
            '/ö|œ/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/À|Á|Â|Ã|Ä|Å|Ǻ|Ā|Ă|Ą|Ǎ|А|Ạ|Ả|Ã|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ằ|Ắ|Ặ|Ẳ|Ẵ/' => 'A',
            '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª|а|ạ|ả|ã|ầ|ấ|ậ|ẩ|ẫ|ằ|ắ|ặ|ẳ|ẵ/' => 'a',
            '/Б/' => 'B',
            '/б/' => 'b',
            '/Ç|Ć|Ĉ|Ċ|Č|Ц/' => 'C',
            '/ç|ć|ĉ|ċ|č|ц/' => 'c',
            '/Ð|Ď|Đ|Д/' => 'D',
            '/ð|ď|đ|д/' => 'd',
            '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě|Е|Ё|Э|Ẹ|Ẻ|Ẽ|Ề|Ế|Ệ|Ể|Ễ/' => 'E',
            '/è|é|ê|ë|ē|ĕ|ė|ę|ě|е|ё|э|ẹ|ẻ|ẽ|ề|ế|ệ|ể|ễ/' => 'e',
            '/Ф/' => 'F',
            '/ф/' => 'f',
            '/Ĝ|Ğ|Ġ|Ģ|Г/' => 'G',
            '/ĝ|ğ|ġ|ģ|г/' => 'g',
            '/Ĥ|Ħ|Х/' => 'H',
            '/ĥ|ħ|х/' => 'h',
            '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ|И|Ị|Ỉ|Ĩ/' => 'I',
            '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı|и|ị|ỉ|ĩ/' => 'i',
            '/Ĵ|Й/' => 'J',
            '/ĵ|й/' => 'j',
            '/Ķ|К/' => 'K',
            '/ķ|к/' => 'k',
            '/Ĺ|Ļ|Ľ|Ŀ|Ł|Л/' => 'L',
            '/ĺ|ļ|ľ|ŀ|ł|л/' => 'l',
            '/М/' => 'M',
            '/м/' => 'm',
            '/Ñ|Ń|Ņ|Ň|Н/' => 'N',
            '/ñ|ń|ņ|ň|ŉ|н/' => 'n',
            '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ|О|O|Ọ|Ỏ|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/' => 'O',
            '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º|о|ọ|ỏ|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/' => 'o',
            '/П/' => 'P',
            '/п/' => 'p',
            '/Ŕ|Ŗ|Ř|Р/' => 'R',
            '/ŕ|ŗ|ř|р/' => 'r',
            '/Ś|Ŝ|Ş|Š|С/' => 'S',
            '/ś|ŝ|ş|š|ſ|с/' => 's',
            '/Ţ|Ť|Ŧ|Т/' => 'T',
            '/ţ|ť|ŧ|т/' => 't',
            '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ|У|Ụ|Ủ|Ũ|Ừ|Ứ|Ự|Ữ/' => 'U',
            '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ|у|ụ|ủ|ũ|ừ|ứ|ự|ử|ữ/' => 'u',
            '/В/' => 'V',
            '/в/' => 'v',
            '/Ý|Ÿ|Ŷ|Ы|Ỳ|Ỵ|Ỷ|Ỹ/' => 'Y',
            '/ý|ÿ|ŷ|ы|ỳ|ỵ|ỷ|ỹ/' => 'y',
            '/Ŵ/' => 'W',
            '/ŵ/' => 'w',
            '/Ź|Ż|Ž|З/' => 'Z',
            '/ź|ż|ž|з/' => 'z',
            '/Æ|Ǽ/' => 'AE',
            '/ß/' => 'ss',
            '/Ĳ/' => 'IJ',
            '/ĳ/' => 'ij',
            '/Œ/' => 'OE',
            '/ƒ/' => 'f',
            '/Ч/' => 'Ch',
            '/ч/' => 'ch',
            '/Ю/' => 'Ju',
            '/ю/' => 'ju',
            '/Я/' => 'Ja',
            '/я/' => 'ja',
            '/Ш/' => 'Sh',
            '/ш/' => 'sh',
            '/Щ/' => 'Shch',
            '/щ/' => 'shch',
            '/Ж/' => 'Zh',
            '/ж/' => 'zh'
        ];
        $str = preg_replace(array_keys($foreign_characters), array_values($foreign_characters), $str);
        $str = strtolower($str);
        return $str;
    }
    
    function getWidthHeighAvatar()
    {
        return 250;
    }
    
    function getLinkAvatar()
    {
        return '/themes/default/asset_frontend/img_avatar/';
    }
    
    function getEncodeKey()
    {
        return 'Tzx#vgC+/M1a<(#-(o7&1@8Z6dR3;O.j';
    }
    
    function getDataURI($image)
    {
        $content = $this->getImageRawData($image);
        if ($content)
            return 'data:image/' . $content[1] . ';base64,' . base64_encode($content[0]);
        return $image;
    }
    
    function getImageRawData($image_url)
    {
        $content = false;
        if (function_exists('curl_init')) {
            $opts = array();
            $http_headers = array();
            $http_headers[] = 'Expect:';
            
            $opts[CURLOPT_URL] = $image_url;
            $opts[CURLOPT_HTTPHEADER] = $http_headers;
            $opts[CURLOPT_CONNECTTIMEOUT] = 10;
            $opts[CURLOPT_TIMEOUT] = 60;
            $opts[CURLOPT_HEADER] = FALSE;
            $opts[CURLOPT_BINARYTRANSFER] = TRUE;
            $opts[CURLOPT_VERBOSE] = FALSE;
            $opts[CURLOPT_SSL_VERIFYPEER] = FALSE;
            $opts[CURLOPT_SSL_VERIFYHOST] = 2;
            $opts[CURLOPT_RETURNTRANSFER] = TRUE;
            $opts[CURLOPT_FOLLOWLOCATION] = TRUE;
            $opts[CURLOPT_MAXREDIRS] = 2;
            $opts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;
            
            # Initialize PHP/CURL handle
            $ch = curl_init();
            curl_setopt_array($ch, $opts);
            $content = curl_exec($ch);
            
            # Close PHP/CURL handle
            curl_close($ch);
        }// use file_get_contents
        elseif (ini_get('allow_url_fopen')) {
            if(file_exists($image_url))
                $content = file_get_contents($image_url);
        }
        
        if ($content) {
            $urlParts = pathinfo($image_url);
            $extension = $urlParts['extension'];
            return [$content, $extension];
        }
        # Return results
        return false;
    }

    function checkInArray($array,$value = null,$key = null){
        foreach ($array as $i){
            if($value == $i->$key){
                if($i->status == 'waiting')
                    return 'waiting';
                return 'friend';
            }
        }
        return false;
    }
    
    function returnVersion(){
        return rand(1,1000);
    }
    
    function convertDatetimeObject($string,$format = 'Y-m-d H:i:s'){
        $date = \DateTime::createFromFormat($format, $string);
        return $date;
    }
}