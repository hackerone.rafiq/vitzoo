<?php
/**
 * Created by intersteller.
 * Email: intersteller@gmail.com
 * Project: Global Online Studio
 * Date: 4/14/16
 */

namespace App\Http\Middleware;

use Closure;

class HttpsProtocol
{
    public function handle($request, Closure $next)
    {
        $request->setTrustedProxies([$request->getClientIp()]);
        if (!$request->secure()) {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}