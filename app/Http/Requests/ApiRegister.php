<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use Illuminate\Contracts\Validation\Validator;
class ApiRegister extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|unique:user__users,email|email',
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:40',
            'password' => 'required|max:20|min:5'
        ];
    }
    
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}