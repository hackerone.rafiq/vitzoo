var mysql = require('mysql')
    , async = require('async')

var PRODUCTION_DB = 'vitzoo'
    , TEST_DB = 'app_test_database'

exports.MODE_TEST = 'mode_test'
exports.MODE_PRODUCTION = 'mode_production'

var state = {
    pool: null,
    mode: null
};

exports.connect = function (mode, done) {
    state.pool = mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: '',
        database: mode === exports.MODE_PRODUCTION ? PRODUCTION_DB : TEST_DB
    })

    state.mode = mode
}

exports.get = function () {
  var pool = state.pool;
  if (!pool) return 'Missing database connection.';
  pool.getConnection(function (err, connection) {
    //run the query
    connection.query('select * from user__users', function (err, rows) {
      if (err) throw err;
      else {
        console.log(rows);
      }
    });

    connection.release();//release the connection
  });
};