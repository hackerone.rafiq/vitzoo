"use strict";
// Load required modules
var https = require("https");              // http server core module
var express = require("express");           // web framework external module
//var socketIo = require("socket.io");        // web socket external module
var easyrtc = require("easyrtc");               // EasyRTC external module
var roomUser = {};
var port = '5555';
var host = '127.0.0.1';
var userEasyrtc = {};
var stringGlobal = require("./controller/GetStringController");
var rtc_users = new Map();
var listUserCall = {};
require('dotenv').config({path: '../.env'});
/*
 * -------------------
 * Express
 * -------------------
 */
// Set process name
process.title = "node-easyrtc";

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var app = require('express')();
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var _ = require("underscore");
var Redis = require('ioredis');
var key = '';
var cert = '';
let config = fs.readFileSync("config.json"), connection;
config = JSON.parse(config);
if (config) {
    host = config.host;
    key = config.key;
    cert = config.cert;
}
var options = {
    key: fs.readFileSync(key, 'utf8'),
    cert: fs.readFileSync(cert, 'utf8'),
    ciphers: 'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES256-SHA384',
    honorCipherOrder: true,
    secureProtocol: 'TLSv1_2_method',
    requestCert: true
};

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});
app.set('host', '127.0.0.1');
app.set('port', port);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
/*
 * -------------------
 * Redis
 * -------------------
 */
var redis = require("redis");

var roomUser_request = redis.createClient();
roomUser_request.subscribe('saveUserOfRoom');

roomUser_request.on('message', function (channel, message) {
    message = JSON.parse(message);
    if (typeof message != 'undefined' && message != false)
        roomUser[message.room_id] = {users: message.user_id, relative: message.relative};
});

var calls = redis.createClient();
calls.get('calls', function (chn, value) {
    if (value)
        listUserCall = JSON.parse(value);
});

let deleteCalls = redis.createClient();
/*
 * -------------------
 * easyrtc
 * -------------------
 */
// Start Express http server on port 8080
var server = https.createServer(options, app),
    io = require('socket.io')(server, {'pingTimeout': 86400000});

server.listen(port, function (err, res) {
    if (err) throw err;
    console.log("Server up and running..." + host + ':' + port);
});

// Start Socket.io so it attaches itself to Express server
/*var socketServer = socketIo.listen(webServer, {"log level": 1});*/

/*
 * -------------------
 * socket.io
 * -------------------
 */
var socketioJwt = require("socketio-jwt");
var chatRtc = io.of('/easyrtc');

chatRtc.use(socketioJwt.authorize({
    secret: env('ENCODE_KEY'),
    handshake: true
}));

chatRtc.on('connection', function (socket) {
    
    var user = socket.decoded_token;
    if (user) {
        user.user_id = calculateTime(user.time, user.user_id);
        
    }
    var name = user.last_name + ' ' + user.first_name;
    var user_id = user.user_id;
    if (user_id) {
        listUserCall[user_id] = true;
        calls.set('calls', JSON.stringify(listUserCall));
        let arrayRedis = {
            user_id: user_id,
            type: 'save',
            value: true
        };
        deleteCalls.publish('deleteListCall', JSON.stringify(arrayRedis));
    }
    
    var callingWith;
    /*Generic Event*/
    
    if (user_id) {
        socket.join(user_id);
    }
    
    socket.on('call accepted', function (data) {
        socket.to(user_id).emit('call accepted', data)
    });
    
    // socket.to(70).emit('toggle stream state', user)
    socket.on('hang up', function (data) {
        socket.to(data.user_id).emit('hang up', data);
    });
    socket.on('toggle stream state', function (data) {
        if (user_id && data) {
            socket.to(parseInt(data.target_id)).emit('toggle stream state', data);
        }
    });
    
    /*socket.on('joinOtherClient', function (data) {
     if (user && user.user_id) {
     if (typeof data != 'undefined' && typeof data['destination_id'] != 'undefined') {
     data['user_id'] = user.user_id;
     socket.to(data['destination_id']).emit('joinOtherClient', data);
     }
     }
     });*/
    
    socket.on('sendAudio', function (data) {
        if (user && user.user_id) {
            stringGlobal.getFriends(user_id, function (friend_ids) {
                callingWith = data.receiver_id;
                if (~friend_ids.indexOf(callingWith)) {
                    // data['easyrtcidOfUser'] = easyrtcidOfUser(data.receiver_id);
                    data.initiator = user.user_id;
                    data.name = name;
                    socket.emit('sendAudio', data);
                    socket.to(data.receiver_id).emit('sendAudio', data);
                    user.time = Date.now();
                }
            });
        }
    });
    
    socket.on('disconnect', function () {
        delete userEasyrtc[socket.easyrtc_id];
        delete listUserCall[user_id];
        calls.set('calls', JSON.stringify(listUserCall));
        deleteCalls.publish('deleteListCall', JSON.stringify({user_id:user_id,type:'delete',value:true}));
    })
});

function calculateTime(time, data) {
    return (Date.now() - (new Date(time))) / 1000 / 86400 < 1 && data;
}
/* Get environmental variables */
function env(varname) {
    if (typeof process === 'undefined' || !process.env || !process.env[varname]) {
        return false
    }
    return process.env[varname]
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
}

if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (searchString, position) {
        var subjectString = this.toString();
        if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
            position = subjectString.length;
        }
        position -= searchString.length;
        var lastIndex = subjectString.lastIndexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
    };
}

function easyrtcidOfUser(recipient_id) {
    var users = [];
    if (userEasyrtc) {
        for (var i in userEasyrtc) {
            if (userEasyrtc[i].user_id === recipient_id)
                users.push(i);
        }
    }
    return users;
}
// Start EasyRTC server
easyrtc.listen(app, io, null, function (err, rtcRef) {
    rtcRef.setOption('demosEnable', false);
    rtcRef.setOption('appIceServers',
        [
            {url: "stun:stun.l.google.com:19302"},
            {url: "stun:stun.sipgate.net"},
            {url: "stun:217.10.68.152"},
            {url: "stun:stun.sipgate.net:10000"},
            {url: "stun:217.10.68.152:10000"},
            {
                url: "turn:153.92.43.161:6969",
                username: "vitzoo",
                credential: "vitzoo@2016"
            },
        ]
    );
    rtcRef.setOption("roomDefaultEnable", false);
    rtcRef.setOption("easyrtcidRegExp", /^\/?#?[a-z0-9_.-]{1,32}$/i);
    rtcRef.events.on("roomCreate", function (appObj, creatorConnectionObj, roomName, roomOptions, callback) {
        appObj.events.defaultListeners.roomCreate(appObj, creatorConnectionObj, roomName, roomOptions, callback);
    });
});

easyrtc.setOption("logLevel", '0');

// Overriding the default easyrtcAuth listener, only so we can directly access its callback
easyrtc.events.on("easyrtcAuth", function (socket, easyrtcid, msg, socketCallback, callback) {
    easyrtc.events.defaultListeners.easyrtcAuth(socket, easyrtcid, msg, socketCallback, function (err, connectionObj) {
        if (err || !msg.msgData || !connectionObj) {
            callback(err, connectionObj);
            return;
        }
        connectionObj.setField("credential", msg.msgData.credential, {"isShared": false});
        // console.log("[" + easyrtcid + "] Credential saved!", connectionObj.getFieldValueSync("credential"));
        
        callback(err, connectionObj);
    });
});

// To test, lets print the credential to the console for every room join!
easyrtc.events.on("roomJoin", function (connectionObj, roomName, roomParameter, callback) {
    easyrtc.events.defaultListeners.roomJoin(connectionObj, roomName, roomParameter, callback);
});

