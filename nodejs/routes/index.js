var express = require('express');
var router = express.Router();
var Room =require('../models/Room');
var Message = require('../models/Message');
var time = new Date();
var User = require('../models/User');

router.get('/', function(req, res) {

    if (req.session.user) {
        //delete req.user.password;
        User.find({}, function(err, users) {
            if (err) throw err;

            // object of all the users
            if(users)
                //res.render('pages_temp/index_temp',{title: 'express',user:users,session: req.session});
                res.render('pages_temp/index_temp',{title: 'express',user:users});

        });
    }
    else
        res.render('pages_temp/index_none_temp',{title: 'express'});
});

router.post('/login', function(req, res) {
    //res.send('abc');
    var username = req.body.username;
    var password = req.body.password;

    User.findOne({username:username,password:password},function(err,user){
        if(err){
            console.log(err);
            return res.status(500).send();
        }
        if(!user){
            console.log('khong tim thay user');
            res.send({message:'unsuccessful'});
        }
        else{
            req.session.user = user;
            //console.log(user);
            res.send({message:'successful',user_id:user._id});
        }

    });

});

router.post('/register', function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;

    var newuser = new  User();
    newuser.username = username;
    newuser.password = password;
    newuser.email = email;
    User.findOne({username:username},function(err,user) {
        if(err) throw err;
        if(!user){
            newuser.save(function (err, saveUser) {
                if (err) throw err;
                room_name = saveUser.username + saveUser.id;
                saveRoom(room_name);
                res.redirect('/');
            });
        }
        else{
            res.send("username ton tai, vui long chon username khac");
        }

    });
});

function saveRoom(room_name){
    var new_room = new Room;
    new_room.room_name = room_name;
    new_room.created_time = time.getTime();
    new_room.save(function(err,saveRoom){
        if(err){
            console.log(err);
            return res.status(500).send();
        }

    });
    //return new_room;
}

router.get('/logout', function(req, res){
    req.session.destroy(function(){
        res.redirect('/');
    });
});


module.exports = router;