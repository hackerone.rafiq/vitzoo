module.exports = (function () {
    var emotes,
        codesMap = new Map(),
        primaryCodesMap = new Map(),
        regexp,
        metachars = /[[\]{}()*+?.\\|^$\-,&#\s]/g,
        entityMap;
    var self = {};
    
    function objectToMap(obj) {
        var map = new Map();
        Object.keys(obj).forEach(key => {
            if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
                map.set(key, objectToMap(obj[key]));
            } else {
                map.set(key, obj[key]);
            }
        });
        return map;
    }
    
    /**
     * Define emoticons set.
     *
     * @param {Object} data
     */
    self.define = function (data) {
        var codes, patterns = [];
        emotes = objectToMap(data);
        for (let [name, value] of emotes.entries()) {
            codes = value.get('codes');
            codes.forEach(function (code, i) {
                codesMap.set(code, name);
                // codesMap.set(escape(code), name);
                if (i === 0) {
                    primaryCodesMap.set(code, name);
                }
            });
        }
        for (let code of codesMap.keys()) {
            patterns.push('(' + code.replace(metachars, "\\$&") + ')');
        }
        regexp = new RegExp(patterns.join('|'), 'g');
    };
    
    /**
     * Replace emoticons in text.
     *
     * @param {String} text
     * @param {Function} [fn] optional template builder function.
     */
    self.parse = function (text, fn) {
        return text.replace(regexp, function (code) {
            var name = codesMap.get(code);
            return (fn || self.tpl)(name, code, emotes.get(name).get('title'));
        });
    };
    
    /**
     * Get primary emoticons as html string in order to display them later as overview.
     *
     * @param {Function} [fn] optional template builder function.
     * @return {String}
     */
    self.toString = function (fn) {
        var str = '';
        for (let [code, name] of primaryCodesMap.entries()) {
            str += (fn || self.tpl)(name, code, emotes.get(name).get('title'));
        }
        return str;
    };
    
    /**
     * Build html string for emoticons.
     * @param {String} name
     * @param {String} code
     * @param {String} title
     * @return {String}
     */
    self.tpl = function (name, code, title) {
        return '<span class="emoticon emoticon-' + name + '" title="' + title + '">' +
            code + '</span>';
    };
    return self;
})();