"use strict";
let port = process.env.PORT || 4040;
let host = '';
let userConnection = new Map();
let connectedUsersLogin = new Set();
let name_db = '';
let url = require('url');
let subscribed_users = {};
let roomUser = {};
let promise = require('bluebird');
let userOnlineStatus = {};
let listUserCall = {};
let key = '';
let cert = '';
let stringGlobal = require("./controller/GetStringController");
require('dotenv').config({path: '../.env'});


/*
 * -------------------
 * Express
 * -------------------
 */
let app = require('express')();
let bodyParser = require('body-parser');
let fs = require('fs');

let config = fs.readFileSync("config.json"), connection;
config = JSON.parse(config);
if(config){
    host = config.host;
    key = config.key;
    cert = config.cert;
}
/*let options = {
 key: fs.readFileSync('/etc/nginx/ssl/gos.key'),
 cert: fs.readFileSync('/etc/nginx/ssl/gos.crt'),
 requestCert: true
 };*/
let options = {
    key: fs.readFileSync(key, 'utf8'),
    cert: fs.readFileSync(cert, 'utf8'),
    requestCert: true
};


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});
app.set('host', host);
app.set('port', port);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.get('/', function (req, res) {
    res.send('vitzoo');
});
/*
 *   Write log into log.txt
 * */
let util = require('util');
let logFile = fs.createWriteStream('server.log', {flags: 'a'});
let logStdout = process.stdout;
/* Write log to server.log */
console.log = function () {
    logFile.write(util.format.apply(null, arguments) + '\n');
    logStdout.write(util.format.apply(null, arguments) + '\n');
};
let errFile = fs.createWriteStream('error.log', {flags: 'a'});
/* Write errors to error.log */
console.error = function () {
    errFile.write(util.format.apply(null, arguments) + '\n');
    logStdout.write(util.format.apply(null, arguments) + '\n');
};
/*
 * -------------------
 * Socket Connections
 * -------------------
 */
let socketioJwt = require("socketio-jwt");
let server = require('https').createServer(options, app),
    io = require('socket.io')(server, {'pingTimeout': 86400000});
server.listen(port, function (err, res) {
    if (err) throw err;
    console.log("Server up and running..." + host + ':' + port);
});
let chatText = io.of('/chat');
/*function sendSocketEmitMod(index, data, functionName) {
 io.to(index).emit(functionName, data);
 }*/
/*
 * -------------------
 * Redis
 * -------------------
 */
let redis = require("redis");
let calls = redis.createClient();
/*sender*/
calls.get('calls', function (chn, value) {
    if (value)
        listUserCall = JSON.parse(value);
});
/*
 *  listUserCall[id] = true
 *  redis.set('listCall', listUserCall)
 * */


/**
 *
 */
let deleteListCall = redis.createClient();

deleteListCall.subscribe('deleteListCall');

deleteListCall.on('message', function (channel, message) {
    if (!message) return;
    message = JSON.parse(message);
    if (message.type == 'delete') {
        delete listUserCall[message.user_id];
    } else {
        listUserCall[message.user_id] = message.value;
    }
});

/**
 *
 */
let subscription = redis.createClient();
subscription.subscribe('user');

subscription.on('message', function (channel, message) {
    if (message) {
        let user = JSON.parse(message);
        subscribed_users[user.user_id] = user;
    }
});

let chat = require('./controller/ChatController.js');
let rtc_socket = require('./controller/RtcController.js');

let friend_request = redis.createClient();

friend_request.subscribe('addfriend');
friend_request.on('message', function (channel, message) {
    message = JSON.parse(message);
    if (typeof message === 'object') {
        friendNotification(message);
    }
});

let roomUser_request = redis.createClient();
roomUser_request.subscribe('saveUserOfRoom');
roomUser_request.on('message', function (channel, message) {
    if (message) {
        message = JSON.parse(message);
        roomUser[message.room_id] = {users: message.user_id, relative: message.relative};
        let tem_user = message['user_id'];
        Object.keys(tem_user).forEach(function (item) {
            chatText.to(tem_user[item]['user_id']).emit('appendRelative', message);
        });
    }
});

let fileServer = redis.createClient();
fileServer.subscribe('saveFile');
/* Emit messages after uploading file */
fileServer.on('message', function (channel, message) {
    if (!message) return;
    let data = JSON.parse(message);
    if (data && data['content']) {
        /* Save file message to database */
        saveTextFile(data);
    }
});

let updateAvatar = redis.createClient();
updateAvatar.subscribe('updateAvatar');
updateAvatar.on('message', function (channel, message) {
    if (!message) return;
    let data = JSON.parse(message);
    if (data) {
        let room_id = data.room_id,
            url = data.url,
            room_users = data.target_users;
        delete data.target_users;
        if (room_id) {
            room_users.forEach(function (user) {
                chatText.to(user.user_id).emit('change group picture', data);
            })
        }
    }
});

let new_friend = redis.createClient();
new_friend.subscribe('friend_added');
new_friend.on('message', function (channel, message) {
    if (!message) return;
    let data = JSON.parse(message);
    chatText.to(data.user_id).emit('change status', {user_id: data.target_id, status: 'online', mood: false});
    chatText.to(data.target_id).emit('change status', {user_id: data.user_id, status: 'online', mood: false});
});
/*
 * -------------------
 * mongoose
 * -------------------
 */

let mongoose = require('mongoose');
let autoIncrement = require('mongoose-auto-increment');
mongoose.Promise = promise;
// connect mongodb


if (config) {
    name_db = config.mongodb;
    connection = mongoose.connect(name_db, function (err, res) {
        if (err) {
            console.log('ERROR connecting to: ' + name_db + '. ' + err);
        } else {
            console.log('Succeeded connected to: ' + name_db);
        }
    });
}

// increment index of mongodb
autoIncrement.initialize(connection);
var Message = require('./models/MessageModel');
var MessageStatus = require('./models/MessageStatusModel');
var chatRoom = require('./models/RoomUserModel');
var Room = require('./models/RoomModel');

/*var  CryptoJS = require("crypto-js");*/
/*
 * -------------------
 * mysql
 * -------------------
 */
let mysql = require('mysql');
let models = require('./models');
let StatusModel = models.StatusModel;

/*
 * -------------------
 * connect socket
 * -------------------
 */

chatText.use(socketioJwt.authorize({
    // secret: 'NfL2LW[nQTgTs)Y[B{#!w-`!',
    secret: env('ENCODE_KEY'),
    handshake: true
}));
chatText.on('connection', function (socket) {
    let user = socket.decoded_token;
    let user_id = user.user_id;
    /*stringGlobal.getListStatus(user.user_id,userOnlineStatus);*/
    /*Generic Event*/
    
    user.user_id = calculateTime(user.time, user.user_id);
    if (user && user.user_id) {
        userConnection.set(socket.id, user);
        socket.join(user.user_id);
        connectedUsersLogin.add(user.user_id);
        /*send user online/offline*/
        StatusModel.findOne({where: {user_id: user_id}}).then(function (status) {
            if (status) {
                userOnlineStatus[user_id] = {status: status.status, mood: status.mood};
                stringGlobal.broadcastFriend(user, socket, connectedUsersLogin, 'online', userOnlineStatus);
                setTimeout(function () {
                    stringGlobal.broadcastFriend(user, socket, connectedUsersLogin, 'online', userOnlineStatus);
                }, 1000);
            }
        }).catch(function (err) {
            console.log(err);
        });
    }
    
    rtc_socket(socket, listUserCall, user);
    
    chat(socket, subscribed_users, roomUser, {
        message: Message,
        messageStatus: MessageStatus,
        chatRoom: chatRoom,
        room: Room
    }, userOnlineStatus, connectedUsersLogin);
    
    socket.on('detectOnline', function (data) {
        StatusModel.findOne({where: {user_id: user_id}}).then(function (status) {
            if (status) {
                userOnlineStatus[user_id] = {status: status.status, mood: status.mood};
                stringGlobal.broadcastFriend(user, socket, connectedUsersLogin, 'online', userOnlineStatus);
                setTimeout(function () {
                    stringGlobal.broadcastFriend(user, socket, connectedUsersLogin, 'online', userOnlineStatus);
                }, 1000);
            }
        }).catch(function (err) {
            console.log(err);
        });
    });
    
    socket.on('disconnect', function () {
        //Remove this user when they close the site
        userConnection.delete(socket.id);
        if (user_id && !checkObjectDuplication(userConnection, user_id, 'user_id')) {
            //Remove this user from online list when they close everything related to the site
            stringGlobal.broadcastFriend(user, socket, connectedUsersLogin, 'offline', userOnlineStatus);
            connectedUsersLogin.delete(user_id);
            delete subscribed_users[user_id];
            delete userOnlineStatus[user_id];
        }
    });
});

/**
 * check user exists in array
 * @param obj
 * @param data
 * @param e
 * @returns {boolean}
 */
function checkObjectDuplication(obj, data, e) {
    if (typeof obj !== 'object' || !obj) return false;
    if (obj.get(e) === data) return true;
    for (let o of obj.values()) {
        if (o[e] === data) return true;
    }
    
    return false;
}
function calculateTime(time, user_id) {
    return (Date.now() - (new Date(time))) / 1000 / 86400 < 1 && user_id;
}

function env(varname) {
    if (typeof process === 'undefined' || !process.env || !process.env[varname]) {
        return false
    }
    return process.env[varname]
}

function friendNotification(message) {
    if (typeof message != 'undefined') {
        chatText.to(message.user_id).emit('sendNotice', message);
    }
}

function saveTextFile(data) {
    let new_message = Message();
    new_message.user_id = parseInt(data.user_id);
    new_message.content = stringGlobal.encrypt(data['content']);
    new_message.is_file = data['is_file'];
    new_message.room_id = data['room_id'];
    if (data['flagcall'])
        new_message.flagcall = data['flagcall'];
    new_message.save()
    /* If saving succeeds */
        .then(function (mess) {
            data.date = new Date(new_message.created_at);
            let room_id = mess.room_id;
            let user_id = mess.user_id;
            let users = data['room_users'];
            users.forEach(function (user) {
                    if (data['is_file'] != 2 && data['is_file'] != 3){
                        data.type = "file";
                        chatText.to(user).emit('uploadFileServer', data);
                    }
                    if (user !== user_id) {
                        MessageStatus.findOne({room_id: room_id, user_id: user}, function (err, doc) {
                            let $count = {countMessage: 1, room_id: room_id};
                            // doc is a Document
                            if (err)
                                throw err.message;
                            if (!doc) {
                                let new_message = MessageStatus();
                                new_message.room_id = room_id;
                                new_message.message_id = mess['_id'];
                                new_message.user_id = user;
                                new_message.is_read = 1;
                                new_message.save();
                            } else {
                                if (!doc.is_read) {
                                    doc.message_id = mess._id;
                                }
                                doc.is_read++;
                                doc.save();
                                $count.countMessage = doc.is_read;
                            }
                            if (data['is_file'] != 2 && data['is_file'] != 3)
                                chatText.to(user).emit('countMessage', $count);
                        });
                    }
                }
            );
        }).catch(function (err) {
        throw err;
    });
}