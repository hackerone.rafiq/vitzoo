var fs = require('fs');
var https = require('https');

var express = require('express');
var app = express();

var options = {
    key: fs.readFileSync('./ssl/key.key'),
    cert: fs.readFileSync('./ssl/cert.crt')
};
var serverPort = 5050;
app.get('/', function (req, res) {
    console.log('abc');
    res.send('abc');
});
var server = https.createServer(options, app);
var io = require('socket.io')(server);

io.on('connection', function (socket) {
    console.log('new connection');
});

server.listen(serverPort, function () {
    console.log('server up and running at %s port', serverPort);
});
/*var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.listen(5050, function () {
    console.log('server up and running at %s port', 5050);
});

app.get('/', function (req, res) {
    res.send('xxxxxxxxxxxxxxx');
    console.log('ccccccccc');
});

io.on('connection', function (socket) {
    console.log(123);
});*/