"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var sequelize = new Sequelize("vitzoo", "root", "", {
    host: "127.0.0.1",
    dialect: 'mysql',
    pool: {
        max: 9,
        min: 0,
        idle: 10000
    },
    logging: false
});

var models = [
    'UserModel',
    'FriendModel',
    'StatusModel',
    'NoticeModel',
];

models.forEach(function (model) {
    module.exports[model] = sequelize.import(__dirname + '/' + model);
});

module.exports['sequelize'] = sequelize;