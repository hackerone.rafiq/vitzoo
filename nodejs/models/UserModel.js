module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user__users', {
        first_name: DataTypes.STRING,
        last_name: DataTypes.STRING,
        url : DataTypes.STRING,
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: true,
        underscored: false,
        tableName: 'user__users'
    });
    // return User;
};