module.exports = function (sequelize, DataTypes) {
    var Friend = sequelize.define('chat__friends', {
        user_id: DataTypes.INTEGER,
        friend_id: DataTypes.INTEGER,
        status:DataTypes.STRING
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: true,
        underscored: false,
        tableName: 'chat__friends'
    });
    return Friend;
};