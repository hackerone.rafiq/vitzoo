/**
 * Created by levy on 4/7/2016.
 */
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var collection = 'chat__messages';

var messageSchema = new mongoose.Schema({
    user_id:0,
    content:{type:String},
    flagcall:0,
    is_file:0,/*is_file : 2 call video,audio, 1 file, 0 message,3 miss call*/
    room_id: {type: String},
    date: {type: Date}
},{
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}
});

//userSchema.plugin(autoIncrement.plugin,collection );
messageSchema.plugin(autoIncrement.plugin, {
    model: collection,
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

var Message = mongoose.model(collection,messageSchema);
module.exports = Message;