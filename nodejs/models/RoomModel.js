/**
 * Created by levy on 4/7/2016.
 */
var mongoose = require('mongoose');
var collection = 'chat__rooms';

var messageSchema = new mongoose.Schema({
    _id: {type: String, unique: true},
    title: {type: String},
    is_public:0,
    room_admin:0,
    type:{type:String}
}, {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}
});

var Room = mongoose.model(collection, messageSchema);
module.exports = Room;