module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user__chat_status', {
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        status: DataTypes.STRING,
        mood: DataTypes.STRING
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: true,
        underscored: false,
        tableName: 'user__chat_status'
    });
};