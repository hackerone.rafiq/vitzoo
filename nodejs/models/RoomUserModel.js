/**
 * Created by levy on 4/7/2016.
 */
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var collection = 'chat__room_users';

var messageSchema = new mongoose.Schema({
    room_id: {type: String},
    user_id: 0,
    new_message_date: 0
}, {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}
});

messageSchema.plugin(autoIncrement.plugin, {
    model: collection,
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

var RoomUser = mongoose.model(collection, messageSchema);
module.exports = RoomUser;