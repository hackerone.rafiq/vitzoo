module.exports = function (sequelize, DataTypes) {
    var Notice = sequelize.define('chat__notices', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        user_id: DataTypes.INTEGER,
        friend_id: DataTypes.INTEGER,
        is_read: DataTypes.INTEGER,
        is_confirm: DataTypes.INTEGER,
        is_click: DataTypes.INTEGER,
        room_id:DataTypes.STRING,
        type: {
            type: DataTypes.ENUM,
            values: ['addfriend','newmessage','addgroup','confirm']
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps: true,
        underscored: false,
        tableName: 'chat__notices'
    });
    return Notice;
};