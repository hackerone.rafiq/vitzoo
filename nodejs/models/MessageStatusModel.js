/**
 * Created by levy on 4/7/2016.
 */
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var collection = 'chat__message_status',
    Schema = mongoose.Schema;

var messageSchema = new mongoose.Schema({
    message_id:0,
    user_id:0,
    is_read:0,
    room_id: {type: String}
});

//userSchema.plugin(autoIncrement.plugin,collection );
messageSchema.plugin(autoIncrement.plugin, {
    model: collection,
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

var MessageStatus = mongoose.model(collection,messageSchema);
module.exports = MessageStatus;