let util = require('util');
let fs = require('fs');
let logFile = fs.createWriteStream('server.log', {flags: 'a'});
let logStdout = process.stdout;
console.log = function () {
    logFile.write(util.format.apply(null, arguments) + '\n');
    logStdout.write(util.format.apply(null, arguments) + '\n');
};
let errFile = fs.createWriteStream('error.log', {flags: 'a'});
console.error = function () {
    errFile.write(util.format.apply(null, arguments) + '\n');
    logStdout.write(util.format.apply(null, arguments) + '\n');
};

var stringGlobal = require("./GetStringController");
var encrypt = stringGlobal.encrypt;
var decrypt = stringGlobal.decrypt;
var models = require('../models');
var UserModel = models.UserModel;
var FriendModel = models.FriendModel;
var StatusModel = models.StatusModel;
var NoticeModel = models.NoticeModel;
var request = require('request');
var sequelize = models.sequelize;

var Message, MessageStatus, ChatRoom, Room;

let emoji = require('node-emoji');
let emoticons = require('../emoticons');
// let emotes = require('../emoji-parser');
// emotes.define(emoticons);
let emojiParser = require('../emojis');
let parser = emojiParser.parse;
let reverseParser = emojiParser.reverseParse;
emojiParser = null;
let redis = require("redis");
let http = require('http');
let https = require('https');
let scrape = require('html-metadata');
require('dotenv').config({path: '../.env'});
let globalTimeout = {};
var timeout = 70;
module.exports = function (socket, arrayUser, arrayRoom, mongoose, userStatus, connected) {
    // registration related behaviour goes here...
    let user = socket.decoded_token;
    let user_id = user.user_id;
    /*Generic Event*/
    let name = user.last_name + ' ' + user.first_name;
    globalTimeout[user_id] = false;
    if (mongoose) {
        Message = mongoose['message'];
        MessageStatus = mongoose['messageStatus'];
        ChatRoom = mongoose['chatRoom'];
        Room = mongoose['room'];
    }
    
    var onevent = socket.onevent;
    socket.onevent = function (packet) {
        var args = packet.data || [];
        onevent.call(this, packet);
        packet.data = ["*"].concat(args);
        onevent.call(this, packet);
    };
    socket.on("*", function (event, data) {
        
    });
    
    socket.on('lost messages', function (data) {
        if (typeof data === 'object' && data) {
            stringGlobal.addInterator(data);
            for (var [i, v] of data) {
                if (!arrayRoom[i]) {
                    findRoomUsers(i, user_id, '', arrayRoom, function (i) {
                        getLastestMessages(i, data[i], user_id, arrayRoom, function (messages) {
                            sendLostMessages(socket, messages, user_id);
                        })
                    })
                }
            }
        }
    });
    
    socket.on('change mood', function (data) {
        if (typeof data !== 'string') return;
        if (!userStatus[user_id] || typeof userStatus[user_id] !== 'object') return;
        data = stringGlobal.htmlEntities(data);
        StatusModel.update({mood: data}, {
            where: {user_id: user_id}
        }).then(function (models) {
            if (models) {
                userStatus[user_id].mood = data;
                stringGlobal.getFriends(user_id, function (friend_ids) {
                    let online_friend = stringGlobal.array_intersect(connected, friend_ids);
                    if (online_friend) {
                        for (let friend of online_friend) {
                            let room_id = stringGlobal.uniqueRoom(user_id, friend);
                            socket.to(friend).emit('change mood', {user: user_id, mood: data, room_id: room_id});
                        }
                    }
                })
            }
        }).catch(function (err) {
            throw err;
        });
    });
    
    socket.on('socketSendText', function (data) {
        delete data['token'];
        if (!arrayRoom) arrayRoom = {};
        if (!arrayRoom[data['room_id']]) {
            findRoomUsers(data['room_id'], user_id, data.target_id, arrayRoom, function () {
                renderDataContent(data, function () {
                    saveMessage(data, user, arrayRoom, socket);
                })
            })
        }
        else if (user && data && user.user_id && arrayRoom[data['room_id']] && arrayRoom[data['room_id']]['relative']) {
            renderDataContent(data, function () {
                saveMessage(data, user, arrayRoom, socket);
            });
        }
    });
    
    socket.on('changeStatus', function (data) {
        if (!userStatus[user_id] || typeof userStatus[user_id] !== 'object') return;
        if (typeof data != 'undefined' && data && data.status && data.name && user_id) {
            data.status = parseInt(data.status);
            data.status = data.status > 1 ? data.status + 1 : data.status;
            if (data.status > 4) return;
            StatusModel.update({status: data.status}, {
                where: {user_id: user_id}
            }).then(function (models) {
                // title will now be 'foooo' but description is the very same as before
                if (models) {
                    userStatus[user_id].status = getStatus(data.status);
                    stringGlobal.broadcastFriend(user, socket, connected, 'changeStatus', userStatus);
                }
            }).catch(console.log);
        }
    });
    
    socket.on('typing', function (data) {
        if (!arrayRoom) arrayRoom = {};
        if (user && data && user.user_id && arrayRoom[data['room_id']] && arrayRoom[data['room_id']]['relative']) {
            if (arrayRoom[data['room_id']]['relative'] !== 'nonechat') {
                let users = arrayRoom[data['room_id']].users;
                if (Array.isArray(users)) {
                    let inRoom = users.some(function (user) {
                        if (user_id === user.user_id) return true;
                    });
                    if (inRoom) {
                        users.forEach(function (user) {
                            if (user_id !== user.user_id) {
                                socket.to(user.user_id).emit('typing', {
                                    room_id: data.room_id,
                                    user_id: user_id,
                                    name: name
                                });
                            }
                        });
                    }
                }
            }
        }
    });
    
    socket.on('remove user', function (data) {
        if (!data.target_id) return;
        socket.emit('change status', {user_id: data.target_id, status: 'offline', mood: false, room_id: data.room_id});
        socket.to(user_id).emit('change status', {
            user_id: data.target_id,
            status: 'offline',
            mood: false,
            room_id: data.room_id
        });
        socket.to(data.target_id).emit('change status', {user_id: user_id, status: 'offline', mood: false})
    });
    
    socket.on('count notification message', function (type) {
        if (type == 'notice')
            countNotifiIsRead(user_id, socket);
        else if (type == 'message')
            countMessageIsRead(user_id, socket);
        else {
            countNotifiIsRead(user_id, socket);
            countMessageIsRead(user_id, socket);
        }
    });
    
    socket.on('updateIsRead', function (data) {
        let room_id = data.room_id;
        let type = data.type;
        if (type == 'message')
            updateIsReadMessage(user_id, room_id, socket);
        else
            updateIsReadNotice(user_id, socket);
    });
    
    socket.on('deleteGroup', function (data) {
        var room_id = data['room_id'];
        if (!data['type'])
            user_id && room_id && deleteGroup(user_id, room_id, arrayRoom, socket);
        if (data['type'] == 'deleteUserGroup') {
            var friend_id = data['friend_id'];
            var data = {
                room_id: room_id,
                user_id: friend_id
            };
            socket.to(friend_id).emit('deleteGroup', data);
        }
        
    });
    
    socket.on('update flagcall', function (data) {
        data.user_id = user_id;
        data.url = user.url;
        data.fullname = name;
        updateMessageFlagCall(data,socket);
    });
    socket.on('createGroup', function (data) {
        socket.to(data.friend_id).emit('createGroup', data);
    });
};

function countNotifiIsRead(user_id, socket) {
    NoticeModel.findAndCountAll({
        where: {user_id: user_id, is_read: 0}
    }).then(function (result) {
        let $data = {type: 'notification', count: result.count};
        socket.to(user_id).emit('count notification message', $data);
        socket.emit('count notification message', $data);
    });
}

function countMessageIsRead(user_id, socket) {
    MessageStatus.find({user_id: user_id, is_read: {$gt: 0}})
        .count(function (err, count) {
            let $data = {type: 'message', count: count};
            socket.to(user_id).emit('count notification message', $data);
            socket.emit('count notification message', $data);
        });
}

function updateIsReadMessage(user_id, room_id, socket) {
    if (!user_id || !room_id)
        return;
    MessageStatus.update({user_id: user_id, room_id: room_id}, {
        is_read: 0,
        message_id: 0
    }, {multi: true}).then(function () {
        let $data = {type: 'message', room_id: room_id};
        socket.to(user_id).emit('updateIsRead', $data);
    }).catch(console.error);
}

function updateIsReadNotice(user_id, socket) {
    if (!user_id)
        return;
    NoticeModel.update({is_read: 1}, {
        where: {user_id: user_id, is_read: 0}
    }).then(function () {
        let $data = {type: 'notice'};
        socket.to(user_id).emit('updateIsRead', $data);
    }).catch(console.error);
}

function toInt(val) {
    return val === +val ? parseInt(val, 10) : 0;
}

function getStatus(val) {
    switch (val) {
        case 4:
            return 'busy';
        case 2:
        case 3:
            return 'offline';
        default:
            return 'online';
    }
}

function getObjectMessage(data, Message, array, user, socket) {
    let new_message = Message();
    let room_id = data['room_id'];
    let content = data.content;
    let unrendered = data.unRenderedContent;
    new_message.user_id = parseInt(data['user_id']);
    new_message.content = encrypt(data['content']);
    new_message.is_file = 0;
    new_message.room_id = room_id;
    new_message.date = data['date'];
    new_message.save()
        .then(function (message) {
            user.time = Date.now();
            let target_users = [];
            let self_message = data.htmlContent;
            let others_message = stringGlobal.contentMessage({
                content: data['content'],
                fullname: user['last_name'] + ' ' + user['first_name'],
                url: user.url || false,
                time: data.time,
                uuid: data.id,
                id: new_message.id,
                user_id: parseInt(data['user_id'])
            });
            /*delete data.content;*/
            delete data.unRenderedContent;
            data.message_id = new_message.id;
            for (let val of array) {
                let user_id = val.user_id;
                target_users.push(user_id);
                if (user_id === data.user_id) {
                    data.htmlContent = self_message;
                } else {
                    data.htmlContent = others_message;
                    (function (user_id) {
                        MessageStatus.findOne({room_id: room_id, user_id: user_id}, function (err, doc) {
                            let $count = {countMessage: 1, room_id: room_id};
                            if (err)
                                throw err.message;
                            if (!doc) {
                                let new_messageStatus = getObjectMessageStatus({
                                    room_id: room_id,
                                    user_id: user_id,
                                    message_id: message['_id']
                                }, MessageStatus);
                                new_messageStatus.save();
                            } else {
                                if (!doc.is_read) {
                                    doc.message_id = message._id;
                                }
                                doc.is_read++;
                                doc.save();
                                $count.countMessage = doc.is_read;
                            }
                            socket.to(user_id).emit('countMessage', $count);
                        });
                    })(user_id);
                }
                socket.to(user_id).emit('socketSendText', data);
            }
            getUriData({
                content: content,
                unrendered: unrendered,
                id: message.id,
                uuid: data.id,
                users: target_users
            }, updateMetadata, socket);
            ChatRoom.update({'room_id': room_id}, {new_message_date: Date.now()}, {multi: true})
                .then(function () {
                })
                .catch(console.error);
            
        }).catch(console.error);
    return new_message;
}

function getObjectMessageStatus(data, MessageStatus) {
    let new_message = MessageStatus();
    new_message.room_id = data['room_id'];
    new_message.message_id = parseInt(data['message_id']);
    new_message.user_id = parseInt(data['user_id']);
    new_message.is_read = 1;
    return new_message;
}

// function calculateTime (time, user_id) {
//     return (Date.now() - (new Date(time))) / 1000 / 86400 < 1 && user_id;
// }

function env(letname) {
    if (typeof process === 'undefined' || !process.env || !process.env[letname]) {
        return false
    }
    return process.env[letname]
}

let from = [
    /(?![^{]*.*})`(.+?)`(?![^{]*.*})/g, /*bold `text`*/
    /(?![^{]*.*})''(.+?)''(?![^{]*.*})/g, /*italics ''text''*/
    /(?![^{]*.*})~~(.+?)~~(?![^{]*.*})/g, /*strikethrough ~text~*/
    /(?![^{]*.*})@@(.+?)@@(?![^{]*.*})/g, /*code @@text@@*/
    /{q}(.*){\/q}/g, /*quote {q}text{/q}*/
    /{c}(.*){\/c}/g, /*preview {c}text{/c}*/
    /.+{}/g, /*line break text{br}text2*/
    /{\s*((https?:\/\/.*)|(data:image.*))\s*}/i, /*image {img}*/
    // /(?:(http([s]?)):\/\/)((\w+[-.])*\w+(\/\w+[-.\w]*)*(\?[^\s]*)*)(?![\s]*>)/gim,
    // /(?![^<]*>|[^<>]*<\/)(?:(http([s]?)):\/\/)((\w+[-.])*\w+(\/\w+[-.\w]*)*(\?[^\s]*)*)(?![^<]*>|[^<>]*<\/)/gmi /*Back up for the rules below*/
    /(?![^<]*>|[^<>]*<\/)(?:(http([s]?)):\/\/)((\??\w+[-.]\=?)*\w+(\/\??\w+[-.\w]*\=?)*(\??[^\s]*)*)(?![^<]*>|[^<>]*<\/)/gmi
    // /(^|[^\/])(www\.[\S]+(\b|$))/gim,
    // /[\w.]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,6})+/gim
];

let to = [
    "<strong>$1</strong>",
    "<em>$1</em>",
    "<span style='text-decoration:line-through'>$1</span>",
    "<code>$1</code>",
    "<blockquote>$1</blockquote><br />",
    "<pre class=\"code\">$1</pre><br />",
    "<br />",
    "<a target=\"_blank\" href=\"$1\"><img class=\"\" src=\"$1\" alt=\"$1\" style=\"border:0; max-width: 50%; max-height:500px\" /></a>",
    '<a target="_blank" href="$&" data-link="$1://$3"  class="remote-url">$&</a>',
    // '$1<a target="_blank" href="http://$2"  class="chat-link">$2</a>',
    // '<a target="_blank" href="mailto:$&"  class="chat-link">$&</a>'
];

let render_length = from.length;

function renderStr(str) {
    if (str) {
        str = str.replace(/(\r|\n)/g, "abcdnewlineqwer");
        for (let i = 0; i < render_length; i++) {
            str = str.replace(from[i], to[i]);
        }
        str = str.replace(/abcdnewlineqwer/g, "\n");
    }
    
    return str;
}

function getUriData(data, cb, socket) {
    let regex = /(?:(http([s]?)):\/\/)((\w+[-.])*\w+(\/\w+[-.\w]*)*(\?[^\s]*)*)((#?([^\s]+))?)(?![^\s]*>)/m;
    data.unrendered.replace(regex, function (a, b, c, d) {
        let uri = b + '://' + d;
        let options = {
            url: a,
            jar: request.jar(), // Cookie jar
            headers: {
                'User-Agent': 'webscraper'
            }
        };
        let root_reg = /^https?:\/\/([^\/?#]+)(?:[\/?#]|$)/gi;
        a.replace(root_reg, function (domain) {
            let updateData = {
                id: data.id,
                uuid: data.uuid,
                content: data.content,
                unrendered: data.unrendered,
                domain: domain,
                url: a,
                uri: uri,
                users: data.users,
            };
            scrape(options, function (error, metadata) {
                cb(updateData, error, metadata, socket);
            });
            return domain;
        });
        
        return b + '://' + d;
    });
}

function saveMessage(data, user, rooms, socket) {
    let time = Date.now();
    // let time = stringGlobal.getTimePmAm(times);
    data['date'] = time;
    // data['miliTime'] = time.getTime();
    data.id = UUID.generate();
    var content = {
        content: data['content'],
        fullname: user['last_name'] + ' ' + user['first_name'],
        url: user.url || false,
        time: time,
        uuid: data.id
    };
    var user_id = user.user_id;
    if (!globalTimeout[user_id]) {
        globalTimeout[user_id] = true;
        setTimeout(function () {
            globalTimeout[user_id] = false;
        }, timeout);
        data.htmlContent = stringGlobal.selfMessage(content);
        let userRoom = rooms[data['room_id']];
        socket.emit('socketSendText', data);
        if (userRoom && userRoom['relative'] !== 'nonechat') {
            let array = userRoom['users'];
            if (Message && MessageStatus) {
                data.user_id = user_id;
                getObjectMessage(data, Message, array, user, socket);
            }
        }
    } else {
        content.content = 'You are typing too fast, please slowdown a bit.';
        data.htmlContent = stringGlobal.selfMessage(content);
        socket.emit('socketSendText', data);
    }
}

function getLastestMessages(room_id, mes_id, user_id, rooms, cb) {
    let userRoom = rooms[room_id];
    if (userRoom && userRoom['relative'] !== 'nonechat') {
        if (Message) {
            Message.find({})
                .where('_id').gt(mes_id)
                .where('room_id', room_id)
                .where('user_id').ne(user_id)
                .select('_id created_at is_file content user_id room_id')
                .sort('created_at')
                .then(cb)
                .catch(console.error);
        }
    }
}

function sendLostMessages(socket, messages, user_id) {
    if (messages && messages.length) {
        let lost_messages = [];
        let users = {};
        messages.forEach(function (message) {
            let data = {};
            data.date = new Date(message.created_at);
            let time = data.date.getTime();
            data.room_id = message.room_id;
            data.user_id = message.user_id;
            data.id = UUID.generate();
            if (users[message.user_id]) {
                let user = users[message.user_id];
                data.htmlContent = stringGlobal.selfMessage({
                    content: decrypt(message.content),
                    fullname: user.last_name + ' ' + user.first_name,
                    url: '/themes/default/asset_frontend/img_avatar/' + user.url || false,
                    time: time,
                    id: message.id,
                    uuid: data.id,
                });
                lost_messages.push(data);
            } else {
                var id = message.user_id;
                UserModel.findOne({
                    where: {id: id},
                    attributes: ['id', 'first_name', 'last_name', 'url'],
                }).then(function (user) {
                    if (!user) return;
                    users[message.user_id] = user;
                    data.htmlContent = stringGlobal.contentMessage({
                        content: decrypt(message.content),
                        fullname: user.last_name + ' ' + user.first_name,
                        url: '/themes/default/asset_frontend/img_avatar/' + user.url || false,
                        time: time,
                        id: message.id,
                        uuid: data.id,
                    });
                    socket.emit('lost messages', data);
                }).catch(console.error)
            }
        });
        if (lost_messages.length) {
            socket.emit('lost messages', lost_messages);
        }
    }
}

function updateMetadata(updateData, err, data, socket) {
    if (err) {
        console.error(err);
    } else {
        let openGraph = data.openGraph;
        let general = data.general;
        let title1 = null,
            title2 = null,
            image = null,
            description = null,
            // canonical = null,
            // domain = null;
            site_name = null;
        if (general) {
            title1 = general.title;
            // canonical = general.canonical;
            description = general.description;
        }
        if (openGraph) {
            title2 = openGraph.title;
            site_name = openGraph.site_name;
            // domain = openGraph.url;
            image = openGraph.image.url;
        }
        let metadata = {
            title: title1 || title2 || '',
            image: image,
            description: description,
            url: updateData.url,
            uri: updateData.uri,
            domain: updateData.domain,
            site_name: site_name,
            
        };
        if (!metadata.title && !metadata.image && metadata.description) return;
        let preview = webpreviewEmbed(metadata);
        let content = updateData.content + preview;
        getMessage(updateData.id, function (message) {
            updateMessage(message, content);
            let users = updateData.users;
            socket.emit('update message', {uuid: updateData.uuid, content: content});
            users.forEach(function (user_id) {
                socket.to(user_id).emit('update message', {uuid: updateData.uuid, content: content});
            });
        });
    }
}

function getMessage(id, cb) {
    let msg = new Message();
    Message.findById(id)
    // .then(decryptMessage)
        .then(cb)
        .catch(console.error);
    return msg;
}
//
// function decryptMessage(message) {
//     message.content = decrypt(message.content);
//     return message;
// }

function updateMessage(message, content) {
    message.content = encrypt(content);
    message.save();
    return message;
}

function webpreviewEmbed(data) {
    let acc_style = '';
    if (data.image) {
        acc_style = 'display:none';
    }
    let str = '<div onload="previewImage.showOnLoad(this)" class="accessory" style="' + acc_style + '">' +
        '<div class="embed">';
    // '<div class="preview-provider">' +
    // '<a class="embed-provider" href="' + data.domain + '" target="_blank" rel="noreferrer">' +
    // data.site_name || data.domain +
    // '</a>' +
    // '</div>';
    let title_style, description_style;
    if (data.image) {
        title_style = "position:absolute";
        description_style = "display:none;padding:3px;background:rgba(0,0,0,0.5);position:absolute; overflow:hidden; white-space:nowrap; text-overflow:ellipsis;"
    }
    if (data.description) {
        if (data.image) {
            str += '<div class="embed-description" title="' + data.description + '" style="' + description_style + '"><a class="root-description" href="' + data.domain + '">' + data.domain + '</a></div>';
        } else {
            str += '<div class="embed-description" title="' + data.title + '">' + data.description + '</div>';
        }
    }
    let description = data.description || '';
    if (data.image) {
        str += '<div class="preview-title" style="' + title_style + '">' +
            '<a class="embed-title" href="' + data.url + '" target="_blank" rel="noreferrer">' +
            data.title +
            '</a>' +
            '</div>';
        str += '<a title="' + description + '" class="embed-thumbnail embed-thumbnail-article" href="' + data.url + '" target="_blank" rel="noreferrer">' +
            '<img onload="previewImage.imgLoad(this)" onerror="previewImage.imgError(this)" class="image" src="' + data.image + '" href="' + data.image + '">' + '</a>';
    }
    if (!data.description && !data.image) {
        str += '<div class="preview-title">' +
            '<a class="embed-title" href="' + data.url + '" target="_blank" rel="noreferrer">' +
            data.title +
            '</a>' +
            '</div>';
    }
    str += '</div></div>';
    return str;
}

let UUID = (function () {
    let self = {};
    let lut = [];
    for (let i = 0; i < 256; i++) {
        lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
    }
    self.generate = function () {
        let d0 = Math.random() * 0xffffffff | 0;
        let d1 = Math.random() * 0xffffffff | 0;
        let d2 = Math.random() * 0xffffffff | 0;
        let d3 = Math.random() * 0xffffffff | 0;
        return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
            lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
            lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
            lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
    };
    return self;
})();

function findRoomUsers(room_id, user_id, friend_id, array, cb) {
    if (ChatRoom) {
        ChatRoom.find({room_id: room_id}).select('_id user_id new_message_date')
            .then(function (roomUser) {
                if (!roomUser.length) return;
                let userInRoom = roomUser.some(function (user) {
                    if (user.user_id === user_id) return true;
                });
                let friendInRoom = true;
                if (friend_id) {
                    friendInRoom = roomUser.some(function (user) {
                        if (user.user_id === friend_id) return true;
                    });
                }
                if (!userInRoom || !friendInRoom) return;
                getUserContacts(user_id, friend_id, function (relative) {
                    array[room_id] = {
                        users: roomUser,
                        relative: relative
                    };
                    cb(room_id, user_id);
                })
            })
            .catch(console.error);
    }
}

function getUserContacts(user_id, friend_id, cb) {
    FriendModel.findAll({
        $or: [
            {$and: [{user_id: user_id}, {friend_id: friend_id}]},
            {$and: [{user_id: friend_id}, {friend_id: user_id}]}
        ]
    }).then(function (users) {
        let result, relate_me, relate_friend;
        if (users[0] && users[0].user_id === user_id) {
            relate_me = users[0];
            relate_friend = users[1];
        } else {
            relate_me = users[1];
            relate_friend = users[0];
        }
        if (!relate_me && !relate_friend) {
            result = 'chat';
            return result;
        }
        switch (true) {
            case (relate_me.status == 'waiting' && relate_friend.status == 'stranger'):
                result = 'chat';
                break;
            case (relate_me.status == 'stranger' && relate_friend.status == 'waiting'):
                result = 'chat';
                break;
            case (relate_me.status == 'friend'):
                result = 'chat';
                break;
            case (relate_me.status == 'blocked'):
                result = 'nonechat';
                break;
            case (relate_me.status == 'unfriended' && relate_friend.status == 'blocked'):
                result = 'nonechat';
                break;
            case (relate_me.status == 'unfriended' && relate_friend.status == 'unfriended'):
                result = 'chat';
                break;
            default:
                result = 'nothing';
                break;
        }
        return result;
    }).then(cb).catch(console.error)
}

function renderDataContent(data, cb) {
    data['content'] = stringGlobal.htmlEntities(data['content']);
    data.content = reverseParser(parser(data.content));
    // data.content = emotes.parse(data.content);
    data.unRenderedContent = data.content;
    data.content = renderStr(data.content);
    cb();
}

function deleteGroup(user_id, room_id, rooms, socket) {
    Room.count({room_admin: user_id, _id: room_id}, function (err, count) {
        if (count > 0) {
            deleteNotice(room_id);
            deleteMessage(room_id);
            deleteRoom(room_id, rooms, socket);
        }
    });
}

function deleteNotice(room_id) {
    if (room_id)
        sequelize.query("DELETE n.*,nl.* FROM `chat__notices` AS n JOIN `chat__notices_language` AS nl ON nl.notice_id = n.id WHERE n.room_id = :id",
            {
                replacements: {id: room_id},
                type: sequelize.QueryTypes.DELETE
            })
            .then(function (users) {
            })
            .catch
            (console.error);
}

function deleteMessage(room_id) {
    Message.remove({room_id: room_id})
        .then(function (results) {
        })
        .catch
        (console.error);
    MessageStatus.remove({room_id: room_id})
        .then(function (results) {
        })
        .catch
        (console.error);
    
}

function deleteRoom(room_id, rooms, socket) {
    // var map = new Map(), set = new Set(), arr = [];
    ChatRoom.remove({room_id: room_id})
        .then(function (re) {
            Room.remove({_id: room_id})
                .then(function (re) {
                    if (rooms) {
                        var room = rooms[room_id];
                        var users = room.users;
                        for (var val of users) {
                            var user_id = val.user_id;
                            var data = {
                                room_id: room_id,
                                user_id: user_id
                            };
                            socket.to(user_id).emit('deleteGroup', data);
                        }
                        delete rooms[room_id];
                    }
                })
                .catch
                (console.error);
        })
        .catch
        (console.error);
}

function updateMessageFlagCall(data,socket) {
    let room_id = data.room_id;
    let receiver_id = parseInt(data.receiver_id);
    let user_id = data.user_id;
    let is_file = data.is_file;
    let flagcall = data.flagcall;
    
    Message.findOne({
        room_id: room_id, flagcall: 1
    }).
    then(function (doc) {
        if (doc) {
            doc.is_file = is_file;
            doc.flagcall = flagcall;
            doc.save();
            let arraySocket = {
                is_file: is_file,
                date: doc.created_at,
                room_id:room_id
            };
            if(data.url)
                arraySocket.url = data.url;
            
            if(receiver_id == doc.user_id){
                arraySocket.user_id = receiver_id;
                arraySocket.receiver_id = user_id;
            }else{
                arraySocket.user_id = user_id;
                arraySocket.receiver_id = receiver_id;
            }
            UserModel.findOne({
                where: {id: receiver_id},
                attributes: ['last_name', 'first_name', 'url']
            }).then(function (result) {
                if (result) {
                    arraySocket.fullname = data.fullname;
                    if (receiver_id == doc.user_id){
                        arraySocket.fullname = result.last_name + ' ' + result.first_name;
                        if(result.url)
                            arraySocket.url = result.url;
                    }
                    arraySocket.namereceiver = result.last_name + ' ' + result.first_name;;
                    socket.to(user_id).emit('uploadFileServer', arraySocket);
                    arraySocket.namereceiver = data.fullname;
                    socket.to(receiver_id).emit('uploadFileServer', arraySocket);
                }
            });
        }
    }).catch(function (err) {
        throw err;
        
        /*socket.emit('uploadFileServer', data);
        socket.to(receiver_id).emit('uploadFileServer', data);
        socket.to(user_id).emit('uploadFileServer', data);*/
    });
}