var redis = require("redis");
var roomUser = {};

var roomUser_request = redis.createClient();
roomUser_request.subscribe('saveUserOfRoom');

roomUser_request.on('message', function (channel, message) {
    message = JSON.parse(message);
    if (typeof message != 'undefined' && message != false)
        roomUser[message.room_id] = {users: message.user_id, relative: message.relative};
});

/*var abc = function () {
  return roomUser;
};*/

module.exports = {
    contentMessage: roomUser_request
};