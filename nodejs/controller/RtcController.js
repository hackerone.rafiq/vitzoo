module.exports    = function (socket,listCalls,user) {
    if(user){
        var user_id = user.user_id;
    }
    socket.on("*", function (event, data) {
        
    });
    socket.on('user busy', function (data) {
        let receiver_id = data.receiver_id;
        let room_id = data.room_id;
        let array = {
            user_value: listCalls[user_id] || false,
            receiver_value: listCalls[receiver_id] || false,
            room_id: room_id,
            receiver_id: receiver_id,
            type:data.type
        };
        socket.emit('user busy', array);
    });
    
    socket.on('callee', function (data) {
        let receiver_id = data.receiver_id;
        data['name'] = user['last_name'] + ' ' + user['first_name'];
        data['caller_id'] = user['user_id'];
        data['url_caller'] = user['url'];
        socket.to(receiver_id).emit('callee', data);
        socket.emit('callee connected',{type:'waiting'});
    });
    
    socket.on('hang up',function (data) {
        let receiver_id = data.receiver_id;
        socket.to(receiver_id).emit('hang up', data);
        socket.emit('hang up',data);
        delete listCalls[user_id];
    });
};
