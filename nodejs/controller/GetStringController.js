"use strict";
var models      = require('../models');
var UserModel   = models.UserModel;
var FriendModel = models.FriendModel;
var StatusModel = models.StatusModel;

var crypto = require('crypto'),
    key    = 'hn6IMw1XW3O6KXmfhn6IMw1XW3O6KXmf',
    iv     = 'HHF]8gM<5iyoZyOT';

var selfMessage = function (data) {
    var st     = '';
    var c_file = 'msg-text';
    if (data['type'] == 'file') c_file = 'msg-file';
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" id="' + data.uuid + '">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat">';
    st += '<div style="float:right" class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time><span>' + data['fullname'] + '</span><span class="clock-time"></span></time>';
    st += '<div class="msg chat-' + c_file + ' ' + data['fileClass'] + '">';
    st += '<p class="' + c_file + '">' + data['content'] + '</p>';
    if (data['type'] == 'file')
        st += '<p class="msg-filesize">' + Math.round(data['size'] / 1024) + ' KB</p>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

var contentMessage = function (data) {
    var st      = '';
    var c_file  = 'msg-text';
    var data_id = data.id ? 'data-id="' + data.id + '"' : '';
    if (data['type'] == 'file') c_file = 'msg-file';
    st += '<div class="msg_container base_sent clearfix col-md-10 col-md-offset-1" id="' + data.uuid + '"' + data_id + ' data-userid="'+data.user_id+'">';
    st += '<div class="col-md-1 col-xs-1 col-st">';
    st += '<div class="avatar">';
    if (data.url) {
        st += '<img class="user_avatar" width="100%" src="' + data.url + '"/>'
    } else {
        st += '<i class="fa fa-user"></i>';
    }
    st += '</div>';
    st += '</div>';
    st += '<div class="col-md-11 col-xs-11 ct-chat received-message">';
    st += '<div class="chat-box">';
    st += '<div class="messages msg_sent">';
    st += '<time><span class="message-name" style="cursor: pointer;">' + data['fullname'] + '</span><span class="clock-time"></span></time>';
    st += '<div class="msg chat-' + c_file + ' ' + data['fileClass'] + '">';
    st += '<p class="' + c_file + '">' + data['content'] + '</p>';
    if (data['type'] == 'file')
        st += '<p class="msg-filesize">' + Math.round(data['size'] / 1024) + ' KB</p>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    st += '</div>';
    return st;
};

var arrayContentMessage = function (data) {
    var st = '';
    for (var i = 0; i < data['length']; i++) {
        data['content'] = data['fileName'][i];
        data['size']    = data['fileSize'][i];
        st += contentMessage(data);
    }
    return st;
};

// var encrypt = function (text) {
//     var cipher    = crypto.createCipheriv('aes-128-cbc', key, iv);
//     var encrypted = cipher.update(text, 'utf8', 'binary');
//     encrypted += cipher.final('binary');
//     var hexVal    = new Buffer(encrypted, 'binary');
//     return hexVal.toString('hex');
// };

var encrypt = function (plain_text) {
    var encryptor = crypto.createCipheriv('AES-256-CBC', key, iv);
    return encryptor.update(plain_text, 'utf8', 'base64') + encryptor.final('base64');
};

// function decrypt (text) {
//     var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
//     var dec      = decipher.update(text, 'hex', 'utf8');
//     dec += decipher.final('utf8');
//     return dec;
// }

var decrypt = function (encryptedMessage) {
    var decryptor = crypto.createDecipheriv('AES-256-CBC', key, iv);
    return decryptor.update(encryptedMessage, 'base64', 'utf8') + decryptor.final('utf8');
};

function cantonPairing (k1, k2) {
    return ((k1 + k2) * (k1 + k2 + 1) + k2) / 2;
}

var uniqueRoom = function (i, d) {
    var room_id = '0';
    var md5     = crypto.createHash('md5');
    if (i < d) {
        room_id = cantonPairing(i, d);
        room_id = md5.update(String(room_id)).digest('hex');
    }
    if (i > d) {
        room_id = cantonPairing(d, i);
        room_id = md5.update(String(room_id)).digest('hex');
    }
    return room_id;
};

var entityMap    = {
    '&' : '&amp;',
    '<' : '&lt;',
    '>' : '&gt;',
    '"' : '&quot;',
    // '/': '&#x2F;',
    '\n': '<br/>'
};
var htmlEntities = function (str) {
    return String(str).replace(/[&<>"\n]/g, function (s) {
        return entityMap[s];
    });
};

function addZero (num) {
    num = String(num);
    return num < 10 ? '0' + num : String(num);
}

function getDate (times) {
    // return times.getFullYear() + '/' + addZero(times.getMonth() + 1) + '/' + addZero(times.getDate()) + ' 12:00:00';
    return addZero(times.getDate()) + addZero(times.getMonth() + 1) + times.getFullYear();
}

function getTimePmAm (date) {
    var hours   = date.getHours() % 24;
    var minutes = date.getMinutes();
    var mid     = 'AM';
    if (hours == 0) {
        hours = 12;
    }
    else if (hours > 12) {
        hours = hours % 12;
        mid   = 'PM';
    }
    return addZero(hours) + ':' + addZero(minutes) + ' ' + mid;
}

function getFriends (user_id, cb) {
    FriendModel.findAll({where: {user_id: user_id, status: 'friend'}})
        .then(getFriendIds)
        .then(cb)
        .catch(function (err) {
            console.log(err);
        });
}

function getFriendIds (friends) {
    var friend_ids = [];
    addInterator(friends);
    for (let [i,friend] of friends) {
        friend_ids.push(friend.friend_id);
    }
    return friend_ids;
}

function broadcastFriend (user, socket, connected, type, status, cb) {
    if (user) {
        var user_status = {};
        var user_id     = user.user_id;
        var full_name   = user.last_name + ' ' + user.first_name;
        getFriends(user_id, function (array_friend) {
            if (array_friend.length && connected) {
                var online_friend        = array_intersect(connected, array_friend);
                var online_friend_status = merge_array(online_friend, status);
                if (status[user_id]) {
                    if (status[user_id].status === 'invisible') {
                        status[user_id].status = 'offline';
                    }
                    user_status           = status[user_id];
                    user_status.user_id   = user_id;
                    user_status.full_name = full_name;
                } else if (type === 'offline') {
                    user_status = {
                        status : 'offline',
                        user_id: user_id,
                        mood   : false
                    };
                }
                
                if (type == 'online') {
                    socket.emit('userStatus', online_friend_status);
                }
                
                if (online_friend) {
                    for (let value of online_friend) {
                        socket.to(value).emit('change status', user_status);
                    }
                }
            }
        });
        /*FriendModel.findAll({where: {user_id: user_id, status: 'friend'}}).then(function (friend) {
         if (friend && connected) {
         var array_friend = [];
         for (let value of friend) {
         array_friend.push(value.friend_id);
         }
         
         }
         });*/
    }
}

function getListStatus (user_id, listGlobal) {
    StatusModel.findOne({where: {user_id: user_id}}).then(function (status) {
        listGlobal[user_id] = status.status;
    });
}

function array_intersect () {
    var i, all, shortest, nShortest, n, len, ret = new Set(), obj = {}, nOthers;
    nOthers                                      = arguments.length;
    nShortest                                    = arguments[0].length || arguments[0].size;
    shortest                                     = 0;
    for (i = 0; i !== nOthers; i++) {
        n = arguments[i].length || arguments[i].size;
        if (n < nShortest) {
            shortest  = i;
            nShortest = n;
        }
    }
    
    for (i = 0; i !== nOthers; i++) {
        n = (i === shortest) ? 0 : (i || shortest); //Read the shortest array first. Read the first array instead of the shortest
        for (var elem of arguments[n]) {
            if (obj[elem] === i - 1) {
                if (i === (nOthers - 1)) {
                    ret.add(elem);
                    obj[elem] = 0;
                } else {
                    obj[elem] = i;
                }
            } else if (i === 0) {
                obj[elem] = 0;
            }
        }
    }
    return Array.from(ret);
}

function addInterator (obj) {
    obj[Symbol.iterator] = function * () {
        var obj = this;
        for (let i of Object.keys(obj)) {
            yield [i, obj[i]];
        }
    };
}

function merge_array (array1, array2) {
    addInterator(array2);
    var array = {};
    if (array1 && array2) {
        for (let value of array1) {
            for (let [key,val] of array2) {
                if (value == key) {
                    array[key] = val;
                }
            }
        }
    }
    return array;
}

module.exports = {
    contentMessage     : contentMessage,
    encrypt            : encrypt,
    decrypt            : decrypt,
    htmlEntities       : htmlEntities,
    getDate            : getDate,
    getTimePmAm        : getTimePmAm,
    arrayContentMessage: arrayContentMessage,
    broadcastFriend    : broadcastFriend,
    getListStatus      : getListStatus,
    array_intersect    : array_intersect,
    uniqueRoom         : uniqueRoom,
    getFriends         : getFriends,
    selfMessage        : selfMessage,
    addInterator       : addInterator
};