<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1771102423106383',
        'client_secret' => 'bc83a0a044b302d2d864a41422a8e936',
        'redirect' => 'https://vitzuu.com/facebook_redirect',
    ],

    'twitter' => [
        'client_id' => 'fXf4Ct77we0Dvcq08OYSVtRsG',
        'client_secret' => 'Vd7nHBBBIbMjDKS0dBLZt9kGQovLozjeaPPt2iVac1Rt6Qcr8r',
        'redirect' => 'https://vitzuu.com/twitter_redirect',
    ],

    'google' => [
        'client_id' => '648068553953-4rca3dov40q1itkitcsv76a4i20g4ss0.apps.googleusercontent.com',
        'client_secret' => 'Xh16dxWHOyf7Da-S7KrwALWV',
        'redirect' => 'https://vitzuu.com/google_redirect',
    ],

];
