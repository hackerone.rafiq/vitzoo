# Vitzoo Project#

###Các thư viện đã cài đặt:###

**1. Cafeinated Modules:** thư viện hỗ trợ HMVC cho Laravel
```
https://github.com/caffeinated/modules
```

**2. Cafeinated Themes:** thư viện hỗ trợ theme
```
https://github.com/caffeinated/themes
```

**3. Laravel Localization:** thư viện hỗ trợ multi languages
```
https://github.com/mcamara/laravel-localization
```

**4. SEO Tools:** thư viện hỗ trợ SEO
```
https://github.com/artesaos/seotools
```

**5. Assets:** thư viện hỗ trợ quản lý assets
```
https://github.com/Stolz/Assets
```

**6. Cartalyst Sentinel:** thư viện hỗ trợ user authenticate
```
https://cartalyst.com/manual/sentinel/2.0
```

**7. Cartalyst Tags:** thư viện hỗ trợ tags
```
https://cartalyst.com/manual/tags/2.0
```

**8. Debugbar:** Thư viện hỗ trợ debug
```
https://github.com/barryvdh/laravel-debugbar
```

**9. HTML Min:** Thư viện hỗ trợ compress html content
```
https://github.com/GrahamCampbell/Laravel-HTMLMin
```

**10. Translatable:** Thư viện hỗ trợ quản lý multi languages bằng database
```
https://github.com/dimsav/laravel-translatable
```