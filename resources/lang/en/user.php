<?php
$language = [
    'user_incorrect'    => 'Incorrect email or password.',
    'login_success'     => 'You have successfully logged in.',
    'captcha_failed'    => 'Captcha checking failed, are you sure you are not a robot?',
    "online"            => "Online",
    "user_info"         => "User Information",
    "avatar"            => "Avatar",
    "first_name"        => "First Name",
    "last_name"         => "Last Name",
    "email"             => "Email",
    "role"              => "Group",
    "group_not_exist"   => "Group not exist!",
    "user_not_exist"    => "This user not exist!"
];

return $language;