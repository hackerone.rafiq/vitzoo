<?php
$language = [
    "dashboard"         => "Admin Dashboard",
    "navigation"        => "MAIN NAVIGATION",
    "cms_pages"         => "CMS Pages",
    "user_manage"       => "Users Manage",
    "group_manage"      => "Groups Manage",
    "user_listing"      => "User Listing",
    "config_manage"     => "Config Manage",
    "config_website"    => "Config Website",
    "config_chat"       => "Config Chat",
    "config_cache"      => "Config Cache",
    "report_manage"     => "Report Manage",
    "chat_manage"       => "System Chat Manage",
    "room_list"         => "Room List",
    "bot_list"          => "Bot List"
];

return $language;