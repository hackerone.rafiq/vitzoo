<?php
$language = [
    'login_admincp'     => 'Login Control Panel',
    'login_description' => 'Sign in to start your session',
    'remember_me'       => 'Remember Me',
    'signin'            => 'Sign In',
    'email'             => 'Email',
    'password'          => 'Password',
    'password_confirmation' => 'Password confirmation',
    'user_role' => 'User role',
    'choose_role' => 'Choose user\'s role',
    "login_incorrect"  => "Incorrect information. Please check again!",
    "login_success" => "Login success!",
    'login_fail' => 'Sign in failed. Please check your account.',
    'login_before_chat'=>'You must login to participate in chat'
];

return $language;