<?php
$language = [
    'user_incorrect' => 'Tên tài khoản hoặc mật khẩu không đúng.',
    'login_success' => 'Đăng nhập thành công',
    'captcha_failed' => 'Kiểm tra captcha thất bại, liệu có phải bạn là robot?',
];

return $language;