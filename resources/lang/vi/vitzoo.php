<?php
$language = [
    'Monday' => 'Thứ hai',
    'Tuesday' => 'Thứ ba',
    'Wednesday' => 'Thứ tư',
    'Thursday' => 'Thứ năm',
    'Friday' => 'Thứ sáu',
    'Saturday' => 'Thứ bảy',
    'Sunday' => 'Chủ nhật',
    'January' => 'Tháng 01',
    'February' => 'Tháng 02',
    'March' => 'Tháng 03',
    'April' => 'Tháng 04',
    'May' => 'Tháng 05',
    'June' => 'Tháng 06',
    'July' => 'Tháng 07',
    'August' => 'Tháng 08',
    'September' => 'Tháng 09',
    'October' => 'Tháng 10',
    'November' => 'Tháng 11',
    'December' => 'Tháng 12',
];

return $language;