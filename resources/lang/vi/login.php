<?php
$language = [
    'login_admincp'     => 'Đăng nhập Quản trị',
    'login_description' => 'Nhập đầy đủ thông tin bên dưới',
    'remember_me'       => 'Ghi nhớ',
    'signin'            => 'Đăng nhập',
    'email'             => 'Email',
    'password'          => 'Mật khẩu',
    'password_confirmation' => 'Xác nhận mật khẩu',
    'user_role' => 'Nhóm thành viên',
    'choose_role' => 'Chọn nhóm thành viên',
    "login_success" => "Đăng nhập thành công!",
    'login_fail'=>'Đăng nhập thất bại. Vui lòng kiểm tra lại tài khoản đăng nhập',
    'login_before_chat' => 'Bạn phải đăng nhập trước khi tham gia chat'
];

return $language;